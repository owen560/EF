set obs 100000

cap drop id
gen id = _n-1


set seed 1234
cap drop poi
gen poi = rpoisson(4)
hist poi ,fraction xtitle("yi") ytitle("Pr")

cap drop assign
gen assign = runiform(0,100)

scalar separate = 12
cap drop num
gen num = poi
replace num = 0 if assign<=separate

cap sum num
global mxnum = r(max)+1
cap sum poi
global mxpoi = r(max)+1

cap drop cntnum
gen cntnum=.
cap drop cntpoi
gen cntpoi=.

forvalues i = 1(1)$mxnum {
	cap count if num==`i'-1
	cap replace cntnum=r(N)/100000 in `i'
}

forvalues i = 1(1)$mxpoi {
	cap count if poi==`i'-1
	cap replace cntpoi=r(N)/100000 in `i'
}



#delimit ;
hist num, fraction
	addplot(line cntpoi id in 1/$mxpoi)
	legend(order(1 "Zero-Inflated" 2 "Normal Poisson"));

#delimit cr
