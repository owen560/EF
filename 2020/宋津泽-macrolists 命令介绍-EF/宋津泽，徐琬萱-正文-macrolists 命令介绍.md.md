&emsp;

\> **作者**：宋津泽、徐琬萱 (中山大学)

\> **E-Mail:** <songjz3@mail2.sysu.edu.cn>

&emsp;

[toc]

## 1. 暂元基本介绍

### 1.1 暂元定义及其分类

暂元( Macro )是指以一个简单的单词来代指一串复杂的字符或表达式，总体上分为“局部暂元”( `local` macro )和“全局暂元”( `global` macro )。

“局部暂元”(`local` macro: 其定义的暂元仅在定义它的 do 文件中有效，一旦此 do 文件执行完毕，其中定义的局部暂元将不存在；

“全局暂元”( `global` macro )：在 do 文件执行完毕后仍存在，直至彻底退出 Stata 程序才会消失。

设置暂元的基本语法为 `local/global` name value



### 1.2 暂元的基本功能

**此处引用连玉君老师课件中的小例子进行展示：**

1）存放数字，即将数值赋给此暂元

```
. local a 5
. local b = `a' + 7
. dis `a'
5
. dis `b'
12
```

2）存放文字，包括存放变量名称和长字符，亦可进行相关的变量操作

```
. sysuse auto, clear
(1978 Automobile Data)
. local v1 "price"
. local v2 "weight rep78 length"
. reg `v1' `v2'

      Source |       SS           df       MS      Number of obs   =        69
-------------+----------------------------------   F(3, 65)        =     16.16
       Model |   246375736         3  82125245.5   Prob > F        =    0.0000
    Residual |   330421222        65  5083403.42   R-squared       =    0.4271
-------------+----------------------------------   Adj R-squared   =    0.4007
       Total |   576796959        68  8482308.22   Root MSE        =    2254.6

------------------------------------------------------------------------------
       price |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      weight |   5.252098   1.103427     4.76   0.000     3.048401    7.455794
       rep78 |   844.9462   302.0363     2.80   0.007      241.738    1448.154
      length |  -103.6016   37.78457    -2.74   0.008    -179.0626   -28.14063
       _cons |   6850.952   4312.738     1.59   0.117    -1762.181    15464.08
------------------------------------------------------------------------------
```

3）暂元可进行嵌套，即暂元中的暂元

```
. local a1=2
. local a2 "var"
. local a3=2*`a1'
. local a4 `a`a1''
. local `a2'`a1'=2*`a3'
```

```
. dis `a1'
2
. dis "`a2'"
var
. dis `a3'
4
. dis "`a4'"
var
. dis ``a2'`a1''
8
```

注：调用 *numeric* 类暂元时两侧加“单引号”（左为 back tick，右为单引号）是为了告诉 Stata 这是个暂元；调用 *string* 类暂元时两侧加双引号表示原封不动地显示双引号中的暂元内容。



### 1.3 暂元的应用场景

1）若回归变量较为复杂且数量太多，或是需要将不同的回归方式进行分组罗列时，将变量名称直接写入 do 文档会显得繁琐冗杂，此时将变量赋给暂元，就可以使文档较为简洁美观。

```
. local x var1 var2 var3
. reg y `x'
```

2）在循环中使用暂元会非常方便

```
. local variables var1 var2 var3
. foreach var of varlist `variables'{}
```



## 2.Macrolists介绍

### 2.1 各类操作功能及举例

除了上述简单的基本功能之外，Stata 还为暂元增加了许多的操作功能，使其可以更加便捷高效。

#### 2.1.1 对单个暂元的操作命令

> `uniq` A：将暂元 A 中重复值删除（保留一个）

```
. local A1 a a b c a
. local result1 : list uniq A1
. di "`result1'"
a b c
```

> `dups` A：仅保留 A 中重复值（若 A 中没有重复值则不显示）

```
. local A2 a a b c a
. local result2 : list dups A2
. di "`result2'"
a a
```

> `sort` A：将 A 中元素按字母表或字符值排序

```
. local A3 a a b c a
. local result3 : list sort A3
. di "`result3'"
a a a b c
```

> `retokenize` A：若 A 中元素间间隔多个空格，则多余空格删除只保留 1 个

```
. local A4 "a       a b c a"
. local result4 : list retokenize A4
. di "`result4'"
a a b c a
```

> `sizeof` A：A 中元素的个数

```
. local A5 "a b c a"
. local result5 : list sizeof A5
. di "`result5'"
4
```

> `posof`"element" in A：返回该元素在 A 的第几个位置，若该元素在 A 中不存在，则返回“0”

```
. local A6 "a b c a"
. local result6 : list posof "c" in A6
. di "`result6'"
3
. local result7 : list posof "e" in A6
. di "`result7'"
0
```



#### 2.1.2 对多个暂元的操作命令

> `A|B` ：返回 A 和 B 的并集

```
. local A1 a b c
. local B1 b d c
. local new1 : list A1 | B1
. di "`new1'"
a b c d
```

> `A&B` ：返回 A 和 B 的交集

```
. local A2 a b c d
. local B2 b d e f
. local new2 : list A2 & B2
. di "`new2'"
b d
```

> `A-B` ：将 A 中 B 的元素剔除

```
. local A3 a b c d
. local B3 b a d e f
. local new3 : list A3 - B3
. di "`new3'"
c
```

> `A==B` ：判断 A 和 B 的元素及其顺序是否相同，若相同则返回“1”，若不相同则返回“0”

```
. local A4 a b c d
. local B4 a b c d
. local C4 a c b d
. local new4 : list A4 == B4
. local new5 : list A4 == C4
. di "`new4'"
1
. di "`new5'"
0
```

> `A===B` ：判断 A 和 B 的元素是否相同(不考虑顺序），若相同则返回“1”，若不相同则返回“0”

```
. local A4 a b c d
. local B4 a c b d
. local new4 : list A4 === B4
. di "`new4'"
1
```

> `A in B` ：判断 A 是否是 B 的子集，若是则返回“1”，若不是则返回“0”

```
. local A5 a b c d
. local B5 a b c d e
. local C5
. local new5 : list A5 in B5
. local new6 : list C5 in B5
. di "`new5'"
1
. di "`new6'"
1
```



### 2.2 语法

> `{local|global} macname : list {uniq|dups...} macname`



### 2.3 注释

1）其中`macname`指暂元的名称而不是其赋值。

举例：你需要输入`local result : list list1 | list2`而不需要在`list1`和`list2`的左右两边加上“单引号”和双引号。

2）暂元中包含空格的元素需要用""将其括起来。

举例：暂元 A 中有四个元素`first element second third element 4`，因此第一个和第三个就需要用双引号进行区分:
`"first element" second "third element" 4`。

3)除了`uniq`和`dups`，Stata 会识别暂元中所有的重复项为不同。

举例：假设暂元`A="a b c b"`,暂元`B="b b"`，那么`A|B="a b c b"`;如果暂元 B="b b b",那么`A|B="a b c b b"`。



## 3. 参考文献

- macro lists——Manipulate lists. stata.com. [-pdf-](https://www.stata.com/manuals/pmacrolists.pdf)