*若用户输入的参数不是正整数，报错并提示
*------------------------begin---myfact2.ado----------------------*
capture program drop myfact2
program define myfact2
version 15

capture syntax anything(name=k) 

  if _rc{  // 用户忘了输入数字
    dis as error "You must enter an positive integer"
	exit
  }  
  
  if wordcount(`"`k'"')>1{  // 用户输入了多个数字
    dis as error "You can only enter one integer"
	exit
  }
  
  capture confirm integer number `k' // 用户输入了非整数或文字
  if _rc{
    dis as error "You must enter a positive integer"
	exit
  }
  
  if `k'<0{  // 用户输入了负数
     dis as error "You must enter an positive integer"
	 exit
  }
	 
  *-计算
  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  
  dis "`k'! = "  `a'
  
end 
*------------------------end-----myfact2.ado----------------------*
