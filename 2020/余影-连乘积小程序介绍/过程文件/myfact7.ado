*多个数值的阶乘
*------------------------begin---myfact7.ado----------------------*
capture program drop myfact7
program define myfact7, rclass 
version 15

syntax anything(name=numlist) [, Format(string)]

tokenize "`numlist'"
local j = 1  
while "``j''" !=""{     // `j' 表示房间号；``j'' 表示房间中的内容
  local a = 1
  local k = ``j''       // 注意暂元的引用方式
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  dis "`k'! = " `format' `a'
  return scalar k`k' = `a'   
  local j = `j' + 1
}

end 
*------------------------end-----myfact7.ado----------------------*
