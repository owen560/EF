> Stata：编写 ado 文件技巧——以计算阶乘为例

> **作者**：连玉君（中山大学）、余影（中山大学）
> **邮箱**：<arlionn@163.com>、<yuying29@mail2.sysu.edu.cn>


<br>
<br>

* 在线文本比对，可以查看各个版本的变化 2020/11/6 22:54
*    http://wenbenbijiao.renrensousuo.com/

<br>
<br>

---

**目录**

[TOC]

---

本文讨论了一个在 Stata 使用过程中基础但非常重要的任务——编写 ado 文件。通过一个简单的例子——计算阶乘，由浅入深地讲解如何逐步编写满足不同需求的 ado 文件。

&emsp;

## 1. 初识 ado 文件

在使用 Stata 的过程中，我们可以在 Stata 的命令窗口输入各项命令，采用交互的方式定义程序。但在实际应用中，更常见的方式是通过创建<b>ado 文件（automatic do-ﬁle）</b>来保存程序，将程序变为一个新的命令，在需要使用的时候，通过定义的 ado 文件（程序）的名称调用。相比起一遍又一遍地输入命令，这种方法既方便又不容易出错。

下面我们将创建一个最简单的 ado 文件：（`myfact`为文件名）

```
capture program drop myfact
program define myfact
version 15
  dis "Hello,ado."
end
```

这是编写程序最为基本的一种格式。在最开始加入第一行是为了将 ado 文件（若已经存在）从内存中删去，随后以`program define`开头定义程序，可以加入`version`表明程序的版本，最后以`end`结尾。随后调用`myfact.ado`文件即可运行程序。

```
. myfact
Hello,ado.
```

&emsp;

## 2. 编写 ado 文件基本命令介绍

在这一部分中，我们将简要介绍编写 ado 文件的相关命令。而想要从例子出发由浅入深地了解程序编写技巧的读者可以先跳过这一部分，从第三部分看起，有不明白的地方可以再回头看这一部分的命令介绍。

### 2.1 program 命令

定义程序的基本语法结构如下：

```
program [define] pgmname [, [ nclass | rclass | eclass | sclass ] byable(recall[, noheader] | onecall) properties(namelist) sortpreserve plugin]
```

在这里，`program define`是定义程序的基本命令，其中`define`可以省略；`pgmname`为程序名；`nclass`为默认项，用于表明程序的类型；`rclass`、`eclass`、`sclass`则表示程序的结果分别保存在`r()`、`e()`和`s()`中，方便之后的调用。

### 2.2 capture 命令
我们可以在命令之前加入 capture，当命令出错的时候，自动跳过该行命令（不报错）并继续执行下面的代码。该命令语法结构如下：

```
capture [:] command
```

使用`capture`命令，命令的返回码会保存在`_rc`当中。如果想知道具体的报错信息，可以使用`dis _rc`命令。当没有运行错误时，`_rc`等于 0，否则不为 0。

<em>*在编写程序的时候，可以使用 capture 来判断是否出错，再调用\_rc 编写条件语句。</em>
<em>*在 Stata 中，0 为假，非零数为真。</em>

### 2.3 args 命令

```
args macroname1 [macroname2 [macroname3 ...]]
```

有些时候，我们需要在程序中使用外部输入的变量。这时，我们可以使用`args`命令接收用户的输入参数。`args`命令将第一个命令参数分配给暂元`macroname1`，将第二个参数分配给暂元`macroname2`，依此类推。在编写代码时，我们可以用`'引用其内容，如：

```
`macroname1' ' macroname2'
```

### 2.4 syntax 命令

在编写程序时，我们可以使用`syntax`命令解析命令行，当输入的内容和指定的语法不同时，`syntax`还会报错。
标准的`syntax`命令语法结构如下：

```
syntax [varlist | namelist | anything] [if] [in] [using filename] [= exp] [weight] [, options]
```

对于 varlist，我们可以设置多种格式：

- `syntax varist` 表示变量名`varist`必须设置
- `syntax [varist]` 表示变量名`varist`可以设置也可以不设置
- `syntax varlist(min=2 max=4 numeric)`表示输入变量个数最少为 1，最多为 2，且必须为数值型，`numeric`还可以换成`string`（字符型）、`fv`（factor variables）等。

我们还可以将`varlist`换成`newvarlist`或者`newvarname`（表明生成新变量）；换成`namelist`或者`name`（表明输入的对象可以不是变量）；如果还想进一步放宽要求（如输入的对象为数字），可以将`varlist`换成`anything`,语法结构如下：

```
syntax anything(name=eqlist id="equation list")
```

其中输入参数命名为暂元 eqlist，将 anything 命名为 equation list（仅在报错时使用）。

### 2.5 confirm 命令
在编写程序时，如果想要判断输入的参数或者变量是否为某一类型，我们可以使用`confirm`命令。

```
confirm [integer] number string
```

不添加可选项`integer`时，命令用于确认输入参数为数字；加入可选项`integer`后命令用于确认输入参数为整数。同时`number`还可以换成`matrix`（矩阵）、`scalar`（标量）等。

<em>*在编写程序时，confirm 命令经常与 capture 命令一起使用。</em>
<em>*在使用 Stata 的过程中，help 命令极为重要。可以使用 help 查看以上命令的详细介绍和具体使用方式。</em>

&emsp;

## 3. 使用阶乘的例子逐步了解 ado 文件编写技巧

### 3.1 阶乘概念

阶乘是基斯顿•卡曼（Christian Kramp，1760 ～ 1826）于 1808 年发明的运算符号。一个正整数的阶乘（factorial）是所有小于及等于该数的正整数的积，并且 0 的阶乘为 1。

$$n!=1×2×3×…×(n-1)×n  $$ $$ n!=n×(n-1)!$$$$0！=1$$

### 3.2 在 dofile 中实现特定数字的阶乘运算

在 Stata 中，我们可以使用`forvalues`命令，通过循环计算阶乘。
例如计算 5 的阶乘，我们可以通过下述代码实现：

```
local k = 5    // 阶数
  local a = 1  // 阶乘
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }

  dis "`k'! = " `a'
```

### 3.3 使用 ado 文件计算阶乘

在 3.2 中，如果我们需要多次计算阶乘必须不断改变`k`值并重复执行 dofile 中的命令，但是我们可以通过使用<b>ado 文件</b>简化这些步骤。

我们做了什么调整呢？

```
capture program drop myfact1
program define myfact1
version 15
  args k    // 定义暂元 k，用于接收用户输入的数字
  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  dis "`k'! = " `a'
end
```

相对于 3.2 中的代码，我们在开始加入了`program define myfact1`，在结尾处加入了`end`，这样 Stata 就会自动将其视为命令。同时使用`args`命令，定义暂元`k`，用于接收用户输入的数字。执行完上述代码，就可以通过定义的 ado 文件的名称`myfact1`进行调用，得到和 3.2 中一样的结果。

```
. myfact1  5
5! = 120
```

### 3.4 增加 ado 文件功能

在编写程序的过程中，我们必须考虑到用户的各种行为和需求。通过改进自己的程序，来识别错误的行为并给出有用的提示信息并满足用户的各种需求。下面我们将逐步增添程序的功能，完善上述计算阶乘的程序。

#### 3.4.1 若用户输入的参数不是正整数，报错并提示

在计算阶乘的例子中，由于只有正整数才有阶乘（0！=1 除外），在编写程序的时候，我们需要考虑到用户输入参数不为正整数的情况。如果用户输入的参数不是正整数，我们需要报错并给出有用的信息来提醒用户重新输入正整数。程序代码如下：

```
capture program drop myfact2
program define myfact2
version 15
capture syntax anything(name=k)
  if _rc{  // 用户忘了输入数字
    dis as error "You must enter an positive integer"
	exit
  }
  if wordcount(`"`k'"')>1{  // 用户输入了多个数字
    dis as error "You can only enter one integer"
	exit
  }
  capture confirm integer number `k' // 用户输入了非整数或文字
  if _rc{
    dis as error "You must enter a positive integer"
	exit
  }
  if `k'<0{  // 用户输入了负数
     dis as error "You must enter an positive integer"
	 exit
  }
  local a = 1  //计算
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  dis "`k'! = "  `a'
end
```

相比`myfact1`，`myfact2`考虑了用户忘记输入数字、输入多个数字、输入非整数或文字以及输入负数的情况。使用`capture`，如果用户忘记输入数字，`_rc`不为 0，则输出`You must enter an positive integer`；使用`wordcount`（此时需注意暂元的引用方式），如果用户输入多个数字，输出`You can only enter one integer`；同时为了保证输入数字为整数，还可以使用`confirm`命令。

对上述程序进行调用可以得到

```
. myfact2 5
5! = 120

. myfact2
You must enter an positive integer

. myfact2 5 4
You can only enter one integer

. myfact2 A
You must enter a positive integer

. myfact2 -5
You must enter an positive integer
```

#### 3.4.2 增加选项 format()，以便用户自定义结果的显示格式

除了考虑用户可能的输入参数的情况，我们还可以为程序增添定义输出格式的功能来满足用户的一般需求。程序代码如下：

```
capture program drop myfact3
program define myfact3
version 15
syntax anything(name=k) [, Format(string)]
  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  dis "`k'! = " `format' `a'
end
```

为了定义输出格式，我们在`syntax`命令中加入了可选项`Format`，若用户输入显示格式的代码，程序就将代码存储到暂元`format`中，并通过后续编写的代码，实现自定义结果的显示格式。

**说明：** 可选项 `Format()` 中的大写字母部分就是该选项可以缩写的程度。例如，若使用 `[, Format(string)]`，用户在调用的时候可以缩写为 `f(..)`；但是如果使用 `[, format(string)]，则不能缩写。

对上述程序进行调用可以得到

```
. myfact3 10, format(%-20.0g)
10! = 3628800

. myfact3 10, f(%-20.0g)
10! = 3628800

. myfact3 20
20! = 2.433e+18

. myfact3 20, format(%-20.0f)
20! = 2432902008179999700
```

<em>\*关于格式设定可以查看简书——如何设定 Stata 中数字的显示格式
https://www.jianshu.com/p/cf49613b4227</em>

#### 3.4.3 若用户输入的控制数据显示格式的代码错误，报错并提示

在 3.4.1 中，我们讨论了如果用户输入的参数不是正整数的情况，而在加入`Format`选项后，我们还需要考虑用户输入的控制数据显示格式的代码有误的情况，若输入错误，我们需要报错并给出有用的提示信息。程序的代码如下：

```
capture program drop myfact4
program define myfact4
version 15
syntax anything(name=k) [, Format(string)]
  if "`format'" != ""{
    capture confirm format `format'
	if _rc{
	  dis as error "invalid format(`format'). It should be format(%10.0g)."
	  dis as text  "for help, see {help format}"
	  exit 198
	}
  }
  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  dis "`k'! = " `format' `a'
end
```

相比`myfact3`，`myfact4`加入了条件判断：如果发现用户输入的`format`形式有误，则提示出错，并给出正确示范和查询途径。这大大方便了用户使用，减少了用户查询错误的时间。

若输入

```
myfact4 5, format(1f)
```

我们可以得到

```
. myfact4 5, format(1f)
invalid format(1f). It should be format(%10.0g).
for help, see format
r(198);
```

#### 3.4.4 在保留自定义结果显示格式的基础上，产生新变量

通过前面的部分，我们已经大致了解了如何编写程序，并逐步增添功能。下面的内容我们将进行进一步的拓展，考虑如何产生新变量，并将阶乘的结果赋值给新变量。程序的代码如下：

```
capture program drop myfact5
program define myfact5
version 15
syntax anything(name=k) [, GENerate(string) Format(string)]
  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  if "`generate'" != ""{
    gen `generate' = `a'
  }
  else{
    dis "`k'! = " `format' `a'
  }
end
```

为了产生新变量，我们在`syntax`命令中加入了可选项`GENerate`。若用户输入新变量名，程序就将变量名存储到暂元`generate`中，并通过后续的代码，实现产生新变量并将阶乘的结果赋值给新变量的结果。

运行程序后可进行以下程序测试：

```
clear
set obs 3
gen x = 1

myfact5 5, gen(x5)
myfact5 5, gen(x)
myfact5 5, gen(z1 z2)  // 输入了两个变量名
myfact5 5, gen(1ab)    // 非法变量名
```

#### 3.4.5 利用可选项 rclass，生成并调用返回值

在编写程序的时候，我们可能不仅仅想要通过运行程序得出一个结果，还想要保存结果，以方便之后的调用。我们可以在`program`命令中加入可选项`rclass`来达到这种效果，程序的结果将保存在`r()`中以便调用。
在实际应用中，将结果保存在`r()`中有以下命令：

- `return scalar name = exp` //保存标量 scalar
- `return local name = exp` //保存局部宏 local
- `return matrix name [=] matname [, copy]` //保存矩阵 matrix

运行完程序后，我们可以通过`return list`命令调看程序的返回值。并通过`r()`调用返回值。

在阶乘的例子中，我们想要将阶乘的结果保存并调用，代码如下：

```
capture program drop myfact6
program define myfact6, rclass // New
version 15
syntax anything(name=k) [, Format(string)]
  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  dis "`k'! = " `format' `a'
  return scalar kk = `a'       // New
end
```

我们通过倒数第二行代码将结果保存。随后可以通过`r(kk)`调用返回值：

若输入：

```
myfact6  5
return list
dis "K! = " r(kk)
```

我们可以得到：

```
. myfact6  5
5! = 120

. return list
scalars:
                 r(kk) =  120

. dis "K! = " r(kk)
K! = 120
```

#### 3.4.6 多个数值的阶乘

在以上的例子中，我们专注于单个数值阶乘的计算，如果想要得到多个数值的阶乘需要重复调用`myfact`文件。但是实际上我们可以通过`tokenize`和`gettoken`命令实现对输入参数的解析，获取所需部分,进而实现多个数值阶乘的计算。

- <b>使用`tokenize`解析输入参数的基本命令如下：</b>

  ```
  tokenize [[`]"][string]["[']] [, parse("pchars") ]
  ```

  其中，`string`代表要分解的参数的名称，可选项`parse("pchars")`表示用`pchars`作为各个部分的分隔符，默认为空格。`tokenize`将参数分解为指定部分，并依次保存在局部宏 1、2 中……
  以下为使用`tokenize`的小例子：
  输入

  ```
  *-tokenize
  local k = "3 5 7" // 字符串
  tokenize "`k'"    // 切割
  di "A=|`1'|, B=|`2'|, C=|`3'|, D=|`4'|"
  ```

  可以得到

  ```
  A=|3|, B=|5|, C=|7|, D=||
  ```

- <b>`gettoken`提供了解析输入参数的另一种方式，基本命令如下：</b>
  ```
  gettoken emname1 [emname2] : emname3 [, parse("pchars") quotes qed(lmacname) match(lmacname) bind]
  ```
  其中，可选项`parse("pchars")`表示用`pchars`作为各个部分的分隔符，默认为空格，该命令会获取`emname3`分隔符之前的参数，并将其储存在`emname1`中；如果加入了`emname2`，剩下的参数则将存储在`emname2`中。
  以下为使用`gettoken`的小例子：
  输入
  ```
  *-gettoken
  local k = "3 5 7"
  gettoken 1 k:k
  dis `1'
  gettoken 2 k:k
  dis `2'
  gettoken 3 k:k
  dis `3'
  ```
  可以得到
  ```
  .   gettoken 1 k:k
  .   dis `1'
  3
  .   gettoken 2 k:k
  .   dis `2'
  5
  .   gettoken 3 k:k
  .   dis `3'
  7
  ```
  想要进一步了解`tokenize`和`gettoken`命令可以使用`help`命令。

继续阶乘的例子，为了实现多个数值的阶乘，我们使用了`tokenize`解析输入参数，代码如下：

```
capture program drop myfact7
program define myfact7, rclass
version 15
syntax anything(name=numlist) [, Format(string)]
tokenize "`numlist'"
local j = 1
while "``j''" !=""{     // `j' 表示房间号；``j'' 表示房间中的内容
  local a = 1
  local k = ``j''       // 注意暂元的引用方式
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  dis "`k'! = " `format' `a'
  return scalar k`k' = `a'
  local j = `j' + 1
}
end
```

输入：

```
myfact7 3 4 5
return list
```

我们可以得到：

```
. myfact6 3 4 5
3! = 6
4! = 24
5! = 120

. return list
scalars:
                 r(k5) =  120
                 r(k4) =  24
                 r(k3) =  6
```

&emsp;

## 4. 在 ado 文件中使用 Mata

在第 3 部分中，我们已经简要介绍了编写 ado 文件的方法和技巧，而在实际编写 ado 文件中，我们还可以加入<b>Mata</b>以加快文件执行的速度。许多常用的计量经济学程序可以用矩阵简明地表示，Mata 可以使 Stata 中使用的特定程序的编程更加容易。

关于 Mata 的入门详见连享会 Stata - Mata 系列 (一)：Mata 入门<https://zhuanlan.zhihu.com/p/112747879>

下文将主要介绍如何在 ado 文件中使用 Mata。

首先沿用第三部分阶乘的例子，我们可以尝试使用 Mata 计算 5 的阶乘：

```
mata                 //进入mata环境
  v=(1..5)
  sum1 =1
  for (i=1; i<=length(v); i++) {
    sum1 = sum1*v[i]
  }
  (sum1)
end                  //退出mata环境
```

而为了实现不同阶乘的计算，我们可以将 mata 与 ado 文件联用来简化重复执行上述步骤。
方式一：直接调用 mata 自带的函数`factorial()`计算阶乘，在 ado 文件中输入单行命令`mata：`实现 Mata 环境的调用。

```
capture program drop myfact10
program define myfact10
version 15
args k    // 定义暂元 k，用于接收用户输入的数字
mata:factorial(`k')
end
```

方式二：自行创建 mata 函数，并在 ado 文件中调用。

```
capture program drop myfact7
program define myfact7
version 15
args k    // 定义暂元 k，用于接收用户输入的数字
mata: m1(`k')
end

capture mata mata drop m1()
version 15
mata:
real rowvector m1(real scalar k)
{
real rowvector a
a = (1..k)
sum1 =1
for (i=1; i<=length(a); i++) {
  sum1 = sum1*a[i]
}
return(sum1)
 }
end
```

调用上述两个 ado 文件我们可以得到：

```
. myfact7 5
  120
. myfact8 5
  120
```

&emsp;

## 5. 进一步学习 ado 文件

要想进一步了解编写 ado 文件时使用的更高级的命令，可以参考 Stata 编程手册（Stata Programming Reference Manual）。同时，还可以阅读现有的 ado 文件并在自己的 ado 文件中借鉴前人的方法。以下有三种方法：

- 使用`findfile`命令定位已有的 ado 文件
  ```
  findfile kappa.ado
  ```
- 使用 viewsource 命令可以查看 ado 文档。
  ```
  viewsource kappa.ado
  ```
- 使用 ssc type 命令来查看 SSC 上存档的所有 ado 文件。
  ```
  ssc type whitetst.ado
  ```


## 6. 相关推文

- 专题：[Stata程序](https://www.lianxh.cn/blogs/26.html)
  - [Stata：编写ado文件自动化执行常见任务](https://www.lianxh.cn/news/776e42fc6ecd4.html)
  - [Stata程序：我的程序多久能跑完？](https://www.lianxh.cn/news/3fd7d7958a92b.html)
  - [Stata程序：暂元-(local)-和全局暂元-(global)](https://www.lianxh.cn/news/523000c83ce1f.html)
  - [Stata程序：切割文件路径和文件名](https://www.lianxh.cn/news/7a233062143f9.html)
  - [Stata程序：在我的程序中接纳另一个程序的所有选项](https://www.lianxh.cn/news/24a023b93693f.html)
  - [Stata 程序：数值求解极值的基本思想](https://www.lianxh.cn/news/d750bfe063205.html)
