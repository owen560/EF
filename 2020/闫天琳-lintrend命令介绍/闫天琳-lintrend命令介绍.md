# lintrend命令介绍

## 1. 简介
本篇推文介绍Stata中用于线性趋势检验的命令：lintrend。该命令针对连续结果的类别平均值，或二进制结果的logodds，检验序数或区间X变量的“线性”假设，以及线性趋势的检验（基于线性或逻辑回归）。

## 2. lintrend 语法介绍
### 2.1 基础语法介绍

`lintrend`的语法结构如下：
```stata
· lintrend yvar xvar [if exp] [in range], [groups(#) round(#) int] [graph proportion noline graph_options]
```
· `yvar`: 因变量。可以是二元（0，1）结果，导致按xvar类别计算比例和对数几率，也可以是连续结果，导致按xvar类别计算均值。

· `xvar`: 自变量。可以是区间或序数自变量

### 2.2 option 介绍

`[groups(#) round(#) int]`
| 命令      | 介绍 |
| :--------- | :--: |
| groups(#)     |  将xvar分成样本大小相似的#个类别。连接点放入较低的类别中。因此样本大小可能会略有不同。用每组xvar的均值代表这一组|
| round(#)   |  将xvar四舍五入到最接近的＃|
| integer |  xvar是整数，直接使用其原始值|

`plot()`

`plot`是一个可选的option，可选的图像类型：
- `mean` - xvar类别的均值图（连续yvar）
- `prop` - xvar类别的比例图（二进制yvar）
- `log` - xvar类别的对数赔率（二进制yvar）图
- `both` - 比例和对数赔率类别的图形

`noline` – 不生成图像


### 2.3 下载及安装
```stata
ssc install lintrend
```

## 3. Stata 实操：3个实例

### 3.1 hyperten：年龄与高血压之间的关系

(1)使用groups(#)分组：

数据(hyperten.dta)来源于一项对1784名年龄在24岁到51岁之间的成年人的研究, 将样本分成10个大小大致相等的组，计算每个年龄组中高血压（1=高血压，0=正常）的比例和对数几率，并要求提供比例图和对数几率图。

```stata
use hyperten.dta,clear
describe
```

结果如下图所示，产生4个变量，1784个观察值：

```stata

Contains data from hyperten.dta
  obs:         1,784                          
 vars:             4                          6 Feb 1996 16:49
 size:        12,488                          
----------------------------------------------------------------------------------------
              storage   display    value
variable name   type    format     label      variable label
----------------------------------------------------------------------------------------
sbpavg          float   %9.0g                 Average Systolic Blood Pressure
age             byte    %8.0g                 Current Age
ses             byte    %8.0g      seslbl     Socioeconomic Status
hbp             byte    %8.0g      yesno      High Blood Pressure
----------------------------------------------------------------------------------------
Sorted by: 
```

用lintrend进行线性趋势检验：
```stata
lintrend hbp age, groups(10) plot(both) xlab ylab
```
结果如下：

```stata
The proportion and log odds of hbp by categories of age
  
  (Note: 10 age categories of equal sample size;
     Uses mean age value for each category)

  +------------------------------------------------+
  |  age   min   max    d   total    hbp   logodds |
  |------------------------------------------------|
  | 26.0    24    27   24     246   0.10     -2.22 |
  | 28.5    28    29   27     166   0.16     -1.64 |
  | 30.6    30    31   26     169   0.15     -1.70 |
  | 32.4    32    33   34     179   0.19     -1.45 |
  | 34.5    34    35   43     151   0.28     -0.92 |
  |------------------------------------------------|
  | 36.5    36    37   44     164   0.27     -1.00 |
  | 39.0    38    40   70     220   0.32     -0.76 |
  | 41.9    41    43   66     152   0.43     -0.26 |
  | 45.5    44    47   86     200   0.43     -0.28 |
  | 49.0    48    51   65     137   0.47     -0.10 |
  +------------------------------------------------+
```
所得比率图，对数几率图：
![](https://static01.imgkr.com/temp/3456e14fa56e448f8d5116c2c4dffece.png)

红线绘制了高血压患者的比例与每个类别的平均年龄的关系，圆圈绘制了对数优势与每个类别的平均年龄的关系，用“最佳拟合”线性回归线。图表显示年龄和高血压之间存在正线性关系。

(2)使用round(#)分组：
将年龄按五年的增量进行分组。同样，比例图和对数概率图都是需要的。
```stata
lintrend hbp age, round(5) plot(both) xlab ylab
```

结果如下：
```stata
The proportion and log odds of hbp by categories of age
  
  (Note: age in categories rounded to nearest 5)

  +------------------------------------------------+
  | age   min   max     d   total    hbp   logodds |
  |------------------------------------------------|
  |  25    24    27    24     246   0.10     -2.22 |
  |  30    28    32    72     436   0.17     -1.62 |
  |  35    33    37   102     391   0.26     -1.04 |
  |  40    38    42   117     323   0.36     -0.57 |
  |  45    43    47   105     249   0.42     -0.32 |
  |------------------------------------------------|
  |  50    48    51    65     137   0.47     -0.10 |
  +------------------------------------------------+
```

可以得到比例图和对数概率图：
![](https://imgkr2.cn-bj.ufileos.com/e0f62d9f-86dd-4f3b-b885-ceb34d0d9afe.png?UCloudPublicKey=TOKEN_8d8b72be-579a-4e83-bfd0-5f6ce1546f13&Signature=Fobl69ox4Wzm716eGmBTyFpZZww%253D&Expires=1611664122)

年龄的四舍五入值用于组别中点，每个类别范围为5年。类别的最小可能年龄值（age=25）是23，但是，由于样本中没有这个年龄段的人，所以实际的最小值（24）被用作最小值。对于的最大值也是如此age=50（51而不是52）。对数几率图再次表明了一种线性趋势，尽管高血压在40岁之前增加得更快，但在40岁以上则变慢。

(3)高血压与社会地位之间的关系：
高血压数据集中的另一个变量是社会经济状况的测量(ses)。这是一个序数变量，分为三类：低社会经济地位（1）、中等社会经济地位（2）和高社会经济地位（3）。这个例子要求的比例和对数几率高血压的每一类ses，以及对数概率图。
```stata
lintrend hbp ses, integer plot(log) xlab(1,2,3) ylab
```

结果如下：
```stata
The proportion and log odds of hbp by categories of ses
  
  (Note: ses in categories using original values)

  +-----------------------------------------+
  |      ses     d   total    hbp   logodds |
  |-----------------------------------------|
  |    1:Low   215     670   0.32     -0.75 |
  | 2:Middle   138     537   0.26     -1.06 |
  |   3:High   117     512   0.23     -1.22 |
  +-----------------------------------------+
```
对数概率图：
![](https://static01.imgkr.com/temp/72e3332fb0cc48c9bdda795b686071be.png)

(4)平均收缩压与年龄的关系
前面的例子检验了连续的二元结果，`lintrend`会计算平均数而不是比例和对数几率。假设第一个例子的结果是平均收缩压(sbpavg)而不是高血压。我们将样本分成10组，每组大小大致相等，计算每个年龄组的平均收缩压，并要求绘制每个年龄组的平均收缩压与平均年龄的关系图。
```stata
lintrend sbpavg age, groups(10) plot(mean) xlab ylab
```

关系图如下：
![](https://imgkr2.cn-bj.ufileos.com/439dd80a-59e9-49e6-bbfe-c343ec608344.png?UCloudPublicKey=TOKEN_8d8b72be-579a-4e83-bfd0-5f6ce1546f13&Signature=n4ccwHnoTHcy7GonU%252FPvd5nE3t0%253D&Expires=1611664494)



年龄类别与(1)相同。收缩压随着年龄的增长而增加。这条线周围有波动，但本质上增长是线性的。






















