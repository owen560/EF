**作者**：黄依桐（中山大学）  

**Email**：<huangyt2@126.com>

# 不稳定性下基于 VAR 模型的 Granger 因果检验

## 1. 引言：

### 1.1 VAR模型

在宏观经济模型的实证分析中，我们常常需要研究经济变量之间的动态变化关系， VAR 模型可以帮助我们分析这种关系。

**VAR（Vector Autoregressive）**即向量自回归模型，由 Sims 1980年提出，是指所有当期变量对所有变量的若干滞后项进行线性回归。简单的 VAR 模型可以由单变量时间序列的 AR 过程推出，如二阶 VAR 模型：
$$
\begin{cases}
y_t = \alpha_0 + \alpha_1 y_{t-1} +\alpha_2 y_{t-2} +...+\alpha_k y_{t-k} + \epsilon_{t}  \\
x_t = \beta_0 + \beta_1 x_{t-1} +\beta_2 x_{t-2} +...+\beta_k x_{t-k} + u_{t}
\end{cases}
$$


### 1.2 传统的 Granger 因果检验

VAR 模型系数的一个重要假设检验问题为 **Granger 因果检验（Granger causality test）**。

以上文二阶 VAR 模型为例，给定 $y_t$ 的滞后阶数，若 $x_t$ 的滞后项在以 $y_t$ 为因变量的方程中联合统计显著，即拒绝以下假设：
$$
H_0:\beta_i=0\:\:\:\:\:\:i=1,...,k
$$
则称 $x_t$ 可以 **Granger 预测** $y_t$，或 $x_t$ 为 $y_t$ 的 **Granger 因**。

对任一个内生变量我们都能逐次检定所有其它内生变量是否有 Granger 预测能力。若 内生变量之间彼此具 Granger 预测能力，则这些变量之间存在动态变化关系。需要注意的是以上的零假设隐含参数不随时间改变的前提，即参数随着时间变化是稳定的。



### 1.3 VAR 模型中的不稳定性问题

现实的宏观经济分析中，经济变量之间的关系常常随时间改变，体现在计量经济模型中即为自变量参数随时间改变，我们称之为**不稳定性（instability）**，如：
$$
y_t=\beta_t+\epsilon_{t}\:\:\:\:\:\beta_t=\begin{cases}\beta_1\:\:\:\: t=1,2,...\tau\\ \beta_2\:\:\:\:t=\tau+1,...T\end{cases}
$$
Rossi 在2005年的文章中证明，在不稳定性下，传统 Granger 因果检验可能是无效的，就算 $x_t$ 是 $y_t$ 的 Granger 因， $x_t$ 参数的不稳定性也可能使参数估计值趋近于0，导致无法拒绝零假设从而无法证明二者的 Granger 因果关系。



### 1.4 稳健的Granger因果检验：gcrobustvar

Rossi 认为，不稳定性下，Granger 因果检验应拓展为参数是否在任意时间点都等于0，即：
$$
H_0:\beta_{it}=0\:\:\:\:\:\:i=1,...,k\:\:\:\:\:t=1,...,T
$$
我们称之为稳健的 Granger 因果检验。

文章中还提出稳健的 Granger 因果检验对应的检验统计量，包括：指数沃氏检验 **ExpW\***，均值沃氏检验 **MeanW\***，Nyblom 检验 **Nyblom\***，Quandt 似然比值检验 **QLR\***。基于这些统计量，Rossi  Barbara 和 Yiru Wang 编写了 Stata 命令 `gcrobustvar` ，其基本语法如下：

```stata
gcrobustvar depvarlist, ///
			pos(#, #) ///
			[nocons horizon(#) lags(numlist) trimming(level)]
```

- `depvarlist`：被解释变量序列，即VAR模型内所有变量。必需项。
- `pos`：包含两个整数的数列，分别代表被检验的被解释变量和解释变量在 `depvarlist` 中位置。如 `pos(1,2)` 表明检验 `depvarlist` 中第二个变量是否能Granger预测第一个变量。必需项。
- `nocons`：表明模型无常数项。可选项，默认模型有常数项。
- `horizon`：预测期数。可选项，默认为`horizon(0)` ，即为缩减型VAR模型。
- `lags`：滞后期数的数列。需要注意此处为数列而非最大滞后期数，如 `lags(2)` 表明模型中只包含滞后2期的滞后项，而 `lags(1 2)` 才表明模型中包含1期滞后与2期滞后。Stata中数列的简便表达形式也适用，如 `lags(1/3)` = `lags(1 2 3 4)`。可选项，默认为 `lags(1 2)`
- `trimming`：修整参数，限定参数不稳定性发生的时间范围。如 `trimming(s)` 表明参数的不稳定性发生在 $[ sT, (1-s)T ]$ 时间段中。可选项，默认为 `trimming(0.15)` 。



## 2. Stata实例

### 2.1 实例说明

1. 以下实例来源于 Barbara Rossi, Yiru Wang, 2019, Vector autoregressive-based Granger causality test in the presence of instabilities, Stata Journal, 19(4): 883–899. [-PDF-](https://sci-hub.ren/10.1177/1536867X19893631), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19893631)
2. 数据 GCdata.xlsx 为文章附件。
3. 运用 `gcrobustvar` 命令前应先在 Stata 下载 `matsqrt` 命令。

### 2.2 数据与模型说明

我们利用美国1959-2000年每一季度的通胀率 $\pi_t$、失业率 $u_t$、利率 $R_t$ 数据来实现此命令。导入数据与数据处理过程如下：

```stata
. *导入数据
. import excel gcdata.xlsx, sheet(SW2001) firstrow clear

/*   Notes:
	一共四个变量，168个观察值。
	pdate: 时间。小数点前表示年份，小数点后表示季度。如1959的四个季度按顺序分别表示为1959，1959.25，1959.5，1959.75。
	pi: 季度通胀率。算法为 pi=400*In(P_t/P_{t-1}),其中 P_t 为链式加权的 GDP 价格指数，无单位。
	u: 季度失业率。算法为季度内3个月份失业率的算术平均，单位为百分比。
	R: 季度利率。算法为季度内3个月份利率的算术平均，单位为百分比。
*/

. *时间变量处理与设定
. generate year = int(pdate) // 获得年份
. generate quater = (pdate - int(pdate))*4 + 1 // 获得季度数
. generate tq = yq(year, quater) // 以1960第一季度为0，为每一季度找到对应的编号。
. format tq %tq // 将时间编号转化为以季度划分的时间变量
. tsset tq // 指定时间变量
```



### 2.3 运用gcrobustvar进行稳健的Granger因果检验

文章研究的模型为缩减型 VAR 模型：
$$
\pi_t=c_t^\pi+\Phi_t^{\pi,\pi}(L)\pi_t+\Phi_t^{\pi,u}(L)u_t+\Phi_t^{\pi,R}(L)R_t+\epsilon_t^\pi
$$
其中
$$
\Phi_t^{.,.}(L)=\phi_{1,t}^{.,.}L+\phi_{2,t}^{.,.}L^2+\phi_{3,t}^{.,.}L^3+\phi_{4,t}^{.,.}L^4
$$
我们感兴趣的是 $u$ 是否能 Granger 预测 $\pi$ ，并且想要检验这一关系在不稳定性下是否稳健，对应的零假设为：
$$
H_0:\phi_{j,t}^{\pi,u}=0\:\:\:\:\:\:j=1,...,k\:\:\:\:\:t=1,...,T
$$
需注意，在运用 `gcrobustvar` 命令前应先导入 pvtable.mmat 文件以导入检验统计量对应的p值表。

```stata
. *导入检验统计量对应的p值表
. mata // 进入Mata
------------------------- mata (type end to exit) ------------------------
: mata clear
: mata matuse pvtable,replace // 导入pvtable.mmat
(loading pvap0opt[34,21], pvapiopt[34,21], pvnybopt[34,21], pvqlropt[34,21])
: st_matrix("r(pvap0opt)",pvap0opt)
: st_matrix("r(pvapiopt)",pvapiopt)
: st_matrix("r(pvnybopt)",pvnybopt)
: st_matrix("r(pvqlropt)",pvqlropt)
: end
------------------------------------------------------------------------------
. matrix pvap0opt = r(pvap0opt)
. matrix pvapiopt = r(pvapiopt)
. matrix pvnybopt = r(pvnybopt)
. matrix pvqlropt = r(pvqlropt)
```

导入 pvtable.mmat 文件后即可运用 `gcrobustvar` 。

```stata
. *不稳定性下基于VAR模型的Granger因果检验
. gcrobustvar pi u R, pos(1,2) lags(1/4)
Running the Granger Causality Robust Test...
Setting: 
 Variables in VAR: pi u R 
 Lags in VAR:1 2 3 4 
 h is 0 (reduced-form VAR).
 Trimming parameter is .15 
 Constant is included.
 Assuming homoskedasticity in idiosyncratic shocks. 
```

统计量及其 p值 结果如下：

```stata
Results of Granger Causality Robust Test: Lags of u Granger cause pi 
ExpW*,MeanW*,Nyblom*,QLR* -- and their p-values below

                       ExpW      MeanW     Nyblom      SupLR
statistics(pi:u)  9.1974592  17.047538  4.6890489  23.187923
   p-value(pi:u)  .07383773  .05866136  .07939046  .07069996
```

从 **ExpW\***， **MeanW\***， **Nyblom\***， **QLR\***  4个统计量的 p值 可以看出，可以在 10% 的显著性水平下可以拒绝原假设，故 $u$ 可以 Granger 预测 $\pi$ 。

此外，`gcrobustvar` 命令还会返回沃氏统计量随时间的变化图，并命名为 gcrobustvar\_{被解释变量}_{解释变量}， 如下：

```stata
. graph save gcrobustvar_pi_u, replace
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/基于VAR的Granger因果_Fig1_pi-u_黄依桐.png)



### 2.4 传统 Granger 因果检验与稳健 Granger 因果检验对比

分别以通胀率、失业率、利率作为被解释变量进行传统 Granger 因果检验：

```stata
. varsoc pi u R, maxlag(4)
. qui var pi u R, lags(1/4) dfk small
. vargranger

   Granger causality Wald tests
  +------------------------------------------------------------------------+
  |          Equation           Excluded |     F      df    df_r  Prob > F |
  |--------------------------------------+---------------------------------|
  |                pi                  u |  3.1036     4     151   0.0173  |
  |                pi                  R |  1.3174     4     151   0.2661  |
  |                pi                ALL |  4.1533     8     151   0.0002  |
  |--------------------------------------+---------------------------------|
  |                 u                 pi |  1.2337     4     151   0.2990  |
  |                 u                  R |    3.65     4     151   0.0072  |
  |                 u                ALL |  4.5282     8     151   0.0001  |
  |--------------------------------------+---------------------------------|
  |                 R                 pi |  5.7553     4     151   0.0002  |
  |                 R                  u |  7.7562     4     151   0.0000  |
  |                 R                ALL |   5.298     8     151   0.0000  |
  +------------------------------------------------------------------------+

```

传统 Granger 因果检验显示：5%的显著性水平下， $\pi$ 可以 Granger 预测 $R$ ，$u$ 可以 Granger 预测 $R$ 和 $\pi$ ，$R$ 可以 Granger 预测 $u$ 。

分别以通胀率、失业率、利率作为被解释变量进行稳健 Granger 因果检验，得到各统计量的p值情况整理成表如下：

| 被解释变量：解释变量 | ***ExpW\*** | **MeanW\*** | **Nyblom\*** | **QLR\*** |
| :------------------: | :---------: | :---------: | :----------: | :-------: |
|     $\pi$ : $u$      |    0.07     |    0.06     |     0.08     |   0.07    |
|     $\pi$ : $R$      |    0.01     |    0.20     |     0.03     |   0.00    |
|     $u$ : $\pi$      |    0.20     |    0.44     |     0.22     |   0.08    |
|      $u$ : $R$       |    0.00     |    0.01     |     0.02     |   0.00    |
|     $R$ : $\pi$      |    0.00     |    0.00     |     0.00     |   0.00    |
|      $R$ : $u$       |    0.00     |    0.00     |     0.00     |   0.00    |

稳健 Granger 因果检验显示：5%的显著性水平下，$R$ 可以 Granger 预测 $\pi$ ，但这一点在传统的 Granger 因果检验中无法得出。查看以 $R$ 为被解释变量、$\pi$ 为被解释变量时稳健 Granger 因果检验对应的沃氏统计量随时间的变化图：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/基于VAR的Granger因果_Fig2_pi-R_黄依桐.png)

由图形可知，实际上沃氏统计量在1980年第一季度左右超过了5%的显著性水平对应的值，因此可以拒绝参数的零假设，证明5%的显著性水平下，$R$ 可以 Granger 预测 $\pi$ 。而传统的 Granger 因果检验无法证明这点。由此看出稳健的 Granger 因果检验的优势。 



## 3. 结语

经济模型的实证研究中，常常存在经济变量之间的关系随时间发生改变的现象，即不稳定性。不稳定性下，传统基于 VAR 模型的 Granger 因果检验可能无效。采用稳健的 Granger 因果检验可以较有效地解决这一问题，其 Stata 实现命令为 `gcrobustvar` 。



## 4. 参考资料

- Barbara Rossi, Yiru Wang, 2019, Vector autoregressive-based Granger causality test in the presence of instabilities, Stata Journal, 19(4): 883–899. [-PDF-](https://sci-hub.ren/10.1177/1536867X19893631), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19893631)
- Rossi, B. 2005. Optimal tests for nested model selection with underlying parameter instability. Econometric Theory 21: 962–990. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2005&pages=962-990&author=B.+Rossi&title=Optimal+tests+for+nested+model+selection+with+underlying+parameter+instability) | [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr15-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1017%2FS0266466605050486)

