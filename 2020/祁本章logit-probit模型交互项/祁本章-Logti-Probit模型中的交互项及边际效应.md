# Logti-Probit 模型中的交互项及边际效应

&emsp;
>**Source 1**:Ai, Chunrong, and Edward C. Norton.2003.Interaction Terms in Logit and Probit Models.**Economics Letters**.80(1): 123–29  [-PDF-](https://www.sciencedirect.com/science/article/pii/S0165176503000326?via%3Dihub#BIB2)  

 >**Source 2**: Edward C. Norton, Hua Wang, Chunrong Ai.2004.Computing Interaction Effects and Standard Errors in Logit and Probit Models.**Stata  Journal** 4(2): 154–167. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400206)

&emsp;
>**作者**：祁本章（中山大学）  
>**E-Mail**:** <2833210042@qq.com>  


[TOC]


##  1. 引言
---
经济学家经常估计交互项来推断一个自变量对因变量的影响如何取决于另一个自变量的大小。

尽管交互项在应用计量经济学中有着广泛的应用，许多计量学家和统计学家都知道如何正确地解释交互项，但大多数应用研究者对非线性模型中交互项的系数存在误解。因为非线性模型中交互效应的大小不等于交互项的边际效应，可以是相反的符号，其统计显著性不是用标准软件计算的。

为了更好地理解交互项及边际效应，同时将其在 logit,probit 模型中准确估计。本推文将重点介绍论文 Interaction terms in logit and probit models 中的理念和证明及论文 Computing Interaction Effects and Standard Errors in Logit and Probit Models 中的 公式推导和「Stata」命令。
&emsp;

##  2. 论文 1 原理介绍
---
###  2.1 线性模型交互项及边际效应

  在线性模型中，对两个变量之间交互项系数的解释是直接的。设连续因变量 $\mathit{y}$ 依赖于两个自变量 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ ，它们之间的交互项 $\mathbf{X}$ ，一个包含与 $\mathit{x}_{1}$ 和  $\mathit{x}_{2}$ 无关的常数项的附加自变量向量，以及 $\beta{s}$ 是未知参数。如果 $\mathit{x}_{1}$ 和  $\mathit{x}_{2}$ 是连续的，自变量 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ 的交互效应是 $\mathit{y}$ 的期望值的交叉导数。等式如下：

$$\frac{\partial^2E\big[\mathit{y}&#124\mathit{x}_{1},\mathit{x}_{2},\mathbf{X}\big]}{\partial\mathit{x}_{1}\partial\mathit{x}_{2}}=\beta_{12}$$

如果 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ 是两分的，那么 $\mathit{x}_{1}$ 和  $\mathit{x}_{2}$ 从0到1的变化的交互效应可以通过取离散差来发现，等式如下：

$$\frac{\Delta^2E\big[\mathit{y}&#124\mathit{x}_{1},\mathit{x}_{2},\mathbf{X}\big]}{\Delta\mathit{x}_{1}\Delta\mathit{x}_{2}}=\beta_{12}$$

所以如果 $\mathbf{X}$ 独立于 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ ，那么自变量 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ 的交互作用对于连续和离散交互变量都是 $\beta_{12}$ 。交互效应的统计显著性可用系数 $\beta_{12}$ 的单次$\mathit{t}$ 检验进行检验。

### 2.2 非线性模型交互项及边际效应

然而，非线性模型和线性模型不同。例如，一个除了因变量 $\mathit{y}$ 是虚拟变量，其他均类似于上一例的 probit 模型。因变量的条件平均值为：

$$E\big[\mathit{y}&#124\mathit{x}_{1},\mathit{x}_{2},\mathbf{X}\big]=\Phi(\beta_{1}\mathit{x}_{1}+\beta_{2}\mathit{x}_{2}+\beta_{12}\mathit{x}_{1}\mathit{x}_{2}+\mathbf{X}\beta)=\Phi(u)$$                        

其中 $\Phi$ 是标准正态累积分布，$\mathit{u}$ 代表 $\beta_{1}\mathit{x}_{1}+\beta_{2}\mathit{x}_{2}+\beta_{12}\mathit{x}_{1}\mathit{x}_{2}+\mathbf{X}\beta$ 。假设 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ 是连续的。交互项 $\mathit{x}_{1}\mathit{x}_{2}$ 的交互效应是 $\mathit{y}$ 的期望值的交叉导数。结果如下：

$$\frac{\partial^2\Phi(u)}{\partial\mathit{x}_{1}\partial\mathit{x}_{2}}=\beta_{12}\Phi'(u)+(\beta_{1}+\beta_{12}\mathit{x}_{2})(\beta_{2}+\beta_{12}\mathit{x}_{1})\Phi''(u)$$                    

然而，大多数应用经济学家计算的是交互项的边际效应，即：

$$\frac{\partial\Phi(u)}{\partial\mathit{x}_{1}\partial\mathit{x}_{2}}=\beta_{12}\Phi'(u)$$

很明显， $\beta_{12}\Phi'(u)$ 不等于真正的交互效应。

这些方程对于非线性模型有四个重要的含义。首先，即使 $\beta_{12}$ =0，交互效应也可能是非零的。而对于 $\beta_{12}$ =0的 probit 模型，交互效应为

$$\frac{\partial^2\Phi(u)}{\partial\mathit{x}_{1}\partial\mathit{x}_{2}}&#124_{\beta_{12}=0}=\beta_{1}\beta_{2}\Phi''(u)$$

其次，交互效应的统计显著性不能用交互项系数 $\beta_{12}$ 的简单 $\mathit{t}$ 检验来检验。第三，与线性模型中的交互作用不同，交互作用是以自变量为条件的。（众所周知，在一个非线性模型中，单个不可分割变量的交互效应是以自变量为条件的。）第四，对于不同的协变量值，交互效应可能有不同的符号。因此，$\beta_{12}$ 的符号并不一定表示交互效应的符号。

### 2.3 推导与估计

为了更好地改进应用计量经济学者的实践，论文中推导了一般非线性模型中交互效应的大小和标准误差的公式。这些公式很容易应用于 logit、 probit 和其他非线性模型。

首先介绍一般非线性模型的符号。设 $\mathit{y}$ 表示原始因变量。设向量 $\mathbf{x}$ 为自变量的$\mathtt{k\times1}$ 向量。所以$\mathbf{x}=(\mathit{x}_{1}\cdots\mathit{x}_{k})$ ,给定 $\mathbf{x}$ 的 $\mathit{y}$ 的期望值为:

$$E\big[\mathit{y}&#124\mathbf{x}]=F(\mathit{x},\beta)$$

其中函数F到 $\beta$ 已知，并且是两次连续可微的。让 $\Delta$ 表示差分算子或导数算子，这取决于回归系数是离散的还是连续的。

本文的重点是通过计算交叉导数（或差分）来发现交互效应，而不仅仅是通过观察交互项上的系数。$\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ 对 $\mathit{y}$ 的交互效应是：

$$\mu_{12}=\frac{\Delta^2F(\mathbf{x},\beta)}{\Delta{x}_{1}\Delta{x}_{2}}$$

其估计值设为：

$$\hat\mu_{12}=\frac{\Delta^2F(\mathbf{x},\hat\beta)}{\Delta{x}_{1}\Delta{x}_{2}}$$

$\hat\beta$ 为 $\beta$ 的一致估计量，$\mathit{F}$ 的连续性和 $\hat\beta$ 的一致性确保了 $\hat\mu_{12}$ 对 $\mu_{12}$ 的一致性。而对于估计值 $\hat\mu_{12}$ 的标准误，通过 Delta 方法可得公式如下：

$$\hat\mu_{12}\sim\mathit{N}\left(\mu_{12},\frac{\partial}{\partial\beta'}\big[\frac{\Delta^2F(\mathbf{x},\beta)}{\Delta{x}_{1}\Delta{x}_{2}}]\Omega_{\beta}\frac{\partial}{\partial\beta}\big[\frac{\Delta^2F(\mathbf{x},\beta)}{\Delta{x}_{1}\Delta{x}_{2}}]\right)$$

而 $\hat\mu_{12}$ 的渐进方差的一致估计为:

$$\hat\sigma^2_{12}=\frac{\partial}{\partial\beta'}\big[\frac{\Delta^2F(\mathbf{x},\hat\beta)}{\Delta{x}_{1}\Delta{x}_{2}}]\hat\Omega_{\beta}\frac{\partial}{\partial\beta}\big[\frac{\Delta^2F(\mathbf{x},\hat\beta)}{\Delta{x}_{1}\Delta{x}_{2}}]$$

$\hat\Omega^2_{\beta}$ 是 $\hat\beta$ 的一致协方差估计。t 统计量为 $t=\frac{\hat\mu_{12}}{\hat\sigma_{12}}$ ,且在某些正则条件下，t 统计量具有渐近标准正态分布。对于给定的 $\mathbf{x}$ ,使用 t 统计量检验交互作用效应等于零的假设。这些公式包括许多常用模型，包括 logit、probit、tobit、删失回归模型、带正态误差的对数变换模型、计数模型和持续时间模型。涉及三个或更多变量之间的交互项也可以类似的方式找到。

### 2.4 实际证明

为了说明论文的观点，论文中估计了一个 logit 模型来预测 HMO 的购买概率，因变量是一个虚拟变量，为购买保险的预测概率，三个连续自变量为年龄、日常生活活动的数量（一个人按困难程度从 0 到 6 计数的基本体育活动数量），以及参加卫生组织的县人口百分比及其相互作用。

这些数据主要来自 1993-1996 年的医疗保险当前受益人调查，这是一项对医疗保险可兑现性的纵向调查。有每年 38185 人的观察水平，同时排除了那些居住在没有医疗保健卫生组织服务的县的人。市场渗透率数据则来自医疗保险市场渗透文件。

该模型运行了两次，一次是分析年龄和日常生活能力之间的交互效应，一次是分析年龄和市场渗透率之间的交互效应。

为参考绘制的凹线是由 $\beta_{12}F'(u)\$ 计算的交互项的边际效应。

绘制的结果表如下：
|Variable                  | Mean | Min| Max|Model 1|Model 2|
| ------------------------ | ---- | -- |----|-------|-------|
|Dependent variable        |      |    |    |       |       |
| HMO enrolment            |0.122 | 0  | 1  |       |       | |                          |      |    |    |       |       |
|Independent variables     |      |    |    |       |       |
|  Constant                |      |    |    |3.107**|3.129**|
|                          |      |    |    |(0.045)|(.058)|
| Age-65                   |12.49 | 0  |47|0.0233**|0.0214**|
|                          |      |    |  |(0.0030)|(0.0045)|
|Activities of daily living|0.973 |  0 | 6| 0.105**| 0.179**|
|                          |      |    |  | (0.030)| (0.015)|
|HMO market penetration  |0.090|0.0001|0.516|10.01**|10.33**|  |                        |     |      |     |(0.13)| (0.23)|
|Interaction terms         |    |     |       |     |
|Age$\times$ADLs             |16.9|    0|  270 |0.0049**|      |
|                        |      |     |      |   (0.0018)|
|Age$\times$market penetration| 1.09 | 0|  17.4 |    | 0.028|
|                        |      |      |       |    |(0.017)|

标准误在括号中。*和**表示5%和1%水平的统计显著性。

模型一画出的图如下：
![图 1-a](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig01.png)

图 1-a 的横轴为每位个体购买保险的预测概率，图中每个点则表示不同观测值处的交互效应。

![图 1-b](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig02.png)

图 1-b 的横轴为每位个体购买保险的预测概率，图中每个点则表示不同观测值处的交互效应对应的 t 统计量。 

从图中发现；
1.交互效应的大小和统计意义因观测值而异。虽然年龄与日常生活能力的交互效应系数为负，且具有统计学意义（见上表），但对于许多预测值小于 0.2 的 预测概率来说，交互效应是正的，而不是负的。

2.这个案例的交互效应为正时，交互效应的统计显著性往往强于为负时，甚至有高达 10 的 t 统计量（见图 1-b）。所以不同观测值处交互效应的显著性也不同。

模型二画出的图分别如下：
![图 2-a](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig03.png)

图 2-a 的横轴为每位个体购买保险的预测概率，图中每个点则表示不同观测值处的交互效应。

![图 2-b](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig04.png)

图 2-b 的横轴为每位个体购买保险的预测概率，图中每个点则表示不同观测值处的交互效应对应的 t 统计量。 

从图中发现：
1.在第二个模型中，年龄和市场渗透率之间的交互项系数为负，但在统计上不显著。同样，交互效应的变化很大，并且对许多观察都是积极的（见图 2-a）。

2.即使交互效应本身在统计上不显著，但对大多数观察结果都是显著的（见图 2-b）。

在不同数据集的 logit 模型 和 probit 模型的交互效应图中，该论文认为这两个例子是典型的。

得出结论：当交互效应与预测概率一起作图时，交互效应总呈 S 形。它穿过接近于 $F(u)=0.5$ 的（不正确的）参考线 $\beta_{12}F'(u)\$ 。

同时也证实了交互效应的正负依赖于具体的观测值。而且与单个变量的边际效应不同，当预测概率值不接近 0.5 时，交互效应结果在量级和统计显著性方面最强。同时还发现在相同数据上运行的 logit 和 probit 模型的结果几乎完全相同。
&emsp;

##  3. 论文 2 理念介绍
---
在论文 Ai and Norton(2003) 中，已经介绍了交互项的计算误区并证明了这一点对于计算结果有很大影响，在另一篇论文中，继续探讨了这个问题，并总结出了更加详细的针对 logit 模型和 probit 模型的推导公式，同时介绍了 「Stata」 命令 inteff 来计算修正 logit 和 probit 模型的交互作用和标准误，并画出直观的图。

对于 logit 和 probit 模型，将交互效应定义为预测概率的变化，即 $\mathit{y}$ =1
时 $\mathit{x}_{1}$和 $\mathit{x}_{2}$ 的变化。该论文主要讨论最常见的交互类型，即两种类型之间的交互变量。对于一个有三个交互作用的变量的模型，正确的解释需要取三个导数（或三个离散差），具体推导遵从前一篇论文。

如果一个变量除了交互作用外，还具有更高阶项，则解释也很复杂。对于所有这些比较复杂的模型，原理是和论文1中一样的：取导数或离散差分。

### 3.1 1ogit 模型

logit 模型中，$F(\mathit{u})$ 是常见的logit累积分布函数：

$$F(\mathit{u})=\frac{1}{1+e^{-(\beta_{1}\mathit{x}_{1}+\beta_{2}\mathit{x}_{2}+\beta_{12}\mathit{x}_{1}\mathit{x}_{2}+\mathbf{X}\beta)}}$$

当交互作用的变量都是连续的时，交互效应是相对于 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ 的交叉导数：

$$\frac{\partial^2F(\mathit{u})}{\partial\mathit{x}_{1}\partial\mathit{x}_{2}}=\beta_{12}\{F(\mathit{u})(1-F(\mathit{u}))\}$$ $$+(\beta_{1}+\beta_{12}\mathit{x}_{2})(\beta_{2}+\beta_{12}\mathit{x}_{1})\big[F(\mathit{u})\{1-F(\mathit{u})\}\{1-2F(\mathit{u})\}]$$

当交互变量均为虚拟变量时，交互效应为离散双差：

$$\frac{\Delta^2F(\mathit{u})}{\Delta\mathit{x}_{1}\Delta\mathit{x}_{2}}=\frac{1}{1+e^{-(\beta_{1}+\beta_{2}+\beta_{12}+\mathbf{X}\beta)}}$$ $$-\frac{1}{1+e^{-(\beta_{1}+\mathbf{X}\beta)}}-\frac{1}{1+e^{-(\beta_{2}+\mathbf{X}\beta)}}+\frac{1}{1+e^{-\mathbf{X}\beta}}$$

当一个连续变量和一个虚拟变量相互作用时，相互作用效应是单个导数（相对于$\mathit{x}_{1}$ ）的离散差（相对于 $\mathit{x}_{2}$ ）：

$$\frac{\Delta\frac{\partial F(\mathit{u})}{\partial\mathit{x}_{1}}}{\Delta\mathit{x}_{2}}=(\beta_{1}+\beta_{12})$$$$\left(F\{(\beta_{1}+\beta_{12})\mathit{x}_{1}+\beta_{2}+\mathbf{X}\beta\}\right)\times(1-F\{(\beta_{1}+\beta_{12})\mathit{x}_{1}+\beta_{2}+\mathbf{X}\beta)\})$$$$-\beta_{1}\big[F(\beta_{1}\mathit{x}_{1}+\mathbf{X}\beta)\{1-F(\beta_{1}\mathit{x}_{1}+\mathbf{X}\beta)\}]$$

### 3.2 probit 模型

对于 probit 模型，$F(\mathit{u})$ 是常见的正态累积分布函数:

$$F(\mathit{u})=\Phi(\beta_{1}\mathit{x}_{1}+\beta_{2}\mathit{x}_{2}+\beta_{12}\mathit{x}_{1}\mathit{x}_{2}+\mathbf{X}\beta)$$

当相互作用的变量都是连续的时，相互作用效应是相对于 $\mathit{x}_{1}$ 和 $\mathit{x}_{2}$ 的二次导数：

$$\frac{\partial^2F(\mathit{u})}{\partial\mathit{x}_{1}\partial\mathit{x}_{2}}=\{\beta_{12}-(\beta_{1}+\beta_{12}\mathit{x}_{2})(\beta_{2}+\beta_{12}\mathit{x}_{1})u\}\phi(u)$$

当交互变量均为虚拟变量时，交互效应为离散双差：

$$\frac{\Delta^2F(\mathit{u})}{\Delta\mathit{x}_{1}\Delta\mathit{x}_{2}}=\Phi(\beta_{1}+\beta_{2}+\beta_{12}+\mathbf{X}\beta)$$ $$-\Phi(\beta_{1}+\mathbf{X}\beta)-\Phi(\beta_{2}+\mathbf{X}\beta)+\Phi(\mathbf{X}\beta)$$

当一个连续变量和一个虚拟变量相互作用时，交互效应是单个导数（相对于 $\mathit{x}_{1}$  ）的离散差（相对于$\mathit{x}_{2}$ ）:

$$\frac{\Delta\frac{\partial F(\mathit{u})}{\partial\mathit{x}_{1}}}{\Delta\mathit{x}_{2}}=(\beta_{1}+\beta_{12})\phi\{(\beta_{1}+\beta_{12})\mathit{x}_{1}+\beta_{2}+\mathbf{X}\beta$$ $$-\beta_{1}\phi(\beta_{1}\mathit{x}_{1}+\mathbf{X}\beta)$$
&emsp;

##  4. 论文 2「Stata」命令介绍
---
 `inteff varlist [if exp] [in range] [, savedata(filename[, replace])
            savegraph1(filename[, replace]) savegraph2(filename[, replace])]`


### 4.1 选用 inteff 理由

由于计算交互效应大小的非线性模型，必须计算因变量期望值的交叉导数。交互效应的统计显著性检验必须基于估计交叉偏导数，而不是交互项的系数。

「Stata」的 mfx 和 dprobit 命令可用于估计给定自变量特定值的单个变量的边际效应。但是，当一个变量与另一个变量交互或具有更高阶项时，不应使用这些命令。在这些情况下，mfx 和 dprobit 将估计错误的边际效应。mfx 和 dprobit 命令不知道一个变量是否与另一个变量交互或者有更高阶的项，所以所以它们不能对它取全导数变量。

此外，mfx 和 dprobit 命令的结果会产生误导，因为任何非线性模型的边际效应在每次观测中都是不同的，然而 mfx 和 dprobit 只报告每个变量的一个边际效应。本文中的例子将使人明白边际效应的大小是有分布的，而且往往是相反的符号。

「Stata」 命令 predictnl 可用于派生 inteff 的所有结果。然而，由于 predictnl 非常通用，允许在任何「Stata」估计命令之后进行非线性预测，因此用户必须能够用编写正确的边际效应公式。

由于 inteff 比 predictnl 更易于使用，使用它将导致更少的用户错误，并鼓励更多的研究人员计算正确的交互效果，论文选择使用并介绍 inteff 命令。

### 4.2 inteff 命令概述 

当两个变量相互作用时，新命令 inteff 计算 logit 或 probit 的每个观测值的交互作用效果、标准误和 z statistic。交互变量不能有高阶项，如平方项。该命令设计为在拟合 logit 或 probit 模型后立即运行。

### 4.3 inteff 命令选项

其中 **varlist** 必须与 fitted logit 或 probit 模型相同，并且必须至少包含四个变量。这前四个变量的顺序必须是因变量、自变量1、自变量2和自变量1、2之间的交互项。其他自变量可以在交互项之后加上，即从第五个位置开始。

如果 **varlist** 中的交互项（在第四个位置）是连续变量和虚拟变量的乘积，则第一个自变量$\mathit{x}_{1}$ 必须是连续变量，第二个自变量 $\mathit{x}_{2}$ 必须是虚拟变量。第二个和第三个变量的顺序与它们是连续变量还是虚拟变量无关。

`savedata（filename[，replace]）`指定计算对象的路径和文件名要保存的数据。这给了研究者进一步的选择调查。保存的数据包括以下五个变量，顺序：

1、预测概率     
2、交互效应（用常规线性方法计算）
3、交互效应（按本文建议的方法计算）
4、交互效应的标准差   
5、交互效应的 z 统计量

所有变量都有有意义的名称。例如，在我们运行 logit 模型后，五个变量分别为 _logit_phat , _logit_linear, _logit_ie, _logit_se, and _logit_z。而 probit 模型前缀则为 _probit。

`savegraph1（filename[，replace]）`和`savegraph2（filename[{，replace]）`
保存具有用户指定的名称和路径的图。inteff 命令生成两个散点图。两个图都以预测概率为 X 轴。第一个图描绘了两种交互效应（一个是用本文建议的方法计算的，和另一个用传统的线性方法计算）与预测概率的统计关系。第二个图形绘制交互效应中的 z 统计量与预测概率的统计关系。
&emsp;

##  5. 应用实例
---
### 5.1 数据处理

该论文用两个例子来说明 inteff 命令的使用。两个例子都分析了 2000 年医疗支出小组调查的数据，可用于计算具有全国代表性的卫生保健使用和支出估计数。这些数据可从医疗保健研究和质量机构网站(www.meps.ahrq.gov)得到。

因变量是此人在 2000 年是否有过办公室医生就诊。论文控制了严重的健康状况问题，定义为日常生活活动问题、工具性日常生活活动问题、视力或听力问题。为了控制在获得医疗保健方面的广泛地理差异，我们纳入了以下变量：国家的四个人口普查地区以及该人是否居住在大都市。

### 5.2 Logit 模型应用

第一个例子包括年龄和受教育年限之间的交互作用，两者都是连续变量。在这个例子中，我们拟合了一个 logit 模型。同时对标准误进行了调整，以便对 personid（pid）进行异方差处理。完整的模型还控制了种族、婚姻状况、收入、健康状况和地理区域（由全局变量 $x 汇总），但为了简洁起见，没有报告这些变量的结果。



```stata
use http://www.meps.ahrq.gov, clear

*省略数据处理后* 
 logit $y age educ ageeduc male ins_pub ins_uni $x, nolog cluster(pid)
 Logit estimates                                 Number of obs = 12365                                                    Wald chi2(23) = 9745.78
                                                 Prob >chi(23) = 0.0000
Log pseudo-likelihood = -6889.3644               Pseudo R2     = 0.1189
                        (standard errors adjusted for clustering on pid)
 
--------+------------------------------------------------------------------
opvisits| Coef.  Robust Std.Err  z  P>|z|  [95%Conf.Interval]
     age| .0419025  .0070434   5.95 0.000  .0280977  .0557073
    educ|  .127117  .0236365   5.38 0.000  .0807903  .1734437
 ageeduc|-.0013739  .0005168  -2.66 0.008 -.0023869 -.0003609
    male|-.9765431  .0348741 -28.00 0.000 -1.044895  -.908191
 ins_pub| .5829237  .1043102   5.59 0.000  .3784794   .787368
 ins_uni|-.8781526  .0541354 -16.22 0.000  -.984256 -.7720491
 (output omitted )
   _cons|-1.559739  .3379041  -4.62 0.000 -2.222019 -.8974595
--------+------------------------------------------------------------------ 
```

 在运行这个logit模型之后，我们使用相同的变量列表调用inteff命令，并保存数据。


 ```stata
inteff $y age educ ageeduc male ins_pub ins_uni $x,
> savedata(d:\data\logit_inteff,replace) savegraph1(d:\data\figure1, replace)
> savegraph2(d:\data\figure2, replace)
Logit with two continuous variables interacted
file d:\data\logit_inteff.dta saved
(file d:\data\figure1.gph saved)
(file d:\data\figure2.gph saved)

Variable  |  Obs     Mean    Std.Dev.     Min        Max
----------+------------------------------------------------------------------
_logit_ie | 12365 -.0003334  .0001145  -.0005798   .0001607
_logit_se | 12365  .0001003  .0000311   4.81e-06    .000323
_logit_z  | 12365 -3.401374  1.245229  -6.228868   7.130231
 ```

画出的图如下：
![图 3-a](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig05.png)

![图 3-b](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig06.png)

年龄和受教育年限不仅在传统水平上具有统计学意义，而且它们之间的交互效应也具有统计学意义（ z 统计量为 −2.66 ）。这意味着年龄较大、受教育年限较长的人在一年中更有可能进行办公室访问。

但是，在运行 inteff 命令之后，我们了解到平均交互效应为负（ −0.0003334 ）且变化很大。对于某些观察值，交互效应是积极的，而对于其他观察值，则是消极的（参见图 3-a 和图 3-b ）

这证明了交互效应也取决于其他协变量。在这个例子中，对于那些预测有医生就诊的概率在0.2左右的人（图 3-a 的左端），年龄和受教育年限之间的交互效应对他们一半是正的，一半是负的。如果我们看向图 3-a 的右边，对于那些预测有医生就诊的概率在0.8左右的人，他们的交互效应都是负的。

就交互作用的显著性而言，对于预测概率约为 0.2 的人群，只有少数人具有统计上显著的交互作用。另一方面，对于预测概率在 0.8 左右的人群，交互效应最为显著。同样证明了应该结合具体情况分析结果。

### 5.3 Probit 模型应用

第二个例子包括性别和保险状况之间的相互作用。在这个例子中，我们拟合了一个 probit 模型。如前所述，调整标准误差，以便对人员进行分类，完整的模型也控制了额外的变量，但为了简洁起见，没有报道这些变量

```stata
use http://www.meps.ahrq.gov, clear

*省略数据处理后* 
probit $y male ins_uni male_uni age educ ins_pub $x, nolog cluster(pid)
Probit estimates                                 Number of obs = 12365                                                    Wald chi2(23) = 9391.46
                                                 Prob >chi(23) = 0.0000
Log pseudo-likelihood = -6897.391                Pseudo R2     = 0.1179
                        (standard errors adjusted for clustering on pid)
 
--------+------------------------------------------------------------------
opvisits| Coef.  Robust Std.Err z    P>|z|   [95%Conf.Interval]
    male|-.5922717  .0240826 -24.59  0.000  -.6394726 -.5450707
 ins_uni|-.5653776  .0482592 -11.72  0.000  -.6599639 -.4707912
male_uni| .0539772  .0605362   0.89  0.373  -.0646716  .1726261
     age| .0146619  .0012311  11.91  0.000   .0122489  .0170749
    educ| .0407643  .0047575   8.57  0.000   .0314397  .0500888
 ins_pub| .3275272  .0608681   5.38  0.000   .2082279  .4468265
 (output omitted )
   _cons|-.4692864  .0889194  -5.28  0.000  -.6435652 -.2950076
--------+------------------------------------------------------------------ 
```
在运行这个probit模型之后，我们使用相同的变量列表调用inteff命令，并保存数据.


```stata
. inteff $y male ins_uni male_uni age educ ins_pub $x,
> savedata(d:\data\probit_inteff, replace)
> savegraph1(d:\data\figure3, replace) savegraph2(d:\data\figure4, replace)
Probit with two dummy variables interacted
file d:\data\probit_inteff.dta saved
(file d:\data\figure3.gph saved)
(file d:\data\figure4.gph saved)

Variable  |  Obs     Mean    Std.Dev.     Min        Max
----------+------------------------------------------------------------------
_probit_ie| 12365 -.0092839  .0294776  -.0578116  .0829161
_probit_se| 12365  .0218298  .0023465   .0046057  .0314373
_probit_z | 12365 -.5169928  1.522319  -5.561593  5.530833
```

画出的图如下：
![图 4-a](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig07.png)

![图 4-b](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/祁本章_Logit-Probit_Fig08.png)

在这个例子中，与以往不同的是，男性和未参保者之间的交互效应在统计学上并不显著。在线性模型中，我们可以从这样的结果得出结论：交互效应的影响基本上是零。然而，在非线性问题中，我们发现统计显著性范围很广。

尽管交互项上的系数缺乏统计显著性，但充分的交互效应很大，并且对许多观察结果具有统计显著性（见图 4-a , 图 4-b）。这再次表明，交互效应不等于交互项的边际效应，只看结果表可能会产生误导。
&emsp;

## 6.局限
---
 inteff 命令有两个限制。一是代码只适用于 logit 和 probit 模型，无法适用于所有非线性模型，比如 tobit 和 count 模型。此外，该命令仅适用于没有高阶项的两个变量之间的相互作用。因此要谨慎使用。
&emsp;

 ## 7.总结
---
 在非线性模型中，交互效应的计算和解释非常复杂，不等于其交互项的边际效应。相反，交互效应需要计算交叉导数或交叉差分，且交互作用效应的统计显著性差异很大，需要结合实际问题分析。

 可以用 inteff 命令计算 Logit-Probit 模型，但在使用时要注意使用条件。
&emsp;

## 参考资料  
```Markdown
- Ai, Chunrong, and Edward C. Norton.Interaction Terms in Logit and Probit Models[J].Economics Letters,2003，80(1): 123–29.  [-PDF-]
(https://www.sciencedirect.com/science/article/pii/S0165176503000326?via%3Dihub#BIB2)
- Edward C. Norton, Hua Wang, Chunrong Ai.Computing Interaction Effects and Standard Errors in Logit and Probit Models[J].Stata Journal, 2004,4(2): 154–167.  [-PDF-]
(https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400206)
-Mello, M.M, Stearns, S.C, Norton, E.C. Do Medicare HMOs still reduce health services use after controlling for selection bias? [J].Health Economics, 2002,11(4):323–340. [-PDF-]
(https://onlinelibrary.wiley.com/doi/epdf/10.1002/hec.664)
```