# levelsof 命令介绍及扩展

&emsp;

> **作者**：林浩 (中山大学)  
> **E-Mail:** <1113495193@qq.com>

目录  
`[TOC]`

## 1.简介
levelsof 命令用于获得一个变量的所有观测值中的不同值并按顺序输出

### 1.1 `levelsof` 的用法(以下“[]”中的内容是可选项）
```Stata
levelsof varname [if] [in] [, opinion] 
```
option 可用的指令

- clean               当观测值是字符串时获得不含双引号的字符
- local(macname)      将获得的值列表储存在一个命名的暂元里
- missing             将缺失值加入统计范围
- separate(separator) 把列表中的值用括号中的符号隔开(默认是空格)
- matcell(matname)    获取不同取值的频数并储存在矩阵中
- matrow(matname)     获取数值型变量的不同数值并储存在矩阵中(字符型变量)
- hexadecimal         使用16进制展示数值型变量，具体为%21x，可以保证所有情况获得观测值之间准确的相同和不同


### 1.2 `levelsof` 适用的情况

`levelsof` 可以获得变量的不同值列表，默认储存在r(levels)中，例如：
```Stata
. sysuse auto
(1978 Automobile Data)

. levelsof rep78
1 2 3 4 5

. display r(levels) 
1 2 3 4 5
```
也可以自己命名储存的暂元，例如：
```Stata
. levelsof rep78, miss local(mylevs)
1 2 3 4 5 .

. display "`mylevs'"
1 2 3 4 5 .
```
储存在暂元后可作为列表使用，例如：
```Stata
. levelsof rep78, local(levels)
1 2 3 4 5

. foreach l of local levels {
  2.     di "-> rep78 = `: label (rep78) `l''"
  3.     count if rep78 == `l'
  4. }
-> rep78 = 1
  2
-> rep78 = 2
  8
-> rep78 = 3
  30
-> rep78 = 4
  18
-> rep78 = 5
  11
```

`hexadecimal` 选项可以提高`levelsof`的精确度，适用于观测值是多位小数的情况，例如：
```Stata
. set obs 5
number of observations (_N) was 0, now 5
. generate double x = ln(_n + 1)
. levelsof x
.6931471805599453 1.09861228866811 1.386294361119891 1.6094379124341   1.791759469228055
. foreach level in 'r(levels)' {
2.   count if x == 'level'
3. }
1 
0 
0 
0 
1
```
在这个例子中，使用levelsof获得五个不同的观测值，根据上一个例子，`count`的最终结果应该是各有一个，但实际结果是出现了部分错误，这是由于正常的10进制精度不够，在这种情况下使用16进制命令`hexadecimal`可以解决，例如：
```Stata
. levelsof x, hexadecimal
+1.62e42fefa39efX-001 +1.193ea7aad030bX+000 +1.62e42fefa39efX+000
> +1.9c041f7ed8d33X+000 +1.cab0bfa2a2002X+000
. foreach level in ‘r(levels)’ {
2. display "x =" %10.0g ‘level’
3. count if x == ‘level’
4. }
x = .69314718
1
x = 1.0986123
1
x = 1.3862944
1
x = 1.6094379
1
x = 1.7917595
1
```

## 2.进阶命令
Stata中有两个进阶版的`levelsof`命令，分别是`flevelsof`和`glevelsof`  要使用`flevelsof`和`glevelsof`，可分别通过`findit ftools`和`findit gtools`选择合适的安装包

`flevelsof`与`levelsof`在使用和功能上没有本质上的差别，只是`flevelsof`能够处理更大的数据集，而且速度更快。数据量越大，`flevelsof`的优势就越明显。

相较于`flevelsof`与`levelsof`，`glevelsof`的功能更加全面。`glevelsof`可以获得多个变量的不同值在全部个体中存在的所有组合：
```Stata
glevelsof varlist [if] [in] [, opinion]
```
在这里varlist可以用[+/-] **varname** [[+/-] **varname** [...]]代替

option 可用的指令

- clean                   当观测值是字符串时获得不含双引号的字符
- local(macname)          将获得的值列表储存在一个命名的暂元里
- missing                 将缺失值加入统计范围
- separate(separator)     分割多个变量不同值组合
- nolocal                 不保存在暂元里面，与gen([prefix], [replace])配合
- colseparate(separator)  分割每个不同值组合中不同变量的值
- numfmt(format)          设置结果中数字的格式，默认是%.16g 
- unsorted                不需要按顺序排列
- gen([prefix], [replace])把结果保存在新变量中，或存入原变量
- matasave(str)           把结果保存在矩阵中
- verbose                 可以获得有用的调试信息
- benchmark               对各个步骤进行基准测试
- hashlib(str)            允许用户手动设置spookyhash.dll的路径


下面是`glevelsof`的一些例子：
```Stata
. set seed 42

. clear

. set obs 100000
obs was 0, now 100000

. gen x = "a long string appeared" + string(mod(_n, 10000))

. gen y = int(10 * runiform())

. glevelsof x
macro substitution results in line that is too long 
    The line resulting from substituting macros would be longer than allowed.  The maximum allowed length is 165,216 characters, which is calculated on thebasis of set maxvar.

    You can change that in Stata/SE and Stata/MP.  What follows is relevant only if you are using Stata/SE or Stata/MP.

    The maximum line length is defined as 16 more than the maximum macro length, which is currently 165,200 characters.  Each unit increase in set maxvarincreases the length maximums by 33.  The maximum value of set maxvar is 32,767.  Thus, the maximum line length may be set up to 1,081,527 characters if you set maxvar to its largest value.

try gen(prefix) nolocal or mata(name) nolocal; see help glevelsof for details
r(920);

. glevelsof x, gen(uniq_) nolocal
. gisid uniq_* in 1 / `r(J)'
```
在这个例子中，直接用`glevelsof`处理**x**，结果的长度超出了允许的范围，无法显示，使用`gen(prefix)`解决了这一问题
```Stata
. glevelsof x y, mata(xy) nolocal
(note: raw levels saved in xy; see mata xy.desc())

. glevelsof x, mata(x) nolocal silent
(note: raw levels saved in x; see mata x.desc())

. mata xy.desc()

    xy is a class object with group levels

        | object  |            value | description                           | 
        | ------- | ---------------- | ------------------------------------- | 
        | J       |            64958 | number of levels                      | 
        | knum    |                1 | # numeric by variables                | 
        | numx    | 64958 x 1 matrix | numeric by var levels                 | 
        | kchar   |                1 | # of string by variables              | 
        | charx   | 64958 x 1 matrix | character by var levels               | 
        | map     |     1 x 2 vector | map by vars index to numx and charx   | 
        | lens    |     1 x 2 vector | if string, > 0; if numeric, <= 0      | 
        | charpos |     1 x 1 vector | position of kth character variable    | 
        | printed | 64958 x 2 vector | formatted (printf-ed) variable levels | 

. mata x.desc()

    x is a class object with group levels

        | object  |            value | description                           | 
        | ------- | ---------------- | ------------------------------------- | 
        | J       |            10000 | number of levels                      | 
        | knum    |                0 | # numeric by variables                | 
        | numx    |          [empty] | numeric by var levels                 | 
        | kchar   |                1 | # of string by variables              | 
        | charx   | 10000 x 1 matrix | character by var levels               | 
        | map     |     1 x 1 vector | map by vars index to numx and charx   | 
        | lens    |     1 x 1 vector | if string, > 0; if numeric, <= 0      | 
        | charpos |     1 x 1 vector | position of kth character variable    | 
        | printed |          [empty] | formatted (printf-ed) variable levels |
```
下面一个例子说明`glevelsof`获得多个变量的不同值组合的方法
```Stata
. local varlist foreign rep78

. glevelsof `varlist', sep("|") colsep(", ")
`"0, 1"'|`"0, 2"'|`"0, 3"'|`"0, 4"'|`"0, 5"'|`"1, 3"'|`"1, 4"'|`"1, 5"'
```

## 3.扩展
levelsof应用于获取一个dta文件里变量的所有不同值，而当对象变为某个文件夹下的文件或某个文件夹下的所有「子文件或孙文件夹」时，levelsof无法做到这一点。要获得文件夹中文件或文件夹的信息，可以使用关于文件处理的一些命令：
### 3.1 `dir` 获得当前所在文件路径的指定文件或文件夹的信息
```Stata
dir ["][filespec]["] [, wide]
```
当文件路径中存在空格时，必须要加双引号

类似于python的正则表达式，可以用*表示“任何字符”，例如获得当前路径文件夹被的dta文件：
```Stata
. dir *.dta
 623.8k  10/16/20 21:27  000541.dta        
 344.9k  10/16/20 21:27  600048.dta
```
又例如获得指定路径的dta文件：
```Stata
. dir D:\data\*.dta
```
wide选项可以只获得文件大小和名称：
```Stata
. dir *.dta, w
 623.8k 000541.dta         344.9k 600048.dta  
```

### 3.2 `dirlist` 获得文件的详细信息并分类储存为列表
```Stata
dirlist [filespec]
```
例如：
```Stata
. dirlist *.dta
. return list
macros:
      r(nfiles) : "5"
      r(fsizes) : "638,742 353,172 466,872 1,451,472 375,152"
      r(ftimes) : "21:27 21:27 21:27 21:27 21:38"
      r(fdates) : "2020/10/16 2020/10/16 2020/10/16 "
      r(fnames) : "000541.dta 600048.dta 600519.dta "
```
可以根据return list中的名称以暂元的形式使用

### 3.3 `fs`  获得当前或指定路径里的指定文件名称并储存在暂元r(files)中，而且可以多个要求
```Stata
fs [filespec [filespec [ ... ]]]
```
例如：
```Stata
. fs append_*.dta auto_*.dta
append_0.dta  append_m.dta  append_u.dta  auto_dom.dta  auto_for.dta

. dis `r(files)'
append_0.dta append_m.dta append_u.dta auto_dom.dta auto_for.dta
```

### 3.4 `folders` 获得当前或指定路径里的指定文件夹名称并储存在暂元r(folders)
```Stata
folders [folderspec [folderspec [ ... ]]]
```
直接使用`folders`可获得当前路径的所有文件夹，指定路径可获得指定路径中的所有文件夹，例如：
```Stata
folders D:\stata15
```
`folders`后加当前路径中某些文件夹名可获得该文件夹内的所有文件夹名，例如：
```Stata
folders ado data
```
`folders`后加带*的内容可获得当前路径下符合此命名规律的文件夹名，例如：
```Stata
folders da*
```

## 4.相关命令
### 4.1 `distinct` 获得变量的不同值个数
```Stata
distinct [varlist] [if] [in] [, options]
```
options可用的指令
- missing    把缺失值加入不同值的计算之中
- abbrev(#)  限制变量名的字符数在#以内（#最小可以为5）
- joint      把指定变量的观测值作为组合，获得不同组合的种数
- minimum(#) 只显示指定变量中不同值种数等于或大于#的变量
- maximum(#) 只显示指定变量中不同值种数等于或小于#的变量

例如：
```Stata
. sysuse auto

. distinct ,abbrev(5)
------------------------------
       |     total   distinct
-------+----------------------
  make |        74         74
 price |        74         74
   mpg |        74         21
 rep78 |        69          5
 hea~m |        74          8
 trunk |        74         18
 wei~t |        74         64
 len~h |        74         47
  turn |        74         18
 dis~t |        74         31
 gea~o |        74         36
 for~n |        74          2
------------------------------

. distinct foreign rep78, joint missing
----------------------------------
           |     total   distinct
-----------+----------------------
 (jointly) |        74         10
----------------------------------

 . distinct foreign rep78, joint
----------------------------------
           |     total   distinct
-----------+----------------------
 (jointly) |        69          8
----------------------------------

. distinct rep78 foreign, missing
--------------------------------
         |     total   distinct
---------+----------------------
   rep78 |        74          6
 foreign |        74          2
--------------------------------

. distinct rep78 foreign, missing minimum(3)
--------------------------------
         |     total   distinct
---------+----------------------
   rep78 |        74          6
--------------------------------
```
最后一个变量的不同值数保存在r(ndistinct)，最后一个变量的总观察值保存在r(N)

###  4.2 `valuesof` 获得变量的全部值
```Stata
valuesof varname [if] [in] [, options]
```
options
- missing 包含缺失值
- format(%fmt) 设置数值的格式

## 5. 相关讨论与应用
- Stata List: [Concatenate contents of string variable / local macro](https://www.statalist.org/forums/forum/general-stata-discussion/general/1580213-concatenate-contents-of-string-variable-local-macro), 涉及 `levelsof`, `valuesof`, `runby` 等命令的使用。

## **参考文献**
- Alvaro Carril, [Looping over values of a variable](https://acarril.github.io/posts/looping-over-values) 
- Stata PDF 手册，[**[R]** levelsof](https://www.stata.com/manuals/plevelsof.pdf)
- [Is there a way to tell Stata to try all values of a particular variable in a foreach statement without specifying them?](https://www.stata.com/support/faqs/data-management/try-all-values-with-foreach/)
- 周宏杰，[ftools 命令组之 flevelsof 命令介绍](https://www.ershicimi.com/p/f448a958487f5e06f87fb66307a8051f)
- [gtools 主页](https://gtools.readthedocs.io/en/latest/index.html)
- Nicholas J. Cox, Gary M. Longton, 2008, Speaking Stata: Distinct Observations, Stata Journal, 8(4): 557–568. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800408)
- 连享会，[glevelsof help文档](https://gitee.com/arlionn/stata-gtools/blob/master/build/glevelsof.sthlp)