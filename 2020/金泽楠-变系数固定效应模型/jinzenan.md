
**金泽楠**（中山大学）

Email：jinznsysu@163.com

[toc]

## 1. 引言

在对面板数据进行估计时，我们通常使用个体效应模型进行估计 (individual-specific effects model)，即假定个体有相同的斜率，但可以有不同的截距项，由此采用固定效应模型（Fixed Effects Model, 简记 FE) 或随机效应模型（Random Effects Model, 简记 RE) 进行估计。在此过程中，我们会假定个体固定效应在每一期对yt的偏效应都是相同的。
但是对于长面板数据来说，由于样本容量大，除了可以让每位个体拥有自己的截距项和时间趋势项之外，每个个体还还允许拥有自己的斜率项，这种被称为长面板模型。例如，我们在考虑消费对于不同经济行业门类的拉动效应是不同的，不同地域的人对于某种物品的需求也是各不相同，应该有各自的斜率。

## 2. 模型设定和估计方法

### 2.1 可变系数模型设定及假定

下面我们采用 Wooldridge (2010) 的例子说明变系数模型的设定和估计。一般化的变系数模型如公式 (1) 所示，我们考虑随时间变动的解释变量Wit的系数不再是常数，而是不可观测的、不随时间变动的变量ai，二者的的交乘项，即Witai被纳入到模型 (1) 中。

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0131/230341_6d5b9b28_8432540.png "1.png")

考虑一个评估项目培训效果的案例。假设progit为是否参加培训项目的变量，yit是结果变量，我们得到模型 (2)：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0131/230355_a1b6f50c_8432540.png "2.png")

在普通的固定效应模型中，我们会在估计β之后，估计出部分平均效应。 Polachek 和 Kim (1994) 提出，不同工作经历的人的培训回报可能是因人而异的；Lemieux (1998) 估计的固定效应模型中，不可观测效应在工会和非工会部门中存在异质性。由于长面板数据样本容量更大，可以允许我们设定变系数模型。在模型 (2) 中，培训项目的效果取决于不可观测效应 。尽管我们要估计β， 但我们也对培训项目的平均效果感兴趣，这一平均效果用α表示。

此外该模型还应满足三个假定：1、外生性假定 2、一致性假定 3、无序列条件相关
以上假定均满足时，以保证我们得到渐进有效的估计量

### 2.2 变系数模型的固定效应的Hausman检验的设定

在估计时存在随机效应与固定效应两种情况，正确的识别两种效应的统计显著差别，并解决统计显著偏误非常重要。如果我们错误地把随机斜率当成固定效应，使用了固定效应模型进行估计，结果可能存在偏误。这种错误可能来源于两方面：一是我们忽视了斜率的异质性，二是时间跨度不够，无法支持变系数模型。对于这一问题，我们可以使用 hausman 模型进行检验。

## 3. Hausman 检验
在面板模型的估计中，我们经常使用 Hausman 检验去判断应该使用固定效应模型还是随机效应模型。当模型存在异方差时，为了使估计结果更有效率和更加稳健，通常使用 (bootstrap)法和（art）法进行稳健的 Hausman 检验。
### 3.1  用 xtbsht 命令进行检验
xtbsht 即是 Hausman 检验的 bootstrap 版本，使用了配对聚集样本进行检验 (Cameron et al., 2008, Cameron and Miller, 2015)。同时，这一命令也可以通过比较 FEIS 模型与 FE 模型以及 RE 模型的系数是否存在系统性差异，帮助我们选择合适的模型。

xtbsht 命令的语法如下：
```
xtbsht Model A Model B [,] [keep(varlist)] [reps({num})] [seed({num})]
```
其中，reps为设定自主抽样的次数，seed（num）为设定用于bootstrap的种子

### 3.2  用 xtart 命令进行检验

xtart 是采用 Arellano (1993) 的方法对 FE 与 RE 模型进行检验，是 Hausman test 的 ART (Artificial Regression Test) 版本的典型应用，这个命令也是要在 FEIS 的估计之后使用。在使用这个命令时，要用 estimates store 把估计的结果存成 Model A。 如果没有设定 Model A, xtart 会使用 xtfeis 估计出来的最新的结果作为估计结果。

该命令的语法如下：
```
 xtart [Model A] [,] [addvars(varlist)] ///
  [keepvars(varlist)] [fe] [predicted(stub1 stub2)]
```
其中，fe为固定效应与随机效应之间的系数，re为比较FEIS与RE的系数，addvars为协变量
keep(varlist):ART 只能被 Model A 和 ModelB 的公共系数子集所设定

## 4. Stata 范例

我们将使用数据集 nlswork 演示如何对变系数模型进行估计。nlswork 数据集是美国国家纵贯抽样调查 (NLS , National Longitudinal Survey) 项目对女性工资收入进行的调查,调查时间跨度为 1968 年到 1988 年。主要变量包括经过 GNP 调整的工资自然对数 (ln_wage)、是否在婚 (msp, 1 代表在婚且随配偶居住，0 代表其他情况)、现有岗位任职时间 (tenure) 以及总工作年数 (ttl_exp)。
```
webuse "nlswork.dta", clear
```
首先我们估计面板稳健标准误的普通固定效应模型，即我们不设定 slope()选项。
```
· xtfeis ln_wage msp tenure ttl_exp year, cluster(idcode)

Fixed-effects regression with individual-specific slopes (FEIS)

Group variable: idcode                 Number of obs      =     27533
                                       Number of groups   =      4146

R-sq:  within  = 0.1443                Obs per group: min =         2
                                                      avg =       6.6
                                                      max =        15

Standard errors adjusted for clusters in idcode
----------------------------------------------------------------------
         |           cluster
 ln_wage |  Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
---------+------------------------------------------------------------
     msp |  0.001      0.007     0.21   0.833       -0.012       0.015
  tenure |  0.012      0.001     7.91   0.000        0.009       0.015
 ttl_exp |  0.029      0.002    12.78   0.000        0.025       0.034
    year | -0.003      0.001    -2.39   0.017       -0.006      -0.001
----------------------------------------------------------------------
```
进一步地，我们考虑不同女性的总工作年数 (ttl_exp) 对女性工资的影响是不同的，即使用女性总工作年数变系数固定效应模型进行估计，得到如下结果：
```
. xtfeis ln_wage msp tenure year, slope(ttl_exp) cluster(idcode)

Fixed-effects regression with individual-specific slopes (FEIS)

Group variable: idcode                     Number of obs  =     26521
                                       Number of groups   =      3640
    
R-sq:  within  = 0.0115                Obs per group: min =         3
                                                      avg =       7.3
                                                      max =        15


Standard errors adjusted for clusters in idcode
---------------------------------------------------------------------
         |            cluster
 ln_wage |   Coef.   Std. Err.      t    P>|t|   [95% Conf. Interval]
---------+-----------------------------------------------------------
     msp |   0.009      0.008     1.24   0.217     -0.005       0.024
  tenure |   0.016      0.002     8.69   0.000      0.012       0.020
    year |  -0.005      0.002    -2.92   0.004     -0.009      -0.002
---------------------------------------------------------------------
```
此时我们发现，ttl_exp不再具有唯一的系数，由变系数模型估计出来的也变成了变系数模型。我们因此也可以得到每个变量个体具体的估计值

## 5. 参考文献
•	Ludwig, V. (2019). XTFEIS: Stata module to estimate linear Fixed-Effects model with Individual-specific
Slopes (FEIS). https://EconPapers.repec.org/RePEc:boc:bocode:s458045

•	Wooldridge, J. (2010). Econometrics of Cross Section and Panel Data, Cambridge: MIT Press, 2nd edition, pp. 374-381. -PDF-)

•	陈强. 高级计量经济学及 Stata 应用[M]. 高等教育出版社, 2014.