&emsp;

> **作者**：许曼曼(中山大学)  
> **E-Mail**：<451689616@qq.com>

「**Source** [Rios-Avila, F., 2020, Smooth varying-coefficient models in stata, The Stata Journal, 20 (3): 647-679.](https://sci-hub.do/10.1177/1536867X20953574)」

**目录**
[toc]

## 1. 背景：非参数估计 (Nonparametric Regression) 
### 1.1 非参数估计模型介绍
$$
y_{i}=g\left(W_{i}\right)+e_{i} \quad E\left(\mathrm{e}_{i} \mid W_{i}\right)=0
$$
非参数估计模型如上式所示，其中 $\mathrm{g}(\cdot)$ 的估计方法主要有两种：基于核函数的方法与基于转换函数的方法。

(1) 核函数方法：局部核权重估计 (Stata: npregress kernel) 
$$
\hat{\mathrm{g}}(\mathrm{w})=\mathrm{E}\left(\mathrm{y}_{\mathrm{i}} \mid \mathrm{W}_{\mathrm{i}}=\mathrm{w}\right)=\frac{\sum \mathrm{y}_{\mathrm{i}} K\left(W_{i}, w, h\right)}{\sum K\left(W_{i}, w, h\right)}
$$
其中$h=\left\{h_{\omega 1}, h_{\omega 2}, \ldots, h_{\omega k}\right\}$是带宽向量，$K(\cdot)$是核函数：
$$
K\left(W_{i}, w, h\right)=\prod_{j=1}^{k} K_{j}\left(W_{i j}, \omega_{j}, h_{c j}\right)=\prod_{j=1}^{k} k_{j}\left(\frac{W_{i j}-\omega_{j}}{h_{c j}}\right)
$$

核函数给予离点 w 近的观察值更高的权重，而带宽向量 h 决定使用多少信息用于条件均值的估计。随着 k 的增加和 h 趋近于 0 ，用于估计的有效观察值的数量快速减少。

(2) 以原变量的转换形式 $\mathrm{S}(\cdot)$ 与交叉项 $\mathrm{I}(\cdot)$ 作为解释变量 (Stata: npregress series) 
$$
\mathrm{y}_{i}=S\left(W_{i}\right)^{\prime} \beta_{\omega}+I\left\{S\left(W_{i}\right)\right\}^{\prime} \beta_{I_{W}}+e_{i}
$$
其中转换函数通常是多项式或样条。$\mathrm{S}(\cdot)$ 和 $\mathrm{I}(\cdot)$ 的维度所代表的调节参数决定了估计量的准确性。但随着解释变量和维度的增加，待估系数的数量呈指数方式增加，使得模型自由度快速下降，偏误大。

### 1.2 非参数估计模型的优缺点
非参数估计虽然灵活性较强，不需要设定过多假设，但缺点也较为明显：
- “维度的诅咒”：随着解释变量与维度的增加，待估系数的数量快速增加，要求的数据集越来越大；
- 模型估计与检验的方法计算量很大，不适用于大样本数据；
- 适用于大样本的与非参数模型相关的 Stata 命令较少。

因此，本文主要根据 Rios-Avila 的文章介绍一种半参数估计模型：系数平滑可变模型 (SVCM) 及其 Stata 命令。

## 2. SVCM 建模与估计方法
### 2.1 模型介绍
$$
\mathrm{y}_{i}=X_{i}^{\prime} \beta\left(Z_{i}\right)+e_{i} \quad E\left(\mathrm{e}_{i} \mid X_{i}, Z_{i}\right)=0
$$
SVCM 将解释变量 W 分为两类 W=(X,Z)，其中 X 与 Y 的线性关系是基于 Z 的非线性函数，即 Y 是 Z 条件下 X 的线性函数，系数 $\beta\left(Z_{i}\right)$ 是 Z 的非线性函数。相比非参数估计，SVCM 通过设定 X 对 Y 的线性影响是基于 Z 的条件，减少了“维度诅咒”的影响。

与非参数模型的估计思路相似，$\beta\left(Z_{i}\right)$ 的估计方法有：使用关于 Z 的局部多项式核函数、样条函数等。本文介绍的模型是只有一个平滑变量的 SVCM ，估计方法为核函数估计。

### 2.2 SVCM-模型估计方法：局部核权重估计量 (Local Kernel Estimator) 
$$
\mathrm{y}_{i}=X_{i}^{\prime} \beta\left(Z_{i}\right)+e_{i} \quad E\left(\mathrm{e}_{i} \mid X_{i}, Z_{i}\right)=0
\quad (1)
$$
在本文的 SVCM 中 Z 只包含一个变量，X 是包含常数项的其他解释变量的集合。由于平滑变量只有一个，带宽向量 h 是一个常数。根据核函数的方法，得到的局部常数估计量 (LC) 为：
$$
\hat{\beta}(z, h)=\left\{\sum X_{i} X_{i}^{\prime} k\left(\frac{Z_{i}-z}{h}\right)\right\}^{-1}\left\{\sum X_{i} y_{i} k\left(\frac{Z_{i}-z}{h}\right)\right\}
$$
矩阵形式：
$$
\hat{\beta}(z, h)=\left\{X^{\prime} \kappa_{h}(z) X\right\}^{-1}\left\{X^{\prime} \kappa_{h}(z) Y\right\}
\quad (2)
$$
考虑到接近 Z 边界时 LC 估计量可能有较大的偏差，还可以使用局部线性估计量 (LL) 替代，模型由 (1) 变为下式 (3) ：
$$
\beta\left(Z_{i}\right) \cong \beta(z)+\left(Z_{i}-z\right) \frac{\partial \beta(z)}{\partial z}
$$
$$
\mathrm{y}_{i} \cong X_{i}^{\prime}\left\{\beta(z)+\left(Z_{i}-z\right) \frac{\partial \beta(z)}{\partial z}\right\}+e_{i}
$$
$$
\mathrm{y}_{i} \cong X_{i}^{\prime} \beta(z)+X_{i}^{\prime}\left(Z_{i}-z\right) \frac{\partial \beta(z)}{\partial z}+e_{i}
\quad (3)
$$
式 (3) 表示近似的 $\mathrm{y}_{i}$ 可以通过关于 $\beta(z)$ 的线性扩展式求得，且 $Z_{i}$ 越接近 $z_{i}$ ，近似值越准确。

定义 $\chi_{i}=\left\{X_{i}\left(Z_{i}-z\right) \otimes X_{i}\right\}$ 为 $\chi$ 的第 i 行，$\left(Z_{i}-z\right) \otimes X_{i}$ 表示 $X_{i}$ 中的每个变量都乘以辅助变量 $\left(Z_{i}-z\right)$ 。那么类似于式 (2)，$\beta(z)$ 和 $\frac{\partial \beta(z)}{\partial z}$ 的局部线性估计量 (LL) 可表示为：
$$
\left[\begin{array}{c}
\hat{\beta}(z, h) \\
\frac{\partial \hat{\beta}(z, h)}{\partial z}
\end{array}\right]=\left\{\chi^{\prime} \kappa_{h}(z) \chi\right\}^{-1}\left\{\chi^{\prime} \kappa_{h}(z) Y\right\}
\quad (4)
$$

## 3. SVCM 模型选择
### 3.1 模型选择方法：留一交叉验证 (Leave-one-out Cross Validation) 
SVCM 估计中最重要的参数是带宽 h 。带宽越大，说明更多的观察值被使用，则估计量的方差越小，但同时模型自由度下降，估计量的偏误也越大；另一方面，小带宽下估计量的偏误小，但方差大。标准的 OLS 回归就相当于选择了一个无穷大的带宽 h 以保证估计量的方差最小但有偏；而简单地限定平滑变量为某一特定值的样本下的OLS回归则相当于选择一个 h=0 的带宽，方差大而偏误小。因此，通过交叉验证选择最优的带宽是必要的。

根据 LOO-CV ，最优带宽在 $C V(\mathrm{~h})$ 取值最小时取得：
$$
\begin{array}{l}
h^{*}&=\min _{h} C V(h)=\min _{h} \sum_{i=1}^{N} \omega\left(z_{i}\right)\left\{y_{i}-X_{i}^{\prime} \hat{\beta}_{-i}\left(z_{i}, h\right)\right\}^{2} \\
&=\min _{h} \sum_{i=1}^{N} \omega\left(z_{i}\right)\left\{y_{i}-\hat{y}_{-i}(h)\right\}^{2}
\end{array}
$$
其中 $\hat{\beta}_{-i}\left(z_{i}, h\right)$ 是给定带宽 h ，除去第 i 个观察值后得到的 LOO-CV 系数估计量 $\hat{\beta}\left(z_{i}, h\right)$ ，$\hat{\mathrm{y}}_{-\mathrm{i}}(h)$ 是对应的拟合值，$\omega\left(z_{i}\right)$ 是用以调整 Z 分布不均问题的权重。

从上式来看，求出最优带宽似乎需要很大的计算量 (N组系数)，但实际上估计 $C V(h)$ 不需要那么多的估计方程，原因主要有两点：

一方面，平滑变量虽然是连续变量，但在数据集中的记录形式往往是离散型的，因而待估系数 $\hat{\beta}\left(z_{i}, h\right)$ 比样本数少很多；

另一方面，估计 $C V(h)$ 不一定需要估出 $\hat{\beta}_{-i}\left(z_{i}, h\right)$ ，只需估 LOO 残差 $\widetilde{\mathrm{e}}_{i}(h)=\mathrm{y}_{i}-\hat{y}_{-i}(h)$ ，而该 LOO残差可以由 SVCM 残差 $\hat{\mathrm{e}}_{\mathrm{i}}(h)=y_{i}-X_{i}^{\prime} \hat{\beta}\left(z_{i}, h\right)$ 通过 leverage statistic [$\operatorname{lev}_{i}\left(z_{i}, h\right)$] 得出：
$$
\widetilde{\mathrm{e}}_{i}(h)=\mathrm{y}_{i}-\hat{y}_{-i}(h)=\frac{\mathrm{y}_{i}-\hat{y}_{i}(h)}{1-\operatorname{lev}_{i}\left(z_{i}, h\right)}=\frac{\hat{e}_{i}(h)}{1-\operatorname{lev}_{i}\left(z_{i}, h\right)}
\quad (5)
$$
其中 $\operatorname{lev}_{i}\left(z_{i}, h\right)$ 是局部估计矩阵 $H_{z i}(h)=\chi\left\{\chi^{\prime} \kappa_{h}\left(z_{i}\right) \chi\right\}^{-1} \chi^{\prime} \kappa_{h}\left(z_{i}\right)$ 的第 i 个对角元素：
$$
\operatorname{lev}_{i}\left(z_{i}, h\right)=\chi_{i}\left\{\chi^{\prime} \kappa_{h}\left(z_{i}\right) \chi\right\}^{-1} \chi_{i}^{\prime} \times k(0)
$$

因此，定义 $\mathrm{z}_{p r}=\left(z_{1}, z_{2}, \ldots, z_{J}\right)$ 为包含 Z 所有不同值且从小到大排序的向量，$C V(h)$ 可以重写为如下式子 (估计方程数量从 N 降为 J )： 
$$
C V(\mathrm{~h})=\sum_{j=1}^{J} \sum_{i| Z_{i}=z_{j}} \omega\left(z_{j}\right)\left(\frac{y_{i}-X_{i}^{\prime} \hat{\beta}\left(z_{j}, h\right)}{1-\operatorname{lev}_{i}\left(z_{j}, h\right)}\right)^{2}=\sum_{j=1}^{J} \sum_{i| Z_{i}=z_{j}} \omega\left(z_{j}\right) \widetilde{e}_{i}(h)^{2}
\quad (6)
$$
若使用 (6) 计算过程还是较慢，可以使用 block/binned LL regression 获得 $C V(h)$ 的近似值，那么估计方程数量将更少，从 J 降为 P 。

### 3.2 Stata 实操-模型选择：`vc_bw`/`vc_bwalt`
#### 3.2.1 命令介绍
`vc_bw` 和 `vc_bwalt` 的语法结构相同：
```stata
vc_bw[alt] depvar [indervars] [if] [in], vcoeff(svar) ///
    [knots(#k) km(#km) bwi(#) trimsample(trimvar) kernel(kernel) plot]
```
- `depar`：因变量 Y ；
- `indepvar`：所有对 Y 有条件线性关系的解释变量 X；
- `vcoeff(svar)`： svar 为平滑变量 Z ，vcoeff() 明确用于平滑可变系数估计的变量；
- `knots(#k)` 与 `km(#km)`：可选项，表示以最小化 $C V(h)$ 近似值的方式得出最优带宽。使用 knot(-2) 表示对平滑变量所有不同值进行交叉验证估计；
- `bwi(#)`：设定初始的带宽值 # 以加快寻找最优带宽的过程；
- `trimsample(trimvar)`：给定变量名称，生成虚拟变量以识别出用于交叉验证的子样本；
- `kernel(kernel)`：设置核函数以得到估计所需的权重，默认为高斯函数；
- `plot`: 画出带宽 h 和与之相对应的交叉验证值 $C V(h)$ 的关系图。

#### 3.2.2 命令使用
以某地的月度酒驾传票数量数据集 (dui.dta) 为例，这是一个仅包含五个变量的虚拟数据集。假设认为酒驾传票数量 (**citations**) 受到该地是否处在大学区 (**college**)、酒精饮料是否为税收商品 (**taxes**)、城市水平等变量的影响 (**csize**)，想研究不同水平下的罚款 (**fines**) 对于该线性关系的影响，则可以建立以罚款为平滑变量的 SVCM 。
```
. webuse dui.dta
(Fictional data on monthly drunk driving citations)

. sum

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
       taxes |        500        .704    .4569481          0          1
       fines |        500      9.8952    .7818949        7.4         12
       csize |        500       2.006    .8434375          1          3
     college |        500        .248    .4322843          0          1
   citations |        500      22.018    9.802748          4         80
```

以 **citations** 为因变量，**college**、 **taxes**、 **csize** 为解释变量，**fines** 为平滑变量建立系数平滑可变模型，首先需要通过交叉验证获得最优带宽。
```
. vc_bw citations taxes college i.csize, vcoeff(fines)
Kernel: gaussian
Iteration: 0 BW:   0.5539761 CV:   3.129985
Iteration: 1 BW:   0.6870520 CV:   3.120199
Iteration: 2 BW:   0.7343729 CV:   3.119504
Iteration: 3 BW:   0.7397456 CV:   3.119497
Iteration: 4 BW:   0.7397999 CV:   3.119497
Bandwidth stored in global $opbw_
Kernel function stored in global $kernel_
VC variable name stored in global $vcoeff_
```
命令执行结束后，会列示出交叉验证迭代过程中的几项带宽 h 和交叉验证值 $C V(h)$ ，从中可以看出，最优带宽是 0.7398 ；而且程序会将最优带宽、核函数、平滑变量名称等信息分别储存在宏中：`$opbw_`, `$kernel_`, `$vcoeff_`。

## 4. SVCM 模型估计与统计推断
### 4.1 模型估计：局部核权重估计-LL估计量
$$
\mathrm{y}_{i} \cong X_{i}^{\prime} \beta(z)+X_{i}^{\prime}\left(Z_{i}-z\right) \frac{\partial \beta(z)}{\partial z}+e_{i}
\quad (3)
$$
$$
\left[\begin{array}{c}
\hat{\beta}(z, h) \\
\frac{\partial \hat{\beta}(z, h)}{\partial z}
\end{array}\right]=\left\{\chi^{\prime} \kappa_{h}(z) \chi\right\}^{-1}\left\{\chi^{\prime} \kappa_{h}(z) Y\right\}
\quad (4)
$$
获得最优带宽 $\mathrm{h}^{*}$ 后，要估计模型 (3) ，将 $\mathrm{h}^{*}$ 与平滑变量值 z 带入核函数得到权重，再根据式 (4) 即可得到系数的 LL 估计量。

### 4.2 统计推断：方差-协方差矩阵估计
#### 4.2.1 渐进估计
$$
\begin{aligned}
\widehat{\Sigma}_{\beta}^{a s}\left(z, h^{*}\right) &=\widehat{\operatorname{Var}}_{a s}\left[\begin{array}{c}
\widehat{\beta}\left(z, h^{*}\right) \\
\frac{\partial \widehat{\beta}\left(z, h^{*}\right)}{\partial z}
\end{array}\right] \\
&=q_{c}\left\{\mathcal{X}^{\prime} \mathcal{K}_{h}(z) \mathcal{X}^{\prime}\right\}^{-1}\left\{\mathcal{X}^{\prime} \mathcal{K}_{h}(z) \mathbf{D} \mathcal{K}_{h}(z) \mathcal{X}\right\}\left\{\mathcal{X}^{\prime} \mathcal{K}_{h}(z) \mathcal{X}^{\prime}\right\}^{-1}
\end{aligned}
\quad (7)
$$
其中 $\mathbf{D}$ 是第 i 个元素为 $\hat{e}_{i}\left(h^{*}\right)^{2}$ 的对角矩阵，文献对核回归中的 $\mathrm{q}_{c}$ 尚无较多表述，通常假定 $\mathrm{q}_{c}=1$ 或 $\mathrm{q}_{c}=\frac{N}{\{N-\operatorname{dim}(\chi)\}}$ ，其中 $\operatorname{dim}(\chi)$ 是模型中所有需要拟合的系数数量。

与 HC2(heteroskedasticity-consistent 2) 和 (HC3) 等价的方式：用 $\frac{\hat{\mathrm{e}}_{i}\left(h^{*}\right)^{2}}{1-\operatorname{lev}_{i}\left(z, h^{*}\right)}$ 或 $\left[\frac{\hat{\mathrm{e}}_{i}\left(h^{*}\right)}{1-\operatorname{lev}_{i}\left(z, h^{*}\right)}\right]^{2}$ 代替矩阵 $\mathbf{D}$ 第 i 个元素中的 $\hat{e}_{i}\left(h^{*}\right)^{2}$ ；$\mathrm{q}_{c}=1$ 。

#### 4.2.2 Paired Bootstrap
$$
\begin{aligned}
\widehat{\Sigma}_{\beta}^{\text {boot }}\left(z, h^{*}\right) &=\widehat{\operatorname{Var}}\left[\begin{array}{l}
\widehat{\beta}\left(z, h^{*}\right) \\
\frac{\partial \widehat{\beta}\left(z, h^{*}\right)}{\partial z}
\end{array}\right]\\
&=\frac{1}{M-1}\left[\begin{array}{l}
\widehat{\beta}^{S}\left(z, h^{*}\right)-E\left\{\widehat{\beta}^{S}\left(z, h^{*}\right)\right\} \\
\frac{\partial \widehat{\beta}^{S}\left(z, h^{*}\right)}{\partial z}-E\left(\frac{\partial \widehat{\beta}^{S}\left(z, h^{*}\right)}{\partial z}\right)
\end{array}\right]^{\prime} &\left[\begin{array}{l}
\widehat{\beta}^{S}\left(z, h^{*}\right)-E\left\{\widehat{\beta}^{S}\left(z, h^{*}\right)\right\} \\
\frac{\partial \widehat{\beta}^{S}\left(z, h^{*}\right)}{\partial z}-E\left(\frac{\partial \widehat{\beta}^{S}\left(z, h^{*}\right)}{\partial z}\right)
\end{array}\right]
\end{aligned}
$$
其中 $\widehat{\beta}^{S}\left(z, h^{*}\right)$ 和 $\partial \widehat{\beta}^{S}\left(z, h^{*}\right) / \partial z$ 包含每次 bootstrap 样本 $\mathrm{m}=1, \ldots, M$ 的系数 $\widehat{\beta}_{m}^{S}\left(z, h^{*}\right)$ 和 $\partial \widehat{\beta}_{m}^{S}\left(z, h^{*}\right) / \partial z$ 。

### 4.3 Stata 实操-模型估计与统计推断：`vc_reg`/`vc_preg`/`vc_bsreg`
#### 4.3.1 命令介绍
`vc_reg` 和 `vc_preg` 均对应渐进的方差矩阵，其中 `vc_reg` 使用的误差为局部残差 $\hat{\hat{e}}_{i}\left(h^{*}, z\right)=y_{i}-X_{i}^{\prime} \hat{\beta}\left(z, h^{*}\right)-X_{i}^{\prime}\left(Z_{i}-z\right) \frac{\partial \hat{\beta}\left(z, h^{*}\right)}{\partial z}$ ，得到稳健标准误 (Robust)，而 `vc_preg` 使用的误差为全样本残差 $\hat{\mathrm{e}}_{i}\left(h^{*}\right)=y_{i}-X_{i}^{\prime} \hat{\beta}\left(z_{i}, h^{*}\right)$ ，得到 F 稳健标准误 (F Robust)；`vc_bsreg` 则对应配对bootstrap标准误和置信区间。

当估计单个等式时，`vc_reg`、`vc_preg` 和 `vc_bsreg` 会生成数据集 (_delta_)，包含辅助变量 ($Z_{i}-z$) 和标准化核权重 (_kwgt_)。

`vc_reg`、`vc_preg` 和 `vc_bsreg` 的语法结构相同：
```
vc_[bs|p]reg depvar [indepvar] [if] [in] [, vcoeff(svar) ///
    bw(#) kernel(kernel) cluster(varname) robust hc2 hc3 k(#) klist(numlist)]
```
- `kernel()` 与 `bw()`：设定核函数与带宽，否则默认使用交叉验证后储存在内存中的 $opbw_, $kernel_, $vcoeff_；
- `k(#)` 或 `klist(numlist)`：k(#) 表示选用平滑变量第 # 百分位的值；klist(numlist) 表示选用多个平滑值分别进行 LL 回归，此时系数与方差-协方差矩阵的估计结果分别储存在 e(b#) 和 e(V#) 中；
- `hc2` 或 `hc3`：生成 hc2、hc3 标准误，否则命令 vc_reg 和 vc_preg 分别默认生成稳健标准误、F稳健标准误。
- `cluster(varname)`：生成聚类标准误，不能与 hc2、hc3 一起使用；
- `reps(#)`：对于命令 vc_bsreg，可以使用 reps(#) 设定迭代次数，否则默认为 50 ；
- `pci(#)`：对于命令 vc_bsreg，程序执行后会自动储存 95% 的置信区间，也可以通过 pci(#) 设定置信区间百分位。

#### 4.3.2 命令使用
对平滑变量 **fines** 第 10，50，90 百分位的值 (z=9,10,11) 进行 SVCM 建模，以探究变量 **taxes**，**college**，**csize** 对于 **citations** 的条件效应。
```
regress citations taxes college i.csize fines, robust     //OLS回归

vc_reg citations taxes college i.csize, klist(9)          //SVCM-平滑值9-Robust
vc_reg citations taxes college i.csize, klist(10)         //SVCM-平滑值10-Robust
vc_reg citations taxes college i.csize, klist(11)         //SVCM-平滑值11-Robust

vc_preg citations taxes college i.csize, klist(9)         //SVCM-平滑值9-F Robust
vc_preg citations taxes college i.csize, klist(10)        //SVCM-平滑值10-F Robust
vc_preg citations taxes college i.csize, klist(11)        //SVCM-平滑值11-F Robust

vc_bsreg citations taxes college i.csize, klist(9) seed(1) reps(100)     //SVCM-平滑值9-Bootstrap
vc_bsreg citations taxes college i.csize, klist(10) seed(1) reps(100)    //SVCM-平滑值10-Bootstrap
vc_bsreg citations taxes college i.csize, klist(11) seed(1) reps(100)    //SVCM-平滑值11-Bootstrap
```
对结果进行整理后可以得到如下的表格 (表格1)：
```
----------------------------------------------------------------------------
                    "Number of monthly drunk driving citations"	
                 OLS        SVCM            SVCM            SVCM			
                 (1)         (2)            (3)             (4)	
                           fines=9         fines=10        fines=11
                 β(z)   β(z)    一阶导   β(z)   一阶导   β(z)    一阶导
----------------------------------------------------------------------------
taxes           -4.494  -6.377  3.008   -3.959  1.093   -3.843  -0.0505
Robust se       (0.582) (1.147) (1.373) (0.496) (0.823) (0.736) (0.788)
F Robust se     (0.582) (1.059) (1.210) (0.493) (0.787) (0.711) (0.751)
Bootstrap se    (0.638) (1.322) (1.525) (0.498) (0.967) (0.812) (0.833)
college         5.828   9.871   -4.578  5.305   -3.191  3.797   -1.024
Robust se       (0.588) (1.113) (1.318) (0.516) (0.896) (0.888) (0.963)
F Robust se     (0.588) (1.021) (1.164) (0.513) (0.836) (0.860) (0.915)
Bootstrap se    (0.634) (1.201) (1.381) (0.470) (0.972) (0.884) (0.926)
csize:medium    5.492   6.734   -1.299  5.284   -2.332  3.051   -2.196
Robust se       (0.532) (0.973) (1.125) (0.535) (0.785) (0.782) (0.843)
F Robust se     (0.532) (0.936) (1.069) (0.538) (0.786) (0.772) (0.831)
Bootstrap se    (0.547) (0.932) (1.265) (0.588) (0.760) (0.833) (0.958)
csize:large     11.24   14.99   -4.863  10.60   -3.779  7.784   -2.691
Robust se       (0.571) (1.146) (1.373) (0.510) (0.852) (0.750) (0.764)
F Robust se     (0.571) (1.071) (1.233) (0.509) (0.822) (0.741) (0.751)
Bootstrap se    (0.610) (1.095) (1.323) (0.553) (0.809) (0.749) (0.812)
fines           -7.690          -8.256          -4.906          -3.673
Robust se       (0.384)	        (1.327)	        (0.782)         (0.816)
F Robust se     (0.384)         (1.211)         (0.792)         (0.804)
Bootstrap se    (0.405)         (1.473)         (0.787)         (0.810)
Constant        94.22	23.96           16.80           12.93	
Robust se       (3.949) (1.168)         (0.474)         (0.746)	
F Robust se     (3.949) (1.099)         (0.478)         (0.737)	
Bootstrap se    (4.117) (1.255)         (0.501)         (0.819)	
----------------------------------------------------------------------------
Observations    500         243.19          341.64          203.37	
----------------------------------------------------------------------------
```


## 5. SVCM 模型后估计：`vc_predict`/`vc_test`
### 5.1 命令介绍：`vc_predict`-模型基本统计数据与假设检验
#### 5.1.1 语法结构
```
vc_predict depvar [indepvars] [, vcoeff(svar) ///
    yhat(newvar) res(newvar) looe(newvar) lvrg(newvar) ///
    stest bw(#) kernel(kernel) knots(#) km(#)]
```
- `yhat(newvar)`：生成拟合值；
- `res(newvar)`：生产残差；
- `looe(newvar)`：生成 LOO 残差（leave-one-out error）；
- `lvrg(newvar)`：生成 leverage statistic；
- `stest`：实行近似 F 检验，并报告检验结果；
- `kernel()` 与 `bw()`：设定核函数与带宽，否则默认使用交叉验证后储存在内存中的宏；
- `knots(#)` 与 `km(#)`：以最小化 CV(h) 近似值的方式求最优带宽，可加快运行速度。

#### 5.1.2 模型基本统计数据
命令 `vc_predict` 除了可以获取拟合值等数据，还会自动报告 SCVM 的一些基本统计数据，接下来将逐一介绍。

(1) Log of MSLOOE(Log mean squared leave-one-out errors)
已知 LOO 残差由式（5）可得，当没有输入 `knots()` 和 `km()` 选项时，命令 `vc_predict` 报告准确的 MSLOOE 对数值：
$$
\log \mathrm{MSLOOE}=\log \left\{\sum_{j=1}^{J} \sum_{i \mid Z_{i}=z_{j}}\left(\frac{y_{i}-\widehat{y}_{i}(h)}{1-\operatorname{lev}_{i}\left(z_{j}, h\right)}\right)^{2}\right\}
$$
当输入 `knots()` 和 `km()` 选项，命令将报告近似的 MSLOOE 对数值：
$$
\log \mathrm{MSLOOE}=\log \left\{\sum_{p=1}^{P} \sum_{z_{j} \in \Gamma_{p}} \sum_{i \mid Z_{i}=z_{j}}\left(\frac{y_{i}-\widehat{\hat{y}}_{i}(h)}{1-\widehat{\operatorname{lev}}_{i}\left(\bar{z}_{p}, h\right)}\right)^{2}\right\}
$$

(2) 拟合优度 (Goodness-of-fit)
`vc_predict` 报告的拟合优度的定义方式与标准线性模型的类似，其中 $R_{1}^{2}$ 是常规拟合优度，由于可能为负值，命令还报告了 Henderson 和 Parmeter (2015) 所用的拟合优度 $R_{2}^{2}$ 。
$$
R_{1}^{2}=1-\frac{S S R}{S S T}=1-\frac{\sum\left[\mathrm{y}_{i}-\hat{y}_{i}(h)\right]^{2}}{\sum\left(y_{i}-\bar{y}\right)^{2}}
$$
$$
R_{2}^{2}=\frac{\left\{\sum\left(y_{i}-\bar{y}\right)\left[\hat{y}_{i}(h)-\bar{y}\right]\right\}^{2}}{\sum\left(y_{i}-\bar{y}\right)^{2} \sum\left[\hat{y}_{i}(h)-\bar{y}\right]^{2}}
$$
另外，当使用选项 `knots()` 和 `km()` 以加快运行速度时，式中的 $\hat{\mathrm{y}}_{i}(h)$ 被替换为 $\hat{\hat{y}}_{i}(h)$ 。

(3) 模型自由度与残差自由度
定义 $N \times N$ 的投影矩阵 $S$ 使得 $\hat{Y}=S Y$ ，根据 Hastie 和 Tibshirani (1990)，模型的自由度可以由下式两种方式估得：
$$
d f_{1}=\operatorname{tr}(S)
\quad (8)
$$
$$
d f_{2}=\operatorname{tr}\left(2 S-S S^{\prime}\right)
\quad (9)
$$
在线性回归中，$S=H=X\left(X^{\prime} X\right)^{-1} X$ ，因而两个自由度相同。而在核回归中，$S$ 不是对称矩阵，两者不同，其中 $d f_{1}$ 通常被作为近似的模型自由度使用，而 $N-d f_{2}$ 作为残差自由度。

在 SVCM 中，投影矩阵的定义为：
$$
S=\sum_{j=1}^{J} \gamma_{z j} \chi\left\{\chi^{\prime} \kappa_{h}\left(z_{j}\right) \chi\right\}^{-1} \chi^{\prime} \kappa_{h}\left(z_{j}\right)
$$
其中 $\gamma_{z j}$ 是 $N \times N$ 的矩阵，且当 $Z_{i}=z_{j}$ 时第 i 个对角元素取 1 ，否则取 0 。因此自由度分别为：
$$
d f_{1}=\operatorname{tr}(S)=\sum_{i=1}^{N} S_{i i}=\sum_{i=1}^{N} \operatorname{lev}_{i}\left(z_{i}, h\right)
\quad (10)
$$
$$
d f_{2}=\operatorname{tr}\left(2 S-S S^{\prime}\right) \cong 1.25 \operatorname{tr}(S)-0.5=1.25 d f_{1}-0.5
\quad (11)
$$
`vc_predict` 运行后分别报告模型自由度与残差自由度。当使用选项 `knots()` 和 `km()` 时，将由 $\widehat{\operatorname{lev}}_{i}\left(\bar{z}_{p}, h\right)$ 替代式 (10) 中的 $\operatorname{lev}_{i}\left(z_{i}, h\right)$ 。

(4) 期望核观察值 (Expected kernel observations, E(Kobs))
在非参数估计中，于模型估计中被使用到的有效观察值的数量下降速度越快，模型中使用的解释变量数量越多，带宽越小。期望核观察值 E(Kobs) 是用以表示模型估计过程中被使用到的有效信息的数量的统计值，文献中通常使用 $n|h|$ 作为其估计值，但这种方式在某些情况下是不准确的，因而 Rios-Avila (2020) 提出另一种基于标准化核权重的统计量以表示数据集中信息使用的程度。其中标准化核权重 $K_{\omega}\left(Z_{i}, z, h\right)$ 的定义如下：
$$
K_{\omega}\left(Z_{i}, z, h\right)=k_{\omega}\left(\frac{Z_{i}-z}{h}\right)=\frac{1}{k(0)} k\left(\frac{Z_{i}-z}{h}\right)
$$
$K_{\omega}\left(Z_{i}, z, h\right)$ 的值落在 [0,1] 之间，能够直观的表示估计过程中观察值信息的利用程度。例如，$Z_{i}=z$ 的观察值的标准化核权重为 1 ，说明这些观察值的信息在 LL 估计中被全部使用；而若某观察值的标准化核权重为 0 ，说明其对于 SVCM 的估计没有贡献任何信息。因此，利用 $K_{\omega}\left(Z_{i}, z, h\right)$ 可以估观察值的有效数量。对于某个平滑值 z ，有效的观察值数量可估为：
$$
\operatorname{Kobs}(z)=\sum_{i=1}^{n} k_{\omega}\left(\frac{Z_{i}-z}{h}\right)
$$
由于有效观察值的数量在 z 分布密集的部分自然较多，在分布稀疏部分数量较少，因而对于平滑变量 Z 的全样本，有效观察值的总数即期望核观察值 E(Kobs) 可以用简单的标准化核权重的加权平均来估计，即：
$$
E(\mathrm{Kobs})=\frac{1}{N} \sum_{j=1}^{J} N_{j} \operatorname{Kobs}\left(z_{j}\right)=\frac{1}{N} \sum_{j=1}^{J} N_{j} \sum_{i=1}^{n} k_{\omega}\left(\frac{Z_{i}-z_{j}}{h}\right)
$$
其中 $N_{j}$ 是当 $Z_{i}=z_{j}$ 时的观察值数量。当使用选项 `knots()` 和 `km()` 时，期望核观察值是对更少的 P 个平滑值所对应的标准化核权重进行加权平均，而不是对 J 个平滑值。

特别地，当平滑变量是连续型时，该统计量有如下两个性质：
$$
\lim _{h \rightarrow 0} E(\text { Kobs })=1 \quad \lim _{h \rightarrow \infty} E(\text { Kobs })=N
$$
这两条性质也符合一般对于带宽对估计过程中信息使用量影响的直观理解：至少有一个观察值被用于模型估计，而带宽接近于无穷时，所有的数据都将被用于模型估计。

#### 5.1.3 `vc_predict` 的假设检验：`stest`
除了报告上述的模型统计数据外，`vc_predict` 还可以通过选项 `stest` 进行基本的假设检验。命令执行的检验是 Hastie 和 Tibshirani (1990) 使用近似 F 检验 (approximate F test)，将 SVCM 与如下四个模型相比较：
- Model 0: 
$$
y_{i}=\left(\mathbf{X}_{i}^{\prime} ; Z_{i}\right) \boldsymbol{\beta}_{0}+\epsilon_{i} \quad with \quad \mathrm{df}_{p}=q+1
\quad (12)
$$
- Model 1: 
$$
y_{i}=\left(\mathbf{X}_{i}^{\prime} ; Z_{i} \otimes \mathbf{X}_{i}^{\prime}\right) \boldsymbol{\beta}_{1}+\epsilon_{i} \quad with \quad \mathrm{df}_{p}=2 q
\quad (13)
$$ 
- Model 2: 
$$
y_{i}=\left(\mathbf{X}_{i}^{\prime} ; Z_{i} \otimes \mathbf{X}_{i}^{\prime} ; Z_{i}^{2} \otimes \mathbf{X}_{i}^{\prime}\right) \boldsymbol{\beta}_{2}+\epsilon_{i} \quad with \quad \mathrm{df}_{p}=3 q
\quad (14)
$$
- Model 3: 
$$
y_{i}=\left(\mathbf{X}_{i}^{\prime} ; Z_{i} \otimes \mathbf{X}_{i}^{\prime} ; Z_{i}^{2} \otimes \mathbf{X}_{i}^{\prime} ; Z_{i}^{3} \otimes \mathbf{X}_{i}^{\prime}\right) \boldsymbol{\beta}_{3}+\epsilon_{i} \quad with \quad \mathrm{df}_{p}=4 q
\quad (15)
$$
其中 q 是 X 中包含常数项的解释变量的个数。根据式 (11) 定义 $N-d f_{2}$ 为 SVCM 的残差自由度，$\hat{\varepsilon}_{i}$ 为模型 0, 1, 2 或 3 的残差，$\hat{e}_{i}\left(h^{*}\right)^{2}$ 为 SVCM 的残差，则近似的 F 统计量的定义为：
$$
a F=\frac{\sum \hat{\varepsilon}_{i}^{2}-\sum \hat{e}_{i}\left(h^{*}\right)^{2}}{\sum \hat{e}_{i}\left(h^{*}\right)^{2}} \times \frac{n-d f_{2}}{d f_{2}-d f_{p}}
\quad (16)
$$
该检验的原假设 $H_{0}$ 是模型 0, 1, 2 或 3 为正确模型，$H_{1}$ 为 SVCM 是正确模型。当使用选项 `knots()` 和 `km()` 时，式中的 $\sum \hat{e}_{i}\left(h^{*}\right)^{2}$ 替换为近似 CV(h) 中所对应的残差项。

### 5.2 命令使用：`vc_predict`
```
. vc_predict citations taxes college i.csize, vcoeff(fines) stest
Smooth Varying coefficients model
Dep variable       : citations
Indep variables    : taxes college i.csize
Smoothing variable : fines
Kernel             : gaussian
Bandwidth          :    0.73980
Log MSLOOER        :    3.11950
Dof residual       :    477.146
Dof model          :     18.684
SSR                :  10323.152
SSE                :  37886.159
SST                :  47950.838
R2-1 1-SSR/SST     :    0.78471
R2-2               :    0.79010
E(Kernel obs)      :    277.835


Specification Test approximate F-statistic
H0: Parametric Model
H1: SVCM y=x*b(z)+e
Alternative parametric models:
Model 0 y=x*b0+g*z+e
F-Stat: 8.24705 with pval 0.00000
Model 1 y=x*b0+g*z+(z*x)b1+e
F-Stat: 5.80964 with pval 0.00000
Model 2 y=x*b0+g*z+(z*x)*b1+(z^2*x)*b2+e
F-Stat: 0.75977 with pval 0.65174
Model 3 y=x*b0+g*z+(z*x)*b1+(z^2*x)*b2+(z^3*x)*b3+e
F-Stat: -2.07399 with pval 1.00000
```
```
**非参数模型 (结果略)
reg citations taxes college i.csize fines //OLS 
npregress kernel citations i.taxes i.college i.csize fines, kernel(gaussian)  //Full nonparametric Kernel
```
从报告结果可知，SVCM 的模型自由度为 18.7 ，残差自由度为 477.15 。$R_{1}^{2}=0.784$ 比 OLS 回归模型的大 ($R_{O L S}^{2}=0.7183$)，而比全非参模型的小 ($R_{n p}^{2}=0.8099$)。另外，期望核观察值为 277.8 ，表示平均而言，每一次局部回归都使用了约一半的样本数据。

从近似 F 检验的结果可以看出，在 5% 的显著性水平下，SVCM 模型优于模型 0,1，但不能拒绝平滑系数的二次方交叉项与三次方交叉项模型 (即模型 2,3)。另外，观察到模型 3 的 P 值为 1 ，说明三次方交叉项模型优于 SVCM 。

### 5.3 命令介绍：`vc_test`-J检验
#### 5.3.1 $\widehat{\mathcal{J}}$ 检验
由于近似 F 统计量 (16) 的分布无法得知，Cai，Fan 和 Yao (2000) 提出了另一种通过 wild bootstrap 来获得统计量经验分布的假设检验方法，其统计量与 (16) 相似但没有调整自由度：
$$
\widehat{\mathcal{J}}=\frac{\Sigma \widehat{\epsilon}_{i}^{2}-\Sigma \widehat{e}_{i}\left(h^{*}\right)^{2}}{\Sigma \widehat{e}_{i}\left(h^{*}\right)^{2}}
$$
检验的假设与近似 F 检验的相同，当 $\widehat{\mathcal{J}}$ 统计量大于临界值时，拒接原假设。

#### 5.3.2 语法结构
```
vc_test depvar [indepvars] [if] [in] [, vcoeff(svar) ///
    bw(#) kernel(kernel) knots(#) km(#) degree(#d) wbsrep(#wb)]
```
- `degree(#d)`：设定原假设中的模型，#d 可以为 0, 1, 2 或 3，分别对应模型 (12)-(15)，若无设定，默认为 degree(0);
- `wbsrep(#wb)`：设定获取统计量 $\widehat{\mathcal{J}}$ 的经验分布时 wild bootstrap 的次数，默认数值为 50。此外，命令还会报告 $\widehat{\mathcal{J}}$ 统计量经验分布的第 90、95、97.5 百分位的临界值以供检验推断。

### 5.4 命令使用：`vc_test`
由 `vc_predict` 的近似 F 检验结果知，在 5% 的显著性水平，SVCM 优于模型 0,1 而劣于模型 2,3。因此接下来使用命令 `vc_test` 对中间的模型 1 与模型 2 做进一步的检验。
```
//检验模型 1
. vc_test citations taxes college i.csize, degree(1) wbsrep(200) seed(1) 
(Estimating J statistic CI using 200 Reps)

Specification test.
H0: y=x*b0+g*z+(z*x)*b1+e
H1: y=x*b(z)+e
J-Statistic      :0.16869
Critical Values
90th   Percentile:0.09382
95th   Percentile:0.10351
97.5th Percentile:0.10685

//检验模型 2 
. vc_test citations taxes college i.csize, degree(2) wbsrep(200) seed(1) 
(Estimating J statistic CI using 200 Reps)

Specification test.
H0: y=x*b0+g*z+(z*x)*b1+(z^2*x)*b2+e
H1: y=x*b(z)+e
J-Statistic      :0.01410
Critical Values
90th   Percentile:0.01177
95th   Percentile:0.01490
97.5th Percentile:0.01725
```
$\widehat{\mathcal{J}}$ 检验与近似 F 检验的结果一致，但 $\widehat{\mathcal{J}}$ 结果较详细：在 97.5% 显著性水平下也可拒绝模型 1；而模型 2 在 10% 水平被拒绝，在 95% 水平下不可被拒绝，说明引入平滑系数二次方的交叉项的模型可能与 SVCM 没有明显优劣之分，但本文仍使用 SVCM 探究酒驾传票受到的条件效应。

## 6. SVCM 可视化：`vc_graph`
### 6.1 命令介绍
`vc_graph` 能够使用命令 `vc_[bs|p]reg` 执行后储存在内存 e() 中的系数、方差、置信区间等数据，绘制系数 $\beta(z)$ 或系数的一阶导 $\frac{\partial \beta(z)}{\partial z}$ 与平滑变量 Z 的关系图。语法结构如下：
```
vc_graph [varlist] [, ci(#) constant delta xvar(xvarname) graph(stub) rarea ci_off pci addgraph(str)]
```
- `varlist`：SVCM 中所有解释变量的子集；
- `ci(#)` 或 `ci_off`：ci(#) 可以通过输入数字 0-100 设定置信区间的百分位，否则默认为第 95 百分位；也可以使用 ci_off 选项在图形中免去置信区间；
- `constant`：该选项可绘画常数项与平滑变量的关系图；
- `delta`：绘画 varlist 中变量一阶导的系数 $\frac{\partial \beta(z)}{\partial z}$ 与平滑变量的关系图，否则默认绘画变量 X 本身的系数 $\beta(z)$。此外，同时使用选项 delta 与 constant，可绘画辅助变量 $Z_{i}-z$ 的系数图。
- `xvar(xvarname)`：该选项可绘画平滑可变系数与 xvarname 的关系图，其中 xvarname 为 SVCM 所用平滑变量 Z 经过单调函数转换所得的变量。
- `graph(stub)`：该选项提供一个存根，用作已创建绘图的前缀，这些绘图保存在内存中。默认值是 graph(grph)，即连续地对绘图进行编号。
- `rarea`：使用“面积”图来估计置信区间，默认情况下使用 rcap 。 
- `pci`：该选项与 vc_bsreg 一起使用时，会绘制基于百分位数的置信区间，而不是基于正态分布的区间，此选项不能与 ci() 同时使用。
- `addgraph(str)`：在图形中增加一个点，比如输入 "vc_graph x1, addplot(scatter g x1)"，会生成一个包含点 (g,x1) 的关系图。

### 6.2 命令使用
在上文中，已使用命令 `vc_[bs|p]reg` 对 z=9,10,11 三个平滑值进行模型估计，要绘画出系数与平滑值的关系图以更直观地反映罚款对条件效应的影响，需要对更多的平滑变量值 z 进行模型 SVCM 估计，这可以通过设定 `vc_[bs|p]reg` 中选项 klist() 的值来完成。
```
//对z=7.4~12的24个平滑值进行 SVCM 估计
. vc_preg citations taxes college i.csize, klist(7.4(.2)12)  
Estimating SVCM over 24 point(s) of reference
Smoothing variable:  fines
Kernel function   :  gaussian
Bandwidth         : 0.73980
vce               : robust
Estimating Full model
More than 1 point of reference specified
Results will not be saved in equation form but as matrices
```
```
vc_graph taxes college i.csize    //系数 β与 z 的关系图 (图1)
graph combine grph1 grph2 grph3 grph4
graph export fig1.png, replace

vc_graph taxes college i.csize,  delta   //系数一阶导与 z 的关系图 (图2)
graph combine grph1 grph2 grph3 grph4
graph export fig2.png, replace
```
得到的关系图分别如下所示：
![图1 系数与平滑变量关系图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20210127230424.png)


![图2 系数一阶导与平滑变量关系图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20210127230504.png)

由表格 1 第一栏的 OLS 回归结果可知，整体而言，当酒精饮料被列为税收商品时，该地区每月的酒驾传票数量会下降 4.5 次，结合图 1 可知税收对于传票数量的这种效应在罚款条件下的变化范围为 5~15，且在罚款低时税收的效应更大，当罚款上升至 10 以后，税收的条件效应不再变化，这与图 2 中当罚款大于 10 时 $\frac{\partial \beta(z)}{\partial z}$ 接近 0 相一致。

类似地，当该地区建有大学的时候，每月的酒驾传票数量会上升 5.8 次，大学的这种条件效应随着罚款的增加而减小，变化范围为 5~15，且当罚款高于11时，是否有大学对传票的条件效应不再变化。

城市水平为中等对于传票次数的影响相对于城市水平为高等时的影响较小，且由其 $\frac{\partial \beta(z)}{\partial z}$ 始终接近 0 可知中等的城市水平基于罚款的条件效应基本保持不变。高等的城市水平对于传票次数的条件效应则明显随着罚款的增加而减小，变化范围为 10~30。

## 7. 结语
系数平滑可变模型 (SVCM) 刻画了解释变量在一定条件下 (某些平滑变量) 对于被解释变量的线性关系，可用于研究这种关系下系数随着平滑变量的改变而变化的趋势，且相比非参数估计模型，SVCM 更好地减免了“维度诅咒”的影响。