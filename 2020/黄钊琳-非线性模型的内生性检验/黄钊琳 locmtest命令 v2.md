# locmtest命令—非线性模型的内生性检验
&emsp;
>**作者**：黄钊琳（中山大学）  
>**E-Mail**:** <907641684@qq.com>  
  
[TOC]


## 1.引言
---
在许多的经济应用中，经常会产生非线性关系。

即使潜在的内生变量其实是非线性地进入关系，我们通常假设其线性进入关系，然后运用 Hausman 检验。然而在存在这些非线性的情况下，Hausman 检验对潜在的内生变量是否外生是不知情的。  

因此，当结果变量和离散的潜在内生变量之间是非线性关系时，为了评估非线性模型的内生性，locmtest 命令应运而生。本文主要介绍 locmtest 命令的应用背景、基本思想和实例应用。
&emsp;
## 2.背景介绍
---
本命令是在 Lochner 和 Moretti (2015, Review of Economics and Statistics 97: 387–397) 提出的非线性模型的内生性检验基础上开发的。  
### 2.1 适用场景

值得注意的是，在 Lochner 和 Moretti (2015) 基础上开发的测试并不适用于所有非线性模型，而只适用于以下情况：  

假设我们估计变量 $s_{i}$ 对结果 $y_{i}$ 的影响，其中 $s_{i} \in\{0,1,2,3, \ldots, S\}$ ，$\mathbf{x}_{i}$ 是外生变量，因此可以写出以下的等式：   

$$y_{i}=\sum_{j=1}^{S} D_{i j} \beta_{j}+\mathbf{x}_{i}^{\prime} \gamma+\varepsilon_{i}\quad (1) $$  

当 $s_{i} \geq j$ 时，$D_{i j}=1$ ，反之则为0，且 $E\left(\varepsilon_{i}\right)=0$ ，$E\left(\varepsilon_{i} \mid \mathbf{x}_{i}\right)=0$  

如果 $s_{i}$ 是潜在的内生变量，在研究中通常估计 $s_{i}$ 与 $y_{i}$ 存在线性关系如下式：  

$$y_{i}=s_{i} \beta^{L}+\mathbf{x}_{i}^{\prime} \gamma^{L}+\nu_{i}\quad (2) $$  

但不同的是它隐含的假设是 $s_{i}$ 对结果的影响在所有 $s_{i}$ 水平上都是一致的，而 (1) 式则允许不同 $s_{i}$ 水平对结果变量有不同的影响。  

因此，本命令只在 (1) 式和 (2) 式描述的特定模型中适用。同时不难看出，还需要满足三个至关重要的条件：
-  必须存在单个有限值离散潜在内生变量 $s_{i}$ ；
-  外生变量 $\mathbf{x}_{i}$ 是可加可分的，线性进入方程；
-  总体中所有系数均为均匀系数。   

### 2.2 理论前提

在之前的研究中， (2) 式中内生性问题通常是使用 Hausman 检验即基于 OLS 和 IV 或 2SLS 估计的比较，但是 Lochner 和 Moretti 发现，当真实关系用 (1) 描述时，用 (2) 估计可以导致不同的普通最小二乘 OLS 、 IV 和两阶段最小二乘 2SLS ，如果用 Hausman 检验有可能会导致错误的内生性结论。因此， Lochner 和 Moretti 针对这种情况提出了一个新的稳健性检验。  

下面给出当非线性模型 (1) 是正确时，会导致线性模型 (2) 中的 $\widehat{\beta}^{L}$  在OLS 和 IV 或 2SLS 中出现不同的加权平均值的数学证明。  

首先从 IV 估计看起，标准的 IV 估计会使得 (2) 式中的 $\beta_{j}$ 收敛到 (1) 式中特定水平权重 $\beta_{j}$ 的值。我们只考虑一个工具变量 $z_{i}$ ，定义 $\mathbf{M}_{x}=I-x\left(x^{\prime} x\right)^{-1} x^{\prime}$ ，同时假设存在变量 $w$ ，让 $\tilde{w}=\mathbf{M}_{x} w$ ， IV 估计中 $\beta_{j}$ 的估计量如下：  

$$\begin{aligned}\widehat{\beta}_{\mathrm{IV}}^{L} &=\left(z^{\prime} \mathbf{M}_{x} s\right)^{-1} z^{\prime} \mathbf{M}_{x} y \\
&=\left(z^{\prime} \mathbf{M}_{x} s\right)^{-1} z^{\prime} \mathbf{M}_{x}\left(\sum_{j=1}^{S} \mathbf{D}_{j} \beta_{j}+x \gamma+\varepsilon\right) \\
&=\sum_{j=1}^{S} \widehat{\omega}_{j}^{\mathrm{IV}} \beta_{j}+\left(\widetilde{z}^{\prime} \widetilde{s}\right)^{-1} \widetilde{z} \varepsilon
\end{aligned}$$  

其中，$\begin{aligned} \widehat{\omega}_{j}^{\mathrm{IV}}=\left(\widetilde{z}^{\prime} \widetilde{s}\right)^{-1} \widetilde{z}^{\prime} \mathbf{D}_{j}\end{aligned}$ 紧接着为了求出 $\beta_{j}$ 的概率极限和收敛到该极限的必要假设，考虑两个线性关系:
- $s_{i}=\mathbf{x}_{i}^{\prime} \delta_{s}+\eta_{i}$   
其中 $\delta_{s}=\left\{E\left(\mathbf{x}_{i} \mathbf{x}_{i}^{\prime}\right)\right\}^{-1} E\left(\mathbf{x}_{i} s_{i}\right)$   
且 $E\left(\mathbf{x}_{i} \eta_{i}\right)=0$
- $z_{i}=\mathbf{x}_{i}^{\prime} \delta_{z}+\zeta_{i}$  
其中 $\delta_{z}=\left\{E\left(\mathbf{x}_{i} \mathbf{x}_{i}^{\prime}\right)\right\}^{-1} E\left(\mathbf{x}_{i} z_{i}\right)$  
且 $E\left(\mathbf{x}_{i} \zeta_{i}\right)=0$  

满足假设1：已知 $E\left(z_{i} \varepsilon_{i}\right)=0$ ，在控制了 $\mathbf{x}_{i}$ 之后 $z_{i}$ 与 $s_{i}$ 相关，假设 $E\left(z_{i} \eta_{i}\right) \neq 0$       
的前提下，那么可以得到 $\beta_{j}$ 的概率极限为：  

$$\widehat{\beta}_{\mathrm{IV}}^{L} \stackrel{p}{\longrightarrow} \sum_{j=1}^{S} \omega_{j}^{\mathrm{IV}} \beta_{j}\quad (3) $$  

对所有的 $j=1,2...S$，权重 $w_{j}$ 的和为1，式子如下： 

$$\omega_{j}^{\mathrm{IV}}=\frac{\operatorname{Pr}\left(s_{i} \geq j \mid \mathbf{x}_{i}\right) E\left(\zeta_{i} \mid s_{i} \geq j\right)}{\sum_{q=1}^{S} \operatorname{Pr}\left(s_{i} \geq q \mid \mathbf{x}_{i}\right) E\left(\zeta_{i} \mid s_{i} \geq q\right)} \geq 0$$  

而 $w_{j}$ 的样本估计量 $\widehat{\omega}_{j}^{\mathrm{IV}}=\left(\widetilde{z}^{\prime} \widetilde{s}\right)^{-1} \widetilde{z}^{\prime} \mathbf{D}_{j}$ ，又

$$D_{i j}=\omega_{j} s_{i}+\mathbf{x}_{i}^{\prime} \alpha_{j}+\psi_{i j}, \quad j=1,2, \ldots, S\quad (4)$$  

因此不难得出 $\widehat{\omega}_{j}^{\mathrm{IV}} \stackrel{p}{\longrightarrow} \omega_{j}^{\mathrm{IV}}$  

同理对于 2SLS ，如果有多个工具变量 $z_{i}=\left(z_{i 1}, \ldots, z_{i I}\right)$ 让 $z_{i l}=\mathbf{x}_{i}^{\prime} \delta_{z l}+\zeta_{i l}$ ，其中 $\delta_{z l}=\left\{E\left(\mathbf{x}_{i} \mathbf{x}_{i}^{\prime}\right)\right\}^{-1} E\left(\mathbf{x}_{i} z_{i l}\right)$ 且 $E\left(\mathbf{x}_{i} \zeta_{i l}\right)=0$  

满足假设1和假设2： $z_{i}$ 的相关性矩阵 $E\left(\zeta_{i}\zeta_{i}^{\prime}\right)$ 是满秩矩阵的情况下，同样 $\beta^{L}$ 估计量依概率收敛到 $\beta_{j}$ 的加权平均:  

$$\widehat{\beta}_{2 \mathrm{SLS}}^{L} \stackrel{p}{\longrightarrow} \sum_{j=1}^{S} \omega_{j}^{2 \mathrm{SLS}} \beta_{j}\quad (5) $$  

同理，2SLS的 $w_{j}$ 也是一致的，其样本估计量即是 (4) 式中 $s_{i}$ 系数的估计量。  

最后，为了考虑为什么 Hausman 检验可能得出错误结论，OLS 估计如下：  

$$\begin{aligned}
\widehat{\beta}_{\mathrm{OLS}}^{L} &=\left(s^{\prime} \mathbf{M}_{x} s\right)^{-1} s^{\prime} \mathbf{M}_{x} y \\
&=\sum_{j=1}^{S} \widehat{\omega}_{j}^{\mathrm{OLS}} \beta_{j}+\left(\widetilde{s}^{\prime} \widetilde{s}\right)^{-1} \widetilde{s} \varepsilon
\end{aligned}$$  

而 $\widehat{\omega}_{j}^{\mathrm{OLS}}=\left(\widetilde{s}^{\prime} \widetilde{s}\right)^{-1} \widetilde{s}^{\prime} \mathbf{D}_{j}$ ，如果内生性不成立，那么：
$$\widehat{\beta}_{\mathrm{OLS}}^{L} \stackrel{p}{\longrightarrow} \sum_{j=1}^{S} \omega_{j}^{\mathrm{OLS}} \beta_{j}\quad (6) $$  

同理 $w_{j}$ 也是一致的。

因此，标准的 Hausman 测试是基于 OLS 和 IV 或 2SLS 估计之间的差异。 然而，上述分析表明，即使在没有内生性的情况下，OLS 和 IV 或 2SLS 估计量也会收敛到不同的加权平均值；见 (3)、 (5) 和 (6)。 如果 OLS 和 IV 或 2SLS 的权重有很大的不同，这会产生不同的OLS和IV或2SLS估计，反过来又会导致使用标准 Hausman 检验出现错误。

### 2.3 Wald 检验
Lochner 和 Moretti 发现，如果 $s_{i}$ 是外生的，那么 $E\left(\varepsilon_{i} \mid s_{i}\right)=0$ ，那么OLS的估计量 $B=\left(\beta_{1}, \ldots, \beta_{S}\right)$ 在非线性模型 (1) 中是一致的。如果这个结果和 (5) 式中 2SLS 估计的 $\beta_{j}$ 概率极限值结合。可以得到结论如下：
$$\widehat{\beta}_{2 \mathrm{SLS}}^{L}-\widehat{\omega}_{2 \mathrm{SLS}}^{\prime} \widehat{B} \stackrel{p}{\longrightarrow} 0\quad (7) $$  

反之，如果 $s_{i}$ 是内生的，则此结论不成立。

因此，Lochner 和 Moretti 提出的 Wald Test 主要分为两步，一是将所有参数的帧估计 (frame estimation) 作为一种叠加广义矩（GMM）问题来导出他们的渐进分布；二是利用δ方法导出 (7) 所呈现的变换的渐近分布。  

省略 GMM 的编写过程，以下定理总结了 Lochner 和 Moretti（2015）的主要结果。  

**定理**：在假设 1 和 2 的前提下，如果 $E\left(\varepsilon_{i} \mid s_{i}\right)=0$ ，那么  
$$W_{n}=n\left\{\frac{\left(\widehat{\beta}_{2 \mathrm{SLS}}^{L}-\widehat{\omega}_{2 \mathrm{SLS}}^{\prime} \widehat{B}\right)^{2}}{\widehat{\mathrm{G}} \hat{\mathrm{V}} \widehat{\mathrm{G}}^{\prime}}\right\} \stackrel{d}{\longrightarrow} \chi^{2}(1)$$  

Lochner 和 Moretti 采取了蒙特卡罗模拟 1000 个观察值，发现当 $s_{i}$ 为外生，真实模型是 (2) 式时，Wald 检验与 Durbin-Wu-Hausman(DWH) 检验相同，两者均在 0.05 的显着性水平上无法拒绝外生性。如果 $y_{i}$ 和 $s_{i}$ 之间的真实关系是非线性，则 Wald 测试继续拒绝 5% 显著性，而DWH则随着非线性程度的增加而以增加的速度拒绝。 因此如果样本大小大于 1,000 个观测值，渐近结果表现应该更明显。下文有实例可以更好说明。  

## 3.locmtest 命令
---
`locmtest depvar (varlist1 = varlist_iv) [indepvars] [if] [, graph coefficients] `


### 3.1 概述  
locmtest用于计算 Lochner–Moretti (LM) 检验的外生性，在输入命令后，会显示(1)式 OLS、2SLS 和 IV 估计的 $\beta_{j}$ 值，以及 Wald Test、Naive Test 和 DWH test 的数值和 P 值，如果在 5% 显著性水平下拒绝假设，则表明存在内生性。如果不拒绝外生性，我们可以使用 locmtest 的结果来部分解释 OLS 和 2SLS 之间的差异   

### 3.2 命令选项
其中 **depvar** 是要使用的因变量，**varlist1** 是离散内生变量，**varlist_iv** 是工具变量集，**indepvars** 是外生变量列表。需要注意的是，虽然此命令允许 **indepvars** 中出现因子变量，但它不允许 **varlist_iv** 中出现因子变量。  

`graph` 命令可以显示 (1) 式中 OLS 特定水平的估计量图表，包括 $B=\left(\beta_{1}, \ldots, \beta_{S}\right)$ ，估计权重 $\widehat{\omega}_{\mathrm{OLS}}^{\prime}=\left(\widehat{\omega}_{1}^{\mathrm{OLS}}, \ldots, \widehat{\omega}_{S}^{\mathrm{OLS}}\right)$ ,以及2SLS 的估计权重 $\widehat{\omega}_{2 \mathrm{SLS}}^{\prime}=\left(\widehat{\omega}_{1}^{2 \mathrm{SLS}}, \ldots, \widehat{\omega}_{S}^{2 \mathrm{SLS}}\right)$  

`coefficients` 命令会显示 (1) 式中OLS特定水平估计量矩阵，同样包括 $B$、 $\widehat{\omega}_{\mathrm{OLS}}^{\prime}$ 、$\widehat{\omega}_{2 \mathrm{SLS}}^{\prime}$  

### 3.3 应用实例
- 例1：我们以Card（1995a）的数据来估计教育对收入的影响。
$$\ln w_{i}=s_{i} \beta^{L}+\mathbf{x}_{i}^{\prime} \gamma^{L}+\nu_{i}\quad (8) $$  

其中 $\ln w_{i}$ 是一小时收入的对数，$s_{i} \in\{1,2, \ldots, 18\}$ 是教育年限。我们担忧在这种情况下，$s_{i}$ 是内生的，因为有一些未观察到的个体特征决定了收入和教育年限，例如，一个人的观察能力。
  
现在假设群体中，由于羊皮效应，$\ln w_{i}$ 和 $s_{i}$ 之间的真实关系是非线性的：  
$$\ln w_{i}=\sum_{j=2}^{18} D_{i j} \beta_{j}+\mathbf{x}_{i}^{\prime} \gamma+\varepsilon_{i}\quad (9) $$  

此时，即使 $s_{i}$ 是外生的，使得 $E\left(\varepsilon_{i} \mid s_{i}\right)=0$ ，我们也可以使用 (8) 式从 OLS 和 IV 获得不同的估计，因此如果我们只使用标准的 Hausman 检验，那么很可能导致不正确的结论。下面运用 locmtest 命令进行 LM 检验。  

  
```stata
use http://www.stata.com/data/jwooldridge/eacsap/card, clear
locmtest lwage (educ = nearc4) exper expersq,graph coefficient

=================================================== 
Output for the Lochner & Moretti (2015) Wald Test  
=================================================== 
 
Output Variable y: lwage
Endogenous Variable s: educ
Instruments z: nearc4
 
Number of observations = 3010
Number of Categories of Endogenous variable is: 18
Number of Dummies is: 17
 
The number of Excluded Instruments is: 1
 
Estimated Coefficients 
 
             |      Coef.    Std. Err.
-------------+------------------------
         OLS |  .09317071   .00357785
          IV |  .25871555   .03373941
       RWOLS |  .09072257   .00573885
-------------+------------------------
 
Estimated Test Statistics 
             |      Test     p-value
-------------+------------------------
     LM-Wald |   24.19655   8.699e-07
  Naive Wald |   30.12477   4.051e-08
    DWH Test |  41.823868   1.162e-10
-------------+------------------------
 
NOTES: 
 
RWOLS = Reweighted OLS using TSLS Weights 
 
LM-Wald = Lochner-Moretti Wald Test
 
Naive Wald = [ (IV - OLS) / SD(IV-OLS) ]^2 
 
DWH Test: Durbin-Wu-Hausman Test (Augmented Regression).
 
```  

注意到结果，IV 估计系数值大于 OLS 估计系数值，这与学校教育的内生性可能会高估 OLS 的教育效果的假设相反。 虽然对这一结果有几种解释，如测量误差和学校教育影响的个体异质性，但收入与学校教育中的非线性关系也是原因。考虑到可能的羊皮效应，这种可能性无疑大大提高了。Naive Wald 和 DWH 检验都拒绝外部性。LM 检验也拒绝外部性，这降低了人们对前几次检验的结论是由于错误判断关系导致的担忧。 

---

- 例2：我们的第二个例子使用 Lochner 和 Moretti（2015)，研究教育对犯罪的影响。他们估计的线性模型是：
$$p_{i}=s_{i} \beta^{L}+\mathbf{x}_{i}^{\prime} \gamma^{L}+\nu_{i}\quad (10) $$  

$p_{i}$ 是虚拟变量，如果调查对象在监狱，其值为1，否则为0。$s_{i} \in\{1,2, \ldots, 18\}$ 是教育年限。但是多年的教育 $s_{i}$ 可能是内生的，因为有一些未观察到的因素（例如耐心）决定了一个人的教育程度，也可能决定了个人的犯罪倾向。  

再次假设真实模型如下：
$$p_{i}=\sum_{j=1}^{18} D_{i j} \beta_{j}+\mathbf{x}_{i}^{\prime} \gamma+\varepsilon_{i}\quad (11) $$  

我们只关注在黑人男性中这一效应的影响，应用命令得到结果：
```stata
use http://eml.berkeley.edu//~moretti/inmates, clear  

*省略数据处理后* 
locmtest prison (educ = ca9 ca10 ca11) i.rage i.year i.state i.birthpl i.birthpl#i.BBeduc   

=================================================== 
Output for the Lochner & Moretti (2015) Wald Test  
=================================================== 
 
Output Variable y: prison
Endogenous Variable s: educ
Instruments z: ca9 ca10 ca11
 
Number of observations = 401529
Number of Categories of Endogenous variable is: 19
Number of Dummies is: 18
 
The number of Excluded Instruments is: 3
 
Estimated Coefficients 
 
             |      Coef.    Std. Err.
-------------+--------------------------
         OLS |  -.00369034   .00008333
          IV |   -.0047513   .00115743
       RWOLS |  -.00073792   .00017873
-------------+--------------------------
 
Estimated Test Statistics 
             |      Test     p-value
-------------+------------------------
     LM-Wald |   11.944147  .00054819
  Naive Wald |   .97566386  .32327168
    DWH Test |   .51540357  .47280942
-------------+------------------------
 
NOTES: 
 
RWOLS = Reweighted OLS using TSLS Weights 
 
LM-Wald = Lochner-Moretti Wald Test
 
Naive Wald = [ (IV - OLS) / SD(IV-OLS) ]^2 
 
DWH Test: Durbin-Wu-Hausman Test (Augmented Regression).
```  

值得注意的是，无论是 Naive Wald 还是 DWH 检验都不能拒绝外部性，但是 LM 检验拒绝它。因此在这个例子中，水平特定效应的差异可能导致标准 Hausman 检验在应该拒绝时不能拒绝外部性。  

---
- 例3：依然是研究例2教育对犯罪的影响，但是对象换成了白人男性。模型假定与例2相同。   

此次命令同时包括了`graph`和`coefficient`选项。

```stata
use http://eml.berkeley.edu//~moretti/inmates, clear  

*省略数据处理后* 
 locmtest prison (educ = ca9 ca10 ca11) i.rage i.year i.state i.birthpl, graph coefficients
 

=================================================== 
Output for the Lochner & Moretti (2015) Wald Test  
=================================================== 
 
Output Variable y: prison
Endogenous Variable s: educ
Instruments z: ca9 ca10 ca11
 
Number of observations = 3209138
Number of Categories of Endogenous variable is: 19
Number of Dummies is: 18
 
The number of Excluded Instruments is: 3
 
Estimated Coefficients 
 
             |      Coef.    Std. Err.
-------------+--------------------------
         OLS |  -.00099111   .00001191
          IV |  -.00114869   .00036243
       RWOLS |  -.00120313     .000034
-------------+--------------------------
 
Estimated Test Statistics 
             |      Test     p-value
-------------+------------------------
     LM-Wald |   .02247255  .88083682
  Naive Wald |   .20211212  .65302138
    DWH Test |   .16365057  .68581755
-------------+------------------------
 
NOTES: 
 
RWOLS = Reweighted OLS using TSLS Weights 
 
LM-Wald = Lochner-Moretti Wald Test
 
Naive Wald = [ (IV - OLS) / SD(IV-OLS) ]^2 
 
DWH Test: Durbin-Wu-Hausman Test (Augmented Regression).

Estimated Coefficients: 

             |    B           seB       W2SLS     seW2sls     Wols     seWols
-------------+------------------------------------------------------------------
           1 | -.0004088   .0011741   .0037614   .0004353   .0072537   .0000132
           2 |  .0045219   .0012997   .0061937     .00047   .0085221   .0000143
           3 |  -.001657   .0009236     .01063   .0005368   .0109205   .0000164
           4 | -.0010993   .0007393   .0224401   .0006503   .0145451   .0000194
           5 |  .0012797   .0006386   .0393125   .0008261   .0187105   .0000225
           6 | -.0001663   .0005139   .0590797   .0010652   .0236794   .0000259
           7 |  .0000373   .0003851   .0833742   .0013781   .0316599   .0000307
           8 | -.0020164    .000276   .1191036   .0018339   .0426187   .0000363
           9 |  .0013537    .000223   .1539329   .0021541   .0668838   .0000444
          10 | -.0023834   .0002292   .1452373   .0019494   .0809498   .0000476
          11 |  -.001019   .0002225   .1335972   .0018293   .0928632   .0000511
          12 | -.0046185   .0001729    .151474   .0020407   .0986295    .000055
          13 | -.0014042   .0001508   .0016496   .0028537   .1147939   .0000602
          14 | -.0004354   .0001853   .0166923    .002551   .1120058    .000057
          15 | -.0014717   .0002178   .0173397   .0023118   .0993689   .0000538
          16 |  .0007271   .0002121   .0225779   .0020952   .0889882   .0000522
          17 | -.0003403     .00023   .0088103    .001667   .0512865   .0000451
          18 |  .0009268   .0002471   .0047934   .0014149   .0363205   .0000395
           
```  

绘制出的图如下：
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/locmtest命令_Fig1_黄钊琳.png)
  

结果表明，Naive Wald 和 DWH 测试都不能拒绝 $s_{i}$ 的外生性。 LM 检验也不能拒绝 $s_{i}$ 的外生性，这降低了人们对前一次检验的结论是由于犯罪-教育关系中的非线性所致的担忧，但由于例2得到的结果，不难发现非线性关系应该在黑人男性的情况下是很重要的。 

在绘制出的图 和 Stata 输出的估计系数矩阵中对估计系数进行了描述。矩阵和图也给出了 OLS 和 2SLS 权重的估计。从图可以看出，12 - 16 岁受教育年限的 OLS 权重较高，而 9 - 12 岁受教育年限的 2SLS 权重较高。这意味着9年到12年的教育过渡对 2SLS 估计有实质性的影响。   
  
## 4. 局限 
--- 
正如 Lochner 和 Moretti 在论文中写道:

"This Wald test represents only a partial solution to the problem of estimating multiple per-unit treatment effects with limited instruments."

如果检验拒绝外部性，此检验无法为拟合正确模型提供任何帮助。


## 参考资料  
```Markdown
- Michael P. Babington, Javier Cano-Urbina. A Test for Exogeneity in the Presence of Nonlinearities. 2016, 16(3):761-777.  [-PDF-](https://www.sci-hub.ren/10.1177/1536867x1601600313)
```
