# 神奇的dynsimpie：动态组合因变量模型的图示

&emsp;


> **作者：** 傅蕴璋 (中山大学)        
> **E-mail:**  <15096089762@163.com> 

&emsp;

**目录**

[TOC]



## 1 dynsimpie 与组合变量

组合变量（Compositional Variables）表示同时具有常数性质的数据，它在实证分析领域中非常常见。通常组合变量是由每个分析变量的总和为100%的值构成。例如岩石中各类矿石的比例，选举中的各党派比例，这些变量的总和均为1。尽管组合变量被广泛的使用，但它的性质仍然使得组合变量不能被通常的方法建模为因变量，特别是对于跨多个时间点的组合变量。

针对此情况 Philips, Rutherford, Whitten 引入了`dynsimpie`命令，这是一个帮助估计和解释动态组合因变量模型结果的命令。设动态组合因变量为 $y_{tj}$。其中 $t$ 代表时间， $j$ 代表不同类别。`dynsimpie`命令首先将因变量 $y_{tj}$ 转换为$\Delta s_{tj}$:

$$\Delta s_{tj}=\ln(y_{tj}/y_{t1}) \forall j\neq1$$

 ECM 模型根据下式推算：

$$\Delta s_{tj}=\beta_{0j}-\alpha_js_{t-1}+\beta_{Lj}x_{t-1}+\beta_{Sj}\Delta x_t+\epsilon_{jt}$$

其中

●$\Delta s_{tj}$是因变量 $j$ 从 $t-1$ 到 $t$ 时根据当 $j=1$ 时 $y_{tj}$ 的值所记录的变化率

●$\beta _{0j}$是常数项

●$x_t$是表示时间t的自变量

●$-\alpha _j$是衡量长期误差修正过程的调整参数

●$\beta _{Sj}$是衡量短期效应的参数

●$\beta _{Lj}$是参数向量，与$-\alpha _j$结合可以估计每个自变量长期变化的影响。

●$\epsilon _{jt}$是扰动项

由于估计参数量大，且在处理 $y_{jt}$ 时使用非线性方法，难以直观感受变量的变化。因此`dynsimpie`命令生成了一张基线变化图（Change-From-Baseline Plot）帮助解释动态组合因变量模型。

完整的`dynsimpie`命令包含了四个 ado 文件：`dynsimpie`、`cfbplot`、`effectsplot`和`dynsimpiecoef`。在使用之前，需要安装`clarify`命令（Tomz, Wittenberg, and King 2003）和`coefplot`命令（Jann 2014）。通过这些新命令，可以在单个选项中设定多个冲击量。

若上文提到的 ECM 模型无法满足需求，也可以使用 LDV 模型， LDV 模型如下：

$$s_{tj}=\Phi_js_{t-1,j}+\beta_{0j}+\beta_{sj}x_t+\epsilon_{jt}$$

●其中$\Phi_j$是 LDV 模型中的参数，与 $\beta_{sj}$ 可以联合估计长期变化

●其他变量和系数与 ECM 模型相同

在使用`dynsimpie`命令后，可以使用`cfbplot`和`effectsplot`生成曲线图，它们显示了基于模型的，对每类成分因变量短期和长期影响的模拟。`cfbplot`是基线变化图（Change-From-Baseline Plot）的缩写，它显示模拟输出的图形。随时间绘制每个成分因变量的预期比例，以及相关的置信区间。除了基本的`cfbplot`命令外，还可以使用`effectsplot`命令显示来自估计的`dynsimpie`结果的预测图。

`dynsimpiecoef`命令生成`dynsimpie`中使用的SUR回归（Seemingly Unrelated Regression）系数结果的图形。在此过程中，`dynsimpiecoef`使用两种不同的方式来显示结果：根据因变量排序的系数和根据自变量排序的系数。

## 2 dynsimpie 命令

### 2.1 语法

`dynsimpie`命令的语法如下：

**dynsimpie** $indepvars$ [$if$] [$in$],**dvs**($varlist$) [  **ecm ar**($numlist$)  **shockvars**($varlist$)  **shocvars**($numlist$)  **shockvals**($numlist$)  **dummy**($varlist$) **dummyset**($numlist$) **dummyshockvar**($varname$) **<u>inter</u>action**($varname$)  **intype**($string$)  **<u>t</u>ime**(#)  **sigs**($numlist$)  **range**(#)  **<u>percent</u>age  pv  <u>killtab</u>le**  ]

●$indepvars$ 是一个连续的自变量列表，在模型中这些变量不会受到反事实冲击。

●当选择ECM模型时，`dynsimpie`会自动将这些变量（以及在其他选项中添加的任何附加变量）转换为滞后项和用于估计误差一阶差分项，以便以错误更正形式进行估计。``dynsimpie`默认运行 LDV 模型。其他选项允许在模型中添加虚拟变量，使多个自变量同时受到冲击，并估计相乘交互作用。

### 2.2 Options

●**dvs()** 是组成因变量的组成成分表，每一个变量都需要用比例或百分比表示（这些比例百分比的总和必须为1）。如果不满足格式，命令将会提示错误信息。

●**ecm** 输入此选项则选择 ECM 模型。它将因变量和自变量自动转换为滞后项和用于估计误差一阶差分项。如果不使用**ecm**选项则默认使用 LDV 模型。

●**ar()** 是模型中的因变量滞后量。它最多可以输入三个值，且允许非连续值。但使用 ECM 模型时不允许使用此选项。

●**shockvar()** 是受 **shockvals()** 中设定的单周期反事实冲击影响的连续自变量列表。所有冲击的时间在 **time()** 中设定。**shockvars()** 中列出的变量不能同时包含在 $indepvars$ 中。

●**shockvals()** 是在 **time()** 中设定的，**shockvars()** 中每个变量在时间 $t$ 的一段时间内的震动量的数字列表。**shockvals()** 设定的数值必须等于 **shockvars()** 中的变量数。

●**dummy()** 是模型中虚拟变量的向量。在`dynsimpie`中，ECM 模型中的虚拟变量以滞后量和一阶差分的形式输入估计方程。

●**dummyset()** 为 **dummy()** 中设定的每个虚拟变量设定可选值。默认 **dummy()** 中的每个虚拟变量都将设置为0。

●**dummyshockvar()** 是一个虚拟变量，在 **time()** 中设定，它在时间 $t$ 处受到单周期反事实冲击。**dummyshockvar()** 中设定的变量不能同时在 **dummy()** 中设定。使用 ECM 模型时，冲击首先影响时间 $t$ 的一阶差分 **dummyshockvar()** ，并持续一个时间段。这个冲击会影响滞后的 **dummyshockvar()** ，从 $t+1$ 开始，持续到这个周期的剩余部分。如果不同时设定此选项和 **shockvars()** 将导致全基线模拟。

●**interaction()** 是一个二分变量，不包括在其他地方作为控制或冲击的变量，以进入乘法交互。在`dynsimpie`中，在 **interaction()** 中输入的变量与 **shockvars()** 中输入的第一个变量进行交互。

●**intype()** 可以设为 **intype(on)** 或者 **intype(off)** 。**interaction()** 中设定的变量在 **intype(on)** 时取1，在 **intype(on)** 时取0。

●**time(#)** 是选项 **shockvars()** 和 **dummyshockvar()** 中设定的变量经历一个周期冲击的时间。默认为 **time(5)** 。

●**sigs()** 表示置信区间百分比，即在多少百分比的置信区间下显著。默认值为95%置信区间，**sigs()** 最多可以输入两个显著性水平百分比。

●**range(#)** 给出要模拟的场景的长度。在冲击发生时 **range(#)** 必须比 **time(#)** 更大。默认值为 **range(20)** 。

●**percentage** 生成百分比图以取代默认的期望比例的图。

●**pv** 计算预测的比例，而不是默认的期望比例。

●**killtable** 禁止自动生成SUR结果。

### 2.3 cfbplot 与 effectsplot

在使用`dynsimpie`命令后，可以继续使用`cfbplot`和`effectsplot`命令。

`cfbplot`显示模拟输出的基线变化图（Change-From-Baselin Plot）。每个成分因变量的预期比例随时间绘制，以及相关的置信区间。

而`effectsplot`命令则显示来自估计结果的效果图（预测图）。这就产生了图形，显示了每个成分因变量的长期和短期效应，以及相对应的置信区间。



## 3 dynsimpiecoef 命令

### 3.1 语法

`dynsimpiecoef`命令的语法如下：

**dynsimpiecoef** $indepvars$ [$if$] [$in$], **dvs**($varlist$) [**ecm ar**($numlist$)   **dummy**($varlist$)  **<u>inter</u>action**($varname$)  **smoth**  **sigs**($numlist$)  **row**(#)  **xsize**(#)  **all**  **<u>vert</u>ical**  **angle**($angle$) **<u>killtab</u>le**]

●$indepvars$是一个连续的自变量列表，在模型中这些变量不会受到反事实冲击。

### 3.2 Options

●**dvs()** 是组成因变量的组成成分表，每一个变量都需要用比例或百分比表示（这些比例百分比的总和必须为1）。如果不满足格式，命令将会提示错误信息。

●**ecm** 输入此选项则选择 ECM 模型。它将因变量和自变量自动转换为滞后项和用于估计误差一阶差分项。如果不使用 **ecm** 选项则默认使用 LDV 模型。

●**ar()** 是模型中的因变量滞后量。它最多可以输入三个值，且允许非连续值。但使用 ECM 模型时不允许使用此选项。

●**dummy()** 是模型中虚拟变量的向量。

●**interaction()** 是一个二分变量，不包括在其他地方作为控制或冲击的变量，以进入乘法交互。在`dynsimpie`中，在 **interaction()** 中输入的变量与 **shockvars()** 中输入的第一个变量进行交互。

●**smooth** 为50个等距区间（1,3……,99）添加了具有强度渐变颜色和不同大小的置信区间，如`coefplot`。默认情况下，`dynsimpiecoef`将生成95%置信区间的系数。**sigs()** 不能与 **smooth** 共存。

●**sigs()** 表示置信区间百分比，即在多少百分比的置信区间下显著。默认值为95%置信区间，**sigs()** 最多可以输入两个显著性水平百分比。

●**row()** 表示组合图的行数。根据模型中因变量类别的数量和自变量的数量，可能需要一个具有多行的组合图。如果没用使用 **vertical** 选项，则默认为 **row(1)** 。使用 **vertical** 选项默认为 **row(3)** 。

●**xsize()** 是以英寸为单位的组合图形的宽度。根据不同的模型中因变量的数量和自变量的数量，需要使用此选项使组合图的宽度变化。默认值为 **xsize(5)**。

●**all** 允许比较所有因变量类别对的 SUR 结果。如果选择 **all** 选项，`dynsimpiecoef`会自动为所有可能的因变量组合生成系数图，而不考虑变量的先后顺序。

●**vertical** 设定`dynsimpiecoef`在因变量类别的所有可能成对组合中为特定自变量创建系数图。使用此选项，`dynsimpiecoef`为每个自变量生成一组系数图，并将这些变量的系数图自动单独保存在工作目录中

●**angle()** 设定组合图形中x轴上标签的角度。仅在 **vertical** 选项使用时 **angle()** 选项才有效。默认值为 **angle(90)** ，这意味者所有标签都与x轴呈90度角。

●**killtable** 禁止自动生成SUR结果。默认情况下，结果中会显示估计表格。

## 4 Stata 实操示例

在本文中一共要用到六个命令，输入以下代码以获取六个命令

```Stata
net install st0448_1, from(http://www.stata-journal.com/software/sj20-3)
ssc install clarify,replace
ssc install coefplot,replace
```

`dynsimpie`命令允许通过 LDV 模型规范来估计解释变量对合成结果的影响，该规范被称为SURs系统。`dynsimpie`命令允许用户决定因变量和自变量的滞后项，以及选择的二分变量和连续冲击变量之间的乘法交互作用。为了说明`dynsimpie`命令的功能，我们使用与 Philips、Rutherford 和 Whitten（2016b，a）的每月英国政党支持数据。这些数据来自2004-2010年英国工党执政期间，工党领袖和首相从布莱尔（Tony Blair）到布朗（Gordon Brown）的更迭。在此期间，三个主要政党是工党、保守党和自由民主党。在我们的例子中，对这三个主要政党的支持率是我们的因变量。

假设我们有兴趣估计在控制认同首相政党的受访者百分比情况下，平均国民经济回顾性评估中单位标准差增加对英国政党支持率的影响。进一步设想，我们有理由相信，前一个时期的政党构成对当前时期存在影响。此外，我们可以预计布朗和布莱尔的任期会不同。

`dynsimpie`命令可以使用乘法交互和 LDV 模型来很好地建模，按照代码所示，**intype(on)**，所以交互变量被假定为1（Brown是幂）。**killtable** 选项可以从 `Stata` 的结果中省略回归表。在此后，我们使用`cfbplot` 命令来生成一个基线变化图。

使用`net get`命令获取数据;或者访问[此网站][http://fmwww.bc.edu/repec/bocode/u/UK_AJPS.dta]获取数据。

```Stata
net get st0448_1, from(http://www.stata-journal.com/software/sj20-3)
use uk_ajps.dta
dynsimpie all_pidW, dvs(Lab Ldm Con) ar(1) shockvars(all_nat_retW) >shockvals(0.4882241) interaction(brown) time(5) range(20) 
>intype(on) sigs(90 95) killtable
```

结果如下(路径不需保持一致，以自己电脑的路径为准)

```Stata
(note: file C:\Users\67203\AppData\Local\Temp\ST_4a54_000001.tmp not found)
file C:\Users\67203\AppData\Local\Temp\ST_4a54_000001.tmp saved
number=2, sig =90, sig2=95 
(note: file C:\Users\67203\AppData\Local\Temp\ST_4a54_000002.tmp not found)
file C:\Users\67203\AppData\Local\Temp\ST_4a54_000002.tmp saved
number of observations (_N) was 0, now 100
(100 real changes made)
(100 real changes made)

Simulation in Progress...please wait (100)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5 
.................................................    50
..................................................   100
(note: j = 1 2 3)

Data                               wide   ->   long
-----------------------------------------------------------------------------
Number of obs.                        1   ->       3
Number of variables                  25   ->      10
j variable (3 values)                     ->   sort
xij variables:
var_pie_ll_sr1_1 var_pie_ll_sr1_2 var_pie_ll_sr1_3->var_pie_ll_sr1_
var_pie_ul_sr1_1 var_pie_ul_sr1_2 var_pie_ul_sr1_3->var_pie_ul_sr1_
var_pie_ll_lr1_1 var_pie_ll_lr1_2 var_pie_ll_lr1_3->var_pie_ll_lr1_
var_pie_ul_lr1_1 var_pie_ul_lr1_2 var_pie_ul_lr1_3->var_pie_ul_lr1_
var_pie_ll_sr2_1 var_pie_ll_sr2_2 var_pie_ll_sr2_3->var_pie_ll_sr2_
var_pie_ul_sr2_1 var_pie_ul_sr2_2 var_pie_ul_sr2_3->var_pie_ul_sr2_
var_pie_ll_lr2_1 var_pie_ll_lr2_2 var_pie_ll_lr2_3->var_pie_ll_lr2_
var_pie_ul_lr2_1 var_pie_ul_lr2_2 var_pie_ul_lr2_3->var_pie_ul_lr2_
-----------------------------------------------------------------------------
```

输入`cfbplot`和`effectsplot`命令以绘制图片

文字结果如下

```Stata
. cfbplot

. effectsplot

(note:  named style med not found in class symbolsize, default attributes used)
(note:  named style med not found in class symbolsize, default attributes used)
(note: file graph.gph not found)
(file graph.gph saved)
```

图片结果可以参考以下[论文](http://sci-hub.ren/10.1177/1536867X20953570)。

## 参考资料

- Jung, Y. S., F. D. S. Souza, A. Q. Philips, A. Rutherford, and G. D.  Whitten. 2020. A command to estimate and interpret models of dynamic compositional dependent variables: New features for dynsimpie.  Stata Journal 20: 584-603. [PDF](http://sci-hub.ren/10.1177/1536867X20953570).
- Philips, A. Q., A. Rutherford, and G. D. Whitten. 2016. dynsimpie: A command to examine dynamic compositional dependent variables.  Stata Journal 16: 662-677. 
[PDF](http://sci-hub.ren/10.1177/1536867X1601600307).
- Philips, A. Q., A. Rutherford, and G. D. Whitten. 2016. Dynamic pie: A strategy for modeling trade-offs in compositional variables over time. American Journal of Political Science 60: 268-283. [PDF](http://sci-hub.ren/10.1111/ajps.12204).
- `dynsimpie`命令[help文档][http://fmwww.bc.edu/repec/bocode/d/dynsimpie.hlp]