# 退休消费之谜：来自断点回归的证据


> **论文作者：** Erich Battistin, Agar Brugiavini, Enrico Rettore, and Guglielmo Weber  
> **推文作者：** 陈俊睿 (中山大学)        
> **E-mail:**  <512979356@qq.com>   


[TOC] 

## 前言
诺贝尔获奖者 Modigliani 的生命周期假说认为，理性的消费者会根据一生的的收入，利用储蓄与借贷平滑消费，使一生的消费效用最大化。Modigliani 建立了总消费函数，认为当期消费与现期收入、未来收入和现期财产有关。因此，消费者在退休前后的消费行为应该是平滑的。


![生命周期基说示意图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E5%81%87%E8%AF%B4.jpg)
￼
然而实际情况却并非如此：多位学者在研究中都观察到人们在退休时还是会有突然性的消费下降，学者们将这一突变称为“退休消费谜题”（The Retirement Consumption Puzzle）。文献中提到这一突然下降的原因有如下：休闲时间增加导致的偏好变化、养老金数额比预料中低、消费者短视导致预期失误等。

本文为这一现象补充了证据，并提出新的见解。本文研究发现，退休后的意大利家庭非耐用消费支出下降 9.8% ，食物消费支出下降 14.1%，主要原因在于退休后工作相关支出降低和休闲增加的替代作用。

## 一、 文章简介

这是一篇 2009 年发表在 American Economic Review 上的一篇文章，利用意大利 1993 - 2004 年的微观数据，基于工具变量 (IV) /全局非线性断点回归(RDD)估计法，验证了退休消费谜题的存在。文章发现：退休后家庭非耐用消费支出下降 9.8%，食物消费支出下降 14.1%。

在这基础上,本文验证了本文结论与 Modigliani 的生命周期假说并不矛盾，消费的下降可能是休闲时间增加的结果。

本文探究造成消费下降的其他原因，发现非耐用品下降的原因是与工作相关的支出商品下支出的降，并且这一结论不受家庭收入、家庭规模等因素的影响。

## 二、 理论模型和数据设定
### 1.研究背景

退休是指根据国家有关规定，劳动者因年老而退出工作岗位。许多国家都建立了完备的养老金制度以支持劳动者退休后的日常开支。在意大利，养老金类似于固定债券的收益，每月领取固定的养老金，养老金金额自动与终身平均收入挂钩。当劳动者退休时，劳动者还将获得一大笔一次性支付的补贴收入。

在意大利，一旦劳动者达到了退休的资格，劳动者有权决定自己退休还是继续工作。工人满足以下两个条件之一即可获得退休资格：
1. 满足一定**年龄要求**；
2. 累计缴纳退休金达到一定**年限**。

具体细则如下图所示：

![意大利退休要求](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E6%84%8F%E5%A4%A7%E5%88%A9%E9%80%80%E4%BC%91%E8%A6%81%E6%B1%82.png)


### 2. 模型建立
定义 $Y_{1}$ 和 $Y_{0}$ 分别表示家庭在退休和非退休状态下消费支出，利用对因果推理潜在结果方法。那么，令：

$$\beta=Y_{1}-Y_{0}$$

$\beta$ 衡量的就是退休行为对家庭消费变动因果性影响，本文解答“退休消费之谜”的关键。但是，在测度 $\beta$ 的过程中以下困难：
1. 数据 $Y_{1}$ 和 $Y_{0}$ 非同期，难以测度两者之差。
2. 退休并非强制性，退休的决定具有内生性。

因此，本文引入“退休资格”来解决内生性。“退休资格”的获取不会受到消费大小的影响，具有良好的外生性。因此，“退休资格”可以通过样本的年龄与养老金的缴费记录进行认定。

本文定义外生工具变量 “距离 $S$ ” ，表示职工距离获得“退休金领用资格”的时间长度。并设定哑变量 $Eligibility$ 表示是否具有退休资格，只有当 $S≥0$ 时，职工才具有退休资格（ $Eligibility=1$ ）；当 $S<0$ 时，职工并不具有法定退休资格（ $Eligibility=0$ ）。

需要注意的是，由于退休为非强制性，具备有资格退休并不一定意味着个人实际上已经退休。养老金资格的外生性也包含一个假设：如果没有退休，我们假定个人的消费行为将在是否具备养老金领用资格的临界值附近保持连续。因此，用具备退休资格人群的消费之差，除以平均的退休比例，可以得到退休行为对家庭消费变动因果性影响，有以下等式：

$$E\left\{\beta \mid R=1, S=0^{+}\right\}=\frac{E\left\{Y \mid S=0^{+}\right\}-E\left\{Y \mid S=0^{-}\right\}}{E\left\{R \mid S=0^{+}\right\}}$$

但是，在实际的数据中，存在部分测算中没有获得退休资格，但是自我报告已经退休的样本出现。本文假定数据收集中自我报告的结果不存在偏误，而在于退休金资格的认定存在偏误。本文采用污染采样模型，上述对模型做出了如下修正：

$$E\left\{\beta \mid R=1, S=0^{+}\right\}=\frac{E\left\{Y \mid S_{o b s}=0^{+}\right\}-E\left\{Y \mid S_{o b s}=0^{-}\right\}}{E\left\{R \mid S_{o b s}=0^{+}\right\}-E\left\{R \mid S_{o b s}=0^{-}\right\}}$$

根据上文的理论模型，本文建立了如下回归模型：

$$\text{(1) } Y_{s, t}=\beta_{0}+\beta_{1} R_{s, t}+\beta_{2} S+\beta_{3} S^{2}+\varepsilon_{s, t}$$

用家庭支出平均值（ $Y_{s,t}$ ）对 男性退休户主比例（ $R_{s,t}$ ）进行回归，控制$S$、$S^{2}$，并控制调查年度的固定效应。

由于，为了缓解回归的内生性，引入工具变量退休资格比例作为退休比例的代理变量：

$$\text{(2) } R_{s, t}=\gamma_{0}+\gamma_{1} Eligibility (S \geq 0)+\gamma_{2} S+\beta_{3} S^{2}+\nu_{s, t}$$


### 3. 数据来源与样本选择
使用了意大利银行 1993 年至 2004 年家庭收入和财富调查(the Bank of Italy Survey on Household Income and Wealth，下文简称 SHIW)的数据。这项调查同时收集了样本有关食品、非耐用消费品和家庭总支出的数据。

SHIW 的调查也收集了受访者的个人信息，包括当前或最近一份工作的信息、向公共退休养老金计划缴款的年限等。因为存在残疾、社会救济、征兵、学生等“非工作养老金领取者”的存在，导致工作与退休群体加总不为 100%。样本组成如下图所示：

![样本组成](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E6%A0%B7%E6%9C%AC%E7%BB%84%E6%88%90.png)

因为 SHIW 数据样本组成情况中，女性教育程度低，劳动力参与度低。因此，本文仅使用男性户主或单身男性的家庭样本。并剔除退休资格数据缺失的样本、非单身男性或男性户主的样本以及“非工作养老金领取者”的样本，并将断点回归带宽限定在获得退休资格前后 10 年内，得到如下图样本数据：

![样本筛选](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E6%A0%B7%E6%9C%AC%E7%AD%9B%E9%80%89.png)

SHIW 的调查共有6次，本文选取的带宽（band）为 10 ，剔除 $S=0$ 的样本，最终得到由 20 个 $S$ 值和 6 次调查产生的 120 个单元（cells），每一个单元包括在不同物品上的家庭平均消费支出（ $Y_{s,t}$ ）、平均退休家庭比例（ $R_{s,t}$ ）。


## 三、 实证结果
### 1. 核心结论：退休会导致非耐用品消费与食品消费的下降

本文对提出的理论模型进行回归，第一阶段的回归探究“**退休资格**”对于实际退休数量的影响。

$$\text{(2) } R_{s, t}=\gamma_{0}+\gamma_{1} Eligibility (S \geq 0)+\gamma_{2} S+\beta_{3} S^{2}+\nu_{s, t}$$

结果如下图所示：

```
First-stage regressions
-----------------------

      Source |       SS           df       MS      Number of obs   =       120
-------------+----------------------------------   F(8, 111)       =    166.40
       Model |  12.3994228         8  1.54992785   Prob > F        =    0.0000
    Residual |  1.03389312       111  .009314352   R-squared       =    0.9230
-------------+----------------------------------   Adj R-squared   =    0.9175
       Total |  13.4333159       119  .112885007   Root MSE        =    .09651

------------------------------------------------------------------------------
         R   |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
 Eligibility |   .4349947   .0380644    11.43   0.000     .3595675    .5104218
           S |   .0169943   .0030673     5.54   0.000     .0109162    .0230724
         S^2 |  -.0005738   .0002718    -2.11   0.037    -.0011123   -.0000353
    dum_1995 |   .0230442   .0305194     0.76   0.452     -.037432    .0835205
    dum_1998 |   .0508942   .0305194     1.67   0.098     -.009582    .1113705
    dum_2000 |   .1172279   .0305194     3.84   0.000     .0567517    .1777042
    dum_2002 |   .1400281   .0305194     4.59   0.000     .0795519    .2005044
    dum_2004 |   .1693907   .0305194     5.55   0.000     .1089144     .229867
       _cons |   .2082045   .0234373     8.88   0.000     .1617619    .2546471
------------------------------------------------------------------------------

```

从上表报告的第一阶段的回归结果可以看出，$R^{2}=0.92$，$Eligibility$ 的系数为 0.435，标准误为 0.038。表明在第一步回归中，退休资格对与退休状态的解释力度良好，表明许多人在获得退休资格以后立刻选择了退休。

图示结果如下：

![退休数量变化](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E9%80%80%E4%BC%91%E6%AF%94%E4%BE%8B%E6%96%AD%E7%82%B9%E5%9B%9E%E5%BD%92.jpg)
第二阶段的回归中，利用公式（1），检验退休对于消费相关性。

$$\text{(1) } Y_{s, t}=\beta_{0}+\beta_{1} R_{s, t}+\beta_{2} S+\beta_{3} S^{2}+\varepsilon_{s, t}$$

回归结果显示，退休后，非耐用品和食品的消费支出都有所下降。

具体来看，对于**非耐用品**的消费，退休以后非耐用品消费下降了 9.83%，在 90% 的置信水平上显著。

```
Instrumental variables (2SLS) regression        Number of obs     =        120
                                                F(8, 111)         =      38.50
                                                Prob > F          =     0.0000
                                                R-squared         =     0.6223
                                                Root MSE          =     .05977

------------------------------------------------------------------------------
             |               Robust
         mcn |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
           R |  -.0983277    .056666    -1.74   0.085    -.2106151    .0139597
           S |  -.0055121   .0026928    -2.05   0.043    -.0108481    -.000176
         S^2 |   -.000288   .0001508    -1.91   0.059    -.0005868    .0000107
    dum_1995 |   .0018884   .0186214     0.10   0.919    -.0350111    .0387879
    dum_1998 |  -.0334648   .0189046    -1.77   0.079    -.0709256     .003996
    dum_2000 |   .0121598   .0205777     0.59   0.556    -.0286163    .0529359
    dum_2002 |   .0210096   .0238977     0.88   0.381    -.0263453    .0683645
    dum_2004 |   .0843976   .0202403     4.17   0.000       .04429    .1245051
       _cons |    9.77691   .0254221   384.58   0.000     9.726534    9.827285
------------------------------------------------------------------------------
Instrumented:  pen
Instruments:   esse_m esse_m2 dum_1995 dum_1998 dum_2000 dum_2002 dum_2004
               elig
------------------------------------------------------------------------------

```

在回忆类问题中，非耐用品消费的回忆往往不够精确，信噪比更高，这可能是该回归dum_置信度只有90%的原因。( see Martin Browning, Thomas F. Crossley, and Weber (2003) for an appraisal )

有关非耐用品支出的断点回归示意图如下：

![非耐用品支出](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E9%9D%9E%E8%80%90%E7%94%A8%E5%93%81%E6%96%AD%E7%82%B9%E5%9B%9E%E5%BD%92.jpg)

在**食品支出**上，退休以后食品支出的总额下降了 14.09%，在 95% 的置信水平上显著。然而，食品支出既包括外出食品支出也包括家庭内食品支出，该数据并不能进一步说明食品支出下降的原因，让需要进一步的探索。

```
Instrumental variables (2SLS) regression        Number of obs     =        120
                                                F(8, 111)         =      37.59
                                                Prob > F          =     0.0000
                                                R-squared         =     0.7124
                                                Root MSE          =     .05578

------------------------------------------------------------------------------
             |               Robust
          mf |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
           R |  -.1409689    .054418    -2.59   0.011    -.2488019   -.0331359
           S |  -.0027591   .0025764    -1.07   0.287    -.0078644    .0023462
         S^2 |  -.0000821   .0001409    -0.58   0.561    -.0003614    .0001971
    dum_1995 |  -.0675309   .0149879    -4.51   0.000    -.0972304   -.0378314
    dum_1998 |  -.1365082   .0161065    -8.48   0.000    -.1684242   -.1045922
    dum_2000 |    -.13855   .0184983    -7.49   0.000    -.1752056   -.1018944
    dum_2002 |  -.1420846   .0184317    -7.71   0.000    -.1786083    -.105561
    dum_2004 |  -.1042948   .0186887    -5.58   0.000    -.1413277    -.067262
       _cons |   6.160132   .0232825   264.58   0.000     6.113996    6.206268
------------------------------------------------------------------------------
Instrumented:  pen
Instruments:   esse_m esse_m2 dum_1995 dum_1998 dum_2000 dum_2002 dum_2004
               elig
------------------------------------------------------------------------------

```

有关食品支出的断点回归示意图如下：

![食品支出](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E9%A3%9F%E5%93%81%E6%96%AD%E7%82%B9%E5%9B%9E%E5%BD%92.jpg)

本文还对工具变量的外生性进行了过度识别检验，结果显示工具变量的外生性良好。本文的核心结论具有稳健性。



### 2. 进一步探究

#### 2.1 是否与生命周期假说矛盾？No！

前文提到，生命周期假说认为理性经济人会平滑一生的消费，以达到效用最大化。但是，退休后消费支出的突然下降似乎与生命周期假说矛盾。

因此，本文运用Cobb-Douglas组合效用函数，验证本文结论是否与生命周期假说矛盾，建立如下模型：

$$U=\frac{\left(C^{\alpha} l^{1-\alpha}\right)^{1-\gamma}}{1-\gamma}$$

其中，C 代表非耐用品的消费（Consumption），l 指休闲消费支出（leisure），$\gamma$ 是跨期替代弹性（EIS）的倒数。该模型不考虑不确定性。

经过分析可知，当 $\gamma>1$ 时，消费与休闲相互替代，消费下降可作为休闲增加的结果。而依据本文的数据，退休带来的是休闲时间的大幅上涨。本文利用经验数据，估算出 $\alpha=0.44$ ，$\gamma=1.19$，$\gamma$ 的标准差为 0.12 。

因此，本文验证本文的核心结论与生命周期假说不矛盾，消费下降可以作为休闲时间增加的替代结果。

#### 2.2 贫困带来的流动性困境限制？No！

在一些有影响力的论文中发现，相对贫穷的人群退休后消费下降的幅度最大，因此提出：消费后支出的下降原因可能是退休者为老年的储蓄不足的短视行为引起。

但是本文认为在意大利这种情况不太可能发生。因为意大利的养老金系统会在工人退休以后会，支付给退休者**一大笔一次性支付退休金**。从直觉上来说，贫穷与短视的行为不可能成为消费下降的原因，相反，一大笔退休金的领用甚至可能加大消费支出。

本文也建立了实证模型进行研究。本文利用 SHIW 中的家庭财富数据，并用教育水平、家庭规模、是否夫妻等变量对样本的家庭财富数据进行预测以补足缺失值。然后，选择预测财富值中**较低的三分之一**作为贫穷子样本进行研究。研究模式与前文核心结论部分一致，采用全局非线性回归，研究获得退休金领用资格对消费下降的因果性影响。

尽管第一阶段的拟合效果优异（$R^{2}=0.935$，Eligibility 的系数为 0.407 ，方差为 0.0382 ），但是第二阶段的结果效果极差。对非耐用品的消费影响不显著不为0，对食品消费的影响系数为 -2.7% ，标准差为 0.088 。

本文对这一结果给出了解释：其一，工人在退休时能收到一大笔退休金，缓解了贫穷的影响。其二，贫穷子样本的群体多为蓝领，他们在工作时多使用政府补贴的交通工具上下班、在工厂免费的食堂吃饭、有免费的工作服。而白领则相反，开车上班、多在酒吧和餐馆吃饭、买昂贵的服装满足工作场合需要。相对来说，退休对白领的影响要更大。


#### 2.3 家庭规模变化导致？No！

受限于意大利的特殊国情，意大利许多成年人因为没有找到稳定的工作或没有足够财力买房，往往会选择与父母同住。而前文提到，工人退休时会收到一大笔退休金，有研究表明，很多家庭利用这一退休金为儿女购置房产，帮助子女独立生活。因此，退休的决定和子女离开家庭的决定可能共同受到是否具有“退休资格”影响。

如果退休消费下降是因为家庭规模缩小，那么消费下降并不会带来效用下降。为了排除家庭规模的影响，本文对家庭消费进行修正，常见的方式是以人均消费进行回归以修正偏误；进一步考虑家庭人数增多带来的规模经济效应，将消费除以家庭成员数量的平方根来衡量消费带来的效用更具有解释力。结果如下图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E5%AE%B6%E5%BA%AD%E8%A7%84%E6%A8%A1%E4%BF%AE%E6%AD%A3%E5%9B%9E%E5%BD%92.png)

该图上面板表示“退休资格”对于家庭规模的一系列影响，可以看出户主获得退休资格以后，家庭规模与家庭中成年孩子的数量显著减少，而对于年幼孩子的数量、夫妻比例没有显著影响。

中面板与下面版分别是按家庭人数与按家庭人数的平方根进行修正的消费支出。本文认为考虑规模经济的修正更有解释意义，因此证明退休将导致非耐用型商品消费下降 4.1% (不显著)，食物支出下降 8.4% (临界显著)。

#### 2.4 与工作有关费用支出减少有关？Yes！

前文的回归验证了退休将导致非耐用品与食品消费支出下降，但是受限于 SHIW 数据的可得性，非耐用品与食品支出的含义比较模糊，没有更为详细的支出信息。特别是消费支出，没有区分外出消费和家庭内消费。

本文从 ISTAT 获得了 2002 年包含不同样本家庭消费支出的日级数据。这份数据虽然有当前就业、家庭组成、住所大小等信息，但是缺少有关养老金领取资格的数据，无法识别样本领用养老金的资格，不能应用前文的回归模型。

因此，本文应用了分组检验的模式，比较退休与非退休家庭支出上的差别：将样本分成两组，一组年龄在 50-54 岁之间，大多数有工作；另一组年龄在 65-69 岁之间，大多数都是退休人员。如下图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%99%88%E4%BF%8A%E7%9D%BF_RDD%E8%AE%BA%E6%96%87%E5%A4%8D%E7%8E%B0_%E7%BB%86%E5%88%86%E6%94%AF%E5%87%BA%E5%88%86%E7%BB%84%E6%A3%80%E9%AA%8C.png)


该表第一列列示了不同种类的消费支出，第二、三列是两组的直接比较，第四、五列考虑了不同因素的影响进行修正，包括居住的地区、家庭内等效成年人数量、住宅面积。在第四、五列的调整中，本文认为所有的孩子都已经离开了父母家庭，而第六、七列认为孩子并未全部离开，因此第六、七列的比较结果根据四五列对比的结果再将 65-69 岁组的家庭比家里增加 1/4 个等效成年人进行调整。

分组检验显示，60 岁晚期群体比 50 岁早期群体的非耐用品消费下降了 510 欧元每月。而其中，外出食品支出、衣服之处、交通支出下降的幅度最大。这三样是典型的于工作相关的消费支出。家庭内的食物支出的直接比较虽然下降剧烈，但是在经过人均修正后，内部食物支出减少的幅度较小。

从这个分组建议检验中可以得出结论，退休消费下降完全可能是由于与工作相关支出的减少，以及从市场商品到家庭生产食品的替代。

## 四、总结

本文使用了1993年至2004年期间的食品和总非耐用家庭支出的微观数据，调查了意大利退休时消费下降的规模。并采用断点回归的方法，通过利用养老金资格的外生性来修正退休决策的内生性，发现有相当多的一部分个体一旦符合资格后就会退休，并估计出因符合资格而退休导致的非耐久消费下降比例为9.8%。本文验证了这种下降是由与工作相关的支出减少或休闲替代增加所致。本文也表明，退休导致与父母同住的成年子女数量显著下降也是退休消费下降的主要原因。


## 五、相关研究

引用本文的后续相关研究主要集中于生命周期理论的讨论，主要争论点在于：退休消费之谜是否存在与退休消费之谜的原因探寻。也有部分研究围绕着生命周期理论，研究退休政策的设计等。

相关文献有：

```
Aguiar, Mark, and Erik Hurst. “Lifecycle Prices and Production.” The American Economic Review, vol. 97, no. 5, 2007, pp. 1533–1559.

Attanasio, Orazio P., and Guglielmo Weber. “Consumption and Saving: Models of Intertemporal Allocation and Their Implications for Public Policy.” Journal of Economic Literature, vol. 48, no. 3, 2010, pp. 693–751.

Aguiar, Mark, and Erik Hurst. “Deconstructing Life Cycle Expenditure.” Journal of Political Economy, vol. 121, no. 3, 2013, pp. 437–492.

Haider, Steven J., and Melvin Stephens. “Is There a Retirement-Consumption Puzzle? Evidence Using Subjective Retirement Expectations.” The Review of Economics and Statistics, vol. 89, no. 2, 2007, pp. 247–264.

Attanasio, Orazio, et al. “Changes in Consumption at Retirement: Evidence from Panel Data.” The Review of Economics and Statistics, vol. 93, no. 3, 2011, pp. 1094–1099.

Dong, Yingying, and Arthur Lewbel. “Identifying the Effect of Changing the Policy Threshold in Regression Discontinuity Models.” The Review of Economics and Statistics, vol. 97, no. 5, 2015, pp. 1081–1092.

Angelini, Viola, et al. “Ageing and Unused Capacity in Europe: Is There an Early Retirement Trap?” Economic Policy, vol. 24, no. 59, 2009, pp. 463–508.

Chai, Jingjing, et al. “Optimal Portfolio Choice over the Life-Cycle with Flexible Work, Endogenous Retirement, and Lifetime Payouts.” Review of Finance, vol. 15, no. 4, 2011, pp. 875–907.
```

RDD选取特征值相近的不同样本，考虑特征值在临界变化点对其他变量的影响。

相比DID，RDD不需要面板数据，在涉及到没有对照样本的数据时，RDD可以广泛应用。且和其他方法相比, 学术界普遍认为运用断点回归设计更接近准自然实验, 估计的结果更加准确。

运用了RDD的相关文献具体有以下：

```
Stancanelli, Elena G. F., and Arthur van Soest. “Retirement and Home Production: A Regression Discontinuity Approach.” The American Economic Review, vol. 102, no. 3, 2012, pp. 600–605.

Boomhower, Judson, and Lucas W. Davis. “A Credible Approach for Measuring Inframarginal Participation in Energy Efficiency Programs.” Journal of Public Economics, vol. 113, 2014, pp. 67–79.

Attanasio, Orazio, et al. “Changes in Consumption at Retirement: Evidence from Panel Data.” The Review of Economics and Statistics, vol. 93, no. 3, 2011, pp. 1094–1099.

Dong, Yingying. “Regression Discontinuity Applications with Rounding Errors in the Running Variable.” Journal of Applied Econometrics, vol. 30, no. 3, 2015, pp. 422–446.

Dong, Yingying. “Jumpy or Kinky? Regression Discontinuity without the Discontinuity.” MPRA Paper, 2010.

Barbanchon, Thomas Le. “The Effect of the Potential Duration of Unemployment Benefits on Unemployment Exits to Work and Match Quality in France.” Labour Economics, vol. 42, 2016, pp. 16–29.

Brauw, Alan de, and Daniel Gilligan. Using the Regression Discontinuity Design with Implicit Partitions: The Impacts of Comunidades Solidarias Rurales on Schooling in El Salvador. 2011.

Bernal, Noelia, et al. “The Effects of Access to Health Insurance: Evidence from a Regression Discontinuity Design in Peru.” Journal of Public Economics, vol. 154, 2017, pp. 122–136.

```

RDD特别适用于**衡量连续变量在临界值导致资格变化**的场景，本文养老金缴纳年龄是很好的一个案例。除此之外，还可以运用于以下场景：高考分数导致是否能进入某所大学的影响，年龄导致的升迁资格，工作年限导致的落户政策。Brauw 也在他的论文中探讨了多个变量确定某一资格的情况下，如何继续使用 RDD ，甚至是使用隐式的权重来判定是否具备某项资格（2011）。

引用了本文为参考文献的后续研究中。此类文献的主要研究内容还是有关退休消费之谜，验证消费等变量在消费前后的突变。

部分文献将RDD的应用于政策效果的评估。Boomhower 用 RDD 检验了能源补贴在住宅节能项目（residential energy-efficiency program）上的效率性，发现大约一半的参与者会在没有补贴的情况下采用这项技术，且即使补贴金额低得多，大多数家庭也会参加，证明了对节能项目的补贴是非必需的。Dong 将 RDD 应用于评估美国医疗保险制度对保险覆盖范围的影响，用中国的强制退休政策研究中国的退休消费谜题。Bernal 用 RDD 评价秘鲁在 21 世纪初进行的健康保险制度改革。

## 六、参考文献

- Battistin, Erich, et al. “The Retirement Consumption Puzzle: Evidence from a Regression Discontinuity Approach.” The American Economic Review, vol. 99, no. 5, 2008, pp. 2209–2226. [PDF](https://www.sci-hub.ren/10.1257/aer.99.5.2209#)


- Imbens, Guido W., and Thomas Lemieux. “Regression Discontinuity Designs: A Guide to Practice.” Journal of Econometrics, vol. 142, no. 2, 2008, pp. 615–635.

- Heckman, James J., et al. “The Economics and Econometrics of Active Labor Market Programs.” Handbook of Labor Economics, vol. 3, no. 1, 1999, pp. 1865–2097.

- Bansal, Ravi, and Amir Yaron. “Risks for the Long Run: A Potential Resolution of Asset Pricing Puzzles.” Journal of Finance, vol. 59, no. 4, 2004, pp. 1481–1509.


