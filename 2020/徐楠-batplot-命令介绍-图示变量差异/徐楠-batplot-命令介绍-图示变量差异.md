&emsp;

> **作者：** 徐楠(中山大学)        
> **E-mail:** xunan3@mail2.sysu.edu.cn

&emsp;

---

**目录**

[TOC]

---



## 1. 背景介绍

### 1.1 一致性分析

在临床测量中，常常面临比较两种检测方法的问题，比如A是经过验证的“金方法“，B是发现的新方法，那么A、B两种方法的检测效果是否一致？

因此通常需要将一种新的测量技术与一种已建立的测量技术进行比较，以确定新技术是否能够取代旧技术。如果新方法与旧方法得到的结果完全一致，就可以用新方法替代旧方法。但由于客观因素的限制，实践中两种方法的结果不可能完全相同，因此需要借助方法来评估两种方法一致的程度，并结合特定标准判定两种方法的一致性。



### 1.2 Bland-Altman 法

1986年，Bland JM 和 Altman DG 首次提出了 Bland-Altman 分析，它的基本思路是计算出两种测量结果的一致性界限，并用图形直观地反映一致性界限及两种方法测量差距的分布情况，最后结合临床实际经验，分析两种测量方法是否具有一致性。



* 一致性界限

  在进行两种方法的测定时，通常是对同一批受试对象同时进行测量。这两种方法一般不会获得完全相同的结果，而总是存在着一定趋势的差异。比如，一种方法的测量结果经常大于（或小于）另一种方法的结果，这种差异被称为**偏倚（bias)**。

  Bland-Altman 使用偏倚来刻画两种方法的一致性。偏倚可以用两种方法测定结果的差值的均数 $\overline{d}$ 进行估计， $\overline{d}$ 的变异情况则用差值的标准差$S_d$ 来描述。如果差值的分布服从正态分布，则95%的差值应该位于
  $$
  [\overline{d}-1.96S_d, \overline{d}+1.96S_d]
  $$
  之间。我们称这个区间为95%的一致性界限（95% limits of agreement, 95% LoA），绝大多数差值都位于该区间内。如果两种测量结果的差异位于一致性界限内在临床上是可以接受的，则可以认为这两种方法具有较好的一致性，即两种方法可以互换使用。

  

* Bland-Altman 图

  在二维直角坐标系中绘制 Bland-Altman 图，横轴表示两种方法测量每个对象的平均值，纵轴对应两种方法测量每个对象的差值，同时借助直线表示差值均值、95%一致性界限等信息。如下图所示，上下两条水平实线代表95%一致性界限的上下临界值，虚线带代表差值均数为0，实线代表差值的均数。

    <img src="http://pumpkinnnnnn.gitee.io/final-of-2020-empirical-finance/batplot命令介绍_Fig1_BA图示例_徐楠.png" alt="Fig1 BA图示例" style="zoom:20%;" />

  

  **两种测量方法的一致性程度越高，代表差值均数的实线越接近代表差值均数为0的虚线。** 根据一致性界限内的最大差值及一致性界限外的数据比例，结合临床上的可接受程度，通过 Bland-Altman 图可以对两种方法的一致性做出评价。



* 应用 Bland-Altman 法的一致性评价

  Bland JM 和 Altman DG 在《国际护理研究杂志》发表的 [*评估两种临床测量方法之间一致性的统计方法*](https://www.sciencedirect.com/science/article/pii/S0020748909003204) 一文中提供了显示良好一致性的例子（图：氧饱和度检测仪和脉冲血氧饱和度仪测量的氧饱和度的比较）。

  

    <img src="http://pumpkinnnnnn.gitee.io/final-of-2020-empirical-finance/batplot命令介绍_Fig2_一致性评价示例_徐楠.png" alt="Fig2 一致性评价示例" style="zoom:100%;" />

  

  

  Bland-Altman 图显示，两种方法的平均差异是 0.42 个百分点，95% 置信区间为 0.13-0.70。尽管脉冲血氧计的饱和度往往给出一个较低的读数，但一致性界限的临界值（-2% 和 2.8% ）足够小，因此结合临床经验，得出新方法可以用于代替旧方法的一致性评价结论。



* Bland-Altman 图的趋势

  在前面的分析中，假定在测量范围内，差值没有以任何系统的方式变化，但事实可能并非如此，差值的大小与分散度都可能发生变化。本文主要介绍的 batplot 命令较前文的 Bland-Altman 图改变了差值均值的衡量方法，使用两种方法差值的趋势线取代所有数据均值的水平线，以刻画差值大小在测量范围内的系统性变化（如下图所示）。

  

  <img src="http://pumpkinnnnnn.gitee.io/final-of-2020-empirical-finance/batplot命令介绍_Fig3_Bland-Altman 图的趋势_徐楠.png" alt="Fig3 Bland-Altman 图的趋势" style="zoom:80%;" />





## 2. batplot 命令解析

**```batplot``` 命令生成根据趋势调整的 Bland-Altman 图。其中趋势由均值与差值均值的线形回归拟合得出。根据趋势调整对于规模差异较大的两变量的一致性分析尤为重要。**



### 2.1 语法介绍

```Stata
 batplot varname1 varname2 [if] [in] [, options]
```

```batplot``` 命令后输入用于一致性分析的两个变量名，并选择实现功能。



### 2.2 batplot 选项详解

```batplot``` 命令提供选项列表如下：

| 选项                       | 功能介绍                                   |
| -------------------------- | ------------------------------------------ |
| info                       | 以副标题显示 Bland-Altman图的基本信息      |
| <u>val</u>abel (*varname*) | 对超出一致性范围的点使用变量名称进行标记   |
| shading (min max)          | 指定着色阴影范围                           |
| notrend                    | 指定原始的Bland Altman绘图（没有趋势）绘制 |
| moptions                   | 为位于一致性范围之外的标记点指定展示选项   |
| <u>sc</u>atter             | 指定最终绘图的散点部分的展示选项           |
| twoway_options             | 支持二维图绘制功能，如标题、标签           |



以下命令介绍基于Stata系统自带的 ```auto.dta``` 数据集进行展示，方便读者理解。

```Stata
sysuse auto, clear
```

其中：

```mpg``` 表示行驶的里程数(Mileage)

```turn``` 表示轮胎转动圈数(Turn Circle)

```make``` 表示汽车生产厂商



#### （1）batplot 最简形式

* 最简命令的格式仅需输入用于一致性分析的两个变量名即可：
* 呈现结果的图中展示横纵坐标轴、一致性界限（阴影）、差值的趋势线。

```Stata
batplot mpg turn
```



<img src="http://pumpkinnnnnn.gitee.io/final-of-2020-empirical-finance/batplot命令介绍_Fig4_batplot最简形式_徐楠.png" alt="Fig4 batplot最简形式" style="zoom:80%;" />



#### （2）info 选项使用

* ```info``` 选项帮助用户在图表上以文字形式清晰呈现一致性分析的结果。
* 使用 ```info``` 选项时，在图表副标题处将提供 Bland-Altman 图的基本信息，包括差值均值、一致性限制、超出一致性限制区间的比例等。注意：同时使用 ```notrend``` 选项时副标题稍有不同，较不使用 ```notrend``` 选项时在结果窗口新增 Bland-Altman 图的基本信息展示。

```Stata
 batplot mpg turn, info
 batplot mpg turn, info notrend
 /*结果窗口展示：
 Mean difference     = -18.35135135135135
 Limits of agreement = (-36.88690235522478,.1841996525220715)
 Averages lie between  26.000 and 38.000
 */
```



#### （3）valabel 选项使用

* ```valabel``` 选项帮助用户在图表上呈现异常值的信息，方便后续查找分析。
* 使用```valabel(varname)``` 选项时，可以对超出一致性限制范围的点进行标记，标记内容为用户输入的对应变量名称 ```varname ```。

```Stata
 batplot mpg turn, valabel(make) // 使用 make 变量名称标记异常值
```



<img src="http://pumpkinnnnnn.gitee.io/final-of-2020-empirical-finance/batplot命令介绍_Fig5_valabel_徐楠.png" alt="Fig5 valabel" style="zoom:80%;" />



#### （4）shading 选项使用

* ```shading``` 选项指定阴影范围。默认情况下，着色的限制由 xlabel 选项中的值确定。



#### （5）notrend 选项使用

* ```notrend``` 选项实现不含趋势的 Bland-Altman 图的呈现。

```Stata
 batplot mpg turn, notrend
```



<img src="http://pumpkinnnnnn.gitee.io/final-of-2020-empirical-finance/batplot命令介绍_Fig6_notrend_徐楠.png" alt="Fig6 notrend" style="zoom:80%;" />



#### （6）moptions 选项使用

* ```moptions``` 选项为标记点提供更美观的展示形式（注：需要与 ```valabel``` 选项一同使用）。
* ```moptions``` 选项指定位于一致性界限之外的点的标记形式，支持 ```scatter##marker_options``` 中的选项功能，可以 ```help scatter##marker_options``` 进行查看。

```Stata
batplot mpg turn, valabel(make) moptions(mlabp(9))
// 将标记点变量名移至标记点左侧，使得变量名不超出图表展示范围
```



#### （7）scatter 选项使用

* ```scatter``` 选项为散点部分提供展示形式选择，可以```help scatter ```进行查看。



#### （8）二维图选项使用

* 支持二维图表绘图功能，详情见 ```help twoway_options``` 。

```Stata
batplot mpg turn, title(Agreement between mpg and turn) xlab(26(4)38)
// 增加图表标题，并规定 x 轴绘制范围与刻度
```





## 3. Stata 实操

### 3.1 安装命令

```Stata
ssc install batplot, replace
```



### 3.2 应用实例

* 获取数据

  以下数据来源为 *Bland-Altman分析在临床测量方法一致性评价中的应用*（陈卉, 2007），在测量16名检查者心功能指标 EDV 值时，分别用多次屏气电影法 M RI (mEDV) 和单次屏气电影法 M RI (sEDV) 测量的数据如下所示：

| examiner_number |  medv  |  sedv  |
| :-------------: | :----: | :----: |
|        1        | 123.25 | 86.32  |
|        2        | 126.83 | 136.65 |
|        3        |  79.7  | 73.33  |
|        4        | 129.23 | 133.19 |
|        5        | 110.23 | 119.34 |
|        6        | 116.53 | 101.29 |
|        7        | 88.35  |  88.4  |
|        8        | 90.04  | 113.25 |
|        9        | 129.32 | 131.44 |
|       10        | 211.44 | 210.5  |
|       11        | 142.93 | 124.82 |
|       12        | 135.41 | 112.81 |
|       13        | 135.63 | 139.93 |
|       14        | 92.58  | 92.13  |
|       15        | 75.82  | 77.04  |
|       16        |  93.9  | 90.14  |





* Bland-Altman 图绘制

```
batplot medv sedv, title(Agreement between medv and sedv) info valabel(examiner_number) xlab(100(50)200)
```

运行结果如下图：

<img src="http://pumpkinnnnnn.gitee.io/final-of-2020-empirical-finance/batplot命令介绍_Fig7_实操_徐楠.png" alt="Fig7 实操" style="zoom:80%;" />





## 参考文献

* Altman B D G . Statistical methods for assessing agreement between two methods of clinical measurement[J]. International Journal of Nursing Studies, 2010.  [-Link-](https://www.sciencedirect.com/science/article/pii/S0020748909003204)

* 陈卉.Bland-Altman分析在临床测量方法一致性评价中的应用[J].中国卫生统计,2007(03):308-309+315.  [-Link-](https://kns.cnki.net/KXReader/Detail?TIMESTAMP=637476166962278750&DBCODE=CJFD&TABLEName=CJFD2007&FileName=ZGWT200703035&RESULT=1&SIGN=mWkiw5zgKj9YILCleB%2fvNMckAZE%3d)

