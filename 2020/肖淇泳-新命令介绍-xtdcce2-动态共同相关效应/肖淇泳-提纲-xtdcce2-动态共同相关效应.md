# xtdcce2—动态共同相关效应

&emsp;

> **作者：** 肖淇泳 (中山大学)  
> **邮箱：** <xiaoqy25@mail2.sysu.edu.cn>
> **Source：** Jan Ditzen, 2018, Estimating Dynamic Common-Correlated Effects in Stata, Stata Journal, 18(3): 585–617. [-PDF-](https://www.sci-hub.ren/10.1177/1536867x1801800306)

&emsp;

**1.引言**

在本篇推文中，我们首先会对截面相关及其解决方法进行简单介绍，之后重点阐述解决方法中的共同相关效应模型和动态共同相关效应模型，并对用于检验截面相关的 CD 检验进行简要说明。我们还将介绍相应的 Stata 命令 xtdcce2, 最后给出具体的应用实例。

**2.理论介绍**

- 截面相关 (Cross-sectional Dependence)

- 共同因子模型 (Common Factor Model)

- 共同相关效应（Common Correlated Effects)
  
- 动态共同相关效应 (Dynamic Common Correlated Effects)

**3. xtdcce2 命令介绍**

- 基本介绍

- 安装方法

- 语法结构

**4. Stata 实例**

- 数据结构

- 共同相关效应估计

- 动态共同相关效应估计

**5. 总结**

**6.参考文献**

**7.相关推文**