### 动态共同相关效应模型 
- `xtdcce2` https://gitee.com/arlionn/xtdcce2 // 更新日期 2020/10/26 14:08
- Jan Ditzen, 2018, Estimating Dynamic Common-Correlated Effects in Stata, Stata Journal, 18(3): 585–617. [[PDF\]](https://sci-hub.tw/10.1177/1536867X1801800306)，[[-幻灯片-]](https://www.stata.com/meeting/uk16/slides/ditzen_uk16.pdf)。

基于幻灯片确定推文的结构吧。 

- `help xtdcce` //Estimating Dynamic Common Correlated Effects in Stata

应用该模型的文献都比较新，参见 [xtdcce 应用文献](https://academic.microsoft.com/paper/2508039176/citedby/search?q=xtdcce%3A%20Estimating%20Dynamic%20Common%20Correlated%20Effects%20in%20Stata&qe=RId%253D2508039176&f=&orderBy=0)，可以参考这些文章作一篇实证分析论文。如有好的想法，我们可以一起讨论。 

## 参考文献

- Chudik, A., and M. H. Pesaran. 2015. Large panel data models with cross-sectional dependence: A survey. In The Oxford Handbook of Panel Data, ed. B. H. Baltagi, 3–45. New York: Oxford University Press.
- Pesaran, M. 2006. Estimation and inference in large heterogeneous panels with a multifactor error structure. Econometrica 74: 967–1012.
- Pesaran, M.H., and R. Smith. 1995. Estimating long-run relationships from dynamic heterogeneous panels. Journal of Econometrics 68: 79–113.
- Shin, Y., M. H. Pesaran M.H., and R. P. Smith. 1999. Pooled mean group estimation of dynamic heterogeneous panels. Journal of the American Statistical Association 94: 621–634.