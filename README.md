## 实证金融 (Empirical Finance) 课程主页

<br>

## NEWs 
- `2022/11/11 18:03` [EF00-选题和课程报告格式要求](https://gitee.com/arlionn/EF/wikis/%E8%AF%BE%E7%A8%8B%E6%8A%A5%E5%91%8A/EF00-%E9%80%89%E9%A2%98%E5%92%8C%E8%AF%BE%E7%A8%8B%E6%8A%A5%E5%91%8A%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md) 已发布。

## 课件
- **课件下载：**
  - FTP 地址：<ftp://ftp.lingnan.sysu.edu.cn/> &rarr; [2022_实证金融_本科] 文件夹。
  - 账号 = 密码 = lianyjst
  - 请保护版权，未经本人同意，请勿散布于网络。
- **答疑：** 
  - 学习过程中的多数问题，都可以在 [**FAQs**](https://gitee.com/arlionn/PX/wikis/%E8%BF%9E%E7%8E%89%E5%90%9B-Stata%E5%AD%A6%E4%B9%A0%E5%92%8C%E5%90%AC%E8%AF%BE%E5%BB%BA%E8%AE%AE.md?sort_id=2662737) 找到答案。
  - 亦可访问 [连享会主页](https://www.lianxh.cn) 和 [连享会知乎](https://www.zhihu.com/people/arlionn/) 查阅相关推文。 
  - 推文搜索方式：在 Stata 命令窗口输入 `lianxh DID RDD` 即可呈现包含 **DID** 和 **RDD** 关键词的推文。

## 作业
- [【点击查看作业】](https://gitee.com/arlionn/EF/wikis/%E6%8F%90%E4%BA%A4%E6%96%B9%E5%BC%8F%E5%92%8C%E6%97%B6%E7%82%B9.md?sort_id=1628096) 
  - 第一次作业 [HW01](https://gitee.com/arlionn/EF/wikis/%E4%BD%9C%E4%B8%9A/HW01.md) 已发布。提交截止时间：`2022.9.30`
  - 第二次作业 [HW02](https://gitee.com/arlionn/EF/wikis/%E4%BD%9C%E4%B8%9A/HW02.md) 已发布。提交截止时间：`2022.10.07`
- **交作业入口**
  - &#x1F449; **【[点击提交](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】**，个人作业和期末课程报告均通过此处提交 
- Notes：
  - [1] 若需更新，只需将同名文件再次提交即可。
  - [2] 提交截止时间为 deadline 当日 23:59，逾期不候。


## 课程报告
> Update: `2022/11/11 17:54`  
- :apple:  [EF00-选题和课程报告格式要求](https://gitee.com/arlionn/EF/wikis/%E8%AF%BE%E7%A8%8B%E6%8A%A5%E5%91%8A/EF00-%E9%80%89%E9%A2%98%E5%92%8C%E8%AF%BE%E7%A8%8B%E6%8A%A5%E5%91%8A%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md)
-  课程报告提纲和任何阶段性文稿都可以随时通过 &#x1F449; **【[交作业入口](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】** 提交，我看到后会联系你，以便及时修改优化。
  - 提交时的标题请按照 **【[交作业入口](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】** 页面中的格式要求填写。
- 课程报告截止时间：`2023/01/10` 





![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/底部图片_水墨小舟001.png)
