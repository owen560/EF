## 特征值、特征向量与主成分分析

&emsp; *> **作者**：姚和平 (中山大学)*

​    *> **E-Mail:** <yaohp@mail2.sysu.edu.cn>*  

***

在这篇推文里，你会看到：

* 什么是特征值、特征向量、QR 算法
* 特征值与特征向量在动力系统上的应用
* 主成分分析（PCA）的基本原理
* 特征值的意义——衡量数据在特征方向上的信息量
* Stata 主成分分析实操

你不会看到：

* 复杂繁琐的数学推导（为了通俗易懂，笔者已极力简化数学过程，但一些简单的公式依然很难避免）
* 很多很长的代码（你将了解Stata代码的简洁优美）

***

[TOC]

***

特征值和特征向量是线性代数里十分重要的概念，在经济学、统计学、物理、化学等学科中有着广泛的应用。这篇文章将为读者细致地讲解这两个概念并引入到其重要应用应用 PCA 的介绍。作为引入，我们先来看一个现实案例。

### 猫头鹰群落演化问题

1990年，环境保护学家与木材行业就砍伐太平洋西北部原始森林是否会造成斑点猫头鹰灭绝的问题上争论不休。数学学家们因此开始了对斑点猫头鹰种群的动力学研究，R.Lamberson (2005) 及其同事建立了以每年种群数量为区间的演化模型，时间为$k=0,1,2,\ldots$。猫头鹰的生命周期自然分为三个阶段：幼年期$j_k$（$1$岁以前）、半成年期 $s_k$（$1-2$岁）、成年期 $a_k$（$2$岁以后），第$k$年的种群量可以用向量 $\boldsymbol{x}_{k}=\left(j_{k}, s_{k}, a_{k}\right)$ 表示。根据统计数据，新的幼年猫头鹰在$k+1$年的数量是成年猫头鹰在 $k$ 年数量的0.33倍，每年$18\%$的幼年猫头鹰得以生存进入半成年期，$71\%$的半成年猫头鹰和$94\%$的成年猫头鹰生存下来被计为成年猫头鹰，如下图所示。

- <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig1_群落演化_姚和平.png" alt="avatar" style="zoom:40%;" />

我们用数学公式来表达这一模型：
$$
j_{k+1} = 0.33a_k \tag{1}
$$

$$
s_{k+1} = 0.18j_k\tag{2}
$$

$$
a_{k+1} = 0.71s_k + 0.94a_k\tag{3} 
$$

将上面式子改写成矩阵形式，就将该问题转换成一个形式为$\boldsymbol{x}_{k+1}=A \boldsymbol{x}_{k}$的方程，要看猫头鹰会不会灭绝，只需要计算出$\lim\limits_{n \to \infty}A^k x_0$即可。
$$
\left[\begin{array}{l}
j_{k+1} \\
s_{k+1} \\
a_{k+1}
\end{array}\right]=\left[\begin{array}{lll}
0 & 0 & 0.33 \\
0.18 & 0 & 0 \\
0 & 0.71 & 0.94
\end{array}\right]\left[\begin{array}{l}
j_{k} \\
s_{k} \\
a_{k}
\end{array}\right]\tag{4}
$$
上式表达除了可以用来表示猫头鹰群落的演化过程，还可以表示类似如城市人口流动、传染病SEIR模型等，在日常生活中应用十分广泛。但是$A^k$毕竟是一个矩阵的乘方运算，还要求极限，看上去非常的复杂和不直观，因此我们引入了特征值和特征向量的概念，它们能使上述计算变得十分简单。

### 特征值与特征向量

#### 定义

学过线性代数的读者都知道，矩阵乘法$A \boldsymbol{x} =\boldsymbol{b}$的本质就是对向量$\boldsymbol{x}$进行线性变换，但我们却很难想象得到的结果$\boldsymbol{b}$与起始向量$\boldsymbol{x}$有什么联系，它们并不像我们以前学的代数乘法那么形象，就是一个简单的倍数关系，原始量经过放大或缩小就可以得到结果。但有没有可能把复杂抽象向量乘法转化为普通的代数乘法呢?答案是可以！请看下面的例子（Lay, 2016)。

设 $A=\left[\begin{array}{cc}3 & -2 \\ 1 & 0\end{array}\right], \boldsymbol{u}=\left[\begin{array}{c}-1 \\ 1\end{array}\right], \boldsymbol{v}=\left[\begin{array}{l}2 \\ 1\end{array}\right]$.下图是 $\boldsymbol{u}$ 和 $\boldsymbol{v}$ 乘以 $A$ 后的图像。可以看出$A\boldsymbol{v}$与$A\boldsymbol{u}$不同，正好就是$2\boldsymbol{v}$，$A$就像普通乘法一样只是把$\boldsymbol{v}$拉长了。

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig2_特征值定义_姚和平.png" style="zoom: 50%;" />

因此我们可以看到，对于一个矩阵$A$，确实存在向量$\boldsymbol{v}$使得向量乘法的效果与标量乘法等价，即$A \boldsymbol{v}=\lambda \boldsymbol{v}$，这样我们就得到了我们开始想要的结果。为了方便起见，**人们把能使$A \boldsymbol{v}=\lambda \boldsymbol{v}$的这个$\lambda$称作矩阵$A$的特征值，向量$\boldsymbol{v}$称为特征向量**。

聪明的读者可能会想到，通过定义条件$A \boldsymbol{x}=\lambda \boldsymbol{x}$并不太容易找到特征值和相应的特征向量，而且特征值和特征向量常常并不止一对，那我们是否有一个简单的方法来找到一个矩阵的全部特征值和它所对应的特征向量呢？当然有！把$A \boldsymbol{x}=\lambda \boldsymbol{x}$做一个小变形，变为$(A-\lambda I) \boldsymbol{x}=\mathbf{0}$，那么该式子的非零解（也称为非平凡解）就是我们想求得的特征向量。根据矩阵可逆定理，当且仅当$\det A=0$，$A\boldsymbol{x}=\mathbf{0}$存在非零解，于是我们可以得到推论：

**数 $\lambda$ 是 $n \times n$ 矩阵 $A$ 的特征值的充要条件是 $\lambda$ 是特征方程 $\det(A-\lambda I)=0$ 的根**.

因此我们可以通过解特征方程 $\det(A-\lambda I)=0$ 来求出一个矩阵的特征值$\lambda$，进而求出每个特征值对应的特征向量。以下将通过一个小例子来演示特征值求解过程。

例子：

求 $A=\left[\begin{array}{rr}2 & 3 \\ 3 & -6\end{array}\right]$ 的特征值.
$$
A-\lambda I=\left[\begin{array}{rr}
2 & 3 \\
3 & -6
\end{array}\right]-\left[\begin{array}{cc}
\lambda & 0 \\
0 & \lambda
\end{array}\right]=\left[\begin{array}{cc}
2-\lambda & 3 \\
3 & -6-\lambda\end{array}\right]
$$

$$
\operatorname{det}(A-\lambda I)=\operatorname{det}\left[\begin{array}{cc}
2-\lambda & 3 \\
3 & -6-\lambda
\end{array}\right]=0
$$

$$
\begin{aligned}
\operatorname{det}(A-\lambda I) &=(2-\lambda)(-6-\lambda)-(3)(3) \\
&=-12+6 \lambda-2 \lambda+\lambda^{2}-9 \\
&=\lambda^{2}+4 \lambda-21=(\lambda-3)(\lambda+7)
\end{aligned}\tag{5}
$$

因式分解后得到的$(5)$式也被称为特征方程，因此$A$的特征值为$3$或者$-7$,分别代回$\det(A-\lambda I)=0$ 就可以得出分别对应的特征向量是$(3,1)$和$(1,-3)$。

特征值和特征向量还有一个非常有趣的性质，**如果我们能够矩阵$A$拆解成$A=P \Lambda P^{-1}$，而且矩阵$\Lambda$是一个对角矩阵（除对角线元素外的元素均为零），那么矩阵$P$中的列向量就是$A$的特征向量，$\Lambda$对角线元素就是$A$的特征值，而且各特征向量与特征值在列位置上相互对应**。这个过程也叫矩阵的对角化，此时**矩阵$A$与$\Lambda$具有相同的特征值**，我们称他们为相似矩阵。这个性质在之后讲解主成分分析（PCA)中有重要作用。

***

#### 应用到动力系统

让我们回到斑点猫头鹰的演化问题，特征值与特征向量是我们解决动力系统的关键。你可能会问，这种将矩阵乘法变为标量乘法的方法对于任意一个特定方阵来说，只有少数几个特征向量有效，比如在斑点猫头鹰的问题里，种群量$\boldsymbol{x}$并不一定是转移矩阵$A$的特征向量。答案是，我们通常可以将任意一个非特征向量$\boldsymbol{x}_0$用矩阵$A$的特征向量的线性组合来表示（只要那些特征向量线性无关）:
$$
\boldsymbol{x}_{0}=\boldsymbol{c}_{1} \boldsymbol{v}_{1}+\cdots+c_{n} \boldsymbol{v}_{n}\tag{6}
$$
以斑点猫头鹰为例，这里$\boldsymbol{v}_i$都是转移矩阵$A=\left[\begin{array}{lll}
0 & 0 & 0.33 \\
0.18 & 0 & 0 \\
0 & 0.71 & 0.94
\end{array}\right]$的特征向量，$\boldsymbol{x}_{0}$是猫头鹰种群$t=0$时的种群量。那么就有：
$$
\begin{aligned}
\boldsymbol{x}_{1}=A \boldsymbol{x}_{0} &=c_{1} A \boldsymbol{v}_{1}+\cdots+c_{n} A \boldsymbol{v}_{n} \\
&=c_{1} \lambda_{1} \boldsymbol{v}_{1}+\cdots+c_{n} \lambda_{n} \boldsymbol{v}_{n}
\end{aligned}
$$
其中$c_i$和$\lambda_i$都是标量，依次迭代后一般有：
$$
\boldsymbol{x}_{k}=c_{1}\left(\lambda_{1}\right)^{k} \boldsymbol{v}_{1}+\cdots+c_{n}\left(\lambda_{n}\right)^{k} \boldsymbol{v}_{n} \quad(k=0,1,2, \cdots)\tag{7}
$$
这样我们就把一个复杂的矩阵乘法问题转换为一个简单的标量乘法问题，但这样会对我们的运算产生什么样的影响呢？具体到斑点猫头鹰问题，我们可以计算出转移矩阵$A$的特征值分别为$\lambda_{1}=0.98$，$\lambda_{2}=-0.02+0.21 i$，$\lambda_{3}=-0.02-0.21 i$，其中因为$\left|\lambda_{2}\right|^{2}=\left|\lambda_{3}\right|^{2}=(-0.02)^{2}+(0.21)^{2}=0.0445$，所以三个特征值的绝对值都小于1。因此我们可以用$(7)$式将 $t=k$ 时的猫头鹰种群量用下式表示出来：
$$
\boldsymbol{x}_{k}=c_{1}\left(\lambda_{1}\right)^{k} \boldsymbol{v}_{1}+c_{2}\left(\lambda_{2}\right)^{k} \boldsymbol{v}_{2}+c_{3}\left(\lambda_{3}\right)^{k} \boldsymbol{v}_{3}\tag{8}
$$
由于所有的特征值都小于$1$所以当$k$趋向正无穷时，式 (11) 右边的每一项都趋于零向量. 因此, 实序列 $\left\{\boldsymbol{x}_{k}\right\}$ 也趋于零向量。很不幸，该模型最终预测斑点猫头鹰最终将全部灭亡 (Lamberson, 1992)。

***

#### $QR$算法

对于求一个$n\times n$矩阵特征值的问题，虽然上面我们介绍了像$(7)$式那样因式分解特征方程的算法，但在实际中，当$n\geq3$时，除非是精心选择的矩阵，否则对其进行因式分解并不容易，而且对于 $n \geqslant 5$ 的一般 $n \times n$ 矩阵，没有公式或有限算法来求解其特征方程。因此，最佳的求特征值的算法会完全避开特征多项式。事实上，matlab的算法就是先计算出特征值$\lambda_{1}, \cdots, \lambda_{n}$, 然后通过展开乘积 $\left(\lambda-\lambda_{1}\right)\left(\lambda-\lambda_{2}\right) \cdots\left(\lambda-\lambda_{n}\right)$ 来得到 $A$ 的特征多项式。那这种不需要解特征方程的方法是什么呢？接下来来进行介绍。

$QR$算法的核心思路是由矩阵$A$出发产生一个矩阵序列，这些矩阵都相似于矩阵$A$，而且一部分矩阵接近于上三角矩阵。线性代数中两条关于特征值的简单定理：

* 假如 $A$ 和 $B$ 是 $n \times n$ 矩阵, 如果存在可逆矩阵 $P$, 使得 $P^{-1} A P=B$, 则称 $A$ 相似于 $B$，相似的矩阵有相同的特征值。
* 上三角矩阵的对角线上的元素就是该矩阵的特征值。

根据上面两条定理，我们可以很容易看出，$A$产生的矩阵序列中的近上三角矩阵的对角线元素就是$A$的特征值（当然这只是一个估算的结果，因为有时无法产生真正的上三角矩阵）。那么我们应该怎么产生这些相似的上三角矩阵呢？答案是通过$QR$分解。
$QR$分解是指将矩阵$A$分解成两个矩阵$Q$和$R$的乘积，其中$Q$是正交矩阵，满足$Q^{\mathrm{T}}=Q^{-1}$，$R$是一个上三角矩阵。Stata 中的 $qrd()$ 函数可以完成这一过程：

	// --QR分解--
	mata                                  //进入 Mata环境
	A = ( 1, 2, 3 \ 4, 5, 6 \ 7, 8, 9 )
	qrd(A, Q, R)
	st_matrix("Q",Q)                      //将 Meta里的矩阵转换到stata环境中
	st_matrix("R",R)                      //同上
	end                                   //退出 Mata环境
	mat list Q
	mat list R
当矩阵$A$拆解成两个矩阵$A=QR$后，产生的新矩阵就是$A_1=RQ$，紧接着我们又对$A_1$进行$QR$分解，重复之前的步骤，这样每次产生的新矩阵$A_k(k=1,2,\ldots)$就是产生的矩阵序列。证明$A_k$与$A$相似是很简单的：
$$
A_{k+1}=R_{k} Q_{k}=Q_{k}^{-1} Q_{k} R_{k} Q_{k}=Q_{k}^{-1} A_{k} Q_{k}=Q_{k}^{T} A_{k} Q_{k}
$$
随着新矩阵$A_i(i=1,2,\ldots)$的不断产生，新矩阵会不断收敛成为一个上三角矩阵，这就是我们最终想要的结果（因为上三角矩阵的对角线元素就是矩阵$A$的特征值）。将这个过程总结成算法是非常简洁的。
$$
\begin{aligned}
&\text { set } A_{0}=A \\
&\text { for } i=1,2 \cdots, k \text { (until convergence) } \\
&\quad \text { compute } A_{i-1}=Q_{i} R_{i} \\
&\quad A_{i}=R_{i} Q_{i} \\
&\text { end }
\end{aligned}
$$
到目前为止，我们已经讲解清楚了特征值和特征向量，那么接下来让我们来看一下主成分分析 (Principal Component Analysis) ，它是特征值和特征向量的一个主要应用方向。

***

### 主成分分析（PCA)

在大数据时代，我们面对的数据往往更多的特征。过多的变量会使我们的程序变得更慢，也会让我们更难以发现数据之间的关系。幸运的是，在真实的世界里，我们通常可以在不损失太多信息的前提下把一些彼此关联十分紧密的变量进行合并和去掉一些无关变量。比如，下面左图中的数据点看似在三维空间中但实际上都落在一个二维平面上，这是因为描述数据点的某一个变量与其他两个变量完全线性相关，因此只需要调整一下坐标系就可以去掉一个变量；右图中数字周围的空白部分不提供信息，同样可以去掉(Géron, 2019) 。通过对原数据进行一些调整来减少变量个数的方法就叫做降维，而 $PCA$ 是迄今为止使用最广泛降维算法之一。接下来将介绍 $PCA$ 的基本原理和 Stata 实操的内容。

<center class="half">
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig3_3d2d_姚和平.png" width="250">
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig4_mnist_姚和平.png" width="250">
</center>

#### 基本原理

从上面的介绍中，我们可以看出降维算法主要有两个功能：**去相关和去冗余**。通俗来说，去相关是指尽可能减少变量间的相关性，去冗余是指去除那些方差很小的无关变量。$PCA$算法可以完美地做到上述的功能，让我们来看一下它是怎么实现的。
假设现有数据集 $X_{m \times n}$ , $m$ 表示变量个数，$n$ 表示样本数量。那么有什么数学工具可以同时表示数据集中变量的相关性和方差呢？协方差矩阵！其定义如下，因此协方差矩阵对角线衡量变量间方差，其余元素衡量变量间的协方差。
$$
C=\left(c_{i j}\right)_{m \times m},\ c_{i j}=\operatorname{Cov}\left(X_{i}, X_{j}\right), i, j=1,2, \ldots, m\tag{9}
$$
协方差矩阵的求解公式如下：
$$
C=\frac{1}{m-1} X^{T} X \tag{10}
$$
为达到上述两个目标，我们希望对原数据集进行一个简单变换，使**变换后矩阵的协方差矩阵除对角线外元素都等于$0$，对角线上元素表示原来变量的方差**。很自然我们会想到上文提到的对角化，因为对角矩阵恰好满足上述性质，因此我们将对原数据集的协方差矩阵进行对角化，得到其特征值和特征向量。
$$
&C=\frac{1}{m-1} X^{\mathrm{T}} X=P \Lambda P^{-1}\\
&P^{-1}\left(\frac{1}{m-1} X^{\mathrm{T}} X\right) P =\Lambda \tag{11}
$$
这时我们会发现矩阵$XP$的协方差矩阵恰好为对角矩阵$\Lambda$：
$$
\begin{aligned}
C^{\prime} &=\frac{1}{m-1}(X P)^{\mathrm{T}} X P \\
&=P^{\mathrm{T}}\left(\frac{1}{m-1} X^{\mathrm{T}} X\right) P \\
&=P^{-1}\left(\frac{1}{m-1} X^{\mathrm{T}} X\right) P =\Lambda\\
\end{aligned}\tag{12}
$$
因此，用原数据集$X$的协方差矩阵的特征向量矩阵$P$对原矩阵进行变换，可以使变换后的数据集$XP$的协方差矩阵等于原来的对角矩阵，即变换后的数据集$XP$有如下性质：

* $XP$变量间彼此不相关
* 原数据集$X$协方差矩阵的特征值衡量了$XP$各变量的方差

我们将 $P$ 中的特征向量称为主成分，对原数据集 $X$ 右乘 $P$ 相当于对原数据集变换了坐标轴，原来的坐标轴是各原始变量，新的坐标轴是各主成分。这种变换实现了这样的目的：在保证不损失信息的同时，使变换后各新变量不相关，原数据集$X$协方差矩阵的特征值大小表示各主成分解释新数据的方差大小（因为变换没有信息损失，所以也表示各主成分解释原始数据方差的大小）。因此新数据集各主成分对应特征值的大小表示了该主成分的重要性，之后我们可以借此来对主成分进行筛选，达到去冗余的目的。

接下来 我们将进入降维的步骤，去除那些相对不重要的主成分。当去除一些主成分（坐标轴）时，我们相当于就将数据从高维空间投影到一个低维空间。下图是一个简单的例子(Géron, 2019)，现在我们将数据从二维平面投影到一条直线上，我们显然会选择投影到$c_1$上，因为这样保留了最大的方差（信息量），在主成分分析中我们也会做同样的选择。

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig5_pcaex_姚和平.png" style="zoom: 50%;" />

因此在 PCA 算法中我们只会保留那些投影后保留了原数据最大方差的主成分，而 $(12)$ 式告诉我们，特征值越大，主成分方差越大。于是我们按照特征值从大到小的顺序将对应的特征向量从左到右排列，只保留变换矩阵 $\mathrm{P}$前 $d$ 列，记为 $\mathrm{P}_d$（ $d$ 的大小可以自由选择，下面的实例讲解中会提供一些选择参考）。最终我们对上面的变换 $\mathbf{X}_{d-\text { proj }}=\mathbf{X} \mathbf{P}$ 修正为：
$$
\mathbf{X}_{d-\text { proj }}=\mathbf{X} \mathbf{P}_{d}\tag{13}
$$
$\mathbf{X}_{d-\text { proj }}$就是我们最终希望得到的降维后的数据矩阵。另一方面，上图也揭示了特征值、特征向量与主成分分析的核心联系。我们回到特征值的定义：
$$
A \boldsymbol{v}=\lambda \boldsymbol{v}
$$
**我们现在换一种理解方式，现在 $A$ 是原数据集，$A \boldsymbol{v}$看成是将数据集 $A$ 投影到向量$\boldsymbol{v}$上， $\lambda$ 就衡量了数据集投影到某个特征向量（主成分）上的方差大小（在上图里，$\lambda$就是数据集投影到主成分后的长度）。我们已经知道 PCA 算法的本质就是寻找主成分，然后将数据集投影到主成分上的过程，而在 PCA 中主成分就是特征向量，因此 PCA 算法的核心逻辑和特征值的定义式 $A \boldsymbol{v}=\lambda \boldsymbol{v}$ 有着惊人的吻合，特征值 $\lambda$ 的大小刚好就衡量了对应主成分解释原数据的方差，而这也正是理解特征值、特征向量与主成分分析之间关系的核心，这也是特征值的重要意义之一——它衡量了数据在特征向量方向上所包含的信息量！**

做个总结，PCA算法操作过程如下：

* 求出原数据 $\mathbf{X}$ 协方差矩阵的特征值和特征向量
* 保留特征值前 $d$ 大的特征向量，组成转换矩阵 $\mathbf{P}_{d}$
* 降维后矩阵 $\mathbf{X}_{d-{proj}}=\mathbf{X} \mathbf{P}_{d}$

值得注意的是，虽然我们上面用协方差矩阵来进行推导，但在实际中我们常常用相关系数矩阵进行代替，两者作用相似，但前者比起后者更容易受到量纲的干扰。另一方面，并不是所有的数据都适合使用PCA算法，PCA算法对数据变量之间的相关关系有限制，在使用前应先对数据进行检验，具体在下面示例中展示。接下来我们来看一下如何在Stata中进行实际操作和结果解读。

***

#### Stata 实操

接下来我们使用Jackson书中的一个例子来进行程序演示。该数据集记录了一百名成年男性左右耳的听力情况，测量结果是左耳和右耳在四个不同频率上的最小可辨识强度，比如，变量$lft1000$指的是左耳在1,000赫兹时的情况。

	use https://www.stata-press.com/data/r17/audiometric, clear //载入数据
	sum lft* rght* //观察统计性质
	correlate lft* rght* //相关系数矩阵
	pca lft* rght* //对所有变量进行pca降维
	screeplot //画出陡坡图
	estat kmo //进行KMO检验
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值和PCA_Fig6_解释方差表_姚和平.png" style="zoom:67%;" />

图$7$是stata给出的解释方差表，第二列给出了每个主成分对应的特征值，如上文所说，该特征值的大小表示了该主成分解释数据集方差（信息量）的大小，所有特征值加起来就是原数据集总的方差（信息量）大小。该数据集总方差为$8$，第一个主成分方差约为$3.93$，解释了总方差的$49\%(3,93/8)$，同理，第二个主成分解释了总方差的$20\%(1.6/8)$，每个主成分解释的百分比对应于解释方差表的第四列。第五列第$n$行数据表示前$n$个主成分累计解释方差的占比。解释方差表可以帮助我们判断提取主成分的个数，目前主要有3种方法可以帮助大家判断提取主成分的数量，分别是：

* 特征值大于$1$;
* 解释数据变异的比例;
* 陡坡图检验

$(1)$特征值大于$1$。一般来说，如果某一项主成分的特征值小于1，那么我们就认为该主成分对数据方差的解释程度比单个变量小，应该剔除。不过缺点是如果研究结果中某些主成分的特征值十分接近1，那么该方法对提取主成分数量的提示作用将变得不明显。对应到本文中，我们似乎应该只保留前两个主成分，但第三个主成分是否应该保留则需要其他方法进行辅助判断。
$(2)$解释数据变异的比例。既往研究认为提取的主成分至少应该解释$5-10\%$的数据变异或者提取的主成分应至少累计解释$70-80\%$的数据方差。根据些一指标，我们认为应该提取前三位主成分(前三位主成分累计解释$81.53\%$的数据方差)。
这种判断方法的不足在于比较主观，我们既可以提取$70\%$，也可以提取$80\%$，而这$10\%$的比例差异往往导致提取主成分数量的不同。
$(3)$陡坡图检验。陡坡图是根据各主成分对数据方差的解释程度绘制的图。图上，横轴对应主成分解释方差从大到小点的序号，纵轴对应特征值。我们通过“陡坡趋于平缓”的位置判断提取主成分的数量。在本研究中，第四主成分之后的数据趋于平缓，因此我们认为可以提取前四位主成分。
以上三种方法得出的结果不一定一致，研究者应根据实际情况综合进行判断。

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig7_scree_姚和平.png" style="zoom: 33%;" />

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig8_旋转成分矩阵_姚和平.png" style="zoom:67%;" />

图$8$是Stata给出的第二个表，称为旋转成分矩阵，其实就是我们上面说到的特征向量矩阵$P$，因为这个矩阵让原数据集变幻了坐标轴（也可以说是旋转了数据），因此有了旋转成分矩阵的名字。这个表传递了这样的信息：每一列是对应主成分的特征向量，同时因为原矩阵通过右乘该转转成分矩阵得到了新数据，因此这张表也说明了数据变换后各主成分与原数据各变量的关系。比如，第一列表示了：
$$
\begin{aligned}
Comp1 = &0.4011lft500 + 0.4210lft1000 + 0.3664lft2000 + 0.2809lft4000 + 0.3433rght500 \\
&+ 0.4114rght1000 + 0.3115rght2000 + 0.2542rght4000
\end{aligned}
$$
我们前面提到过，进行PCA之前要进行检验，KMO检验是一个不错的选择，我们来看一下怎么解读：

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig9_kmo_姚和平.png" style="zoom:50%;" />

KMO检验主要用于主成分提取的数据情况。一般来说，KMO检验系数分布在0到1之间，如果系数值大于0.6，则认为样本符合数据结构合理的要求，但大于0.8会更好。对于单个变量的分析结果，如果系数大于0.5，则认为单个变量满足要求；如果系数大于0.8，则认为单个变量结果很好。在本研究中，任一变量的KMO检验结果均大于0.6，总体检验结果大于0.7，结果不算太好，但仍可以接受。Kaiser (1974) 给出了一个通用的判断KMO检验值的法则：

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/特征值与PCA_Fig10_kmomeasure_姚和平.png" style="zoom: 33%;" />

在了解完上面所有信息后，我们可以生成降维后的数据了。我们选择提取前三个主成分，通过原数据生成三个主成分$f_1,f_2,f_3$。

	pca lft* rght*, components(3)  //选择前三个主成分降维
	predict f1-f3                  //生成新数据

至此我们已经完成了PCA的全部过程。在进行实证研究或机器学习中，经过PCA降维后的数据往往会比原数据在模型上拟合精度更高，也更有利于我们在繁杂的数据中快速找到规律，这其实就是一个数据净化的过程。需要注意的是，PCA是需要提前对数据进行中心化的，程序里的函数已经自动执行这一步骤，各位读者如果选择自行处理的话要记得加上数据中心化的步骤。







### 参考文献

[1] Aurélien Géron. Hands-on machine learning with Scikit-Learn, Keras, and TensorFlow: Concepts,
tools, and techniques to build intelligent systems. O’Reilly Media, 2019.
[2] J Edward Jackson. A user’s guide to principal components. Vol. 587. John Wiley & Sons, 2005.
[3] Ian Jolliffe. “Principal component analysis”. In: Encyclopedia of statistics in behavioral science
(2005).
[4] Henry F Kaiser. “An index of factorial simplicity”. In: Psychometrika 39.1 (1974), pp. 31–36.
[5] Roland H Lamberson et al. “A dynamic analysis of northern spotted owl viability in a fragmented
forest landscape”. In: Conservation Biology 6.4 (1992), pp. 505–512.
[6] David C Lay. “Linear Algebra and its applications 5th edition”. In: Pearson (2016).
[7] Michael N Mitchell. A visual guide to Stata graphics. Stata Press, 2008.
[8] Karl Pearson. “LIII. On lines and planes of closest fit to systems of points in space”. In: The Lon-
don, Edinburgh, and Dublin philosophical magazine and journal of science 2.11 (1901), pp. 559–
572.
[9] Gilbert Strang et al. Introduction to linear algebra. Vol. 3. Wellesley-Cambridge Press Wellesley,
MA, 199