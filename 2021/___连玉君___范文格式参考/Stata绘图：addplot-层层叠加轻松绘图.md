
&emsp;

> **作者：** 袁子晴 (香港大学)    
> **邮箱：** <yzq0612@foxmail.com>


&emsp;

---

**目录**
[[TOC]]

---

&emsp;


&emsp;

## 1. 问题背景

在 Stata 中绘制较为多个图层的叠加的复杂图表时，或者在已经绘制好的图表上进行二次编辑时，我们可以使用 `addplot` 命令来实现。该命令与 `twoway` 绘图命令的附加选项 `addplot_option` 的区别在于它是独立于原有绘图命令的，可以在原有绘图命令之后单独运行，其优势在于：

- 如果你必须修改一个图形，而你又不能重新运行原来的绘图命令，或者你只有图形文件，而没有用于创建图形的数据，那么 `addplot` 允许你在图形生成后向其添加元素;
- `addplot` 可以实现单一绘图命令中难以实现的效果（例如，包含多个子图的情况下自定义单个子图）；`addplot` 还可以替代图形编辑器的一些功能。

&emsp;

## 2. Stata 外部命令介绍

首先通过 `ssc install addplot` 来安装外部命令 `addplot` ，其命令结构如下：

```stata
 addplot [graphname] [numlist] [, nodraw] : twoway_plots
```

- `graphname` 是需要修改的内存图的名称，若省略该选项，则默认使用当前内存中的图形；
- `numlist` 提供要单独修改的子图的编号，当修改 `graph combine` 或 `by()` 创建的图形时，命令 `addplot` 默认所有子图，但如果你只想修改第二和第三个子图，你可以输入：`. addplot 2 3: ...`；
- `nodraw` 适用于对同一个图形多次使用 `addplot` 但不想每次都更新图形，因为如果在后续使用 `addplot` 时修改了绘图数据会改变之前的绘图结果，这一选项避免了这一情况；
- 可以添加选项 `norescaling` 阻止 `addplot` 命令重新编译坐标轴（以便保留坐标轴的标签和范围）；
- 可以添加选项 `legend(off)` 避免添加多余的图例。
- 冒号后面 `twoway_plots` 代表可以加入 Stata  `twoway` 绘图命令

&emsp;

## 3. Stata 示例

### 3.1. 添加数据标签和箭头

```stata
. sysuse auto, clear
. twoway scatter price mpg

. addplot: scatter price mpg if price>15000 | mpg>40, ///
           msymbol(i) mlabel(make) ///
           mlabposition(9) legend(off)

. addplot: pcarrowi 14000 23 15500 21.5 (3) "expensive car" ///
                     2000 38  5000 40.7 (6) "high mileage car"
```



<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211130151303.png" style="zoom:50%;" />

### 3.2. 修改现有图形

第一个示例展示了 `addplot` 如何分别为已命名的图形添加标题：
```stata
sysuse auto
scatter price mpg, name(g1, replace)
scatter price weight, name(g2, replace)
addplot g1: , title(price by mpg)
addplot g2: , title(price by weight)
graph drop g1 g2
```

第二个示例展示了 `addplot` 如何修改现有的以 `.gph` 为后缀的图形文件：
```stata
sysuse auto
scatter price mpg
graph save mygraph.gph
graph drop Graph

graph use mygraph.gph
addplot mygraph: pcarrowi 14000 23 15500 21.5 "expensive car", legend(off)
graph drop mygraph
erase mygraph.gph
```

第三个示例展示了如何单独修改分组生成的子图：

```stata
sysuse auto
scatter price mpg, by(foreign)
addplot: , xline(21.3, lp(dash)) yline(6165, lp(dash)) norescaling
addplot 1: , xline(19.8) yline(6072) norescaling
addplot 2: , xline(24.8) yline(6385) norescaling
graph drop Graph
```


<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211207150604.png" style="zoom:50%;" />

以上的三个示例来源于 `help addplot` 帮助文档，均是单击即可运行。




```stata
  sysuse auto, clear
  
  proportion rep if foreign==0 & rep78>=3
  estimates store domestic
 
  proportion rep if foreign==1 & rep78>=3
  estimates store foreign
 
  coefplot domestic foreign, vertical recast(bar) barwidth(0.3) fcolor(*.5) ///
      ciopts(recast(rcap)) citop citype(logit) format(%9.2f) ///
      addplot(scatter @b @at, ms(i) mlabel(@b) mlabpos(2) mlabcolor(black))
```

### 3.3. 为柱状图添加数值标签

下图是与 `coefplot` 系数可视化命令的结合，添加数值作为标签，并绘制了置信区间：

```Stata
sysuse auto, clear

proportion rep if foreign==0 & rep78>=3
estimates store domestic

proportion rep if foreign==1 & rep78>=3
estimates store foreign

coefplot domestic foreign, vertical recast(bar) ///
    barwidth(0.3) fcolor(*.5) ///
    ciopts(recast(rcap)) citop citype(logit) format(%9.2f) ///
    addplot(scatter @b @at, ms(i) mlabel(@b) mlabpos(2) mlabcolor(black))
```

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211207153920.png" style="zoom:50%;" />

### 3.4. 为系数图添加基准线

系数可视化命令 `coefplot` 不允许针对子图的 `xline()` 选项，因此我们可以在 `coefplot` 创建图形后使用`addplot`来添加基准线：

```stata
sysuse auto, clear
logit foreign mpg trunk length turn
coefplot ., bylabel(Log odds) ||  ///
         ., bylabel(Odds ratios) eform || ///
		  , drop(_cons) nolabel byopts(xrescale)
addplot 1: , xline(0) norescaling
addplot 2: , xline(1) norescaling
```

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211207152236.png" style="zoom:50%;" />

同样地，我们可以为 `coefplot` 创建的子图用 `addplot` 分别添加 y 轴基准线：

```stata
scatter mpg turn, by(foreign) jitter(2) msymbol(Oh)
summarize mpg if foreign==0, meanonly
addplot 1: , yline(`r(mean)') norescaling
summarize mpg if foreign==1, meanonly
addplot 2: , yline(`r(mean)') norescaling
summarize mpg, meanonly
addplot: , yline(`r(mean)', lpattern(dash)) norescaling
```

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211207152040.png" style="zoom:50%;" />

### 3.5. 为散点图添加分组标签

如果根据数值大小分为高、中、低三组时，可以按照下面示例将分组情况更为直观地展示出来：

```stata
 scatter mpg turn, jitter(2) msymbol(Oh)
 addplot: (scatteri 10.0 52 10.0 53 19.5 53 19.5 52, recast(line) lp(l))  ///
  (scatteri 20.5 52 20.5 53 29.5 53 29.5 52, recast(line) lp(l))          ///
  (scatteri 30.5 52 30.5 53 40.0 53 40.0 52, recast(line) lp(l)),         ///
  graphregion(margin(r=11)) legend(off) norescaling                       ///
  text(15 52.5 "low" 25 52.5 "medium" 35 52.5 "high",                     ///
  orientation(rvertical))
```

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211207151939.png" style="zoom:50%;" />

### 3.6. 为累计概率分布图添加边缘图

在对变量进行描述性统计分析时，通常会绘制累计概率分布图 (**cumulative distribution plots**)，进一步地添加纵向维度的边缘直方图 (**cumulative distribution plots**) 有助于我们直观地观察数据堆叠情况，下面的示例为我们展示了如何实现这一过程：

```
  sysuse auto,clear
  set scheme sj
  quantile mpg, rlopts(lc(none)) ms(oh) yla(, ang(h))
  count if mpg < .
  egen prob = total(1/`r(N)'), by(mpg)
  egen tag = tag(mpg)

  generate nprob = -prob
  quantile mpg, rlopts(lc(none)) ms(oh) yla(, ang(h))  ///
  addplot(spike nprob mpg if tag, horizontal) legend(off)
```

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211207152146.png" style="zoom:50%;" />

### 3.7. 非参数估计：分段求取均值

这个例子由连玉君老师提供，源于 [Stata 假期班-高级班](https://gitee.com/arlionn/PX) 的课件。该例展示了非参数估计中最基本的「分段求取均值」的过程。

**第一步**：通过模拟的方式产生数据。数据生成过程 (DGP) 如下：

$$
y = sin(0.18\pi x) + e, \quad x\sim U[0,10], \quad e\sim N(0,(0.05x)^2)
$$

为了展示不同样本数下的结果，生成了两组样本，分别为 $N1=400$ 个观察值；$N2=40$ 个观察值，对应的 Stata 数据文件分别存储为 **NP_sim01.dta** 和 **NP_sim02.dta**

**第二步**：将样本等分为 8 组，用 `egen` 命令计算每组的均值，进而使用 `twoway line` 绘制基础图形 (各个区间的均值线)。为了提升可视化，分别将单数组和双数组的散点图设定为红色 (red) 和蓝色 (blue)，这需要通过循环语句来完成，相当于绘制了 8 张 (共 8 组分段数据) 透明的图片。在此过程中，使用 `addplot` 命令将这些图片「覆盖」在基础图片上即可完成整个图片的绘制工作。 

```stata
//----------------- DGP - Simulate datasets-------- 
//Full sample, N=400
clear
set seed 135 
set obs 400
gen x = runiform()*10          // x~U[0,1]
gen e = rnormal()*(0.05*x)   // e~N(0, 0.05x)
gen y = sin(0.18*_pi*x) + e

save "NP_sim01.dta", replace 

//Subsample, N=40
set seed 1
sample 40 , count
save "NP_sim02.dta", replace
//-------------------------------------------------


//分段求均值

*-样本1：N = 400
use "NP_sim01.dta", clear 
twoway scatter y x, msize(*0.2)

*-样本2：N = 40
use "NP_sim02.dta", clear 
twoway scatter y x, msize(*0.2)

sort x 
gen K = group(8) // 把样本等分成 K 组
bysort K: egen ymean = mean(y)

list x y K ymean, sepby(K)

twoway scatter ymean x, msymbol(+) msize(*0.4)

//图示：分段均值和散点图
//-------------------------------------b---------------
 twoway line ymean x if K==1, msize(*1.2) lcolor(red) //scheme(s1mono)
 forvalues k = 1(2)7{
   addplot: line ymean x if K==`k', lcolor(red) legend(off) //recast(area)
   addplot: scatter  y x if K==`k', msize(*1.2) msymbol(+)  mcolor(red) legend(off)
 }
 forvalues k = 2(2)8{
   addplot: line ymean x if K==`k', lcolor(blue) legend(off)
   addplot: scatter  y x if K==`k', msize(*1.2) msymbol(oh) mcolor(blue) legend(off)
 }
//-------------------------------------o---------------
```

输出图片效果如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/NP_sim01_bins-mean-01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/NP_sim01_bins-mean-02.png)


&emsp; 

最后，不知你是否觉得这个图片的风格很清爽，完全不同于 Stata 默认的粗笨风格。若想实现这个风格，只需在绘图之前执行如下命令，将绘图模板设定为 `white_tableau` 风格即可。

有关绘图模板的设定方法，参见往期推文 [Stata绘图：一个干净整洁的-Stata-图形模板qlean](https://www.lianxh.cn/news/9300f2b9f1960.html) 或 [Stata黑白图形模板：中文期刊风格的纯黑白图形](https://www.lianxh.cn/news/79e3faf4e3dc3.html)。

```stata
. ssc install schemepack, replace //安装white_tableau 模板
. set scheme white_tableau //设定绘图风格为white_tableau
```

若需重设为 Stata 默认绘图风格，可以按需执行如下两条命令之一：
```stata
. set scheme s2color  // Stata 默认, 彩色
. set scheme s2mono   // Stata 默认, 黑白
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/NP_sim01_bins-mean-03.png)
> Stata 自带模板 **s1momo** 的输出效果



&emsp;

## 4. 参考文献

- [A note on adding objects to an existing twoway graph](https://sci-hub.mksa.top/10.1177/1536867x1501500308)
- Cox, N.J., 2021, Stata tip 141: Adding marginal spike histograms to quantile and cumulative distribution plots, 21(3):838-846. [-PDF-](https://journals.sagepub.com/doi/full/10.1177/1536867X211045583)
- Ben Jann, `coefplot`: Plotting regression coefficients and other estimates in Stata, [-Link-](http://repec.sowi.unibe.ch/stata/coefplot/varia.html)
- Ben Jann, 2015, A Note on Adding Objects to an Existing Twoway Graph, Stata Journal, 15(3): 751–755. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500308)
- Ben Jann, 2015, Stata Tip 122: Variable Bar Widths in Two-Way Graphs, Stata Journal, 15(1): 316–318. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500120)
- Ben Jann, 2018, Color Palettes for Stata Graphics, Stata Journal, 18(4): 765–785. [-PDF-](https://sci-hub.ren/10.1177/1536867X1801800402), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800402)
- Ben Jann, 2018, Customizing Stata Graphs made Easy (Part 1), Stata Journal, 18(3): 491–502. [-PDF-](https://sci-hub.ren/10.1177/1536867X1801800301), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800301)
- Ben Jann, 2018, Customizing Stata Graphs Made Easy (Part 2), Stata Journal, 18(4): 786–802. [-PDF-](https://sci-hub.ren/10.1177/1536867X1801800403), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800403)
- Daniel Bischof, 2017, New Graphic Schemes for Stata: Plotplain and Plottig, Stata Journal, 17(3): 748–759. [-PDF-](https://sci-hub.ren/10.1177/1536867X1701700313), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700313)
- Lars Ängquist, 2014, Stata Tip 117: Graph Combine—Combining Graphs, Stata Journal, 14(1): 221–225. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400115)
- Mark D. Chatfield, 2018, Graphing Each Individual's Data over Time, Stata Journal, 18(3): 503–516. [-PDF-](https://sci-hub.ren/10.1177/1536867X1801800302), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800302)
- Tim P. Morris, 2019, Stata tip 131: Custom legends for graphs that use translucency, Stata Journal, 19(3): 738–740. [-PDF-](https://sci-hub.ren/10.1177/1536867X19874263), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19874263)


&emsp;

## 5. 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
> &emsp; `lianxh 绘图 模板`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace` 


- 专题：[Stata绘图模板](https://www.lianxh.cn/blogs/24.html)
  - [Stata绘图极简新模板：plotplain和plottig-T251](https://www.lianxh.cn/news/37ea99dab6efb.html)
  - [Stata绘图：一个干净整洁的-Stata-图形模板qlean](https://www.lianxh.cn/news/9300f2b9f1960.html)
  - [Stata：图形美颜-自定义绘图模板-grstyle-palettes](https://www.lianxh.cn/news/8c1819ff61a8a.html)
  - [Stata黑白图形模板：中文期刊风格的纯黑白图形](https://www.lianxh.cn/news/79e3faf4e3dc3.html)
- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [普林斯顿Stata教程(二) - Stata绘图](https://www.lianxh.cn/news/64495e9c4801c.html)
- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata 绘图：用 Stata 绘制一打精美图片-schemes](https://www.lianxh.cn/news/0f2537275983f.html)
  - [常用科研统计绘图工具介绍](https://www.lianxh.cn/news/021856c11e46b.html)
  - [Stata空间计量：莫兰指数绘图moranplot命令介绍](https://www.lianxh.cn/news/b17e298a7b346.html)
  - [Stata绘图-组间差异可视化：不良事件火山图、点阵图](https://www.lianxh.cn/news/f9680c4be14fe.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-上篇](https://www.lianxh.cn/news/59bc1ff7d027a.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-下篇](https://www.lianxh.cn/news/180daa074ef27.html)
  - [Stata绘图：柱状图专题-T212](https://www.lianxh.cn/news/df9729c5de34d.html)
  - [Stata绘图：回归系数可视化-论文更出彩](https://www.lianxh.cn/news/324f5e3053883.html)
  - [Stata绘图：世行可视化案例-条形图-密度函数图-地图-断点回归图-散点图](https://www.lianxh.cn/news/96989b0de4d83.html)
  - [Stata绘图：随机推断中的系数可视化](https://www.lianxh.cn/news/ce93c41862c16.html)
  - [Stata绘图：重新定义坐标轴刻度标签](https://www.lianxh.cn/news/c32c903235e7d.html)
  - [Stata绘图：用-bytwoway-实现快速分组绘图](https://www.lianxh.cn/news/4d72752d51859.html)
  - [Stata绘图：一个干净整洁的-Stata-图形模板qlean](https://www.lianxh.cn/news/9300f2b9f1960.html)
  - [Stata绘图：在图片中添加虚线网格线](https://www.lianxh.cn/news/126f5d3d6fae5.html)
  - [Stata绘图：怎么在Stata图形中附加水平线或竖直线？](https://www.lianxh.cn/news/34bb158641831.html)
  - [Stata绘图：制作教学演示动态图-GIF](https://www.lianxh.cn/news/a0cd3eaaecf80.html)
  - [Stata绘图：绘制一颗红心-姑娘的生日礼物](https://www.lianxh.cn/news/e6a97fe962611.html)
  - [Stata绘图：bgshade命令-在图形中加入经济周期阴影](https://www.lianxh.cn/news/b89fd3c601e31.html)
  - [Stata绘图：让图片透明——你不要掩盖我的光芒](https://www.lianxh.cn/news/ed0d5216d7eb7.html)
  - [Stata绘图：多维柱状图绘制](https://www.lianxh.cn/news/01fab25337d76.html)
  - [Stata绘图：用暂元统一改变图形中的字号](https://www.lianxh.cn/news/dabe656d89faf.html)
  - [一文看尽 Stata 绘图](https://www.lianxh.cn/news/ae36a721cfe18.html)
  - [Stata绘图：绘制单个变量的时序图](https://www.lianxh.cn/news/f62630d968b9e.html)



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

