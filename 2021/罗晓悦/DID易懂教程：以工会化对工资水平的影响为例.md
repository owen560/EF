
&emsp;

> **作者：** 罗晓悦 (中山大学)  
> **邮箱：** <luoxy77@mail2.sysu.edu.cn>

> **备注：本推文翻译自以下推文，特此致谢！  
> **Source:** Fernando Rios-Avila, Unions and wage gains: a DID approach. [-Link-](https://friosavila.github.io/playingwithstata/main_did_union.html)

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

&emsp;

## 1. 当前常用的 DID 方法的局限性
### 1.1 模型推演
将一个人的工资取对数，记为 $y0_{it}$。在没有工会的情况下，工资的对数可以和以下三个因子构成函数：无法观测到的个体固定效应 $γ_{i}$，对所有个体造成相同影响的年份效应 $δ_{t}$，以及残差项 $ε_{it}$：
$$y 0_{i t}=\gamma_{i}+\delta_{t}+e_{i t}$$  

由于有些人可能会选择加入工会，而工会化可能会对不同的个体产生异质性影响，因此它可以作为一个控制变量。但是，我们难以观测这个异质性的来源。为了简化分析，假设工会化的效果是完全异质性的，因为不同的人、不同的时间或者不同的人和时间的组合，都可能会影响这一效果。当人们加入工会时，他们的工资水平为：$$y 1_{i t}=\gamma_{i}+\delta_{t}+\theta_{i t}+e_{i t}$$  

这样，工会化对个体工资水平的影响可以简单归纳为上述两个函数估算出的两种工资水平之间的差异，从而可以估计出平均处理效应 (ATE)、处理组的平均处置效应 (ATT)、对照组的平均处置效应 (ATU)，甚至可以得到平均处置效应的分布情况：$$T E_{i} t=y 1_{i t}-y 0_{i t}=\theta_{i t}$$  

为了识别处置效应，我们进一步假设工会化是一种永久性的影响，即只要曾经加入工会，那么他将永远是工会成员。在这种情况下，我们使用当前常用的 DID 模型来估计处置效应：$$y_{i t}=\gamma_{i}+\delta_{t}+\theta * \text { union }_{i t}+e_{i t}$$  

### 1.2 局限性
当前已经有许多论文讨论过上述 DID 模型的局限性，以及由这一模型得到的处置效应 $θ$ 可能存在的偏误。DID 的基本思路是利用时间差异和是否工会成员的差异来估计处置效应的加权平均值。可是这个对比是存在漏洞的，因为本来应该属于处理组的观测值，在最开始被当做控制组来进行处理了。换句话说，对于加入过工会的个体来说，在加入工会的时点之前 union=0，此时他们被视作控制组，而加入工会后 union=1，他们又变成了处理组。对此，Goodman-Bacon（2020）和作者都对这个问题进行了解释。  

由于这一 DID 方法估计得到的 $θ$ 不准确，因此对两个固定效应 $γ$ 和 $δ$ 的估计可能也存在偏差，因此我们不应当采用双向固定效应模型进行分析。

&emsp;

## 2. did_imputation命令介绍
### 2.1 理论部分
我们依旧使用工会化和工资的案例。  

Butts 和 Gardner（2021）以及 Borusyak 等人（2021）认为，可以先对非处理组（即未加入工会）的观测值进行回归，得到对所有个体都会造成相同影响的固定效应因子，以及排除了固定效应的影响的估计值（即残差项），而后利用残差项对处理组的观测值进行处理，最终就可以得到处置效应的估计值。  

**Step1 估计固定效应和残差项**  
对于最原始的三因子模型，使用从未加入工会的个体的数据进行回归：$$y_{i t}=\gamma_{i}+\delta_{t}+e_{i t}$$
上述回归估计出了个体固定效应 $\hat{\gamma}_{i}$ 和年份固定效应 $\hat{\delta}_{t}$，以及排除了固定效应影响的残差项 $\hat{e}_{it}$。Butts 等人（2021）和 Borusyak 等人（2021）认为，上述回归得到的残差项 $\hat{e}_{i t}$ 在剔除了个体固定效应和年份固定效应后，依旧包含工会化的影响：$$y_{i t}-\hat{\gamma}_{i}-\hat{\delta}_{t}=\hat{e}_{i t}$$

**Step2 估计处置效应**  
Butts 等人（2021）提出的两步 DID 法在以下模型中引入了哑变量 union，并进行第二次回归，从而获得处置效应的估计值：$$\hat{e}_{i t}=\theta * \text { union }_{i t}+e_{i t}$$  

Borusyak 等人（2021）则通过对 $\hat{e}_{i t}$ 赋予一定的权重以求得加权平均值，从而获得感兴趣的因素的处置效应估计值：$$T E_{K}=E\left(\hat{e}_{i t}, w_{i t}\right)$$  

两方的作者从不同方向突破了原来的瓶颈。这篇文章将对 Borusyak 等人提出的方法 `did_imputation` 进行讲解。

### 2.2 语法结构
`did_imputation`命令是 Borusyak 等人（2021）根据以上估计方法所编写的 Stata 新命令，其语法结构如下：

```stata
did_imputation Y i t Ei [if] [in] [estimation weights] [, options]
```

- `Y`：被解释变量
- `i`：代表每个个体的唯一代码
- `t`：时间变量
- `Ei`：处理组的干预时点。当这一项为缺失值时，默认将其归为控制组
- `estimation weights`：为每个观测值赋予一定的权重，在第一步的回归和最终估计处置效应的过程中引入这一权重。本文并不涉及
- `options`：此处介绍本文涉及的 3 个选项  
  - `horizons`：设定干预时点后的报告期，需要填入区间范围，数值必须是非负整数
  - `pretrend`：输入一个正整数 k 后，在干预时点前的 k 期，命令将只对控制组的观测值进行回归，并用于平行趋势检验
  - `autosample`：加入此选项后，在回归时将自动剔除无法估算双向固定效应的观测值，并发出警告

&emsp;

## 3. Stata实例
### 3.1 数据处理
在数据选取上，作者采用了 **nslwork** 数据集。作者指出，这个数据集中只包含年轻女性的样本，所以从这个案例得出的结论可能无法推广到其他群体中。  
```stata
webuse nlswork, clear

/*    Notes:
数据中每个个体为一名14-26岁的女性，包括了其工资，以及其他一些人口社会学特征。

   year：受访年份
   union：是否加入工会（1表示加入工会）
   wks_work：受访年份的前一年工作的周数
   hours：每周工作的小时数
   ln_wage：工资水平的对数
*/
```
这个数据集中的哑变量 union 存在缺失值，对此作者进行了以下处理：  
> 1、对于每个受访者，如果 union 变量是缺失值，那么令这一项等于前一个受访年份的 union 值  
> 2、只要受访者在受访期间曾表示过自己加入了工会，那么将假设她自那一年起一直都是工会成员  
> 3、对于从未提及自己是否加入工会的受访者，也就是在所有受访年份中 union 变量都是缺失值，她们将被直接剔除  
> 4、对于每个剩下的受访者，在她“第一次”加入工会的年份之前，如果 union 变量为缺失值，将假设她们在那个时候并没有加入工会  
```stata
bysort id (year):replace union = union[_n-1] if union==.
bysort id (year):replace union = 1 if union[_n-1]==1
bysort id (year):egen flag = sum(!missing(union))
drop if flag==0
replace union =0 if union==.
```
由于每个个体加入工会的年份不同，作者把她们加入工会的第一年设置为干预时点：
```stata
bysort id: egen aux = min(year) if union==1
bysort id: egen gvar = max(aux)
```

&emsp;

### 3.2 实证结果分析
在进行回归前，我们需要下载以及检查更新以下程序包：
```stata
ssc install did_imputation
ssc install ftools
ssc install reghdfe
ssc install event_plot
ssc install addplot
```

在分析工会化对工资影响时，作者把模型最大程度地简化了，除了个人和时间的固定效应以外没有控制任何其他因素。命令方面，作者只使用了 `did_imputation` 命令中的基本语法，并对结果进行了绘图。  
```stata
did_imputation ln_wage id year gvar, horizons(0/15) pretrend(10) autosample
event_plot, default_look
```
回归结果及其绘图如下：（由于图像结果更为直观，后文将不再展示 Stata 结果）
```stata
                                                Number of obs     =     25,320
------------------------------------------------------------------------------
     ln_wage |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        tau0 |   .1402649   .0105697    13.27   0.000     .1195488    .1609811
        tau1 |   .1836363   .0164734    11.15   0.000     .1513492    .2159235
        tau2 |   .1407663    .016456     8.55   0.000     .1085131    .1730194
        tau3 |   .1740875   .0180989     9.62   0.000     .1386143    .2095607
        tau4 |   .1498706   .0274324     5.46   0.000      .096104    .2036371
        tau5 |   .1571927   .0188661     8.33   0.000     .1202158    .1941695
        tau6 |   .2139189   .0278652     7.68   0.000     .1593041    .2685337
        tau7 |   .1357196   .0253128     5.36   0.000     .0861074    .1853317
        tau8 |   .1621922   .0267014     6.07   0.000     .1098584    .2145259
        tau9 |   .1033001   .0386124     2.68   0.007     .0276212    .1789789
       tau10 |   .1615491   .0293586     5.50   0.000     .1040074    .2190908
       tau11 |   .1792902   .0365591     4.90   0.000     .1076358    .2509447
       tau12 |   .0729035   .0418943     1.74   0.082    -.0092078    .1550149
       tau13 |   .1786444   .0475709     3.76   0.000      .085407    .2718817
       tau14 |   .1457279   .0603516     2.41   0.016     .0274409    .2640149
       tau15 |   .0807922   .0476766     1.69   0.090    -.0126522    .1742366
        pre1 |   .1107894   .0310708     3.57   0.000     .0498918     .171687
        pre2 |   .1007835     .02689     3.75   0.000     .0480802    .1534869
        pre3 |   .0469902    .024383     1.93   0.054    -.0007995      .09478
        pre4 |   .0242747   .0293535     0.83   0.408    -.0332571    .0818065
        pre5 |   .0250323   .0238816     1.05   0.295    -.0217748    .0718393
        pre6 |  -.0138787   .0330943    -0.42   0.675    -.0787424     .050985
        pre7 |   .0115458   .0247486     0.47   0.641    -.0369605    .0600522
        pre8 |  -.0008114   .0248461    -0.03   0.974    -.0495089    .0478861
        pre9 |  -.0232657    .025459    -0.91   0.361    -.0731644     .026633
       pre10 |    .003627   .0233433     0.16   0.877     -.042125     .049379
------------------------------------------------------------------------------
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/event_lnwage.png)
> **图片说明**  
> 横轴：探究的时期，其中 “0” 表示干预时点  
> 纵轴：回归所得的估计参数值  
> 阴影部分上界：置信区间的上界  
> 阴影部分下界：置信区间的下界

从结果可以看到，加入工会确实对工资有着正向的影响（大约 15%）。不过，在受访者加入工会之前的 2-3 个调查期间内，她们的工资水平已经有所上升，这可能是由工资谈判或者受访者本身的工作变动造成的。但是再往前观察，我们会发现工会化对工资水平的影响非常微弱，因此可以认为平行趋势假设是成立的。  

接下来把因变量更换为每年工作的周数和每周工作的小时数，分别进行回归并作图：
```stata
ssc install addplot
did_imputation wks_work id year gvar, horizons(0/15) pretrend(10) autosample
event_plot, default_look
addplot:,title("Wks work per year") legend(off)

did_imputation hours id year gvar, horizons(0/15) pretrend(10) autosample
event_plot, default_look
addplot:,title("Hours work per week") legend(off)
```
<center class = "half">
<img src = "https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/event_wks_work.png" width = "50%" align = left><img src = "https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/event_hours.png" width = "50%" align = right>
</center>  

从回归结果中可以发现，在受访者加入工会前，工会化就已经对工作时长造成了一定的影响，似乎并不是那么符合平行趋势假设。不过整体而言，加入工会后工作时长依旧增加了。

&emsp;

### 3.3 安慰剂检验
在进行安慰剂检验时，作者进行了以下处理，从而确保模型中不存在处置效应：  
> 1、把所有曾经加入工会的受访者数据剔除，从而保证剩余的数据不受处置效应的影响  
> 2、对于剩下的受访者，对每个数据赋予随机数。假设她们不可能在受访期间的前两年加入工会，然后结合受访年份和随机数，随机把 union 替代为 1  
> 3、按照之前的方式处理数据  
> 4、重复分析

这一安慰剂检验建立在随机分布之上，因此作者设置了 4 次随机数重复进行回归（即为种子的赋值可以随机更换）。如果可以保证回归结果不体现出处置效应和预期效应的影响，也不违反平行趋势假设，那么原假设成立。
```stata
set seed 1
replace union = 0
replace union =1 if runiform()<0.05 & year >= 70  
bysort id (year):replace union = 1 if union[_n-1]==1
bysort id: egen aux2 = min(year) if union==1
bysort id:egen gvar2 = max(aux2)
did_imputation ln_wage id year gvar2, horizons(0/15) pretrend(10) autosample
event_plot, default_look
```
<center class = "half">
<img src = "https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/event_f1.png" width = "50%" align = left><img src = "https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/event_f2.png" width = "50%" align = right>
</center>
<center class = "half">
<img src = "https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/event_f3.png" width = "50%" align = left><img src = "https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/event_f4.png" width = "50%" align = right>
</center>
然而，安慰剂检验证明原估计并不成立。通过图形我们可以观察到，四次检验中都出现了比较明显的预期效应，即在干预时点前几期工资水平都略有上涨（大约 10%）。

&emsp;

## 4. 结论
本次回归运用了一个非常简单的数据集来分析工会化对工资增长的影响，但是安慰剂检验的结果拒绝了原假设，这个问题很可能是由难以控制和观测的其他随时间变化的因素造成的。不过，工会化和工资增长的关系是一个非常有意义的话题，值得进一步的探究。  

此外，如本例所示，这个新的 DID 方法或许不应该被盲目地应用。作者认为，这个方法或许仅适用于估计因果效应的安慰剂检验，而非用于排除一些由难以观测、控制和解释的因素造成的影响。

&emsp;

## 5. 参考资料和相关推文
- Goodman-Bacon, A. (2021). Difference-in-differences with variation in treatment timing. Journal of Econometrics. [-PDF-](http://goodman-bacon.com/pdfs/ddtiming.pdf)  
- Rios-Avila, F. (2020). Understanding what went wrong with DID (or DnD/DD/Diff in Diff)? [-Link-](https://friosavila.github.io/playingwithstata/main_didmany.html)  
- Butts, K., & Gardner, J. (2021). Did2s: Two-Stage Difference-in-Differences. arXiv preprint arXiv:2109.05913. [-PDF-](https://arxiv.org/pdf/2109.05913)  
- Borusyak, K., Jaravel, X., & Spiess, J. (2021). Revisiting event study designs: Robust and efficient estimation. arXiv preprint arXiv:2108.12419. [-PDF-](https://arxiv.org/pdf/2108.12419)