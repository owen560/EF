
&emsp;

> **作者：** 吴奕玮 (中山大学)        
> **E-mail:**  <wuyw56@mail2.sysu.edu.cn>   

> **编者按**：本文主要摘译自下文，特此致谢！     
> **Source:** Abaluck, J., A. Adams-Prassl, 2021, What do consumers consider before they choose? Identification from asymmetric demand responses, The Quarterly Journal of Economics, 136 (3): 1611-1663. [-Link-](https://doi.org/10.1093/qje/qjab008)


&emsp;


---

**目录**

[TOC]

---





  



&emsp;

  

## 1. alogit模型简介

### 1.1 理论背景

`alogit`为 Abalack 和 Adams（2017）中的消费者选择模型计算了一个（非）专注的logit模型。

  

传统离散选择模型通常假设消费者在做出选择时考虑所有可用的选项。考虑集模型是对离散选择模型的一种概况，它放宽了个人考虑所有商品的假设。考虑集产生的一种可能原因就是注意力不集中或有界理性。

`alogit`的核心思想在于，它在分析消费者选择这个二元选择模型中加入了注意力这一因素。一个不专注于某特定商品的消费者在选择商品时，不仅根据商品的特性，还关注该商品的概率。

另外，`alogit` 还可以估计模型的一个版本，其中注意力的概率仅取决于默认商品，即 DSC 模型。在这个版本中，消费者要么关注所有商品并做出选择，要么保持注意力不集中并默认为默认商品。

  

### 1.2 Stata 实操


在alogit模型中，考虑集是有概率的。每个备选方案都有不被考虑的可能性，这是由模型估计的。因此，存在概率非零的完全为空的考虑集。为了较好地确定模型，当考虑集为空时，用户应该说选择了哪个选项。默认选项通过`default()`指定。如果缺少，则用户必须通过`consider()`指定哪些商品被考虑的概率为 1（因此永远不会有空集）。

`alogit`将空集的概率存储在`e(mp0)`中，如果它是不可忽略的，则向控制台输出警告（当传递`consider()`时，为了计算该概率，将忽略它）。


#### 命令安装
```stata
net describe alogit, from(http://fmwww.bc.edu/RePEc/bocode/a)

net install alogit.pkg, replace

net get alogit.pkg, replace  // Stata 范例数据和代码
```
#### 命令语法

```stata
alogit depvar [indepvars] [if] [in] , {default(varname) | consider(varname)} group(varname) [options]
```
其中，`depvar`表示消费者的选择，`indepvars`是影响选择和注意力的协变量，`zvars`是只影响注意力的变量。`options` 主要包含以下内容：

`group(varname) `：个体标识

`default(varname)`：表示每个个体的默认选择商品的二元变量

`consider(varname)`：表示关注概率为1的商品的二元变量

`avars(varlist) `：在注意力概率估计中使用`varlist`代替`indepvars`

`zvars(varlist)` ：在注意力概率估计中包含`varlist`

`exclude(varlist) `：从注意力概率估计中排除`varlist`

`noconstant`：估计`P(A)`时不要自动添加常数

`model(str)`：模型选择`alogit` 或`dsc`

`b0(numlist) `：计算`P(Y)`时独立变量的起始参数

`g0(numlist) `：计算`P(A)`时，`indepvars`或`avars`的启动参数

`d0(numlist)` ：计算`P(A)`时`zvars`和常数的起始参数

`exp `：估计`exp(b) `而不是 `b`

`method(str)` ：方法选择`exact` 或` importance`

`reps(int)` ：`method(importance)`方法下随机抽样的选择集数量

`noprob(str)`：如何处理P(A) = 0的情况，`error`或`drop`

&emsp;

```stata
. alogit chosen price_dm proddum*, ///
>                 group(benetime) method(exact) default(proddum9) ///
>                 technique(nr) difficult b0(`betavec')
Using method 'exact' to compute the likelihood.
Balanced panel. n = 7,450, j = 10

initial:       log likelihood = -16354.487
rescale:       log likelihood = -16305.861
rescale eq:    log likelihood = -15724.777
Iteration 0:   log likelihood = -15724.777  (not concave)
Iteration 1:   log likelihood = -15660.292  (not concave)
Iteration 2:   log likelihood = -15552.299  (not concave)
Iteration 3:   log likelihood = -15406.521  
Iteration 4:   log likelihood = -15400.143  
Iteration 5:   log likelihood = -15393.281  
Iteration 6:   log likelihood = -15392.646  
Iteration 7:   log likelihood = -15392.606  
Iteration 8:   log likelihood = -15392.602  
Iteration 9:   log likelihood = -15392.602  

Attentive logit regression                        Log likelihood  = -1.539e+04
                                                        Number of obs = 74,500

------------------------------------------------------------------------------
      chosen | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
PY           |
    price_dm |  -.1956824   .0275155    -7.11   0.000    -.2496118    -.141753
    proddum1 |   1.465463   .5388418     2.72   0.007     .4093527    2.521574
    proddum2 |  -.0651336   .4777768    -0.14   0.892    -1.001559    .8712916
    proddum3 |   .6252745   .4755208     1.31   0.189    -.3067291    1.557278
    proddum4 |   .6293878    .466022     1.35   0.177    -.2839984    1.542774
    proddum5 |   .7071189   .4784509     1.48   0.139    -.2306276    1.644865
    proddum6 |  -.7367531   .1209819    -6.09   0.000    -.9738733   -.4996328
    proddum7 |  -1.279593   .1411422    -9.07   0.000    -1.556227   -1.002959
    proddum8 |  -1.184582   .1372708    -8.63   0.000    -1.453628   -.9155361
    proddum9 |  -.5614824   .1178784    -4.76   0.000    -.7925197    -.330445
-------------+----------------------------------------------------------------
PA           |
    price_dm |   .1365664   .0169554     8.05   0.000     .1033344    .1697983
    proddum1 |  -2.872102    .176639   -16.26   0.000    -3.218308   -2.525896
    proddum2 |   -2.67366   .2884712    -9.27   0.000    -3.239053   -2.108267
    proddum3 |  -2.694818   .2087934   -12.91   0.000    -3.104046   -2.285591
    proddum4 |  -2.704148   .2047001   -13.21   0.000    -3.105353   -2.302944
    proddum5 |  -2.592284   .2044904   -12.68   0.000    -2.993078    -2.19149
    proddum6 |   .1516943   .1915878     0.79   0.428    -.2238109    .5271995
    proddum7 |   .1226172    .291815     0.42   0.674    -.4493298    .6945642
    proddum8 |   .2577786   .2300109     1.12   0.262    -.1930346    .7085917
    proddum9 |   .1032341   .1761176     0.59   0.558    -.2419501    .4484184
       _cons |   .6238753   .2168381     2.88   0.004     .1988804     1.04887
------------------------------------------------------------------------------

```

## 2. Abaluck and Adams-Prassl (2021)

### 2.1 问题背景

文章利用引入注意力的考虑集模型，识别价格或其他商品特征变化对消费者选择的影响。过往研究认为特征变化通过考虑或效用影响消费者选择，并由此分为两类模型。第一类，默认考虑模型（DSC）通过考虑影响选择，消费者“沉睡”并选择默认选项，或从所有产品中做出积极的选择。第二类，替代考虑模型（ASC）通过效用影响选择，消费者对每种商品都有一个取决于相关商品特征的、独立的考虑概率。

这两种类型的经验模型通常依赖于关于所考虑的商品的辅助数据，或者依赖于结构函数点识别的附加排除限制。这些排除限制通常是有问题的，并且可能与经济理论相矛盾。哪些变量影响效用哪些变量影响注意力，都与对象的部分识别有关。


### 2.2 实证模型

#### 2.2.1 基本框架
考虑个体$i$在$J+1$种产品中进行离散选择，$\mathcal{J}=\{0,1,...,J\}$，其中$J\ge1$。每个产品$j$与价格$p_j$相关。价格向量 $\mathbf{p}=[p_0,...,p_J]$ 在 $\mathbb{R}_{++}^{J+1}$ 上成立。

让$\mathcal{P}(\mathcal{J})$代表商品的考虑集，的任何给定元素都以$C$为索引，那么包含商品$j$的考虑集就给出为：

$$
\mathbb{P}(j)=\{C:\{0, j\} \subseteq C \in \mathcal{P}(\mathcal{J})\}
$$

选择概率采取以下形式：
$$
s_j(\mathbf{p})=\sum_{C \in \mathbb{P}(j)} \pi_C(\mathbf{p}) s_j^{\star}(\mathbf{p} \mid C),
$$

其中，$s_j\equiv s_j(\mathbf{p})$ 是市场价格为$\mathbf{p}$时商品$j$被购买的观察概率，$\pi_C(\mathbf{p})$ 是给定观察特征下商品集$C$被考虑的概率，$s_j^{\star}(\mathbf{p} \mid C)$ 是商品$j$从商品集$C$中被选中的概率。

$$
\sum_{C \in \mathcal{P}(\mathcal{J})} \pi_C(\mathbf{p})=1, \quad \sum_{j \in C} s_j^{\star}(\mathbf{p} \mid C)=1
$$

##### 基础理论假设
Daly-Zachary 条件：未观察到的潜在选择概率$s_j^{\star}(\mathbf{p} \mid C)$，在 $\mathbb{R}_{++}^{J+1}$ 上处处满足以下条件：
1. 性质：$s_{j}^{\star}(\mathbf{p}|C)\geqslant0,\sum_{j\in C}s_{j}^{\star}(\mathbf{p}|C)=1,$ 
$\frac{\partial^Js_j^*(\mathbf{p}|C)}{\partial p_0\ldots\partial p_{j-1}\partial p_{j+1}\ldots\partial p_J}$ 恒存在，大于等于0且连续。
2. 对称性：交叉价格衍生品是对称的。
$$
\dfrac{\partial s_j^*(\mathbf{p}|C)}{\partial p_j}=\dfrac{\partial s_{j'}^*(\mathbf{p}|C)}{\partial p_j}.
$$

3. 无名义幻觉：$s_j^\star(\mathbf{p}+\delta|C)=s_j^\star(\mathbf{p}|C).$

##### 基础数据假设
在 $\mathbf{p} \in\mathbb{R}_{++}^{J+1}$ 上观察到的人口市场份额、自有和交叉价格衍生品包含如下变量：
$$
\left\{s_j(\mathbf{p}),\dfrac{\partial s_j(\mathbf{p})}{\partial p_{j'}}\right\}_{j,j'\in\mathcal{J}}.
$$
#### 2.2.2 默认特定模型（DSC）
DSC模型中，默认商品和非默认商品的市场份额形式如下：
$$
\begin{aligned}
& s_0(\mathbf{p})=\left(1-\mu\left(p_0\right)\right)+\mu\left(p_0\right) s_0^{\star}(\mathbf{p} \mid \mathcal{J}) \\
& s_j(\mathbf{p})=\mu\left(p_0\right) s_j^{\star}(\mathbf{p} \mid \mathcal{J}) \quad \text { for } j>0
\end{aligned}
$$
其中，微分函数 $\mu\left(p_0\right)$ 给出了考虑所有可能产品的概率，$s_0^{\star}(\mathbf{p} \mid \mathcal{J})$ 给出了在考虑所有产品的条件下选择商品$j\in \mathcal{J}$的概率

##### DSC假设
当 $p_0\to\infty,\mu(p_0)\to1\text{.}$


##### 识别考虑概率的变化
$$
\frac{\partial\log(\mu_{0})}{\partial p_{0}}=\frac{1}{s_{j}(\mathbf{p})}\bigg[\frac{\partial s_{j}(\mathbf{p})}{\partial p_{0}}-\frac{\partial s_{0}(\mathbf{p})}{\partial p_{j}}\bigg].
$$
如果默认选项的价格是通过引起消费者的“觉醒”影响考虑，则非默认选项会对价格更加敏感。

##### 识别考虑的水平
$$
\begin{aligned}\mu(\tilde{p}_0)=\exp\left(-\int_{\tilde{p}_0}^\infty\frac{1}{s_j(\mathbf{p})}\left[\frac{\partial s_j(\mathbf{p})}{\partial p_0}-\frac{\partial s_0(\mathbf{p})}{\partial p_j}\right]d p_0\right).\end{aligned}
$$
##### LOGIT考虑下$\mu({p}_0)$的识别

$$
\begin{aligned}\mu(p_0)=\frac{\text{exp}(\gamma_0+\gamma_p p_0)}{1+\text{exp}(\gamma_0+\gamma_p p_0)}.\end{aligned}
$$


#### 2.2.3 替代特定模型（ASC）
ASC模型中，考虑集概率的形式如下：
$$
\pi_C(\mathbf{p})=\prod_{j \in C} \phi_j\left(p_j\right) \prod_{j^{\prime} \notin C}\left(1-\phi_{j^{\prime}}\left(p_{j^{\prime}}\right)\right)
$$

其中，商品$j$被考虑的概率 $\phi_j \equiv \phi_j\left(p_j\right)$ 是一个只关于商品自身特征的微分函数，而且对于所有 $p_0$ 有 $\phi_j\left(p_0\right)=1$。观察到市场份额的形式为：
$$
s_j(\mathbf{p})=\sum_{C \in \mathbb{P}(j)} \prod_{l \in C} \phi_l\left(p_l\right) \prod_{l^{\prime} \notin C}\left(1-\phi_{l^{\prime}}\left(p_{l^{\prime}}\right)\right) s_j^{\star}(\mathbf{p} \mid C)
$$


##### ASC假设
1. 当 $p_0\to\infty,\phi_j(p_j)\to1\text{.}$
2. 在 $\mathbf{p}\in\mathbb{R}^{J+1}_{++}$ 上，$s_0(\mathbf{p})-s_0(\mathbf{\bar{p}}_j)\neq0$S

##### 识别考虑概率的变化
$$
\frac{\partial\log(\phi_j)}{\partial p_j}=\frac{\frac{\partial s_0(\textbf{p})}{\partial p_j}-\frac{\partial s_j(\textbf{p})}{\partial p_0}}{\textbf{s}_0(\textbf{p})-s_0(\bar{p})}
$$
##### $\phi_j({p}_j)$的识别
$$
\phi_j(\tilde{p}_j)=\exp\left(-\int_{\tilde{p}_j}^\infty\frac{\frac{\partial s_0(\mathbf{p})}{\partial p_j}-\frac{\partial s_j(\mathbf{p})}{\partial p_0}}{s_0(\mathbf{p})-s_0(\bar{\mathbf{p}}_j)}\text{d}p_j\right)
$$

#### 2.2.4 混合考虑集模型
混合考虑集模型中，默认商品和非默认商品的市场份额形式如下：
$$
\begin{aligned}
& s_0(\mathbf{p})=\left(1-\mu\left(p_0\right)\right)+\mu\left(p_0\right) \sum_{C \in \mathbb{P}(0)} \prod_{l \in C} \phi_l\left(p_l\right) \prod_{l^{\prime} \notin C}\left(1-\phi_{l^{\prime}}\left(p_{l^{\prime}}\right)\right) s_0^{\star}(\mathbf{p} \mid C) \\
& s_j(\mathbf{p})=\mu\left(p_0\right) \sum_{C \in \mathbb{P}(j)} \prod_{l \in C} \phi_l\left(p_l\right) \prod_{l^{\prime} \notin C}\left(1-\phi_{l^{\prime}}\left(p_{l^{\prime}}\right)\right) s_j^{\star}(\mathbf{p} \mid C) \quad \text { for } j>0,
\end{aligned}
$$

##### 识别考虑概率的变化
$$
\frac{\partial s_{j}(\mathbf{p})}{\partial p_{0}}-\frac{\partial s_{0}(\mathbf{p})}{\partial p_{j}}=\frac{\partial\log(\mu_{0})}{\partial p_{0}}s_{j}(\mathbf{p})-\frac{\partial\log(\phi_{j})}{\partial p_{j}}(s_{0}(\mathbf{p})-s_{0}(\mathbf{\bar{p}}_{j})).
$$
#### 2.2.5 过度认同
鉴于文章的假设，不完美的考虑是引起不对称交叉衍生品的唯一机制。然而，放宽背景假设可能会引起不对称性的其他来源，错误地归因于注意力不集中。因此，文章的模型是过度识别的，并且框架预测的不对称具有特定的结构。当$J\gt2$时，对数考虑概率的导数被过度识别；从直觉上看，交叉价格衍生工具的差异大于考虑概率。




### 2.3 结果探讨
在实验设计方面，文章先在实验室用自己产生的消费实验数据验证了识别结果的实际意义，而后使用该模型预测了医疗保险 D 部分“智能默认”政策的效果，并对结果进行规范性评估。

#### 2.3.1 实验室验证
作者对149名耶鲁大学的学生进行了离散选择的消费实验。选择了耶鲁大学书店出售的10种商品，清单价格从19.98美元到24.98美元不等。每个参与者都有25美元，从这10种商品中随机选择的子集中做出50个选择，价格也是随机的清单价格的三分之一加上0到16美元之间的均与分布的金额。消费者在显示的子集中所有产品的图像以及价格中，被要求选择他们的首选方案。把出现在受访者屏幕上的产品子集视为考虑集。每种商品出现在屏幕上的概率是由作者事先固定的——这种概率在不同的商品和价格之间变化，如果商品的价格较高，则更有可能被考虑。

作者使用关于受访者看到的所有数据，包括提前设置好的考虑系数，给出了“真实”的考虑和偏好系数。同时，在偏好参数的情况下， 采用最大似然估计法估计的条件 Logit 参数估计，与文章设定的模型估计结果进行比较。

结果显示，条件Logit模型中，一个由所有10种商品组成的考虑过的选择集的价格效应不到真实值的三分之一。条件Logit模型错误地从高价产品更有可能被考虑的事实中推断出消费者不是真正不喜欢高价。此外，条件Logit固定效应是有系统性偏差的，它们混淆了考虑和效用——那些很少出现在考虑的选择集中的产品被假定为低效用。相比之下，文章设定的考虑集模型能够准确地恢复产生选择的过程。最大似然法和间接推理策略估计给出的考虑价格影响的置信区间均包含真实值。考虑集模型也恢复了偏好的固定效应，准确区分了考虑和偏好。

#### 2.3.2 医疗保险D部分的有限考虑和智能默认

医疗保险D部分计划向美国的老年提供处方药保险。该计划创建于2006年，以应对药品支出的增加，为当时没有处方药保险的老年医疗保险受益人带来大量的自付费用。主要分析样本包括100,000名随机选择的非双重受益人，他们在2008-2009年加入了该计划。我们将样本限制在我们观察到的前一年计划的受益人 (放弃了17.5%的受益人)。为了管理估计替代性特定注意参数的计算负担，作者也重新严格了样本，只包括市场份额至少占有关州的1.5%的计划。平均而言，受益人在某一年面临11.9个计划的选择， 93.7%的人坚持前一年购买的同一计划 (默认计划) 。这与之前对医疗保险D部分选择行为的研究相一致 。

在给出模型的结构性估计前，作者对个体 $i$ 在 $t$ 年是否转换计划的指标与默认计划的属性和替代计划的平均属性（包含年份和受益人固定效应）进行面板回归，以简要证明部分惯性是由于默认计划和替代计划的特征不对称性导致的注意力不集中所驱动。结果显示，在所有规格中，转换决策对违约保费和免赔额的敏感度明显高于竞争对手的属性 （对于保费，对违约的敏感度几乎是竞争对手属性的三倍）。许多消费者在每个时期都不主动搜索，但如果默认计划变得足够糟糕，就会被诱导做出主动选择。在这种情况下，默认计划的属性通过效用和促使消费者主动选择来影响选择。

为了量化有限考虑与效用在合理选择医保计划中的重要性，该部分选用了文章设定的混合模型。即消费者要么“沉睡”，选择默认的商品；如果默认的商品变得足够坏，他们就会“醒来”，做出积极的选择。在不考虑注意力的条件 Logit 模型中，消费者对保费的权衡比自付费用更重，即使通过自付费用控制了这些自付费用的财务影响，消费者似乎也对计划属性有反应。绝大多数消费者可能选择默认计划。在条件逻辑模型下的适应成本为 1,224 美元。

接着利用`alogit` 对同一样本进行混合模型的估计。考虑有限的注意力后，该模型下的适应成本为 287 美元，不到假设充分考虑的成本的四分之一。在条件 Logit 模型的估计中，消费者是喜欢风险的。然而，考虑到有限的注意力，他们似乎是规避风险的，消费者对计划属性的反应比条件 Logit 模型所暗示的要大。


出于上述保险计划选择模式的惯性，Handel和Kolstad (2015) 提出了“智能默认”政策——如果个人从这种转换中获得的货币收益超过某一阈值，则将其转换为每年可用的最低成本计划，所有参保者都将保留选择退出默认计划的能力，切换回原来的计划，或选择任何可用的替代计划。该政策尚未实施。文章依靠现有数据和设定的模型来预测消费者对政策的反应，并对结果进行规范评估。结果表明，尽管D部分中的大多数惯性是由疏忽造成的，但仍存在与效用相关的显著调整成本。文章模拟了“智能违约”政策的福利效应，发现消费者改为较低成本的计划可以产生巨大的收益。这与预测“智能默认”政策不会对选择行为和消费者福利产生影响的假设充分考虑的模型形成鲜明对比。

## 3. 总结
大量模型表明，偏离 Slutsky 对称性是不完善注意力的表现。Abaluck and Adams-Prassl (2021)的建设性识别结果采用了DSC和ASC模型的加强结构，从离散选择数据中识别出考虑概率，提供了分析各种环境下改变默认值的政策的通用经验框架。
模型揭示向默认选项的转变是由于不专注。通过允许“觉醒”个体在一定范围内考虑替代选项，模型避免了夸大与效用有关的转换成本。该模型是根据数据估计考虑概率。如果没有明确的注意力微观基础，我们无法预测改变潜在的背景时注意力会如何转移。但应用表明，模型的福利结论对关于这种转移的其他假设是可靠的。该考虑集模型在未来的相关政策场景中有机会大展身手。



## 参考资料和相关推文

- Bernheim, B. Douglas, Andrey Fradkin, and Igor Popov. 2015. The Welfare Economicsof Default Options in 401(k) Plans. _American Economic Review_ 105: 2798 2837.[-PDF-](https://www.nber.org/system/files/working_papers/w17587/w17587.pdf)



- 专题：[Stata命令](https://www.lianxh.cn/blogs/43.html)
  - [Stata新命令：面板-LogitFE-ProbitFE](https://www.lianxh.cn/news/18a8416f25cce.html)
- 专题：[面板数据](https://www.lianxh.cn/blogs/20.html)
  - [Stata：面板Logit的边际效应和处理效应估计-mfelogit](https://www.lianxh.cn/news/7012e029c7ce4.html)
- 专题：[交乘项-调节-中介](https://www.lianxh.cn/blogs/21.html)
  - [Logit-Probit中的交乘项及边际效应图示](https://www.lianxh.cn/news/80c79d978aa61.html)
- 专题：[Probit-Logit](https://www.lianxh.cn/blogs/27.html)
  - [全面解读Logit模型](https://www.lianxh.cn/news/c6d6badebe2a7.html)
  - [Logit-Probit：非线性模型中交互项的边际效应解读](https://www.lianxh.cn/news/16508d946978a.html)
  - [秒懂小罗肥归：logit与mlogit详解](https://www.lianxh.cn/news/e7215baf788bf.html)
  - [reg2logit：用OLS估计Logit模型参数](https://www.lianxh.cn/news/b9a9a25cce908.html)
  - [feologit：固定效应有序Logit模型](https://www.lianxh.cn/news/f3467f72cfbf2.html)
  - [Stata：多元 Logit 模型详解 (mlogit)](https://www.lianxh.cn/news/270f2c9e75d4a.html)
  - [Stata：Logit模型一文读懂](https://www.lianxh.cn/news/e18031ffad4f3.html)
  - [详解 Logit/Probit 模型中的 completely determined 问题](https://www.lianxh.cn/news/a03aa1b7e32e0.html)
  - [Stata：Logit 模型评介](https://www.lianxh.cn/news/011888bd49e07.html)
  - [Stata：嵌套 Logit 模型 (Nested Logit)](https://www.lianxh.cn/news/d5e00bfb17a7c.html)

