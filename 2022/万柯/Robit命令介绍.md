# robit 模型及应用

> &#x1F353; 点击右上方的【编辑】按钮即可查看本文原始 Markdown 文档，以便了解格式。

> **作者：** 万柯 (中山大学)  
> **E-mail:** <wank@mail2.sysu.edu.cn>

&emsp;

---

**目录**

[TOC]

---

&emsp;

## 1. 模型简介

本篇推文介绍 robit 回归模型：`robit` 。使用该模型的优点在于，基于 robit 模型的推断对离群观测值的存在具有稳健性。该模型将 probit 回归模型中的正态分布替换为自由度已知的 t 分布。7 df 被推荐，类似于 logit 链接函数，但是受离群值影响较小。一般来说， robit 连接函数的自由度越小,受到离群值的影响越小。在极限情况，当 k 趋于无穷时，含有 k 个自由度的 robit 连接函数会成为 probit 连接函数。
此外，想要用 robit 模型的更多功能的同学可以使用 glm 命令，并通过 robit 连接选项 xlink。例如，robit y x1 x2, df(4)等价于 glm y x1 x2， family(binomial) link(robit4) 当说明非标准最大化选项或显示选项是必要的，使用 glm 代替 robit 可能是有利的。

## 2. 下载及安装

由于 `robit` 命令使用了 `xlink` 命令其中的连接函数 ，因此二者需同步安装。

```Stata
.ssc install xlink, replace
.ssc install robit, replace
```

## 3. robit 语法介绍

### 3.1 基础语法介绍

`robit` 的语法结构如下：

```Stata
· robit  depvar [indepvars] [if] [in] [weight], dfreedom(#) [options]
```

其中：

- `depvar`表示因变量，且必须为二值因变量，`indepvars`是自变量
- `dfreedom(#)`是必要的，确定了 robit 模型中 t 分布的自由度，可以选择 1 到 10 之间的整数。

- `weight` 用于设定数据的权重，

### 3.2 options 介绍

`[, options]`
| options | 介绍 |
| :--------- | :--: |
| `noconstant` | 去除了模型中的常数项|
| `offset (`**varname_o**`)` | 将**varname_o**作为偏移量包含在模型中，并将系数约束为 1 |
|`constraints(`**numlist**`)` | 给定在估计期间应用的线性约束。默认值是执行无约束估计。 |
|`asis`|强制保留了完美的预测变量及其相关的，完美预测的观测值|
| `vce (vcetype)` |用于指定所报告的稳健型标准误的类型。可能的类型包括异方差稳健型标准误 (`robust`)，聚类调整的稳健型标准误(`cluster`)，自体抽样法稳健型标准误(`bootstrap`)和刀切法稳健型标准误(`jackknife`)|
|`level(#)`| 用于指定置信水平，如果没有，则设置为 95。|
|`noheader`|去除了输出中的表头信息。系数表仍然显示|
|`notable`|去除了系数表的输出。表头仍然显示|
|`collinear` |使得估计时不会忽略共线性变量。这个选项很少使用，因为共线变量使模型难以识别。但是，我们可以向模型添加约束来识别它。例如，如果变量 x1 和 x2 共线，但是我们限制 x2 上的系数是 x1 上的系数的倍数，那么模型就可以在有共线的变量时被识别。在这种情况下，我们指定共线，以便 x1 和 x2 都保留在模型中。 |
|`coeflegend`|Stata 不显示系数结果，而是显示系数的图例以及如何在表达式中指定它们。 |
|`difficult` |由于在非凹区域，似然函数可能难以最大化。没有人能保证 difficult 会比默认值更好;有时这样更好，有时又糟。只有当默认步进器声明收敛,且最后一次迭代是“非凹”的时，或者当默认步进重复发出“非凹”消息并对对数概率只有很小的改进时，才应该使用`difficult`选项。|
|`from()`|指定回归系数的初始值 |

## 4. Stata 实操应用

下面使用 Stata 系统数据展示 `robit` 命令的实际用法和效果。

```Stata
sysuse "auto.dta", clear //调用系统数据
robit foreign mpg weight, dfreedom(4) vce(robust） //使用自由度为4的robit模型，同时保持稳健性
```

如下为执行 `robit` 命令后的结果，

```Stata
Iteration 0:   log pseudolikelihood = -29.530355
Iteration 1:   log pseudolikelihood = -27.462724
Iteration 2:   log pseudolikelihood = -27.421741
Iteration 3:   log pseudolikelihood = -27.421695
Iteration 4:   log pseudolikelihood = -27.421695

Model: Robit with 4 d.f.

Number of obs: 74
Wald chi2(2): 23.050627
Prob > chi2: 9.877e-06
Log pseudolikelihood: -27.421695
------------------------------------------------------------------------------
             |               Robust
     foreign | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
foreign      |
         mpg |  -.1124357   .0814722    -1.38   0.168    -.2721184     .047247
      weight |  -.0026655   .0006224    -4.28   0.000    -.0038854   -.0014456
       _cons |   9.289056   3.261723     2.85   0.004     2.896196    15.68192
------------------------------------------------------------------------------

```

## 5. 结语

本篇推文主要使用 Stata 实操举例介绍了 robit 回归模型 —— `robit` ，

## 参考资料和相关推文

- [Liu, C. H. 2004. Robit Regression: A Simple Robust Alternative to Logistic and Probit Regression.](https://onlinelibrary.wiley.com/doi/10.1002/0470090456.ch21)

* 专题：[Stata 命令](https://www.lianxh.cn/blogs/43.html)
  - [Stata 新命令：面板-LogitFE-ProbitFE](https://lianxh.cn/news/18a8416f25cce.html)
* 专题：[Probit-Logit](https://lianxh.cn/blogs/27.html)
  - [二元选择模型：Probit 还是 Logit？](https://lianxh.cn/news/979079951adf2.html)