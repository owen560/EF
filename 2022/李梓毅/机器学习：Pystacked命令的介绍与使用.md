# 机器学习：Pystacked命令的介绍与使用

&emsp;

> &#x1F353; 点击右上方的【编辑】按钮即可查看本文原始 Markdown 文档，以便了解格式。  

> **作者:** 徐沫然(中山大学) ，李梓毅 (中山大学)  
   
> **E-mail:** <xumr6@mail2.sysu.edu.cn>, <leicn@mail2.sysu.edu.cn>   

> **Source:** 
- Harrison, D. and Rubinfeld, D.L (1978). Hedonic prices and the demand for clean air. J. Environ. Economics & Management, vol.5, 81-102, 1978.
- Hastie, T., Tibshirani, R., & Friedman, J. (2009). The elements of statistical learning: data mining,inference, and prediction. Springer Science & Business Media.
- [Wolpert, David H. Stacked generalization. Neural networks 5.2 (1992): 241-259.](https://www.sciencedirect.com/science/article/abs/pii/S0893608005800231?via%3Dihub)

&emsp;

---


**目录**

* [1. 引言](#1)
* [2. 语法](#2)
 * [2.1 两种格式](#2.1)
 * [2.2 基学习器类型](#2.2)
 * [2.3 选项](#2.3)
 * [2.4 其他选项](#2.4)
* [3. 后估计和预测选项](#3)
  * [3.1 后估计](#3.1)
  * [3.2 预测](#3.2)
* [4. Pipelines](#4)
* [5. 实例](#5)
  * [5.1 例子一：波士顿房价预测](#5.1) 
  * [5.2 例子二：垃圾邮件分类](#5.2) 
* [6. 参考文献](#6)
* [7. 相关推文](#7)

---

&emsp;

# <span id="1">1. 引言</span>

> pystacked是Stata中一个常用在叠回归（Wolpert, 1992）的命令。叠回归是一种是利用多个模型的输出集成起来产生新模型的一种模型集成技术。现有的基学习器包括线性回归、逻辑回归、Lasso回归、岭回归、弹性网络、支持向量机、梯度提升树以及神经网络。

- pystacked也可以仅用于单个基学习器，因此，pystacked也可以是一个容易使用的scikit-learn数据库中机器学习算法接口。
- pystacked要求至少需要**Stata 16.0**（或者更新的版本）。

安装及更新pystacked命令的方法如下：

```
net install pystacked, from(https://raw.githubusercontent.com/aahrens1/pystacked/main) replace
```

如果无法通过上述命令安装，也可以尝试通过`search pystacked`找到相应的安装包进行安装。

若想了解更多关于skicit-learn数据库的信息，可以参考[网站](https://scikit-learn.org/stable/install.html)里的内容。

&emsp;

# <span id="2">2. 语法</span>

## <span id="2.1">2.1 两种格式</span>

- pystacked使用有两种使用语法。第一种格式为：

```
pystacked depvar predictors [if exp] [in range] [,methods(string) cmdopt1(string) cmdopt2(string) ...  pipe1(string) pipe2(string) ...  xvars1(varlist) xvars2(varlist) ...  otheropts ]
```

第二种格式为：

```
pystacked depvar predictors || method(string) opt(string) pipeline(string) xvars(varlist) || method(string) opt(string) pipeline(string) xvars(varlist) || ...  || [if exp] [in range] [, otheropts ]
```

在第一种格式之中，`methods(string)`用来选择基学习器，其中`string`表示所使用的各种基学习器，默认为`"ols lassocv gradboost"`,而`cmdopt1(string)`到`cmdopt10(string)`可以调节基学习器来源的库，其中默认为来自**scikit-learn**。`pipe(string)`可以用Python中的预处理预测器（详情参考后文[Pipelines](#4)部分）。而`xvars(varlist)`用于说明一个每一个具体学习器用来进行预测的自变量。

在第二种语法当中，基学习器不再具有数量限制（但是基学习器的增加会使得计算变得更加复杂，运算时间将变得更长）。`methods(string)`和`opt(string)`将基学习器添加进计算之中。其中每个基学习器都使用“||”相互隔开。

## <span id="2.2">2.2 基学习器类型</span>

- `method(string)`可以添加的基学习器有如下几种：

格式|基学习器
:-:|:--:
method(ols)|Linear regression(regression only)
method(logit)|Logistic regression (classification only)
method(lassoic)|Lasso with penalty chosen by AIC/BIC (regression only)
method(lassocv）|Lasso with cross-validated penalty
method(ridgecv）|Ridge with cross-validated penalty
method(elasticcv）|Elastic net with cross-validated penalty
method(svm）|Support vector machines
method(gradboost）|Gradient boosting
method(rf）|Random forest
method(linsvm）|Linear SVM
method(nnet）|Neural net

## <span id="2.3">2.3 选项</span>

- `cmdopt1(string)`（第一种格式）和`opt(string)`(第二种格式)可以将option传递给基学习器，例如可以将随机森林最大深度传递给基学习器。默认的代码库为scikit-learn。

## <span id="2.4">2.4 其他选项</span>

- `otheropts`包括很多其他选项：

option|解释
:-:|:--
type(string)|`type(reg)`(或者`type(regress)`)声明为回归器，`type(class)`(或者`type(classify)`)声明为分类器
finalest(string)|用于将所有基学习器进行组合。默认为无截距非负最小二乘法，附加约束的权重总和为1（`finalest(nnls1)`）。备选方案还有`finalest(nnls0)`（附加约束的权重总和为0）、`finalest(singlebest)`（所有基学习器组合后拥有最小的MSE）、`finalest(ols)`以及`finalest(ridge)`（岭回归）
nosavepred|不存储任何值（如果想要进行预测则不能使用）
nosavetransform|不能存储基学习器的计算结果（如果想要进行预测则不能使用）
njobs(int)|并行计算的数量，默认值是0，-1表示使用所有可用CPU，-2则表示比-1使用的CPU少一个

&emsp;

# <span id="3">3. 后估计和预测选项</span>

## <span id="3.1">3.1 后估计</span>

### <span id="3.1.1">3.1.1 后估计表</span>

- 在估计之后，pystacked命令可以报告一个**叠回归**和**基学习器**样本内和样本外（可选）的表现表。对于回归问题，这个表格会报告**MSPE**（均方预测误差）；对于分类问题，会报告**混淆矩阵**。可以使用`holdout`选项可以显示样本外估计的表现。格式如下：

```
pystacked [, table holdout[(varname)] ]
```

### <span id="3.1.2">3.1.2 后估计图</span>

- 在估计之后，pystacked命令可以报告一个**叠回归**和**基学习器**样本内和样本外（可选）的表现图。对于回归问题，图像会比较被解释变量的预测值和实际值。对于分类问题，图像默认会报告ROC曲线；同时，用户也可以选择添加报告分布概率预测的直方图。和`table`选项一样，用户可以使用`holdout`选项可以显示样本外估计的表现。

## <span id="3.2">3.2 预测</span>

得到预测值的语法：

```
predict type newname [if exp] [in range] [, pr xb ]
```

得到每个基学习器拟合值的语法：

```
predict type stub [if exp] [in range] [, transform ]
```

option|解释
---|:--:
pr|预测概率（只在分类器中可用）
xb|该选项是默认的，预测回归的拟合值或者分类的拟合值
transform|预测每个基学习器的拟合值

# <span id="4">4. Pipelines</span>

> Pipelines意为一种传递给**基学习器**的管道。其中，正则化线性学习器默认使用`stdscaler`管道，而对于其他学习器，则没有默认的管道。

- Scikit-learn使用管道会实时地对输入数据进行预处理,而管道可以用来估算**缺失观测**或**创造预测因子**的变换，如交互效应和多项式。

- 目前支持的管道共有九种，如下所示（左边为Pipeline名称，右边为函数形式展示）:

管道名称|函数形式展示
:-:|:--:
stdscaler|StandardScaler()
stdscaler0|StandardScaler(with_mean=False)
sparse|SparseTransformer()
onehot|OneHotEncoder()()
minmaxscaler|MinMaxScaler()
medianimputer|SimpleImputer(strategy='median')
knnimputer|KNNImputer()
poly2|PolynomialFeatures(degree=2)
poly3|PolynomialFeatures(degree=3)


- 管道可以通过`pipe(string)`或`pipeline(string)`传递给基础学习器。

&emsp;

# <span id="5">5. 实例</span>

## <span id="5.1">5.1 例子一：波士顿房价预测</span>

- 目的：分析13个房屋属性参数与房屋中间价的关系。

### <span id="5.1.1">5.1.1 数据集介绍</span>

- 该数据集观测值共有506个，预测因子（变量）共有13个。
- 预测因子中只有一个为离散数据(CHAS，值为0或者1)，其余的均为连续数据。

>解释变量

变量名称|变量含义展示
:-:|:--:
CRIM|每个城镇人均犯罪率
ZN|超过25000平方英尺用地被划为居住用地的百分比
INDUS|非零售商用地百分比
CHAS|查理斯河（Charles River）虚拟变量(当临近查尔斯河时取1;否则取0)
NOX|氮氧化物浓度(千万分之一)
RM|住宅平均房间数目
AGE|1940年前建成自用单位比例
DIS|到5个波士顿就业服务中心的加权距离
RAD|无障碍径向高速公路指数
TAX|每万元物业税率
PTRATIO|小学师生比例
B|其意义为黑人比例指数，等于1000 (Bk - 0.63)^2，其中Bk是城镇中黑色人种占总人口的比例
LSTAT |下层经济阶层比例

>被解释变量

变量名称|变量含义展示
:-:|:--:
MEDV|业主自住房屋中值（单位：千美元）

>变量统计值

```
      crim                zn             indus             chas              nox               rm       
 Min.   : 0.00906   Min.   :  0.00   Min.   : 0.460   Min.   :0.00000   Min.   :0.3850   Min.   :3.561  
 1st Qu.: 0.07309   1st Qu.:  0.00   1st Qu.: 4.935   1st Qu.:0.00000   1st Qu.:0.4480   1st Qu.:5.900  
 Median : 0.22325   Median :  0.00   Median : 9.125   Median :0.00000   Median :0.5380   Median :6.223  
 Mean   : 3.45286   Mean   : 11.98   Mean   :11.140   Mean   :0.07345   Mean   :0.5549   Mean   :6.308  
 3rd Qu.: 2.80872   3rd Qu.: 20.00   3rd Qu.:18.100   3rd Qu.:0.00000   3rd Qu.:0.6240   3rd Qu.:6.669  
 Max.   :88.97620   Max.   :100.00   Max.   :27.740   Max.   :1.00000   Max.   :0.8710   Max.   :8.780  
      age              dis              rad              tax           ptratio          black            lstat       
 Min.   :  2.90   Min.   : 1.137   Min.   : 1.000   Min.   :188.0   Min.   :12.60   Min.   :  0.32   Min.   : 1.730  
 1st Qu.: 47.25   1st Qu.: 2.104   1st Qu.: 4.000   1st Qu.:277.0   1st Qu.:16.90   1st Qu.:375.93   1st Qu.: 7.037  
 Median : 77.15   Median : 3.099   Median : 5.000   Median :336.0   Median :18.70   Median :391.38   Median :11.330  
 Mean   : 68.77   Mean   : 3.745   Mean   : 9.195   Mean   :404.2   Mean   :18.33   Mean   :357.07   Mean   :12.622  
 3rd Qu.: 93.75   3rd Qu.: 5.113   3rd Qu.: 8.000   3rd Qu.:666.0   3rd Qu.:20.20   3rd Qu.:396.31   3rd Qu.:16.703  
 Max.   :100.00   Max.   :10.710   Max.   :24.000   Max.   :711.0   Max.   :21.20   Max.   :396.90   Max.   :37.970  
      medv      
 Min.   : 5.00  
 1st Qu.:17.40  
 Median :21.30  
 Mean   :22.77  
 3rd Qu.:26.57  
 Max.   :50.00
```

### <span id="5.1.2">5.1.2 代码</span>

>加载住房数据

```
insheet using https://statalasso.github.io/dta/housing.csv, clear 
```

>叠加回归与套索，随机森林和梯度提升，其中，权重将决定每个基学习器对最终堆叠预测的贡献大小。

```
pystacked medv crim-lstat, type(regress) pyseed(123) methods(lassocv rf gradboost) 
```

>在该例子中请求MSPE表

```
pystacked, table
```

>使用前400个观测值重新估计，并请求MSPE表（包括样本内和默认值）

```
pystacked medv crim-lstat if _n<=400, type(regress) pyseed(123) methods(lassocv rf gradboost)
```

>保留样本(所有未使用的观测值)被报告

```
pystacked, table holdout 
```

>保留样本的预测情况与实际情况的图例

```
pystacked, graph holdout 
```

>存储预测值

```
predict double yhat, xb
```

>保存每个基学习器的预测值

```
predict double yhat, transform
```

#### 以下为使用者可能会遇到的编程难点：

>学习器特定的预测因子

- 语法1
pystack允许为每个基础学习器使用不同的预测集，例如线性估计如果以交互作用的形式作为输入提供，结果可能比原来的优秀。此处使用交互作用和二阶套索的多项式。

```
pystacked medv crim-lstat, type(regress) pyseed(123) methods(ols lassocv rf) xvars2(c.(crim-lstat)##c.(crim-lstat)) 
```

- 语法2
使用替代语法来演示同样的方法，比第一种方法更方便。

```
pystacked medv crim-lstat || m(ols) || m(lassocv) xvars(c.(crim-lstat)# #c.(crim-lstat)) || m(rf) || , type(regress) pyseed(123)
pystacked medv crim-lstat || m(ols) || m(lassocv) pipe(poly2) || m(rf) || , type(regress) pyseed(123)
```
>基学习器的选项

- 语法1
我们可以使用**cmdopt*N*（字符串）**(其中N代表数字)将选项传递给基学习器。在本例中，我们更改了随机森林的最大树的深度。由于随机森林是第三个基学习器，因此我们使用**cmdopt3(max_depth(3))**。

```
pystacked medv crim-lstat, type(regress) pyseed(123) methods(ols lassocv rf) pipe1(poly2) pipe2(poly2) cmdopt3(max_depth(3))
```

- 语法2
使用备选语法可以获得与上述相同的结果，其中备选语法对基础学习者的**数量**没有限制。

```
pystacked medv crim-lstat || m(ols) pipe(poly2) || m(lassocv) pipe(poly2) || m(rf) opt(max_depth(3)) , type(regress) pyseed(123)
```

>单个基学习器
- 使用者可以使用一个单一的基学习器，例如本例便使用了传统的随机森林：

```
pystacked medv crim-lstat, type(regress) pyseed(123) methods(rf)
```

>权重的决定

- 实际上，使用者还可以使用预定义的权重。此处为OLS赋值0.5，为套索赋值0.1，并暗中为随机前设赋值0.4。

```
pystacked medv crim-lstat, type(regress) pyseed(123) methods(ols lassocv rf) pipe1(poly2) pipe2(poly2) voting voteweights(.5 .1)
```

## <span id="5.1">5.1 例子二：垃圾邮件分类</span>

- 目的：预测电子邮件是否为垃圾邮件(即未经请求的商业电子邮件)，其中每个观察结果对应一封电子邮件。

### <span id="5.2.1">5.2.1 数据集</span>

- 该数据集从UCI机器学习存储库中的Spambase数据集获得,本次数据包括4601个观测值和57个变量。

>解释变量

变量名称|变量含义展示
:-:|:--:
V1-V48|电子邮件中匹配特定单词的单词百分比，等于100*(该单词出现的次数出现在电子邮件中)除以电子邮件中的总字数。
V49-V54|电子邮件中匹配特定字符的字符百分比，等于100*（在电子邮件中出现的字符）除以电子邮件中的字符总数。
V55|大写字母不间断序列的平均长度
V56|最长不间断的大写字母序列的长度
V57|电子邮件中大写字母的总数

其中，关于每个解释变量（V1-V48，V49-V54）具体对应的单词，可参见[该链接](https://archive.ics.uci.edu/ml/datasets/spambase)。

>被解释变量

变量名称|变量含义展示
:-:|:--:
V58|虚拟变量，表示该电子邮件是否被视为垃圾邮件(1)或非垃圾邮件(0)。

### <span id="5.2.2">5.2.2 代码</span>

>首先，加载垃圾邮件数据。

```
insheet using https://archive.ics.uci.edu/ml/machine-learning-databases/spambase/spambase.data, clear comma

```

>在这个示例中，其添加了支持使用4个核进行并行化的选项**njobs(4)**。
并且，该实例考虑了三个基础学习器，分别是logit，随机森林和梯度增强，代码如下：

```
pystacked v58 v1-v57, type(class) pyseed(123) methods(logit rf gradboost) njobs(4) pipe1(poly2)
```

>其次，需要进行样本外分类。由于数据是按结果排序的，使用该实例进行了随机打乱数据的操作，代码如下所示：

```
set seed 42
gen u = runiform()
sort u
```

>打乱过后，先对前2000次观测结果进行估计:

```
pystacked v58 v1-v57 if _n<=2000, type(class) pyseed(123) methods(logit rf gradboost) njobs(4)
pipe1(poly2)
```

>由此我们可以得到预测的概率或预测的类别：

```
predict spam, class
predict spam_p, pr
```

>在样本内与样本外均进行混淆矩阵操作：

```
pystacked, table
pystacked, table holdout
```

>保留混淆矩阵的样本：

```
gen h = _n>3000
pystacked, table holdout(h)
```

>保留样本的ROC曲线（目的是为组合后的图指定一个副标题）：

```
pystacked, graph(subtitle(Spam data)) holdout
```

>最后，我们可以通过**hist**选项决定是否默认保留样本的预测概率。为单个学习器指定箱子的数量。

```
pystacked, graph hist lgraph(bin(20)) holdout
```

&emsp;



# <span id="6">6. 参考文献</span>

> - Harrison, D. and Rubinfeld, D.L (1978). Hedonic prices and the demand for clean air. J. Environ. Economics & Management, vol.5, 81-102, 1978.
- Hastie, T., Tibshirani, R., & Friedman, J. (2009). The elements of statistical learning: data mining,inference, and prediction. Springer Science & Business Media.
- [Wolpert, David H. Stacked generalization. Neural networks 5.2 (1992): 241-259.](https://www.sciencedirect.com/science/article/abs/pii/S0893608005800231?via%3Dihub)


&emsp;

# <span id="6">7. 相关推文</span>
> **Note：** 以下推文可使用 `lianxh 函数, m` 命令自动生成。
安装最新版 lianxh 命令： `ssc install lianxh,replace` 

- 专题 >> [专题课程](https://www.lianxh.cn/blogs/44)
    - [专题课：文本分析-爬虫-机器学习-2022年4月](https://www.lianxh.cn/news/88426b2faeea8.html)
- 专题 >> [论文写作](https://www.lianxh.cn/blogs/31)
    - [Semantic scholar：一款基于机器学习的学术搜索引擎](https://www.lianxh.cn/news/8a744c39b665b.html)
- 专题 >> [Stata教程](https://www.lianxh.cn/blogs/17)
   - [Stata-Python交互-7：在Stata中实现机器学习-支持向量机](https://www.lianxh.cn/news/f1359e7fa9488.html)
- 专题 >> [Stata命令](https://www.lianxh.cn/blogs/43)
   - [Stata：双重机器学习-多维聚类标准误的估计方法-crhdreg](https://www.lianxh.cn/news/7519c2f054479.html)
- 专题 >> [Python-R-Matlab](https://www.lianxh.cn/blogs/37)
   - [MLRtime：如何在 Stata 调用 R 的机器学习包？](https://www.lianxh.cn/news/d2348493a3fa8.html)
- 专题 >> [其它](https://www.lianxh.cn/blogs/33)
   - [知乎热议：机器学习在经济学的应用前景](https://www.lianxh.cn/news/26b71b0008a76.html)
- 专题 >> [机器学习](https://www.lianxh.cn/blogs/47)
   - [知乎热议：如何学习机器学习](https://www.lianxh.cn/news/6e5cb7260859b.html)
   - [机器学习在经济学领域的应用前景](https://www.lianxh.cn/news/5ec10a29aaf88.html)
   - [机器学习如何用？金融+能源经济学文献综述](https://www.lianxh.cn/news/be7339df45904.html)
   - [知乎热议：纠结-计量经济、时间序列和机器学习](https://www.lianxh.cn/news/fa2ecb07451b7.html)
   - [机器学习：随机森林算法的Stata实现](https://www.lianxh.cn/news/b057fea5adb6a.html)
   - [Stata：机器学习分类器大全](https://www.lianxh.cn/news/a50bbdeb589cc.html)
