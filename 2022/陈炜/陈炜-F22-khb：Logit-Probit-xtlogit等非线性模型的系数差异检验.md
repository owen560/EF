# 非线性模型的中介效应之 KHB 法
&emsp;
>**作者**：陈炜（中山大学）  
>**邮箱**：<1139457876@qq.com>
&emsp;
----
**目录** 
[TOC]
----

## 1. 中介效应的研究近况
### 1.1 什么是中介效应
在进行研究的过程中，有时自变量 $X$ 对因变量 $Y$ 的影响并不那么直接，可能 $X$ 对 $Y$ 一部分作用是通过 $X$ 先对 $Z$ 产生影响，$Z$ 再对 $Y$ 产生影响。也就是说不仅 $X$ 自身会对 $Y$ 产生直接的影响，也可能同时找一个 “中间人” 来影响 $Y$ 。这个 “中间人” 就是中介变量，$X$ 影响 $Z$ 再影响 $Y$ 的这个过程就是中介效应。

### 1.2 中介效应的一般检验方式（常见的线性模型）
国内学者在研究中介效应的时候往往会参考温忠麟和叶宝娟在2014年写的《中介效应分析:方法和模型发展》一文，该文章提出了提出了逐步回归法、系数乘积法（Sobel检验）、差异系数法、Bootstrapping 检验法四种中介效应检验方法。文章自身引用量也破万，成为中介效应学习与研究的重要参考。

我们对一般的连续变量进行中介效应的方式是检验假设 $H_{0} : ab = 0$ 或者 $H_{0} : c_{2} = c-c_{1} = 0$（ $X$ 对$Y$的总影响 $c$ 可以由直接效应 $c_{1}$ 与间接效应 $c_{1}$（即 $ab$ ）得到，所以此时 $c_{2} = c-c_{1} = ab$ ）。

### 1.3 中介效应的特殊检验方式（非线性模型）
上述的方法对一般连续变量是有效的。但是如果因变量是分类或者等级变量等**非连续变量**，而自变量是**连续变量**，应该用 Logistic 回归代替通常的线性回归，回归系数的尺度也将由连续变量量尺转换为 Logit 量尺。但是，这样一来，中介变量 $Z$ 对 $X$ 的回归系数 （连续变量的量尺） 与 $Y$ 对 $Z$ 与 $X$ 的回归系数 （ Logit 量尺） 不在相同的尺度上，不同方程得到的回归系数只有量尺相同才具有可比性，$c-c_{1} ≠ ab$ ，**中介效应大小不等于 $ab$** ，导致上面检验 $ab=0$ 或是 $c-c_{1} = 0$ 的简单方式不再适用。

**对于这样的模型，目前学者已提出多个解决方法：**
- 通过标准化 （ standardization ） 转换实现回归系数的等量尺化 (Winship and Mare，1984; Long，1997；MacKinnon，2008；MacKinnon, Lockwood et al.，2007)
- 平均偏效应 （ average partial effect，它反映某一自变量在其均值附近发生微小变动时，对因变量成功发生概率（而非几率）带来的边际影响，各子样本的平均偏效应不容易受到误差方差变异的干扰 ） （ Wooldridge，2002；Cramer，2007 ）；
- 二元响应模型 （ binary response model ）的分解 （ Erikson et al.，2005)。

除上述方法之外，还有学者提出 KHB 方法，即本文介绍重点。KHB 方法与上述方法相比往往有相同或者更好的结果 （ 这里的结果是指 $Z$ 在多大程度上调节 $X$ 和 $Y^{*}$ 之间的关系) 。而且更重要的是通过简单直观的计算分离了离散变量和连续变量的影响，将线性模型的可分解性拓展到了非线性概率模型。

## 2. KHB 方法原理
在这一部分，为了更好的理解 KHB 方法的原理，会将一般方法与KHB方法进行对照，再说明 KHB 方法使用原因的同时说明其原理。

### 2.1 常规方法对于连续变量
常规方法对于连续变量中介效应检验分以下几步走：

（我们定义：$Y^{*}$ 是因变量，$X$ 是关键变量，其影响会部分通过 $X$ 传递给 $Y$ ，$Z$ 是中介变量，$C$ 是控制变量，$\epsilon$ 是误差项。）

1、一般模型回归：

$$Y^{*} =\alpha_{F}+\beta_{F}{x}+\gamma_{F}{Z}+\delta_{F}{C}+\epsilon_{1}\quad(1)$$

2、简化模型回归：

$$Y^{*} =\alpha_{R}+\beta_{R}{x}+\delta_{R}{C}+\epsilon_{2}\quad(2)$$

3、通过比较总效应与直接效应的差异（即间接效应）来对中介效应进验证: $\beta_{I}=\beta_{R}-\beta_{F}$

### 2.2 常规方法对于非连续变量
对二元响应模型而言，由于因变量 $Y^{*}$ 不可观测，所以我们无法估计模型的系数，也就无法直接通过 $\beta_{I}=\beta_{R}-\beta_{F}$ 来进行验证。如果我们定义一个二分变量 $Y$ ，当 $Y^{*}$ 小于0某个阈值 θ 时为0，否则为1，这样虽然可以估计回归系数，但需要对方程中的误差项的方差与分布有要求。如果我们设定其为二元 Logit 模型，那么基础系数的估计结果为 $b_{F}=\frac{\beta_{F}}{\sigma_{F}}$ 和 $b_{R}=\frac{\beta_{R}}{\sigma_{R}}$，其中 $\sigma_{F}$ 和 $\sigma_{R}$ 分别是尺度参数，是对应模型残差标准差的函数，同时 $\sigma_{F}$ ≤ $\sigma_{R}$，因为往模型中加入控制变量 $Z$ 可以减少 $Y^{*}$ 中不可解释的部分。这样一来，类似 $\beta_{I}=\beta_{R}-\beta_{F}$ ，我们也可以计算 $b_{R}-b_{F}=\frac{\beta_{R}}{\sigma_{R}}-\frac{\beta_{F}}{\sigma_{F}}$ ，差值由影响的差值（ $\beta_{R}$ 和 $\beta_{F}$ ）和尺度参数的差值 （ $\sigma_{R}$ 和 $\sigma_{F}$ ）决定。但是这会混淆中介效应的影响与尺度变化（rescale）。

### 2.3 KHB 方法对于上述问题的解答
#### 2.3.1 KHB 法检验步骤
KHB 方法对于连续变量中介效应检验分以下几步走：

1、 “完整模型” 中系数与标准误差的估计：

$$Y^{*}_{1} =\alpha_{F}+\beta_{F}X+\gamma_{F}{Z}+\delta_{F}{C}+\epsilon_{1}\quad(3)$$

2、 “简化模型” 中系数与标准误差的估计：其核心思想是从 $Z$ 中提取 $X$ 中不包含的信息。这将通过Z对X回归来提取；
$$Z=a+bX+\sigma$$

3、定义 $R=Z-(a+bX)$，$R$ 代替 $Z$ 代入方程，得

$$Y^{*}_{2} =\tilde{\alpha}_{R}+\tilde{\beta}_{R}X+\tilde{\gamma}_{R}R+\tilde{\delta}_{F}{C}+\epsilon_{2}\quad(4)$$

4、同2.2中提及进行 $\tilde{b}_{R}$、$\tilde{b}_{F}$ 的构造并相减，得：
$$\tilde{b}_{R}-b_{F}=\frac{\tilde{\beta}_{F}}{\tilde{\sigma}_{F}}-\frac{\beta_{F}}{\sigma_{F}}=\frac{\beta_{R}-\beta_{F}}{\sigma_{F}}\quad(5)$$

> **这个式子中有几点需要说明：**
**(1)**  $R$ 和 $Z$ 不同在于： $Z$ 和 $X$ 相关，而 $R$ 是被分解出来和 $X$ 不相关的部分，所以方程（1）并不比方程（3）模型可以更好的预测，因此两个方程的残差标准差是一样的，即 $\tilde{\sigma}_{R}=\sigma_{F}$ ；
**(2)** $\tilde{\beta}_{R}=\beta_{F}$ ；
**(3)** 这个差值如上述 $\beta_{I}=\beta_{R}-\beta_{F}$ 表示了间接效应。除了通过差值之外，也可以通过比例如 $\frac{\tilde{b}_{R}}{b_{F}}=\frac{\frac{\beta_{R}}{\sigma_{F}}}{\frac{\beta_{F}}{\sigma_{F}}}\quad(6)$ 或者是百分比的形式 $100 × \frac{\tilde{b}_{R}-b_{F}}{\tilde{b}_{R}}=100 × \frac{\frac{\beta_{R}}{\sigma_{F}}-\frac{\beta_{F}}{\sigma_{F}}}{\frac{\beta_{R}}{\sigma_{F}}}=100 × \frac{\beta_{R}-\beta_{F}}{\beta_{R}}\quad(7)$ 。
**根据以上三点可以计算出式（5）**
>**(4)** 回归尺度问题：线性回归中回归 $a$ 系数的显著性用 t 检验，t 统计量为 $\frac{a}{SE(a)}$，当样本容量足够大以至于自由度很大时 （一般是30），t 检验可以看成是 Z 检验，统计量为 $Z_{a}=\frac{a}{SE(a)}$；在 Logistic 回归中，回归系数 $b$ 的显著性检验用 Wald 的 $\chi^2$，检验统计量的平方根为 t 统计量 $\frac{b}{SE(b)}$，当自由度足够大的时候也可以写成  $Z_{b}=\frac{b}{SE(b)}$，所以在转换后，$Z_{a}$ 和 $Z_{b}$ 的尺度是一样的。

#### 2.3.2 KHB法显著性检验
在显著性检验中分以下两种情况：

**1) 一个关键变量和一个中介变量**

此时，我们需要检验假设 $H_{0}:\tilde{b}_{R}-b_{F}=0$，同时我们可以知道 $\tilde{b}_{R}-b_{F}=\frac{\gamma_{F}}{\sigma_{F}}b$。

> **若想使 $H_{0}$ 中的式子不等于0 ，那么就必须满足：**
> (1) 中介变量需要对结果有直接效应的影响，即 $\frac{\gamma_{F}}{\sigma_{F}}≠ 0$；
> (2) $X$ 与 $Z$ 相关，即 $b ≠ 0$。

然后基于上述观察，推导出基于delta方法(Sobel 1982)的间接效应的检验统计量：

$$Z=\frac{\sqrt{N}(\tilde{b}_{R}-b_{F})}{\sqrt{\vec{a}^{'}\vec{\Sigma}\vec{a}}} \sim N(0,1)$$

其中 $\vec{a}$ 是 $\vec{(\frac{\gamma_{F}}{\sigma_{F}},b)}^{'}$, $\vec{\Sigma}$ 是 $\gamma_{F}$ 和 $b$ 的方差协方差矩阵。

**2) 多个关键变量和多个中介变量**

这是一种更加常规的情况，包含了上面提及的简单情况。我们设定 $\vec{z}$ 代表 $J$ 个中介变量的向量，$x$ 为所有关键变量的向量。我们通常想知道在方程中有其他关键向量的时候，$\vec{z}$ 对于某一个关键变量的中介效应。我们假设第 $l$ 个关键变量在给定其他关键变量下对 $y$ 的 Logit 或者 Probit 影响，这样一来，$\vec{z}$ 对 $x_{l}$ 的中介效应影响是在相同尺度上，为 $\tilde{b}_{x_{l}·\tilde{z}x(l)}-b_{x_{l}·zx(l)}$，检验统计量为

 $$\frac{\tilde{b}_{x_{l}·\tilde{z}x(l)}-b_{x_{l}·zx(l)}}{sd(\tilde{b}_{x_{l}·\tilde{z}x(l)}-b_{x_{l}·zx(l)})}$$

其中 $\vec{a}={\theta_{\vec{z}x_{l}·\vec{x_{l}}} \choose \vec{b}_{y\vec{z}·x}}$ , $\vec{\Sigma}={\Sigma_{y\tilde{z}·\tilde{x}\quad 0}\choose 0\quad \Sigma_{y\tilde{z}·\tilde{x}}}$ , $sd(\tilde{b}_{x_{l}·\tilde{z}x(l)}-b_{x_{l}·zx(l)}) = \sqrt{\vec{a}^{'}\vec{\Sigma}\vec{a}}$

## 3. KHB 方法代码及实操
### 3.1 KHB基本代码

在了解了 KHB 法的原理后，它的Stata操作就非常简单了：

`khb model-type depvar key-vars || z-vars [if] [in] [weight] [,options]`
 
变量及选项| 详细解释 
---------|----------
 model-type | 模型类型，如reg、logit、probit等
 depvar | 因变量
 key-vars | 自变量，可以有因子变量
 z-vars | 中介变量，可以有因子变量
 options| **这里列举几个较为常用的options：** Concomitant(varlist)指定控制变量，允许因子变量。 Disentangle向程序请求一个表，该表显示每个控制变量提供的完整模型(总效应)和简化模型(直接效应)之间的差异。 Summary请求所有自变量的分解情况，默认情况下 khb 报告完整模型和简化模型的效果、它们的差异以及它们的标准误。

### 3.2 KHB法数据实操
>由于未能找到U. Kohler, K. B. Karlson, and A. Holm（2011）中所用的实操数据，因此用了另一套数据来进行本部分的数据实操。
>【请注意】以下数据回归实操是为了通过实操让读者更好地了解 KHB 法的用处，并未考虑是否有学术文献支撑以及学术研究价值。

该数据为 Long and Freese (2006) 提供的范例数据 nomocc2.dta。 
>(该数据可通过在stata中输入命令 findit nomocc2.dta 查询并下载)

```
  * 数据背景：
  * 请回答如下问题：有工作的母亲也能够与子女亲密相处，你的观点是？
  *    1 = Strongly Disagree 	(SD)
  *    2 = Disagree 			 (D)
  *    3 = Agree 				 (A)
  *    4 = Stongly Agree 		(SA)
  
  *数据来源：1997-1989, General Social Survey
```
数据基本信息及处理
```
. des /*变量的基本信息*/

Contains data from D:\stata17\FE_Bech\A11_logit\data\新建文件夹\ordwarm2.dta
 Observations:         2,293                  77 & 89 General Social Survey
    Variables:            10                  16 Nov 2005 14:53
                                              (_dta has notes)
------------------------------------------------------------------------------------
Variable      Storage   Display    Value
    name         type    format    label      Variable label
------------------------------------------------------------------------------------
warm            byte    %10.0g     sd2sa      Mom can have warm relations with child
yr89            byte    %10.0g     yrlbl      Survey year: 1=1989 0=1977
male            byte    %10.0g     sexlbl     Gender: 1=male 0=female
white           byte    %10.0g     race2lbl   Race: 1=white 0=not white
age             byte    %10.0g                Age in years
ed              byte    %10.0g                Years of education
prst            byte    %10.0g                Occupational prestige
warmlt2         byte    %10.0g     SD         1=SD; 0=D,A,SA
warmlt3         byte    %10.0g     SDD        1=SD,D; 0=A,SA
warmlt4         byte    %10.0g     SDDA       1=SD,D,A; 0=SA
------------------------------------------------------------------------------------
Sorted by: warm

. sum /*变量的基本统计量*/

    Variable |        Obs        Mean    Std. dev.       Min        Max
-------------+---------------------------------------------------------
        warm |      2,293    2.607501    .9282156          1          4
        yr89 |      2,293    .3986044    .4897178          0          1
        male |      2,293    .4648932    .4988748          0          1
       white |      2,293    .8765809    .3289894          0          1
         age |      2,293    44.93546    16.77903         18         89
-------------+---------------------------------------------------------
          ed |      2,293    12.21805    3.160827          0         20
        prst |      2,293    39.58526    14.49226         12         82
     warmlt2 |      2,293    .1295246    .3358529          0          1
     warmlt3 |      2,293    .4448321    .4970556          0          1
     warmlt4 |      2,293    .8181422    .3858114          0          1

* 数据处理，将非连续变量转化为数字
. gen W=string(warm) 
. destring W,gen(W1)
. gen white1=string(white)
. destring white1,gen(white2)
```
**下面将以上述数据介绍 U. Kohler, K. B. Karlson, and A. Holm（2011）中所提到的 KHB 法的几种用途：**

#### 3.2.1 基本用法 （Basic use）
猜测年龄会对态度有所影响，且这种影响可能会通过受教育程度（以受教育年限衡量）影响人们的态度。中介变量通过 “ || ” 与自变量隔开， `concomitant() ` 中加入控制变量，在这里控制了性别及研究年份。
```
. khb mlogit W1 age || ed,concomitant(male yr89) 

Decomposition using the KHB-Method

Model-Type:  mlogit                                Number of obs     =    2293
Variables of Interest: age                         Pseudo R2         =    0.06
Z-variable(s): ed
Concomitant: male yr89
Results for outcome 1 and base outcome 3
------------------------------------------------------------------------------
          W1 | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
age          |
     Reduced |      0.031      0.004     7.55   0.000        0.023       0.040
        Full |      0.025      0.004     5.80   0.000        0.017       0.034
        Diff |      0.006      0.001     4.54   0.000        0.003       0.009
------------------------------------------------------------------------------
```
$b_{R}$ 和 $b_{F}$ 分别为简化模型和完整模型估计效果，分别代表总效应和直接效应，两者的差值代表间接效应。可以发现年龄会使态度向好发展的概率增加0.031%，在控制教育年限后，年龄的影响降低至0.025，剩下0.006为间接影响。
KHB 方法确保在相同尺度上测量所呈现出来的系数，但是有时 Logit 系数的大小很难解释。Karlson, Holm, and Breen (2010)  提出了由式子（6）和式子（7） 定义的比率和百分比来克服这些问题，而且这两个指标都可以通过一条 Stata 命令实现。
```
. khb mlogit W1 age || ed,concomitant(male yr89) summary notable 

Decomposition using the KHB-Method

Model-Type:  mlogit                                Number of obs     =    2293
Variables of Interest: age                         Pseudo R2         =    0.06
Z-variable(s): ed
Concomitant: male yr89
Results for outcome 1 and base outcome 3

Summary of confounding

        Variable | Conf_ratio    Conf_Pct   Resc_Fact  
    -------------+-------------------------------------
             age |  1.2382813       19.24   .98449812  
    ---------------------------------------------------
```
通过结果可以看出，总效应比直接效应大了1.2倍，总效应的19%是因为教育年限。

#### 3.2.2 比较平均偏效应 （Comparing average partical effects）
实际应用中，有无 $Z$ 的模型之间的平均部分效应的简单比较可能会失真，因此平均偏效应不适合用于效应的分解。但是用 KHB 法可以解决该问题。

```
. khb mlogit W1 age || ed,concomitant(male yr89) ape summary

Decomposition using the APE-Method

Model-Type:  mlogit                                Number of obs     =    2293
Variables of Interest: age                         Pseudo R2         =    0.06
Z-variable(s): ed
Concomitant: male yr89
Results for outcome 1 and base outcome 3
------------------------------------------------------------------------------
          W1 | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
age          |
     Reduced |      0.002      0.000     6.23   0.000        0.002       0.003
        Full |      0.002      0.000     4.48   0.000        0.001       0.003
        Diff |      0.001          .        .       .            .           .
------------------------------------------------------------------------------
 Note: Standard errors of difference not known for APE method

Summary of confounding

        Variable | Conf_ratio    Conf_Pct   Dist_Sens  
    -------------+-------------------------------------
             age |  1.3007402       23.12    .9571126  
    ---------------------------------------------------
```
平均而言，由于年龄发生标准差变化，态度向好发展的概率增加了0.2 %，在控制教育年限后，这一概率基本不变，也就是说，教育年限的提升并不会导致人们态度向好或向差方向发展。

#### 3.2.3 分离中介变量的贡献（Disentangle contributions of mediators）
如果有一个以上的中介变量，`disentangle` 选项能帮助回答哪一个中介变量影响更大的问题。该选项会通过制表显示每个中介变量的贡献。
```

. khb mlogit W1 age || ed male yr89, summary disentangle notable

Decomposition using the KHB-Method

Model-Type:  mlogit                                Number of obs     =    2293
Variables of Interest: age                         Pseudo R2         =    0.06
Z-variable(s): ed male yr89
Results for outcome 1 and base outcome 3

Summary of confounding

        Variable | Conf_ratio    Conf_Pct   Resc_Fact  
    -------------+-------------------------------------
             age |  1.2064567       17.11   1.0421457  
    ---------------------------------------------------

Components of Difference

      Z-Variable |      Coef    Std_Err     P_Diff  P_Reduced  
    -------------+---------------------------------------------
    age          |                                             
              ed |  .0060526   .0013359     115.30      19.73  
            male | -.0005025   .0002982      -9.57      -1.64  
            yr89 | -.0003009   .0006816      -5.73      -0.98  
    -----------------------------------------------------------
```
分解表的前两列显示了每种中介变量的效应差异及其标准误。第一列值的总和为0.005，这是间接效应的总和，第三列标识各种中介变量对间接效应的贡献。最后一列显示总效应的多少是由于各自中介变量的混杂引起的，总计17.1，即总体混杂百分比。

#### 3.2.4 有多个关键变量的情况（More than one key variable）
我们将要分解的变量定义为关键变量。一个命令里可以存在一个关键变量，亦可存在多个。在这种情况下，我们希望能通过一个命令分解每个关键变量。下面，我们将对性别和职业声望进行分解，以年龄和研究年份为控制变量。
```
. khb mlogit W1 male prst || ed, concomitant(age yr89) summary

Decomposition using the KHB-Method

Model-Type:  mlogit                                Number of obs     =    2293
Variables of Interest: male prst                   Pseudo R2         =    0.06
Z-variable(s): ed
Concomitant: age yr89
Results for outcome 1 and base outcome 3
------------------------------------------------------------------------------
          W1 | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
male         |
     Reduced |      0.366      0.141     2.60   0.009        0.090       0.642
        Full |      0.365      0.141     2.59   0.010        0.088       0.641
        Diff |      0.001      0.011     0.11   0.910       -0.020       0.022
-------------+----------------------------------------------------------------
prst         |
     Reduced |     -0.014      0.005    -2.75   0.006       -0.024      -0.004
        Full |     -0.001      0.006    -0.21   0.831       -0.013       0.011
        Diff |     -0.013      0.011    -1.13   0.259       -0.034       0.009
------------------------------------------------------------------------------

Summary of confounding

        Variable | Conf_ratio    Conf_Pct   Resc_Fact  
    -------------+-------------------------------------
            male |  1.0032987        0.33   .94421228  
            prst |  10.616178       90.58   .96294939  
    ---------------------------------------------------
```
通过上述结果可以发现，职业声望的影响受教育年限的影响比性别影响受教育影响程度大。
#### 3.2.5 其他用途
KHB 的其他用途至少还包括：
- 分类变量（ Categorical variables ）
- 有序结果（ Ordered outcome ）
- 多项结果（ Multinomial outcome ）
  
由于前几条为 KHB 法相较于原有方法更为显著的优点，且 nomocc2.dta 数据并不符合进行上述三点实操的条件，因此在此并没有详细阐述。

## 4. KHB 方法总结
KHB 方法可以适用于多种情况，如：上述提到的一个自变量和一个中介变量、多个自变量和多个中介变量；regression, logit, ologit, probit, oprobit, cloglog, slogit, scobit, rologit, clogit, mlogit, xtlogit 或 xtprobit 等模型都可以使用，其他模型也可能输出结果，但可能是实验性的。

另外，中介效应的应用主要是在国内论文的心理学领域而非经济学领域；在国外顶刊论文中非常少，即使文章有使用也会进行调整。因此，虽然对中介效应的不断探索有助于我们不断在计量方面能有更加深入的认知，但是在经济学论文中使用中介效应仍需谨慎。

## 5. 参考文献及资料
- Kohler, U., Karlson, K.B., Holm, A. (2011): Comparing coefficients of nested nonlinear probability models. The Stata Journal 11: 420-438.  [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100306)
- 温忠麟,叶宝娟.中介效应分析:方法和模型发展[J].心理科学进展,2014,22(05):731-745.  [-PDF-](https://kns.cnki.net/kcms2/article/abstract?v=3uoqIhG8C44YLTlOAiTRKgchrJ08w1e7M8Tu7YZds88HvMJUA_sIa_Vm8XKqMAAxjP2_SGApEsfqcwZ91v_eVbkH9js_K4kY&uniplatform=NZKPT)
- 方杰,温忠麟,张敏强.类别变量的中介效应分析[J].心理科学,2017,40(02):471-477.  [-PDF-](https://kns.cnki.net/kcms2/article/abstract?v=3uoqIhG8C44YLTlOAiTRKibYlV5Vjs7iAEhECQAQ9aTiC5BjCgn0RhMmQGr0ZCXPMWpPePPOSWY_mU4xNh8_U7IE9sC5Vk6c&uniplatform=NZKPT)
- 刘红云,骆方,张玉,张丹慧.因变量为等级变量的中介效应分析[J].心理学报,2013,45(12):1431-1442.  [-PDF-](https://kns.cnki.net/kcms2/article/abstract?v=3uoqIhG8C44YLTlOAiTRKjw8pKedNdX5_mkCYmAjR9zQIub4xcyvphBhJNePckFB-k2x5_zejq8N1fKvixqt5GESfpX2n439&uniplatform=NZKPT)
- [​主流: KHB法测度中介效应, 无论线性还是非线性模型, KHB都能分解出直接和间接效应！](https://mp.weixin.qq.com/s/Id9sjE4KpGHAM_M2fgwd1g)

## 6. 相关推文
- 专题：
  - [Stata+R: 一文读懂中介效应分析](https://www.lianxh.cn/news/fc35ac55f5085.html)
  - [全面解读Logit模型](https://www.lianxh.cn/news/c6d6badebe2a7.html)