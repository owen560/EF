&emsp;
> **作者**:罗清扬（中山大学）  
> **邮箱**：<luoqy27@mail2.sysu.edu.cn> 
> textex：内生性-控制函数法中的排他性约束的检验和应对

[toc]

&emsp; 
## 1.控制函数法介绍
### 1.1 概述
内生性是实证研究中常常会碰到的重要问题，长期以来，学者们从不同的思路和视角出发，为内生性问题的解决提供了多种多样的思路和方法。  

控制函数法（Control Function，CF）是处理内生性变量问题的一个常用方法。和传统的 2SLS 、IV 估计等方法一样，CF 也需要找到满足外生性条件和排他性约束的工具变量，并通过两阶段回归得到参数估计值。但不同于 2SLS 直接用第一阶段回归的拟合值替换原内生变量，CF 是将第一阶段回归的残差项作为新的控制变量加入原始模型中，相当于从原始模型的残差项中将与内生变量相关的部分单独提取出来进行控制。

> 在一些设定较为简单的模型中，CF 和 2SLS、IV 方法得到的估计结果是相同的，但 CF 可以更加方便且稳健地进行赫斯曼检验。另外，与传统工具变量法相比，CF 方法在随机系数模型和非线性模型的估计中效率更高，适用范围更广。[Jeffrey M. Wooldridge（2015）](https://sci-hub.ee/10.3368/jhr.50.2.420)详细系统地总结了 CF 方法在不同模型中的用法，下面选取其中常用且较有代表性的几个进行简析。

&emsp;
### 1.2 常系数线性模型
#### 1.2.1 内生变量为连续变量
> 对于系数固定的线性模型而言，CF 法和 2SLS、IV 估计得出的结果是一致的。

基本模型如下所示：

$$ y_1 = \pmb Z'_1\pmb \delta_1 + \gamma_1y_2 +u_1 \quad(1) $$  

其中 **$y_2$** 是内生变量，$\pmb Z'_1$ 是外生变量（包含一个常数项）。  
CF 法首先会将内生变量对全部外生变量（$\pmb Z'_1$ 和工具变量 $\pmb Z'_2$ ）进行回归：

$$ y_2=\pmb Z'_1\pmb\Pi_1 + \pmb Z'_2\pmb\Pi_2 + v_2 \quad(2)
$$

其中 $Cov(\pmb Z_1,v_2)=Cov(\pmb Z_2,v_2)=0$；  
从(2)中得到残差项 **$v_2$** 的拟合值 **$\hat v_2$** :

$$\hat v_2=y_2-\pmb Z'_1\hat{\pmb\Pi}_1 - \pmb Z'_2\hat{\pmb\Pi}_2 \quad(3) $$

假设 **$v_2$** 和 **$u_1$** 之前存在线性关系：

$$ u_1=\rho_1v_2+e_1 \quad(4) $$

其中 $Cov(v_2,e_1)=0$；  
用拟合值代替 **$v_2$** ，将（4）带入（1）得到：

$$ y_1 = \pmb Z'_1\pmb \delta_1 + \gamma_1y_2 +\hat v_2\rho_1 +e_1 \quad(5) $$

新得到的方程（5）中，扰动项 **$e_1$** 与全部解释变量都满足外生性，可以进行 OLS 估计。
> 这类简单模型中，2SLS 和 CF 得到的参数估计结果一致，后者反而还使得获取正确的参数标准差变得更加困难。但 CF也有其独特的优势：可以为 Hausman 检验提供异方差稳健且直观的指示，只需检验 $H_0$: $\rho_1$=0 即可。

#### 1.2.2 内生变量为二元变量
> 当内生变量为二元变量时，整体建模思路和之前基本一致，唯一的区别就在于第一阶段（方程（2））不再是对内生变量进行线性建模，而是建立一个会产生二元响应的模型，如下所示：  

$$ y_2=1[\pmb Z\pmb{\delta_2}+e_2>0] \quad(6) $$ 
$$ e_2 \sim N(0,1) \quad(7) $$ 

其中 $\pmb Z=[\pmb Z_1,\pmb Z_2]$ ；   
根据对 **$e_2$** 分布以及其他一些关于扰动项条件分布的假设，[Wooldridge (2010, Section 21.4.2)](https://www.academia.edu/34835499/Econometric_Analysis_of_Cross_Section_an_)推出  

$$ E(y_1| \pmb{Z},y_2)=\pmb{Z_1 \delta_2}+\gamma_1y_2+\eta_1r_2 \quad(8) $$

其中

$$r_2=y_2\lambda(\pmb{Z\delta_2})-(1-y_2)\lambda(-\pmb{Z\delta_2}) \quad(9) $$

其中 $\lambda(·)= \phi(·) /\Phi(·)$ ，即逆米尔斯比率。  
**$r_2$** 可以作为第一阶段回归得到的一个广义误差项来理解：估计时先对（6）进行估计，得到 
 **$\hat{\pmb{\delta}}_2$** ，然后带入（9）得到 **$\hat{r}_2$** ，最后只需将（5）中的 **$\hat{v}_2$** 变为 **$\hat{r}_2$** 进行估计即可。   
 
#### 1.2.3 内生变量非线性
在更广泛的应用场景中，如果内生变量是非线性的，例如与外生变量进行交乘或者以高次方形式出现时，传统的 IV 估计或 2SLS 需要为含有内生变量的所有项分别寻找或创造可靠的工具变量，而 CF 法依旧可以采取与之前完全相同的两阶段回归进行估计，整个过程显得更加高效便利。  

> 然而，CF 方法的便利性也并不是没有代价的，CF 估计过程的无偏性依赖于第一阶段回归中对内生变量模型设定的准确性，如果设定错误，那么最终的系数估计结果将是有偏的，这个问题在 IV 或 2SLS 估计中则不会出现。  

&emsp;  
### 1.3 相关随机系数线性模型
在一些场景中，解释变量对被解释变量的影响可能存在异质性，这就产生了随机系数模型，如下所示：

$$ y_1 = \pmb Z'_1\pmb \delta_1 + g_1y_2 +u_1 \quad(10) $$

其中，$\pmb \delta_1$ 是固定系数，$g_1$ 是相关随机系数，则

$$g_{i1}=r_1+v_{i1}\quad(11)$$ 
$$E(v_{i1})=0$$

**$r_1$** 是常数，代表 **$y_2$** 对因变量的平均作用。  
此时如果采用 2SLS 估计，方程的扰动项就会变为 **$u_1+v_{i1}y_2$** ，在不施加更强的假设时，会导致 **$\hat{r}_1$** 有偏，而采用 CF 则可以估计出随机系数的平均变化规律。  
首先依旧通过（2）的 OLS 估计得到 $\hat{v}_2$ ，假设 $u_1$、$v_1$ 都和 **$v_2$** 存在线性关系如下：

$$E(u_{i1}|v_{i2})=\eta_1v_{i2}, E(v_{i1}|v_{i2})=\psi_1v_{i2}$$ 

则 CF 的第二阶段方程就可以写作：

$$ y_1 = \pmb Z'_1\pmb \delta_1 + r_1y_2 +\eta_1v_2+\psi_1v_2y_2 \quad(12) $$

将 **$\hat{v}_2$** 带入即可估计得到结果，$g_1$ 的随机性就是由 **$v_2y_2$** 的系数来体现的，此时模型的刻画与现实更加相符。
> 上述方法还可以进一步扩展：  
> * 当 **$y_2$** 是二元变量时，将 **$v_2$** 换为（9）中的广义误差 **$r_2$** 即可；   
> * 当模型中还需要添加交乘项或其他非线性形式时，只需在（12）中加入相应的非线性项进行回归即可，不需要改变第一阶段的回归过程；  
> * 模型中除了基本的内生变量本身之外，其他的变量也可以使用随机系数，处理方式和上述过程完全一致。

&emsp;  
### 1.4 非线性模型
> CF 法在非线性模型内生性问题的处理上显著优于传统的工具变量方法，最容易被使用的场景就是 probit/logit 模型（下述以 probit 模型的建模过程为例）。

probit 基本模型如下：

$$ y_1=1[\pmb Z_1 \pmb\delta_1 +\gamma_1y_2+u_1\geqslant0] \quad(13)$$

假设（2）和（4）依旧成立，则

$$ y_1=1[\pmb Z_1 \pmb\delta_1 +\gamma_1y_2+ \rho_1 v_2+e_1\geqslant0] \quad(14) $$

估计过程中只需将第一阶段回归得到的残差拟合值 **$ \hat{v}_2$** 带入（14），就可以继续使用 MLE 进行普通的 probit 估计了。

&emsp;  
## 2. textex理论背景 
正如上文所示，控制函数法在检验某个变量的内生性时具有显著的优势，同时具备极强的可拓展性，然而，CF 法的使用依旧受限于传统工具变量的限制条件：相关性约束和排他性约束，即：

$$Corr(y_2,\pmb Z_2)\ne0；Corr(u_1,\pmb Z_2)=0 \quad(15)$$  

排他性约束即所使用的工具变量只能通过内生变量对被解释变量产生影响，而不能存在其他的影响途径。关于排他性约束的检验，虽然已有学者提出了一些可行的方法，但大都对模型设定和基本假设有着严格的要求，如果不能满足这些方法的前提条件，那么只能通过大量的理论分析、定性描述或者借助前人的研究结论来进行论证。因此，在使用工具变量的一类文献中，排他性约束的证明过程常常容易受到质疑。  
[D’Haultfoeuille, X., S. Hoderlein, Y. Sasaki (2021)](https://sci-hub.ee/10.1016/j.jeconom.2020.09.012)在 CF 方法的基础上，没有对结构方程的函数形式进行任何约束，在一定假设条件下提出了对排他性约束进行检验的方法，如果排他性约束没有得到满足，还可以适当放松之后再进行估计。为了使讨论具有较强的普适性而不是困于某种特定的函数模式，文章基于下面的基本模型展开：

$$ \left\{
\begin{aligned} 
Y &=g(X,Z,\varepsilon) \\
X &=h(Z,\eta)
\end {aligned}
\right.
\quad(16)
$$  

其中， $\varepsilon$ 和 $\eta$ 是不可观测的扰动项，$X$ 是内生变量，$Z$ 是工具变量，$\eta \sim U(0,1)$ 。

&emsp; 
### 2.1 排他性约束检验
> 排他性约束检验需要有以下假设存在：  
（1）工具变量对内生变量的影响存在异质性；  
（2）第一阶段中，内生变量的约简方程单调递增；  
（3）存在样本个体，使得工具变量与内生变量局部不相关，即在全部样本中，存在一部分样本使得下式成立：

$$ F_{X|Z}(x^*|z)=F_{X|Z}(x^*|z') \quad(17)$$

> 对于这部分样本，工具变量 **$Z$** 的值从 **$z$** 变化至 **$z'$** 并不会影响 **$x$** 的取值。  

基于上述前提可以推出，如果模型满足排他性约束，下式成立 ：

$$ g(X,Z,\varepsilon)=g(X,\varepsilon)\quad(18)$$ 

则对于工具变量和内生变量不相关的样本个体，**$Z$** 取值的变化（从 **$z$** 到 **$z'$** ）不应该引起 **$Y$** 的变化，因此在排他性约束成立的情况下，有

$$Y|X=x^*,Z=z\sim Y|X=x^*,Z=z' \quad(19)$$

所以只需要对（19）进行检验就可以明确排他性约束是否成立了，因此原文对 $F_{Y|x,z}(Y|x^*,z)$ 构建了相应的统计量进行检验。

&emsp; 
### 2.2 放松排他性约束的 CF 估计
如果排他性约束的检验没有通过，此时需要在工具变量对因变量有其他影响途径的条件下进行估计，放松约束后的 CF 估计方法延续了排他性约束检验过程的整体思路，以如下的简单线性模型举例： 

$$Y=g(X, Z, ε) = ε_0 + ε_1X + ε_2Z \quad(20)$$
$$(ε_2\ne0)$$

* 首先对于工具变量和内生变量局部不相关的子样本中，当保持 **$\eta$** 固定不变时，工具变量 **$Z$** 的取值从 **$z$** 变化至 **$z'$** 不会导致 **$X$** 的取值发生变化，此时 **$Y$** 的变化完全由 **$Z$** 引起。所以，利用具有局部不相关的子样本进行 CF 估计，可以得到 **$Z$** 的取值变化对 **$Y$** 取值的平均影响效应 $E(ε_2|\eta)$ 。
* 接下来再对 **$X$** 对 **$Z$** 的平均作用进行估计：令 $\widetilde{Y}=Y-E(ε_2|\eta)Z$ ，用不具备工具变量和内生变量局部不相关性的子样本进行 CF 估计，若 **$Z$** 的取值从 **$z$** 变化至 **$z'$** ，会导致 **$X$** 的值变化，此时 **$\widetilde{Y}$** 的变化只能是 **$Z$** 的变化通过 **$X$** 来进行传导的，因此只需要用 **$\widetilde{Y}$** 对 **$X$** 进行 CF 估计就可以得到 **$X$** 对 **$Y$** 的平均影响作用 $E(ε_1|\eta)$ 。

&emsp;  
## 3. testex 命令介绍
 * 命令安装  
  `ssc install testex`
 * 语法介绍  
  ` testex y x z [if] [in] [, numboot(real)]`  
**$y、x、z$** 分别是因变量、内生变量和工具变量。  
`number(real)` 用于设置计算检验统计量的关键值过程中进行的 bootstrap 次数，默认为 1000 次。

&emsp;  
## 4. Stata 实操
本文为了能够对线性和非线性模型都进行检验，直接使用 Stata 生成模拟数据。
#### （1） DGP1：满足排他性约束的线性模型

$$\left\{
\begin{aligned}
Y &=e_0+e_1x+\beta z \\
X &=\eta(1+z)
\end{aligned}
\right.
\quad(21)
$$

数据生成过程如下：  
``` 
* DGP1：满足排他性约束的线性模型    
set obs 1000  //数据量为1000
set seed 0
gen Z = rbinomial(1,0.5) //Z是工具变量
gen eta = runiform(0,1) //eta是一阶回归中的扰动项
gen e0=rnormal(0,1) //e0是总模型中的扰动项
scalar e1 = 1 //e1是内生变量（X）前面的系数
scalar beta=0 //beta是工具变量前面的系数
gen X=eta*(1+Z) //一阶回归生成X
gen Y=e0+e1*X+beta*Z 
```
此时 $\beta=0$ 是成立的，即排他性约束得到满足，进行检验：
```
testex Y X Z
** 检验结果
/*
------------------------------------------------------------------------------
  Null hypothesis: Exclusion restriction is satified.
------------------------------------------------------------------------------
                                                     KS statistic =      1.468
                                                          p-value =      0.661
                                                     -------------------------
     Fail to reject the null hypothesis at the significance level of 10%.
     Fail to reject the null hypothesis at the significance level of  5%.
     Fail to reject the null hypothesis at the significance level of  1%.
------------------------------------------------------------------------------
*/
```
在 1%、5% 和 10% 的显著性水平上都不可以拒绝原假设，即接受排他性约束成立的原假设。 

#### （2） DGP2：不满足排他性约束的线性模型  
数据生成过程如下：
```
scalar beta=1 //beta是工具变量前面的系数
drop X Y
gen X=eta*(1+Z) //一阶回归生成X
gen Y=e0+e1*X+beta*Z 
```
此时 $\beta\ne0$ ，不满足排他性约束，进行检验：
```
testex Y X Z
** 检验结果
/*
------------------------------------------------------------------------------
  Null hypothesis: Exclusion restriction is satified.
------------------------------------------------------------------------------
                                                     KS statistic =      3.807
                                                          p-value =      0.007
                                                     -------------------------
             Reject the null hypothesis at the significance level of 10%.
             Reject the null hypothesis at the significance level of  5%.
             Reject the null hypothesis at the significance level of  1%.
------------------------------------------------------------------------------
*/
```
在 1%、5% 和 10% 的水平上都拒绝了原假设，说明不满足排他性约束。

#### （3） DGP3：满足排他性约束的非线性模型

$$\left\{
\begin {aligned}
Y &=1(e_0+e_1x+\beta Z) \\
X &=\eta (1+z)
\end {aligned}
\right.
\quad(22)
$$

数据生成过程如下：
```
* DGP3：满足排他性约束的非线性模型
gen Z1 = rbinomial(1,0.5) //Z是工具变量
gen eta1 = runiform(0,1) //eta是一阶回归中的扰动项
gen _e0=rnormal(0,1) //e0是总模型中的扰动项
scalar _e1 = 1 //e1是内生变量（X）前面的系数
scalar beta1=0 //beta是工具变量前面的系数
gen X1=eta1*(1+Z1) //一阶回归生成X
gen Y1=0
gen Yhat=_e1*X+beta*Z+_e0
replace Y1=1 if Yhat>0
```
此时 $\beta=0$ ，满足排他性约束，进行检验如下：
```
testex Y1 X1 Z1
** 检验结果
/*
------------------------------------------------------------------------------
  Null hypothesis: Exclusion restriction is satified.
------------------------------------------------------------------------------
                                                     KS statistic =      0.141
                                                          p-value =      0.989
                                                     -------------------------
     Fail to reject the null hypothesis at the significance level of 10%.
     Fail to reject the null hypothesis at the significance level of  5%.
     Fail to reject the null hypothesis at the significance level of  1%.
------------------------------------------------------------------------------
*/
```
在 1%、5% 和 10% 的显著性水平上都没有拒绝原假设，即接受满足排他性约束的原假设。
#### （4） DGP4：不满足排他性约束的非线性模型
数据生成过程如下：
```
* DGP4：不满足排他性约束的非线性模型
scalar beta1=1 //beta是工具变量前面的系数
drop X1 Yhat Y1
gen Y1=0
gen X1=eta1*(1+Z1) //一阶回归生成X
gen Yhat=_e0+_e1*X1+beta1*Z1 
replace Y1=1 if Yhat>0
```
此时 $\beta\ne0$ ，模型不满足排他性约束，检验如下：
```
testex Y1 X1 Z1
**检验结果
/*
------------------------------------------------------------------------------
  Null hypothesis: Exclusion restriction is satified.
------------------------------------------------------------------------------
                                                     KS statistic =      4.353
                                                          p-value =      0.010
                                                     -------------------------
             Reject the null hypothesis at the significance level of 10%.
             Reject the null hypothesis at the significance level of  5%.
             Reject the null hypothesis at the significance level of  1%.
------------------------------------------------------------------------------
*/
```
在 1%、5% 和 10% 的显著性水平上都拒绝了原假设，说明确实不满足排他性约束。

&emsp;  
## 5. 结语
本文从 CF 估计法的基本原理出发，对其常见的模型形式进行了梳理，在此基础上介绍了CF框架下对工具变量排他性约束进行检验的基本思想，并呈现了相关命令的 Stata 实操过程。

&emsp;
## 6. 参考资料和相关推文
-Wooldridge J M. Control function methods in applied econometrics[J]. Journal of Human Resources, 2015, 50(2): 420-445. [-PDF-](https://sci-hub.ee/10.3368/jhr.50.2.420)  
-D’Haultfœuille X, Hoderlein S, Sasaki Y. Testing and relaxing the exclusion restriction in the control function approach[J]. Journal of Econometrics, 2021. [-PDF-](https://sci-hub.ee/10.1016/j.jeconom.2020.09.012)  
-Blundell R, Powell J L. Endogeneity in nonparametric and semiparametric regression models[J]. Econometric society monographs, 2003, 36: 312-357. [-PDF-](https://discovery.ucl.ac.uk/id/eprint/14712/1/14712.pdf)  
-徐云娇，连享会推文，[工具变量-IV：排他性约束及经典文献解读](https://www.lianxh.cn/news/d4e7734f4d10d.html)  

&emsp;
## 7. 连享会相关推文
> Note：产生如下推文列表的 Stata 命令为：  
> &emsp; `lianxh 控制函数法 排他性约束`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace`   
- 专题：[内生性-因果推断](https://www.lianxh.cn/blogs/19)  
  - [Stata：控制函数法及其Stata应用](https://www.lianxh.cn/news/3187754219c71.html)
  - [工具变量-IV：排他性约束及经典文献解读](https://www.lianxh.cn/news/d4e7734f4d10d.html)