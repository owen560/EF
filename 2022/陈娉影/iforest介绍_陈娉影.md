&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

> **作者：** 陈娉影 (中山大学)  
> **E-mail：** <chenpy57@mail2.sysu.edu.cn> <1144583101@qq.com>

&emsp;


---

**目录**
[TOC]

---

&emsp;


&emsp;

## 1. 简介

`iforest` 是 isolation forest (分离森林) 的简称，适用于连续数据的异常检测。

异常检测又称“离群点检测”，是机器学习研究领域中跟现实紧密联系、有广泛应用需求的一类问题。“异常”，在这里指“容易被孤立的点”，即分布稀疏且距离高密度的点比较远的数据。基于此，可总结出“异常”的两个假设：<br>
①异常数据跟样本中大多数数据不太一样；<br>
②异常数据在整体数据样本中占比比较小。

一般的异常检测方法主要是通过对正常样本的描述，给出一个正常样本在特征空间中的区域，对于不在这个区域中的样本，视为异常。这类方法只优化正常样本的描述，而不优化异常样本的描述，可能造成大量误报或只检测到少量异常。其次，异常数据的一般刻画方法是必须用到各种统计的、距离的、密度的量化指标，来描述异常数据跟其他样本的疏离程度。

而 `iforest` 则直接孤立异常点而刻画数据的疏离程度，不只针对正常样本，也不用借助其他量化指标，更为简单、高效，在学术和实业中都非常实用。

&emsp;

## 2. 算法原理

`iforest` 算法的原理可以用一句话通俗地概括为：异常数据可以通过比正常样本更少的随机特征分割从而被“孤立”出来。

### 2.1 基本思想

假设有一组一维数据 (如图 Fig1_iforest 原理一维) ，对其进行随机切分来把点 A 和点 B 单独割开。

首先，在最大值和最小值之间随机选取一个值 x ，小于 x 的数据分为左边一组，大于等于 x 的分为右边一组。然后，对左右两组数据分别重复上述步骤，直到某组中数据个数小于 2 才停止分割该组。由于点 B 与其他数据比较疏离，可能用很少的次数就分割出来；点 A 跟其他点聚在一起，大概率需要更多的次数才能切分出来。

![Fig1_iforest原理一维](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈娉影_iforest介绍_Fig01.png)

从一维扩展到两维也是同样的 (如图 Fig2_iforest 原理二维) 。只是变为沿着两个坐标轴随机切分来把点 A' 和 B' 分别分割出来。首先，随机选择一个特征维度，在最大值和最小值之间随机取值，再分左右两组。接下来重复上述所有步骤，直到无法细分 (即只剩下一个数据点或剩下的数据全部相同) 。由于点 B' 与其他数据点比较疏离，可能只需要几次切分就可以将它分出；而点 A' 需要的切分次数大概率会更多。

![Fig2_iforest原理二维](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈娉影_iforest介绍_Fig02.png)

### 2.2 分离树 ( itree ) 的构建

`iforest` 采用二叉树 (每个节点有两个子节点或不再分叉) 分割数据，用数据在二叉树中的深度反应其“疏离”程度。所以分离树也即二叉树的构建就是整个算法的第一步：

基于对上文基本思想的介绍，可以更容易地理解分离树的构建：

从连续型的全量数据集中抽取一批样本，然后<br>
① 随机选择一个特征作为起始节点,<br>
② 在该特征的最大值和最小值之间随机取一个值,<br>
③ 样本中小于该值的数据记到左分支，大于等于该值的记到右分支,<br>
④ 在左右两个分支中，重复步骤 ①②③ ，直到满足如下条件： (1) 数据不可再分，即只包含一个数据或全部数据相同； (2) 二叉树达到限定的最大深度。

### 2.3 结合多棵分离树进行预测

构建好分离树就可以开始预测。分离树检测异常的原理是：异常值一般比较稀有，会很快地被分到叶子节点，所以异常值在分离树中从根节点到叶子节点的路径长度 h (x) 很短，故可据此来判断异常数值。但只使用一棵树的结果作为判断依据过于武断，所以需要把多棵分离树结合起来，形成分离森林。 (如图 Fig3_iforest 与 itree ) 

假设 itree 的训练样本中同样落在数据 x 所在叶子节点的样本数为 T.size ，则 x 在这棵 itree 上的路径长度 h (x) 为：

$$h(x) = e+C(T.size) \quad (1)$$

其中， H (n-1) 可用 ln(n-1)+0.5772156649 (欧拉常数) 估算。综合多棵 iTree 后 x 最终的异常分值 Score (x) 为：

$$Score(x) = 2^{- \frac{E(h(x))}{C(ψ)}} \quad (2)$$

其中， E (h (x) ) 表示数据 x 在多棵 iTree 中的路径长度的均值， ψ 表示单棵 iTree 的训练样本数， C (ψ) 表示用 ψ 条数据构建的二叉树的平均路径长度，主要用于归一化。

由异常分值的公式可得，若 x 在多棵分离树中的平均路径长度越短，得分越接近 1 ，x 越异常；若 x 在多棵分离树中的平均路径长度越长，得分越接近 0 ，数据 x 越正常；若 x 在多棵分离树中的平均路径长度接近整体均值，则得分约为 0.5 。

![Fig3_iforest与itree](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈娉影_iforest介绍_Fig03.png)

&emsp;

## 3. iforest 算法

### 3.1 语法结构

```stata
iforest varlist [if] [in] [, options]
```

### 3.2 命令安装

(从 https://github.com/adallak/stataiforest/archive/refs/heads/main.zip 下载压缩包后解压。运行 `iforest` 时需将路径设置为在该压缩包解压缩后的文件夹中。) 


```stata
global PaTH "D:\tempFF"   // 酌情修改路径
!md  $PaTH
cd   $PaTH
*copy "https://github.com/adallak/stataiforest/archive/refs/heads/main.zip" "iforest.zip", replace 
global zipfile "https://github.com/adallak/stataiforest/archive/refs/heads/main.zip"
copy "$zipfile" "iforest.zip", replace 
unzipfile "iforest.zip", replace
  
!copy "$PaTH\stataiforest-main\ifo*" "`c(sysdir_plus)'\i"
!copy "$PaTH\stataiforest-main\gen*" "`c(sysdir_plus)'\g"
  
adopath + "$PaTH\stataiforest-main"
cd "$PaTH\stataiforest-main"
  
* To build Mata libraries run
do make_liforest  //这一步很重要，定义 mata 函数
```


### 3.3 主要选项

- `ntrees(#)`：总体中用的分离树的数量。默认值为 200 。
- `samplesize(#)`：为训练每棵分离树而从数据中提取的样本大小。必须小于观察值个数 n 。
- `maxdepth(#)`：树的最大深度。默认值为 log2 (samplesize) 。
- `extlevel(#)`：指定用于分割数据的超平面的自由度。必须小于数据的观察值个数 n 。
- `nohist`：不要绘制估计得分的直方图 (不加此选项则默认绘制估计得分的直方图) 。
- `hist_options(#)`：直方图选项。

&emsp;

## 4. 命令使用：例子


```stata
clear all
set seed 20327015
gendata, n(600) p(2) type(cluster)                                    //随机生成矩阵。
mat X = r(X)          //将生成的 X1、X2 (二维)的、 400 个观察值的矩阵录入矩阵 X 。
mat Y = r(Y)          //若观测值异常，则允许 1 的二进制向量。
svmat X             //将矩阵 X 中的数据转化为数据，变量为 X1，X2，X3 。
svmat Y             //异常则记 Y1=1 ，否则为 0 。
iforest X1 X2, extlevel(1) ntrees(100) samplesize(256)  //使用 `iforest` 命令对观测值进行估分
mat list e(Score)      //列出 `iforest` 估计的得分。
estat metric Y1       //当真实目标 Y 已知时，我们可以找到预测的最佳阈值。结果表明，0.5 是最佳阈值，故使用该值进行预测。
predict Yhat, threshold(0.7)     //设定正常值占 70% 进行预测，异常值记为 1 ，否则为 0 。

ifroc Y1                //也可以通过可视化 ROC 来评估算法的性能。
```

&emsp;

## 5. 结语

分离森林算法主要有两个参数：一个是分离树的个数；另一个是训练单棵分离树时抽取样本的数目。实验表明，当设定为 100 棵树， 256 个抽样样本数时，大多数情况下就可以取得不错的效果。这也体现了算法的简单、高效。

以下是需要注意的几个问题：

第一，构建分离森林的方法和随机森林类似，都是随机采样部分数据来构建每一棵树，保证不同树之间的差异。但是区别在于分离森林要限制样本的大小。因为采样前初始样本过大时正常值和异常值会有重叠，但采样后较小的子样本可以有效地区分正常值和异常值 (如图 Fig4_初始数据 和 Fig5_采样数据： (a) 采样前， (b) 采样后) 。

![Fig4_初始数据](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈娉影_iforest介绍_Fig04.png)

![Fig5_采样数据](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈娉影_iforest介绍_Fig05.png)

第二，要限制分离树的最大深度。因为异常值比较少，其路径一般比较短。深度设置得过大，会产生很多没有意义的计算，浪费时间和资源。

第三，如果训练样本中异常样本的比例比较高，违背了先前提到的异常检测的基本假设，可能最终的效果会受影响。

第四，异常检测与具体应用场景紧密相关，算法检测出的“异常”不一定是实际所需。比如，在识别虚假交易时，异常的交易未必就是虚假的交易。所以，在选择特征时，需要过滤相关性较低的特征，以免识别出一些不太相关的“异常”。

&emsp;

&emsp;

### 引用及参考资料

- 刘腾飞, 机器学习-异常检测算法 (一) ：Isolation Forest, 知乎, [-Link-](https://zhuanlan.zhihu.com/p/27777266)
- 战争热诚, Python机器学习笔记：异常点检测算法——Isolation Forest,博客园, [-Link-]( https://www.cnblogs.com/wj-1314/p/10461816.html)
- 不可能打工, 孤立森林（Isolation Forest）从原理到实践,博客园, [-Link-](https://www.jianshu.com/p/dbb98e9d8aa4)
- 算法数据侠，高效优雅的异常检测算法——iForest,墨天轮, [-Link-](https://www.modb.pro/db/488603)
- 阿达拉克, [-Link-](https://github.com/adallak/stataiforest)