# 控制函数法及其Stata应用
> **作者：** 张弛（中山大学） 
> **邮箱：** <zhangch369@mail2.sysu.edu.cn@163.com>    

## 1.Motivation：何时选择Control Function Approach（CF方法）？

在对政策效果进行评估时，选择参与到一个项目的主体，可能在某些方面与选择不参与到这个项目的主体存在系统性差异，并且这样的差异可能较难被观察到，这会导致内生性问题。    
<br />
例如， Sapelli and Vial (2002)研究在智利上公立学校和私立学校对二年级学生成绩影响。不同的父母教育程度，不同的家庭经济状况等因素，一方面会直接影响到学生的成绩，另一方面还会影响到学生是选择读公立学校还是私立学校。而这就导致了“选择学校类型”这一变量本身就存在内生性。    
<br />
​而控制函数法的核心思想就在于，通过对内生解释变量（Endogenous Explanary Variables，下文简称EEV）的内生性来源进行提取，然后加入原始模型进行估计，来获得对政策效果的无偏估计。

## 2. 常系数线性模型
### 2.1 内生解释变量为连续型变量
#### 2.1.1 原理简介
假设 $y_{1}$ 为被解释变量，$y_{2}$ 为EEV，$z$ 是 $1 \times L$ 的外生变量（其中，$z_{1}=1，z_{1}$ 是 $z$ 的子向量）,我们估计的原始方程为：

$$y_{1}=\mathbf{z}_{1}\delta_{1} +\alpha_{1} y_{2}+u_{1}$$ (1)
我们的外生性假设为
$$E\left(\mathbf{z}^{\prime} u_1\right)=\mathbf{0}$$ (2)
对 $y_{2}$ 的内生性假设
$$y_2=\mathbf{z} \boldsymbol{\pi}_2+v_2, E\left(\mathbf{z}^{\prime} v_2\right)=\mathbf{0}$$(3)
其中$\pi_2$是$L \times1$的向量。此时，定义残差项之间的关系
$$u_1=\rho_1 v_2+e_1$$(4)
$$\rho_1=E\left(v_2 u_1\right) / E\left(v_2^2\right)$$(5)
此时，$\rho_1$为总体回归系数
将（4）代入（1）
$$y_1=\mathbf{z}_1 \boldsymbol{\delta}_1+\alpha_1 y_2+\rho_1 v_2+e_1$$(6)
此时，该方程所估计的 $\alpha_1$ 即与残差项无关。因为我们将 $y_2$ 中与残差项有关的，存在内生性的部分 $v_2$ 已单独提取出，作为一个新变量加入我们的估计模型中。

#### 2.1.2 检验变量是否为EEV：何时需要使用Control Function

针对
$$y_{i 1}=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{i 2}+\rho_{1} \hat{v}_{i 2}+\text { error }_{i}$$(12)
原假设 $H_0$: $\rho_1=0$
相当于Hausman检验中
$$H_{0}: \operatorname{Cov}\left(y_{2}, u_{1}\right)=0$$(13)
在这样的线性方法中，对 $\delta_1$ 和 $\alpha_1$ 的估计和2SLS方法是一样的。   
<br />
**注意：IV方法和CF方法不一定等价**   
<br />
例如，原方程加入 $y_2$ 的平方项

$$y_{1}=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+\gamma_{1} y_{2}^{2}+u_{1}$$(8)

若我们使用IV方法，我们需要加入 $z_{2}^{2}$ 作为 $y_{2}^{2}$ 的工具变量，此时工具变量向量为 $\left(\mathbf{z}_{1}, z_{2}, z_{2}^{2}\right)$ 。但是若使用CF方法，则需要施加更强的假设：

$$E\left(u_{1} \mid \mathbf{z}, y_{2}\right)=E\left(u_{1} \mid v_{2}\right)=\rho_{1} v_{2}$$(9)

CF方法中，第一阶段回归为：

$$y_{2}=\mathbf{z} \pi_{2}+v_{2}$$(10)

在前述条件下，我们有新的估计方程（11），此时，与2SLS的估计方法不一致

$$E\left(y_{1} \mid \mathbf{z}, y_{2}\right)=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+\gamma_{1} y_{2}^{2}+\rho_{1} v_{2}$$(11)

用第一阶段回归所估计得到的 $\hat{v}_{2}$ 代入上式，用OLS方法估计。    
<br />
这样的假设会使得CF方法更有效，但是稳健性不如IV方法，因为IV方法只需要满足 $E\left(u_{1} \mid \mathbf{z}\right)=0$ 这个假设更弱。


#### 2.1.3 估计方法和Stata实证步骤
第一步：将 $y_2$ 对 $z$ 做回归，获得简化方程的残差项 $\hat{v}_2$       
<br />
第二步：将 $y_1$ 对 $z_1$ ，$y_2$ ，$\hat{v}_2$ 做回归，对(6)式的OLS估计，就是Control Function的估计方法。   
<br />
在第二步的估计中，$\delta_{1}$ , $\alpha_1$ , 
$\rho_1$ 都是一致的。         
<br />
Stata代码如下：（待加入）

### 2.2 EEV为离散型变量

#### 2.2.1 原理方法

CF方法相比于IV方法的优势在于，它可以针对非线性的EEV进行一致性估计，例如EEV可以是一个二值变量。当 $y_2$ 为非线性形式时，我们无法使用IV估计，CF方法是更好的选择。     
<br />
例如，待估模型为：

$$y_{1}=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\gamma_{1} y_{2}+u_{1}$$(14)

在该模型中，我们假设 $y_2$ 为二值变量，满足以下方程

$$y_{2}=1\left[\mathbf{z} \boldsymbol{\delta}_{2}+e_{2} \geq 0\right]$$(15)

运用CF方法，需施加以下假设：

$$e_{2} \sim \operatorname{Normal}(0,1)$$(16)

此时，$E\left(u_{1} \mid \mathbf{z}, y_{2}\right)$ 取决于 $\left(u_{1}, y_{2}\right)$ 的联合分布，且 $\left(u_{1}, y_{2}\right)$ 独立于 $z$ ，$E\left(u_{1} \mid e_{2}\right)=\rho_{1} e_{2}$ ，此时我们估计的方程为

$$E\left(y_{1} \mid \mathbf{z}, y_{2}\right)=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+\rho_{1}\left[y_{2} \lambda\left(\mathbf{z} \boldsymbol{\delta}_{2}\right)-\left(1-y_{2}\right) \lambda\left(-\mathbf{z} \boldsymbol{\delta}_{2}\right)\right]$$(17)

并且

$$E\left(u_{1} \mid \mathbf{z}, y_{2}\right)=\rho_{1}\left[y_{2} \lambda\left(\mathbf{z} \boldsymbol{\delta}_{2}\right)-\left(1-y_{2}\right) \lambda\left(-\mathbf{z} \boldsymbol{\delta}_{2}\right)\right]$$(18)

其中，$\lambda(\cdot)$是逆米尔斯比率（inverse Mills ratio, IMR）


#### 2.2.2 估计步骤和Stata实现

1.估计 $P\left(y_{2}=1 \mid \mathbf{z}\right)=\Phi\left(\mathbf{z} \boldsymbol{\delta}_{2}\right)$ ，获得“广义残差”：

$$\hat{r}_{i 2} \equiv y_{i 2} \lambda\left(\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right)-\left(1-y_{i 2}\right) \lambda\left(-\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right), \quad i=1, \ldots, N$$(19)

2.将 $y_{i 1}$ 对 $\mathbf{z}_{i 1}, y_{i 2}$, $\hat{r}_{i 2}$ OLS回归，获得对 $\lambda_1$ , $\alpha_1$ , $\rho_1$ 的一致估计。     
<br />
Stata代码如下：

#### 2.2.3 EEV是非线性的情况下
假设 $y_2$ 和其他外生变量是交互的
$$y_{1}=\mathbf{z}_{1} \mathbf{\delta}_{1}+y_{2} \mathbf{z}_{1} \gamma_{1}+u_{1}$$(20)
如果 $y_{2} \mathbf{z}_{1}$ 中， $y_2$ 是线性的，可以按照最原始的方法，如果是二值变量，可以使用离散型随机变量的方法，均为以上两种情况的简单变形。

## 3. 相关随机系数模型
### 3.1 相关随机系数是什么
有时候， $y_2$ 的效果也会受到不可观测的因素影响。      
<br />
例如，就读于教会学校的教育回报率，可能会因为入学学生的个体差异而不同，且这些不同是无法观测的。       
<br />
相关随机系数（correlated random coefficient，CRC），指允许一个随机的系数影响解释变量。这个概念最早由 Heckman and Vytlac在1998年提出。     
<br />
在研究处理效应时，CRC模型允许异质性的处理效应和自选择结合。
### 3.2 模型设定
假设估计模型
$$y_{i1}=\mathbf{z}_{i 1} \mathbf{\delta}_{1}+g_{i 1}y_{i2}+u_{i 1}$$(21)
其中，  $y_i$ 与 $u_{i1}$ ，$g_{i 1}$ 相关，并且EEV系数满足以下两个条件
$$g_{i 1}=\gamma_{1}+v_{i 1}$$(22)
$$\gamma_{1}=E\left(g_{i 1}\right)$$(23)
代入(21)得
$$y_{i}=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\gamma_{1}y_{i2}+v_{i 1} y_{i2}+u_{i 1}$$(24)
与此同时，针对(24)式，我们还需要施加如下的外生性假设：
$$E\left(u_{i 1} \mid \mathbf{z}_{i}\right)=0,  E\left(v_{i 1} \mid \mathbf{z}_{i}\right)=0$$(25)

注意，此处不能运用2SLS估计，因为此时估计出来的 $\gamma_1$ 是不一致的，残差为 $e_{i 1}=v_{i 1} y_{i2}+u_{i 1}$ ，仍然内生。     
<br />
假设 $y_{i2}$ 的内生性设定如下：
$$y_{i 2}=\mathbf{z}_{i} \pi_{2}+v_{i 2}, E\left(v_{i 2} \mid \mathbf{z}_{i}\right)=0$$(28)
存在不可观测因素$u_{i 1}$, $v_{i 1}$，他们和 $v_{i2}$ 满足以下线性关系：
$$E\left(u_{i 1} \mid v_{i 2}\right)=\eta_{1} v_{i 2},  E\left(v_{i 1} \mid v_{i 2}\right)=\psi_{1} v_{i 2}$$(29)
并且，我们认为，所有不可观测的变量都与 $\mathbf{z}_{i}$ 独立，所以，我们实际上需要估计的式子是：
$$E\left(y_{i 1} \mid \mathbf{z}_{i}, y_{i 2}\right)=E\left(y_{i 1} \mid \mathbf{z}_{i 1}, y_{i 2}, v_{i 2}\right)=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\gamma_{1} y_{i 2}+\eta_{1} v_{i 2}+\psi_{1} v_{i 2} y_{i 2}$$(30)
接着，我们将 $y_{i 1}$ 对 $\mathbf{z}_{i 1}, y_{i 2}, \hat{v}_{i 2}, \hat{v}_{i 2} y_{i 2}$ 做OLS回归，得到CF估计。     
<br />
此处 $\hat{v}_{i 2} y_{i 2}$ 的系数，衡量的就是随机系数 $v_{i1}$ 对 $y_{i2}$ 的影响。
但是这里必须注意，如果 $v_{i 2}$ 的系数不为0，对交互项 $\hat{v}_{i 2} y_{i 2}$ 的t检验在第一阶段的回归是不可靠的，需要在第二步使用bootstrap去调整方差。     
<br />
在这里，为了方便，我们是对交互项和残差项($\hat{v}_{i 2}, \hat{v}_{i 2} y_{i 2}$)做联合检验，检验是否存在外生性。      
<br />
### 3.3 操作步骤和Stata代码实现
操作步骤：
Stata代码：

### 3.4 模型拓展
（28）式的模型也可以拓展到非线性的（ $\mathbf{z}_{i1}$, $y_{i2}$ ），包括多次方项和交互项。      
<br />
更一般化的模型如下：假设存在一个 $1 \times K_{1}$的 $\mathbf{x}_{i1}$ ，存在这样一个方程 $\mathbf{g}_{1}\left(\mathbf{z}_{i 1}, y_{i 2}\right)$，$\mathbf{x}_{i1}$ 的随机系数为 $\mathbf{b}_{i 1}=\boldsymbol{\beta}_{1}+\mathbf{v}_{i 1}$ 。此时，我们估计的方程为：
$$y_{i 1}=\alpha_{1}+\mathbf{x}_{i 1} \boldsymbol{\beta}_{1}+u_{i 1}+\mathbf{x}_{i 1} \mathbf{v}_{i i}$$
在第一阶段回归，我们获得残差 $\hat{v}_{i 2}$ 
后，之后，我们将 $y_{i 1}$ 对 $1, \mathbf{x}_{i 1}, \hat{v}_{i 2}, \mathbf{x}_{i 1} \hat{v}_{i 2}$ 做OLS回归，并使用非参数的bootstrap方法调整标准差，从而得到有效的估计。
如果 $\hat{\Psi }_{1}$ 是 $\mathbf{x}_{i 1} \hat{v}_{i 2}$ 的 $K_1$ 维的向量，我们则可以估计 $E\left(\mathbf{b}_{i 1} \mid v_{i 2}\right)=\hat{\mathbf{\beta_{1}}}+\hat{v_{i 2}} \mathbf{\hat{\Psi_{1}}}$     
<br />
类似的，对于 $y_2$ 是二值变量的情况，该方法也一样成立。     
<br />
由前所示， $y_2$ 需满足（15）和（16）式的条件：
$$y_{2}=1\left[\mathbf{z \delta _ { 2 }}+e_{2}>0\right]$$(15)
$$e_{2} \sim \operatorname{Normal}(0,1)$$(16)
然后，根据（19）获取标准化的残差
$$\hat{r}_{i 2} \equiv y_{i 2} \lambda\left(\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right)-\left(1-y_{i 2}\right) \lambda\left(-\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right), \quad i=1, \ldots, N$$(19)
最后，将 $y_{i 1}$ 对 $\mathbf{z}_{i 1}, y_{i 2}, y_{i 2} \cdot\left(\mathbf{z}_{i 1}-\overline{\mathbf{z}}_{1}\right), \hat{r}_{i 2}, y_{i 2} \hat{r}_{i 2}$ 做回归，中心化的 $\mathbf{z}_{i 1}$ 帮助我们求出政策的平均效应。     
<br />
注意，针对(30)
$$E\left(y_{i 1} \mid \mathbf{z}_{i}, y_{i 2}\right)=E\left(y_{i 1} \mid \mathbf{z}_{i 1}, y_{i 2}, v_{i 2}\right)=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\gamma_{1} y_{i 2}+\eta_{1} v_{i 2}+\psi_{1} v_{i 2} y_{i 2}$$(30)
的估计，我们不直接讨论 $y_2=1$ 和 $y_2=0$ 两种情况下分别的效果，ATE（Average Treatment Effect）在这个模型下就是 $\gamma_1$ 。

## 4. 非线性模型
当EEVs是连续的，CF方法经常被运用于估计Probit和Tobit模型。
### 4.1 EEVs是连续的
#### 4.1.1 Rivers-Vuong估计方法
假设我们要估计的 $y_1$ 为Probit模型
$$y_{1}=1\left[\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+u_{1} \geq 0\right]$$(31)
EEVs是连续的
$$y_2=\mathbf{z} \boldsymbol{\delta_2}+v_2$$(32)
并且，( $u_1$ , $v_1$ )服从二元正态分布，期望为0， $Var(u_1)=1$ 且与 $\mathbf{z}$ 独立。
令 $\rho_{1}=Corr(u_1,v_2)$ ，此时我们用CF方法的估计为
$$P\left(y_{1}=1 \mid \mathbf{z}, y_{2}\right)=\Phi\left(\mathbf{z}_{1} \boldsymbol{\delta}_{\rho 1}+\alpha_{\rho 1} y_{2}+\theta_{\rho 1} v_{2}\right)$$(33)
在此模型之中每个系数都被乘以了 $\left(1-\rho_{1}^{2}\right)^{-1 / 2}$ 


#### 4.1.2 操作步骤和Stata实现
操作步骤：   
<br />
（1）用OLS方法将 $y_2$ 对 $\mathbf{z}$ 做回归，得到残差 $\hat{v_2}$     
<br />
（2）将 $y_1$ 对 $\mathbf{z_1}$,$y_{2}$ , $\hat{v_{2}}$ 用Probit方法估计系数，并且，原假设H0: $\rho_{1}=0$，对 $\hat{v_2}$ 做t检验。    
<br />
若想求平均处理效应（APE，average partial effects），其实就是要求气节下面这个式子的变化：
$$E_{u_{i l}}\left\{1\left[\mathbf{z}_{1} \mathbf{\delta}_{1}+\gamma_{1} y_{2}+u_{i 1} \geq 0\right]\right\}=\Phi\left(\mathbf{z}_{1} \mathbf{\delta}_{1}+\gamma_{1} y_{2}\right)$$(34)

从而可以求出原始模型 $y_1$ 的估计值（ASF,average structural function）
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \Phi\left(\mathbf{x}_{1} \hat{\boldsymbol{\beta}}_{\rho 1}+\hat{\theta}_{\rho 1} \hat{v}_{i 2}\right)$$(35)

Stata代码如下：


#### 4.1.3 拓展：Blundell and Powell估计方法
BP方法允许 $y_2$ 是一个和 $\mathbf{z}$ 有关的函数，且不一定是线性的。     
<br />
BP方法假设一个结构模型
$$y_{1}=g_{1}(\mathbf{z}_1,y_{2},\mathbf{u}_1)$$(36)
它的ASF定义为
$$\operatorname{ASF}\left(\mathbf{z}_{1}, y_{2}\right) \equiv E_{\mathbf{u}_{i l}}\left[g_{1}\left(\mathbf{z}_{1}, y_{2}, \mathbf{u}_{i 1}\right)\right]$$(37)
这个ASF随着（$\mathbf{z_1}$,$y_2$）的变化而变化，从而求出APE。     
<br />
另一个关键假设是关于 $y_2$ 
$$y_2=g_{2}(\mathbf{z})+v_2$$(38)
并且，($\mathbf{u}_1$,$v_2$) 与$\mathbf{z}$是独立的，且 $v_2$ 的期望等于0,从而 $y_2$ 条件在 $\mathbf{z}$ 的期望为 $g_2(\mathbf{z})$ 。   
<br />
($\mathbf{u}_1$,$v_2$)是独立的于 $\mathbf{z}$ 时，不可观测的 $\mathbf{u}_1$ 与 ($\mathbf{z},y_{2}$) 相关的部分，只体现在 $v_2$ 上，即满足：
$$D\left(\mathbf{u}_{1} \mid \mathbf{z}, y_{2}\right)=D\left(\mathbf{u}_{1} \mid \mathbf{z}, v_{2}\right)=D\left(\mathbf{u}_{1} \mid v_{2}\right)$$(39)
因此，$v_2$ 可以作为 $\mathbf{u}_1$ 的代理变量，由此，第一步，定义如下条件期望：
$$h_{1}\left(\mathbf{z}_{1}, y_{2}, v_{2}\right) \equiv E\left(y_{1} \mid \mathbf{z}_{1}, y_{2}, v_{2}\right)$$(40)
我们要求出的核心结果是：
$$\operatorname{ASF}\left(\mathbf{z}_{1}, y_{2},\right)=E_{v_{i 2}}\left[h_{1}\left(\mathbf{z}_{1}, y_{2}, v_{i 2}\right)\right]$$(41)
因此，要先求出:
$$\hat{v}_{i 2}=y_{i 2}-\hat{g}_{2}\left(\mathbf{z}_{i}\right)$$(42)
然后根据ASF估计量的定义，我们求出：
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \hat{h}_{1}\left(\mathbf{z}_{1}, y_{2}, \hat{v}_{i 2}\right)$$
接下来，我们放宽假定，考虑一个二值响应模型：   
<br />
令 $\mathbf{x_1}$ 是所有内生和外生变量的一个线性组合， $v_2$ 是对 $y_2$ 约简式的一个误差项，我们可以估计一个二值相应模型，这个模型的设定是灵活的，举个例子：
$$P\left(y_{1}=1 \mid \mathbf{z}_{1}, y_{2}, v_{2}\right)=\Phi\left(\mathbf{x}_{1} \boldsymbol{\beta}_{1}+\rho_{1} v_{2}+\eta_{1} v_{2}^{2}+\mathbf{x}_{1} v_{2} \boldsymbol{\psi}_{1}\right)$$
在这里，CF方法的第一步要求我们从最初的简约式中估计出 $\hat{v_2}$ ，第二步要求我们将 $y_{i1}$ 对 $\mathbf{x_{i1}},\hat{v_2},\hat{v_{2}^2},\mathbf{x_{i1}}\hat{v_2}$ 做Probit估计。
检验外生性的方法也是类似的，对后三项做联合检验。之后，我们可以估计ASF：
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \Phi\left(\mathbf{x}_{1} \hat{\boldsymbol{\beta}}_{1}+\hat{\boldsymbol{\rho}}_{1} \hat{v}_{i 2}+\hat{\eta}_{1} \hat{v}_{i 2}^{2}+\mathbf{x}_{1} \hat{v}_{i 2} \hat{\boldsymbol{\psi}}_{1}\right)$$

### 4.2 EEVs是非线性的
前面所述的RV方法无法适用于 $y_2$ 是非连续的情况。在传统方法中，我们需要用MLE的方法去估计。接下来，我们介绍一种使用CF估计的方法：
假设内生解释变量是一个二值变量：
$$y_{2}=1\left[\mathbf{z} \delta_{2}+v_{2} \geq 0\right]$$
在标准二值变量的Probit假设中，没有一致估计参数的CF方法，但是可以通过正常的MLE方法中做对标准化残差的t检验，来判断 $y_2$ 是否外生。
在第一步，我们如前所述，需要估计
$$\hat{r}_{i 2} \equiv y_{i 2} \lambda\left(\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right)-\left(1-y_{i 2}\right) \lambda\left(-\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right), \quad i=1, \ldots, N$$(21)
但是在这里，我们不需要通过估计 $\delta_{2}$ 的系数来检验内生性，而是通过估计下面这个方程，原假设 $H_0$:$\rho_1=0$ ：
$$P\left(y_{i 1}=1 \mid \mathbf{z}_{i 1}, y_{i 2}, r_{i 2}\right)=\Phi\left(\mathbf{z}_{i i} \mathbf{\delta}_{1}+\gamma_{1} y_{i 2}+\rho_{1} r_{i 2}\right)$$
此时，用 $\hat{r}_{i 2}$ 去替代 $r_{i2}$ 。
同样的，我们需要假设 $y_2$ 的内生性全部被 $r_{i 2}$ 捕捉，我们加入和前文类似的假设：
$$D\left(\mathbf{u}_{1} \mid \mathbf{z}, y_{2}\right)=D\left(\mathbf{u}_{1} \mid \mathbf{z}, v_{2}\right)=D\left(\mathbf{u}_{1} \mid v_{2}\right)$$(39)
接着，再根据BP方法，估计ASF
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \Phi\left(\mathbf{z}_{1} \hat{\boldsymbol{\delta}}_{1}+\hat{\gamma}_{1} y_{2}+\hat{\rho}_{1} \hat{r}_{i 2}\right)$$
和连续EEV的情况类似，我们一样也可以使用更灵活的方法去估计以下方程：
$$P\left(y_{i 1}=1 \mid \mathbf{z}_{i}, y_{i 2}, r_{i 2}\right)=\Phi\left(\mathbf{x}_{i1} \boldsymbol{\beta}_{1}+\rho_{1} r_{i 2}+\mathbf{x}_{i 1} r_{i 2} \boldsymbol{\psi}_{1}\right)$$
其中，$\mathbf{x_1}$是（$\mathbf{z_1}$,$y_2$）的函数。
此时，在用 $\hat{r}_{i 2}$ 去替代 $r_{i2}$ 后，我们使用Wald Test，检验$H_0$: $\rho_1=0$，$\Psi_1=0$。此时，我们估计的ASF和前面的方法类似，将 $\hat{v}_2$ 替换成 $\hat{r}_2$ 即可。  
<br />

控制函数法对模型估计的灵活设置，有助于解决更复杂的问题。例如，一个二值估计模型之中，同时存在一个连续的EEv和一个二值EEv，我们在第一阶段估计中估计出来的残差或者标准化残差，可以在第二阶段中以各种形式加入 $y_1$ 的probit模型估计，例如二次项、三次项或者交互项。
## 5. 控制函数法与其他方法的比较
和Matching，和IV的比较