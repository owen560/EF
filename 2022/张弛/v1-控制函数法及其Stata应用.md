# Stata : 控制函数法及其Stata应用
[TOC]
&emsp;
> **作者** : 张弛(中山大学)   
> **邮箱** : <zhangch369@mail2.sysu.edu.cn>    
## 1.Motivation ：何时选择 Control Function Approach(CF方法) ？

在对政策效果进行评估时，选择参与到一个项目的主体，可能在某些方面与选择不参与到这个项目的主体存在系统性差异，并且这样的差异可能较难被观察到，这会导致内生性问题。    
<br />
例如， Sapelli and Vial (2002) 研究在智利上公立学校和私立学校对二年级学生成绩影响。不同的父母教育程度，不同的家庭经济状况等因素，一方面会直接影响到学生的成绩，另一方面还会影响到学生是选择读公立学校还是私立学校。而这就导致了“选择学校类型”这一变量本身就存在内生性。    
<br />
​而控制函数法的核心思想就在于，通过对内生解释变量 ( Endogenous Explanary Variables，下文简称 EEV ) 的内生性来源进行提取，然后加入原始模型进行估计，来获得对政策效果的无偏估计。

## 2. 常系数线性模型
### 2.1 内生解释变量为连续型变量
#### 2.1.1 原理简介
假设 $y_{1}$ 为被解释变量，$y_{2}$ 为EEV，$z$ 是 $1 \times L$ 的外生变量(其中，$z_{1}=1，z_{1}$ 是 $z$ 的子向量),我们估计的原始方程为：

$$y_{1}=\mathbf{z}_{1}\delta_{1} +\alpha_{1} y_{2}+u_{1}\quad (1)$$                  
我们的外生性假设为
$$E\left(\mathbf{z}^{\prime} u_1\right)=\mathbf{0}\quad (2)$$                        
对 $y_{2}$ 的内生性假设
$$y_2=\mathbf{z} \boldsymbol{\pi}_2+v_2, E\left(\mathbf{z}^{\prime} v_2\right)=\mathbf{0}\quad (3)$$                                                              
其中$\pi_2$是$L \times1$的向量。此时，定义残差项之间的关系
$$u_1=\rho_1 v_2+e_1\quad (4)$$
$$\rho_1=E\left(v_2 u_1\right) / E\left(v_2^2\right)\quad (5)$$
此时，$\rho_1$为总体回归系数
将(4)代入(1)
$$y_1=\mathbf{z}_1 \boldsymbol{\delta}_1+\alpha_1 y_2+\rho_1 v_2+e_1\quad (6)$$
此时，该方程所估计的 $\alpha_1$ 即与残差项无关。因为我们将 $y_2$ 中与残差项有关的，存在内生性的部分 $v_2$ 已单独提取出，作为一个新变量加入我们的估计模型中。

#### 2.1.2 检验变量是否为 EEV ：何时需要使用 Control Function 

针对
$$y_{i 1}=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{i 2}+\rho_{1} \hat{v}_{i 2}+\text { error }_{i}\quad (7)$$
原假设 $H_0$: $\rho_1=0$
相当于 Hausman 检验中
$$H_{0}: \operatorname{Cov}\left(y_{2}, u_{1}\right)=0$$
在这样的线性方法中，对 $\delta_1$ 和 $\alpha_1$ 的估计和2SLS方法是一样的。   
<br />
**注意：IV 方法和 CF 方法不一定等价**   
<br />
例如，原方程加入 $y_2$ 的平方项

$$y_{1}=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+\gamma_{1} y_{2}^{2}+u_{1}\quad (8)$$

若我们使用IV方法，我们需要加入 $z_{2}^{2}$ 作为 $y_{2}^{2}$ 的工具变量，此时工具变量向量为 $\left(\mathbf{z}_{1}, z_{2}, z_{2}^{2}\right)$ 。但是若使用CF方法，则需要施加更强的假设：

$$E\left(u_{1} \mid \mathbf{z}, y_{2}\right)=E\left(u_{1} \mid v_{2}\right)=\rho_{1} v_{2}\quad (9)$$

CF方法中，第一阶段回归为：

$$y_{2}=\mathbf{z} \pi_{2}+v_{2}\quad (10)$$

在前述条件下，我们有新的估计方程(11)，此时，与 2SLS 的估计方法不一致

$$E\left(y_{1} \mid \mathbf{z}, y_{2}\right)=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+\gamma_{1} y_{2}^{2}+\rho_{1} v_{2}\quad (11)$$

用第一阶段回归所估计得到的 $\hat{v}_{2}$ 代入上式，用OLS方法估计。    
<br />
这样的假设会使得 CF 方法更有效，但是稳健性不如 IV 方法，因为 IV 方法只需要满足 $E\left(u_{1} \mid \mathbf{z}\right)=0$ 这个假设更弱。


#### 2.1.3 估计方法和Stata实证步骤
第一步：将 $y_2$ 对 $z$ 做回归，获得简化方程的残差项 $\hat{v}_2$       
<br />
第二步：将 $y_1$ 对 $z_1$ ，$y_2$ ，$\hat{v}_2$ 做回归，对(6)式的OLS估计，就是Control Function的估计方法。   
<br />
在第二步的估计中，$\delta_{1}$ , $\alpha_1$ , 
$\rho_1$ 都是一致的。         
<br />
Stata 应用：考虑一个讨论教育回报率的模型   
<br />
```stata
bcuse wage2 //一个关于教育回报的数据集，作为演示的范例
gen exper2=exper^2 
sum educ
gen educ_average=r(mean)

//控制函数法stage1 获取存在内生性的残差项
bootstrap,reps(1000): reg educ meduc feduc black
predict v2,residual  //残差项吸收了内生性

//教育回报率方程其他变量的设置
gen black_educ=black*(educ-educ_average)

//stage2 将内生的残差项加入原始模型回归
bootstrap,reps(1000):reg lwage educ exper exper2 black tenure black_educ v2

//第二阶段回归结果如下，此时估计的educ系数即满足外生性条件

Linear regression                                       Number of obs =    722
                                                        Replications  =  1,000
                                                        Wald chi2(10) = 219.47
                                                        Prob > chi2   = 0.0000
                                                        R-squared     = 0.2177
                                                        Adj R-squared = 0.2067
                                                        Root MSE      = 0.3735

------------------------------------------------------------------------------
             |   Observed   Bootstrap                         Normal-based
       lwage | coefficient  std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
        educ |    .117709     .02196     5.36   0.000     .0746681    .1607499
       exper |   .0170884   .0163619     1.04   0.296    -.0149804    .0491572
      exper2 |  -.0001966   .0007394    -0.27   0.790    -.0016458    .0012525
       black |  -.1394666   .0562328    -2.48   0.013    -.2496808   -.0292524
          IQ |  -.0006156   .0019758    -0.31   0.755     -.004488    .0032568
      tenure |   .0081798   .0030596     2.67   0.008     .0021831    .0141765
  black_educ |  -.0291324   .0238556    -1.22   0.222    -.0758885    .0176237
     married |   .1796887   .0454683     3.95   0.000     .0905725    .2688049
         age |   .0071946   .0059308     1.21   0.225    -.0044297    .0188188
          v2 |  -.0725338   .0219758    -3.30   0.001    -.1156056   -.0294619
       _cons |   4.645004   .2370891    19.59   0.000     4.180318     5.10969
------------------------------------------------------------------------------



```

### 2.2 EEV为离散型变量

#### 2.2.1 原理方法

CF 方法相比于 IV 方法的优势在于，它可以针对非线性的 EEV 进行一致性估计，例如 EEV 可以是一个二值变量。当 $y_2$ 为非线性形式时，我们无法使用 IV 估计， CF 方法是更好的选择。     
<br />
例如，待估模型为：

$$y_{1}=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\gamma_{1} y_{2}+u_{1}\quad(12)$$

在该模型中，我们假设 $y_2$ 为二值变量，满足以下方程

$$y_{2}=1\left[\mathbf{z} \boldsymbol{\delta}_{2}+e_{2} \geq 0\right]\quad(13)$$

运用 CF 方法，需施加以下假设：

$$e_{2} \sim \operatorname{Normal}(0,1)\quad(14)$$

此时，$E\left(u_{1} \mid \mathbf{z}, y_{2}\right)$ 取决于 $\left(u_{1}, y_{2}\right)$ 的联合分布，且 $\left(u_{1}, y_{2}\right)$ 独立于 $z$ ，$E\left(u_{1} \mid e_{2}\right)=\rho_{1} e_{2}$ ，此时我们估计的方程为

$$E\left(y_{1} \mid \mathbf{z}, y_{2}\right)=\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+\rho_{1}\left[y_{2} \lambda\left(\mathbf{z} \boldsymbol{\delta}_{2}\right)-\left(1-y_{2}\right) \lambda\left(-\mathbf{z} \boldsymbol{\delta}_{2}\right)\right]\quad(15)$$

并且

$$E\left(u_{1} \mid \mathbf{z}, y_{2}\right)=\rho_{1}\left[y_{2} \lambda\left(\mathbf{z} \boldsymbol{\delta}_{2}\right)-\left(1-y_{2}\right) \lambda\left(-\mathbf{z} \boldsymbol{\delta}_{2}\right)\right]\quad(16)$$

其中，$\lambda(\cdot)$是逆米尔斯比率 ( inverse Mills ratio ,  IMR )


#### 2.2.2 估计步骤和Stata实现

1.估计 $P\left(y_{2}=1 \mid \mathbf{z}\right)=\Phi\left(\mathbf{z} \boldsymbol{\delta}_{2}\right)$ ，获得“广义残差”：

$$\hat{r}_{i 2} \equiv y_{i 2} \lambda\left(\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right)-\left(1-y_{i 2}\right) \lambda\left(-\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right), \quad i=1, \ldots, N$$

2.将 $y_{i 1}$ 对 $\mathbf{z}_{i 1}, y_{i 2}$, $\hat{r}_{i 2}$ OLS回归，获得对 $\lambda_1$ , $\alpha_1$ , $\rho_1$ 的一致估计。     
<br />

Stata 代码如下：\
可以使用 ``etregress`` 这一命令进行估计，该命令可以完成上述两个步骤。下文展示该命令的范例数据，展示购买健康保险对用药的影响。     
<br />

```stata
webuse drugexp  //stata提供的范例数据和命令
etregress lndrug chron age lninc, treat(ins=age married lninc work) poutcomes cfunction

//在该估计中，内生解释变量为ins，它受到二值变量age，married，lninc，work的影响
//poutcomes：表示用潜在结果模型估计第一阶段回归，如果不加该选项，即表示第一阶段回归为线性回归
//cfunction：表示用控制函数法估计处理效应
//回归结果如下：

Iteration 0:   GMM criterion Q(b) =  2.279e-15  
Iteration 1:   GMM criterion Q(b) =  6.361e-30  

Linear regression with endogenous treatment              Number of obs = 6,000
Estimator: Control function
------------------------------------------------------------------------------
             |               Robust
             | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
lndrug       |
       chron |   .4671725   .0319731    14.61   0.000     .4045064    .5298387
         age |   .1021359     .00292    34.98   0.000     .0964128    .1078589
       lninc |   .0550672   .0225036     2.45   0.014     .0109609    .0991735
       1.ins |  -.8598836   .3483648    -2.47   0.014    -1.542666   -.1771011
       _cons |   1.665539   .2527527     6.59   0.000     1.170153    2.160925
-------------+----------------------------------------------------------------
ins          |
         age |    .021142   .0022961     9.21   0.000     .0166416    .0256424
     married |    .084631   .0359713     2.35   0.019     .0141286    .1551334
       lninc |   .1023032   .0225009     4.55   0.000     .0582022    .1464041
        work |    .288418   .0372281     7.75   0.000     .2154522    .3613837
       _cons |   -.622993    .108795    -5.73   0.000    -.8362273   -.4097587
-------------+----------------------------------------------------------------
    /athrho0 |   .4035094   .1724539     2.34   0.019     .0655059    .7415129
   /lnsigma0 |   .3159269   .0500476     6.31   0.000     .2178353    .4140184
    /athrho1 |   .7929459   .2986601     2.66   0.008     .2075829    1.378309
   /lnsigma1 |   .1865347   .0613124     3.04   0.002     .0663646    .3067048
-------------+----------------------------------------------------------------
        rho0 |   .3829477   .1471637                      .0654124    .6300583
      sigma0 |    1.37153   .0686418                      1.243382    1.512885
     lambda0 |   .5252243    .226367                      .0815532    .9688954
        rho1 |   .6600746   .1685343                      .2046518     .880572
      sigma1 |   1.205066   .0738855                      1.068616     1.35894
     lambda1 |   .7954338   .2513036                      .3028878     1.28798
------------------------------------------------------------------------------
Wald test of indep. (rho0 = rho1 = 0): chi2(2) =    8.88  Prob > chi2 = 0.0118

//其中，Prob > chi2 = 0.0118表示拒绝Wald test的原假设，说明该模型的内生解释变量ins是一个内生的解释变量。
//1.ins系数的估计结果即为去除内生性后的估计结果

```
<br />

其中， ``cfunction`` 定义了该命令要使用控制函数的估计方法， ``poutcomes`` 定义了内生解释变量是一个潜在结果模型( potential outcome model )


#### 2.2.3 EEV是非线性的情况下
假设 $y_2$ 和其他外生变量是交互的
$$y_{1}=\mathbf{z}_{1} \mathbf{\delta}_{1}+y_{2} \mathbf{z}_{1} \gamma_{1}+u_{1}\quad(17)$$
如果 $y_{2} \mathbf{z}_{1}$ 中， $y_2$ 是线性的，可以按照最原始的方法，如果是二值变量，可以使用离散型随机变量的方法，均为以上两种情况的简单变形。

## 3. 相关随机系数模型
### 3.1 相关随机系数是什么
有时候， $y_2$ 的效果也会受到不可观测的因素影响。      
<br />
例如，就读于教会学校的教育回报率，可能会因为入学学生的个体差异而不同，且这些不同是无法观测的。       
<br />
相关随机系数 ( correlated random coefficient，CRC ) ，指允许一个随机的系数影响解释变量。这个概念最早由 Heckman and Vytlac 在1998年提出。     
<br />
在研究处理效应时， CRC 模型允许异质性的处理效应和自选择结合。
### 3.2 模型设定
假设估计模型
$$y_{i1}=\mathbf{z}_{i 1} \mathbf{\delta}_{1}+g_{i 1}y_{i2}+u_{i 1}\quad(18)$$
其中，  $y_i$ 与 $u_{i1}$ ，$g_{i 1}$ 相关，并且 EEV 系数满足以下两个条件
$$g_{i 1}=\gamma_{1}+v_{i 1}\quad(19)$$
$$\gamma_{1}=E\left(g_{i 1}\right)\quad(20)$$
代入(18)得
$$y_{i}=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\gamma_{1}y_{i2}+v_{i 1} y_{i2}+u_{i 1}\quad(21)$$
与此同时，针对(21)式，我们还需要施加如下的外生性假设：
$$E\left(u_{i 1} \mid \mathbf{z}_{i}\right)=0,  E\left(v_{i 1} \mid \mathbf{z}_{i}\right)=0\quad(22)$$

注意，此处不能运用 2SLS 估计，因为此时估计出来的 $\gamma_1$ 是不一致的，残差为 $e_{i 1}=v_{i 1} y_{i2}+u_{i 1}$ ，仍然内生。     
<br />
假设 $y_{i2}$ 的内生性设定如下：
$$y_{i 2}=\mathbf{z}_{i} \pi_{2}+v_{i 2}, E\left(v_{i 2} \mid \mathbf{z}_{i}\right)=0\quad(23)$$
存在不可观测因素$u_{i 1}$, $v_{i 1}$，他们和 $v_{i2}$ 满足以下线性关系：
$$E\left(u_{i 1} \mid v_{i 2}\right)=\eta_{1} v_{i 2},  E\left(v_{i 1} \mid v_{i 2}\right)=\psi_{1} v_{i 2}\quad(24)$$
并且，我们认为，所有不可观测的变量都与 $\mathbf{z}_{i}$ 独立，所以，我们实际上需要估计的式子是：
$$E\left(y_{i 1} \mid \mathbf{z}_{i}, y_{i 2}\right)=E\left(y_{i 1} \mid \mathbf{z}_{i 1}, y_{i 2}, v_{i 2}\right)=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\gamma_{1} y_{i 2}+\eta_{1} v_{i 2}+\psi_{1} v_{i 2} y_{i 2}\quad(25)$$
接着，我们将 $y_{i 1}$ 对 $\mathbf{z}_{i 1}, y_{i 2}, \hat{v}_{i 2}, \hat{v}_{i 2} y_{i 2}$ 做OLS回归，得到CF估计。     
<br />
此处 $\hat{v}_{i 2} y_{i 2}$ 的系数，衡量的就是随机系数 $v_{i1}$ 对 $y_{i2}$ 的影响。
但是这里必须注意，如果 $v_{i 2}$ 的系数不为0，对交互项 $\hat{v}_{i 2} y_{i 2}$ 的t检验在第一阶段的回归是不可靠的，需要在第二步使用bootstrap去调整方差。     
<br />
在这里，为了方便，我们是对交互项和残差项($\hat{v}_{i 2}, \hat{v}_{i 2} y_{i 2}$)做联合检验，检验是否存在外生性。      
<br />
### 3.3 操作步骤和 Stata 代码实现
操作步骤：与线性常系数模型中EEV连续的情况基本类似，只是在第二阶段回归的时候加入EEV和残差的交互项。在这里，我们依然考虑一个讨论教育回报率的模型。 
<br />

```stata
bcuse wage2
gen exper2=exper^2 
sum educ
gen educ_average=r(mean)

//控制函数法stage1 获取存在内生性的残差项
bootstrap,reps(1000): reg educ meduc feduc black
predict v2,residual  //残差项吸收了内生性

//教育回报率方程其他变量的设置
gen v2_educ=v2*educ  //构造残差和eev的交互项
gen black_educ=black*(educ-educ_average)

//stage2 将内生的残差项加入原始模型回归
bootstrap,reps(1000):reg lwage educ exper exper2 black tenure black_educ v2 v2_educ

Linear regression                                       Number of obs =    722
                                                        Replications  =  1,000
                                                        Wald chi2(8)  = 168.93
                                                        Prob > chi2   = 0.0000
                                                        R-squared     = 0.1866
                                                        Adj R-squared = 0.1775
                                                        Root MSE      = 0.3803

------------------------------------------------------------------------------
             |   Observed   Bootstrap                         Normal-based
       lwage | coefficient  std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
        educ |   .1237717   .0145209     8.52   0.000     .0953112    .1522321
       exper |    .006596   .0159191     0.41   0.679    -.0246049    .0377969
      exper2 |   .0005571   .0006782     0.82   0.411    -.0007722    .0018864
       black |  -.1603296   .0564448    -2.84   0.005    -.2709595   -.0496998
      tenure |   .0096416   .0029307     3.29   0.001     .0038976    .0153857
  black_educ |  -.0357143   .0244983    -1.46   0.145    -.0837301    .0123015
          v2 |   .0398098   .0503489     0.79   0.429    -.0588722    .1384918
     v2_educ |  -.0067674   .0034087    -1.99   0.047    -.0134483   -.0000864
       _cons |   4.921734   .2228319    22.09   0.000     4.484991    5.358477
------------------------------------------------------------------------------
```


### 3.4 模型拓展
(28)式的模型也可以拓展到非线性的( $\mathbf{z}_{i1}$, $y_{i2}$ )，包括多次方项和交互项。      
<br />
更一般化的模型如下：假设存在一个 $1 \times K_{1}$的 $\mathbf{x}_{i1}$ ，存在这样一个方程 $\mathbf{g}_{1}\left(\mathbf{z}_{i 1}, y_{i 2}\right)$，$\mathbf{x}_{i1}$ 的随机系数为 $\mathbf{b}_{i 1}=\boldsymbol{\beta}_{1}+\mathbf{v}_{i 1}$ 。此时，我们估计的方程为：
$$y_{i 1}=\alpha_{1}+\mathbf{x}_{i 1} \boldsymbol{\beta}_{1}+u_{i 1}+\mathbf{x}_{i 1} \mathbf{v}_{i i}$$
在第一阶段回归，我们获得残差 $\hat{v}_{i 2}$ 
后，之后，我们将 $y_{i 1}$ 对 $1, \mathbf{x}_{i 1}, \hat{v}_{i 2}, \mathbf{x}_{i 1} \hat{v}_{i 2}$ 做OLS回归，并使用非参数的bootstrap方法调整标准差，从而得到有效的估计。
如果 $\hat{\Psi }_{1}$ 是 $\mathbf{x}_{i 1} \hat{v}_{i 2}$ 的 $K_1$ 维的向量，我们则可以估计 $E\left(\mathbf{b}_{i 1} \mid v_{i 2}\right)=\hat{\mathbf{\beta_{1}}}+\hat{v_{i 2}} \mathbf{\hat{\Psi_{1}}}$     
<br />
类似的，对于 $y_2$ 是二值变量的情况，该方法也一样成立。     
<br />
由前所示， $y_2$ 需满足前面的条件：
$$y_{2}=1\left[\mathbf{z \delta _ { 2 }}+e_{2}>0\right]$$
$$e_{2} \sim \operatorname{Normal}(0,1)$$
然后，获取标准化的残差
$$\hat{r}_{i 2} \equiv y_{i 2} \lambda\left(\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right)-\left(1-y_{i 2}\right) \lambda\left(-\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right), \quad i=1, \ldots, N$$
最后，将 $y_{i 1}$ 对 $\mathbf{z}_{i 1}, y_{i 2} , y_{i 2} \cdot\left(\mathbf{z}_{i 1}-\overline{\mathbf{z}}_{1}\right) , \hat{r}_{i 2}, y_{i 2} \hat{r}_{i 2}$ 做回归，中心化的 $\mathbf{z}_{i 1}$ 帮助我们求出政策的平均效应。     
<br />
注意，针对(26)
$$E\left(y_{i 1} \mid \mathbf{z}_{i}, y_{i 2}\right)=E\left(y_{i 1} \mid \mathbf{z}_{i 1}, y_{i 2}, v_{i 2}\right)=\mathbf{z}_{i 1} \boldsymbol{\delta}_{1}+\gamma_{1} y_{i 2}+\eta_{1} v_{i 2}+\psi_{1} v_{i 2} y_{i 2}\quad(26)$$
的估计，我们不直接讨论 $y_2=1$ 和 $y_2=0$ 两种情况下分别的效果，ATE(Average Treatment Effect)在这个模型下就是 $\gamma_1$ 。

## 4. 非线性模型
当 EEV 是连续的，CF 方法经常被运用于估计 Probit 和 Tobit 模型。
### 4.1 EEV 是连续的
#### 4.1.1 Rivers-Vuong估计方法
假设我们要估计的 $y_1$ 为Probit模型
$$y_{1}=1\left[\mathbf{z}_{1} \boldsymbol{\delta}_{1}+\alpha_{1} y_{2}+u_{1} \geq 0\right]\quad(27)$$
EEV 是连续的
$$y_2=\mathbf{z} \boldsymbol{\delta_2}+v_2\quad(28)$$
并且，( $u_1$ , $v_1$ )服从二元正态分布，期望为0， $Var(u_1)=1$ 且与 $\mathbf{z}$ 独立。
令 $\rho_{1}=Corr(u_1,v_2)$ ，此时我们用 CF 方法的估计为
$$P\left(y_{1}=1 \mid \mathbf{z}, y_{2}\right)=\Phi\left(\mathbf{z}_{1} \boldsymbol{\delta}_{\rho 1}+\alpha_{\rho 1} y_{2}+\theta_{\rho 1} v_{2}\right)\quad(29)$$
在此模型之中每个系数都被乘以了 $\left(1-\rho_{1}^{2}\right)^{-1 / 2}$ 


#### 4.1.2 操作步骤和 Stata 实现
操作步骤：   
<br />
(1)用 OLS 方法将 $y_2$ 对 $\mathbf{z}$ 做回归，得到残差 $\hat{v_2}$     
<br />
(2)将 $y_1$ 对 $\mathbf{z_1}$,$y_{2}$ , $\hat{v_{2}}$ 用 Probit 方法估计系数。并且，原假设H0: $\rho_{1}=0$，对 $\hat{v_2}$ 做 t 检验。    
<br />
若想求平均处理效应 ( APE，average partial effects ) ，其实就是要求下面这个式子的变化：
$$E_{u_{i l}}\left\{1\left[\mathbf{z}_{1} \mathbf{\delta}_{1}+\gamma_{1} y_{2}+u_{i 1} \geq 0\right]\right\}=\Phi\left(\mathbf{z}_{1} \mathbf{\delta}_{1}+\gamma_{1} y_{2}\right)$$(34)

从而可以求出原始模型 $y_1$ 的估计值 ( ASF,average structural function ) 
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \Phi\left(\mathbf{x}_{1} \hat{\boldsymbol{\beta}}_{\rho 1}+\hat{\theta}_{\rho 1} \hat{v}_{i 2}\right)\quad(30)$$   
<br /> 
Stata 代码如下：   估计是否购买补充医疗保险(private)对身体健康状况(docvis)的影响
<br /> 

```stata
use mus17data.dta,clear

//控制函数法stage1 获取存在内生性的残差项
global xlist2 medicaid age age2 educyr actlim totchr
regress private $xlist2 income ssiratio, vce(robust)
predict lpuhat, residual

//控制函数法stage2 将残差项加入原模型回归
poisson docvis private $xlist2 lpuhat, vce(robust) nolog

//第二阶段回归结果如下：
Poisson regression                                      Number of obs =  3,677
                                                        Wald chi2(8)  = 718.87
                                                        Prob > chi2   = 0.0000
Log pseudolikelihood = -15010.614                       Pseudo R2     = 0.1303

------------------------------------------------------------------------------
             |               Robust
      docvis | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
     private |   .5505541   .2453175     2.24   0.025     .0697407    1.031368
    medicaid |   .2628822   .1197162     2.20   0.028     .0282428    .4975217
         age |   .3350604   .0696064     4.81   0.000     .1986344    .4714865
        age2 |  -.0021923   .0004576    -4.79   0.000    -.0030893   -.0012954
      educyr |    .018606   .0080461     2.31   0.021      .002836     .034376
      actlim |   .2053417   .0414248     4.96   0.000     .1241505     .286533
      totchr |     .24147   .0129175    18.69   0.000     .2161523    .2667878
      lpuhat |  -.4166838    .249347    -1.67   0.095    -.9053949    .0720272
       _cons |  -11.90647   2.661445    -4.47   0.000     -17.1228    -6.69013
------------------------------------------------------------------------------



```


#### 4.1.3 拓展： Blundell and Powell 估计方法
BP 方法允许 $y_2$ 是一个和 $\mathbf{z}$ 有关的函数，且不一定是线性的。     
<br />
BP 方法假设一个结构模型
$$y_{1}=g_{1}(\mathbf{z}_1,y_{2},\mathbf{u}_1)\quad(31)$$
它的 ASF 定义为
$$\operatorname{ASF}\left(\mathbf{z}_{1}, y_{2}\right) \equiv E_{\mathbf{u}_{i l}}\left[g_{1}\left(\mathbf{z}_{1}, y_{2}, \mathbf{u}_{i 1}\right)\right]\quad(32)$$
这个 ASF 随着($\mathbf{z_1}$,$y_2$)的变化而变化，从而求出APE。     
<br />
另一个关键假设是关于 $y_2$ 
$$y_2=g_{2}(\mathbf{z})+v_2\quad(33)$$
并且，($\mathbf{u}_1$,$v_2$) 与$\mathbf{z}$是独立的，且 $v_2$ 的期望等于0,从而 $y_2$ 条件在 $\mathbf{z}$ 的期望为 $g_2(\mathbf{z})$ 。   
<br />
($\mathbf{u}_1$,$v_2$)是独立的于 $\mathbf{z}$ 时，不可观测的 $\mathbf{u}_1$ 与 ($\mathbf{z},y_{2}$) 相关的部分，只体现在 $v_2$ 上，即满足：
$$D\left(\mathbf{u}_{1} \mid \mathbf{z}, y_{2}\right)=D\left(\mathbf{u}_{1} \mid \mathbf{z}, v_{2}\right)=D\left(\mathbf{u}_{1} \mid v_{2}\right)\quad(34)$$
因此，$v_2$ 可以作为 $\mathbf{u}_1$ 的代理变量，由此，第一步，定义如下条件期望：
$$h_{1}\left(\mathbf{z}_{1}, y_{2}, v_{2}\right) \equiv E\left(y_{1} \mid \mathbf{z}_{1}, y_{2}, v_{2}\right)\quad(35)$$
我们要求出的核心结果是：
$$\operatorname{ASF}\left(\mathbf{z}_{1}, y_{2},\right)=E_{v_{i 2}}\left[h_{1}\left(\mathbf{z}_{1}, y_{2}, v_{i 2}\right)\right]\quad(36)$$
因此，要先求出:
$$\hat{v}_{i 2}=y_{i 2}-\hat{g}_{2}\left(\mathbf{z}_{i}\right)\quad(37)$$
然后根据 ASF 估计量的定义，我们求出：
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \hat{h}_{1}\left(\mathbf{z}_{1}, y_{2}, \hat{v}_{i 2}\right)\quad(38)$$
接下来，我们放宽假定，考虑一个二值响应模型：   
<br />
令 $\mathbf{x_1}$ 是所有内生和外生变量的一个线性组合， $v_2$ 是对 $y_2$ 约简式的一个误差项，我们可以估计一个二值相应模型，这个模型的设定是灵活的，举个例子：
$$P\left(y_{1}=1 \mid \mathbf{z}_{1}, y_{2}, v_{2}\right)=\Phi\left(\mathbf{x}_{1} \boldsymbol{\beta}_{1}+\rho_{1} v_{2}+\eta_{1} v_{2}^{2}+\mathbf{x}_{1} v_{2} \boldsymbol{\psi}_{1}\right)\quad(39)$$
在这里， CF 方法的第一步要求我们从最初的简约式中估计出 $\hat{v_2}$ ，第二步要求我们将 $y_{i1}$ 对 $\mathbf{x_{i1}},\hat{v_2},\hat{v_{2}^2},\mathbf{x_{i1}}\hat{v_2}$ 做 Probit 估计。
检验外生性的方法也是类似的，对后三项做联合检验。之后，我们可以估计 ASF ：
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \Phi\left(\mathbf{x}_{1} \hat{\boldsymbol{\beta}}_{1}+\hat{\boldsymbol{\rho}}_{1} \hat{v}_{i 2}+\hat{\eta}_{1} \hat{v}_{i 2}^{2}+\mathbf{x}_{1} \hat{v}_{i 2} \hat{\boldsymbol{\psi}}_{1}\right)\quad(40)$$

### 4.2  EEV 是非线性的
前面所述的 RV 方法无法适用于 $y_2$ 是非连续的情况。在传统方法中，我们需要用 MLE 的方法去估计。接下来，我们介绍一种使用 CF 估计的方法：
假设内生解释变量是一个二值变量：
$$y_{2}=1\left[\mathbf{z} \delta_{2}+v_{2} \geq 0\right]\quad(40)$$
在标准二值变量的Probit假设中，没有一致估计参数的 CF 方法，但是可以通过正常的 MLE 方法中做对标准化残差的t检验，来判断 $y_2$ 是否外生。
在第一步，我们如前所述，需要估计
$$\hat{r}_{i 2} \equiv y_{i 2} \lambda\left(\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right)-\left(1-y_{i 2}\right) \lambda\left(-\mathbf{z}_{i} \hat{\boldsymbol{\delta}}_{2}\right), \quad i=1, \ldots, N$$
但是在这里，我们不需要通过估计 $\delta_{2}$ 的系数来检验内生性，而是通过估计下面这个方程，原假设 $H_0$:$\rho_1=0$ ：
$$P\left(y_{i 1}=1 \mid \mathbf{z}_{i 1}, y_{i 2}, r_{i 2}\right)=\Phi\left(\mathbf{z}_{i i} \mathbf{\delta}_{1}+\gamma_{1} y_{i 2}+\rho_{1} r_{i 2}\right)$$
此时，用 $\hat{r}_{i 2}$ 去替代 $r_{i2}$ 。
同样的，我们需要假设 $y_2$ 的内生性全部被 $r_{i 2}$ 捕捉，我们加入和前文类似的假设：
$$D\left(\mathbf{u}_{1} \mid \mathbf{z}, y_{2}\right)=D\left(\mathbf{u}_{1} \mid \mathbf{z}, v_{2}\right)=D\left(\mathbf{u}_{1} \mid v_{2}\right)\quad(41)$$
接着，再根据 BP 方法，估计 ASF
$$\widehat{A S F}\left(\mathbf{z}_{1}, y_{2}\right)=N^{-1} \sum_{i=1}^{N} \Phi\left(\mathbf{z}_{1} \hat{\boldsymbol{\delta}}_{1}+\hat{\gamma}_{1} y_{2}+\hat{\rho}_{1} \hat{r}_{i 2}\right)$$
和连续 EEV 的情况类似，我们一样也可以使用更灵活的方法去估计以下方程：
$$P\left(y_{i 1}=1 \mid \mathbf{z}_{i}, y_{i 2}, r_{i 2}\right)=\Phi\left(\mathbf{x}_{i1} \boldsymbol{\beta}_{1}+\rho_{1} r_{i 2}+\mathbf{x}_{i 1} r_{i 2} \boldsymbol{\psi}_{1}\right)$$
其中，$\mathbf{x_1}$是($\mathbf{z_1}$,$y_2$)的函数。
此时，在用 $\hat{r}_{i 2}$ 去替代 $r_{i2}$ 后，我们使用Wald Test，检验 $H_0$: $\rho_1=0$，$\Psi_1=0$。此时，我们估计的ASF和前面的方法类似，将 $\hat{v}_2$ 替换成 $\hat{r}_2$ 即可。  
<br />

控制函数法对模型估计的灵活设置，有助于解决更复杂的问题。例如，一个二值估计模型之中，同时存在一个连续的 EEV 和一个二值 EEV ，我们在第一阶段估计中估计出来的残差或者标准化残差，可以在第二阶段中以各种形式加入 $y_1$ 的 probit 模型估计，例如二次项、三次项或者交互项。

## 5. 控制函数法与工具变量法的比较
**假设条件**
* 控制函数法：在两个阶段做回归时，残差都需要服从一个特定分布，但无需满足排斥性约束
* IV ：第一阶段对内生变量做回归时，残差无需服从一个特定分布，但需要满足排斥性约束
  
**估计过程**
* 控制函数法：模型设定更为灵活，它可以针对线性，非线性，常系数和随机系数进行估计
* IV ：对模型设定的要求不如CF灵活。例如， EEV 为二值变量时， CF 方法在保持一致估计的情况下，比传统2SLS方法更有效。
* 但是，在多重非线性方程下的 EEV ， IV 方法在一致性方面更加稳健，但是也有可能有效性降低。

**估计结果**
* 控制函数法：可以求出 ATT ( average effect of the treatment on the treated ) ， ATE( average treatment effect )， LATE ( local average treatment effect )
* IV ：只能求 LATE
  
## 6. 相关推文
参考文献:
<br />
- Jeffrey M. Wooldridge, 2015. "Control Function Methods in Applied Econometrics", Journal of Human Resources, University of Wisconsin Press, vol. 50(2), pages 420-445.[-PDF-](http://jhr.uwpress.org/content/50/2/420.short)   
<br />
- Jeffrey M. Wooldridge, 2011. "Control Function And Related Methods",[-PDF-](https://www.eief.it/files/2011/10/slides_3_controlfuncs.pdf)
<br />
- Glewwe, P., P. Todd. Control function methods[C], Impact evaluation in international development: Theory, methods, and practice 267-279.[-PDF-](https://pte.org.pl/wp-content/uploads/2022/04/9781464814976.pdf)
<br />
- Heckman, J., S. Navarro-Lozano, 2004, Using matching, instrumental variables, and control functions to estimate economic choice models, Review of Economics and Statistics, 86 (1): 30-57.[-PDF-](https://direct.mit.edu/rest/article-abstract/86/1/30/57495/Using-Matching-Instrumental-Variables-and-Control?redirectedFrom=fulltext)
<br />
- Altonji, Joseph G. et al. “An Evaluation of Instrumental Variable Strategies for Estimating the Effects of Catholic Schooling.” The Journal of Human Resources XL (2002): 791 - 821.[-PDF-](https://www.nber.org/papers/w9358)
- Sapelli, Claudio, and Bernardita Vial. 2002. “The performance of private and public Schools in the Chilean
Voucher System.” Cuardenos do Economía 39 (118): 423–54.[-PDF-](https://www.jstor.org/stable/41951427#metadata_info_tab_contents)
- Jeffrey M. Wooldridge, 横截面与面板数据的经济计量分析[M].中国人民大学出版社,2007
- 李德华, 微信公众号“计量经济圈”, [非线性模型及离散内生变量处理利器, 应用计量经济学中的控制函数法！](https://mp.weixin.qq.com/s/GoS3mfSYnzY_ts8VvpsxuQ)

## 7. 相关推文
> Note：产生如下推文列表的 Stata 命令为：   
> `lianxh 内生性`  
> 安装最新版 `lianxh` 命令：    
> `ssc install lianxh, replace` 

- 专题：[IV-GMM](https://www.lianxh.cn/blogs/38)
  - [IV专题- 内生性检验与过度识别检验](https://www.lianxh.cn/news/cc3f92710dd7a.html)

- 专题：[内生性-因果推断](https://www.lianxh.cn/blogs/26.html)
  - [内生性！内生性！解决方法大集合](https://www.lianxh.cn/news/224e2b4e170e4.html)
  - [第三种内生性：衡量偏误(测量误差)如何解决？-eivreg-sem](https://www.lianxh.cn/news/18b0a37d8391d.html)
  - [内生性：来源及处理方法-幻灯片下载](https://www.lianxh.cn/news/b770b35f48e17.html)
  - [locmtest：非线性模型的内生性检验](https://www.lianxh.cn/news/f118ef34fd52f.html)
  - [第三种内生性：衡量偏误(测量误差)如何检验-dgmtest?](https://www.lianxh.cn/news/44fa0eb361042.html)
