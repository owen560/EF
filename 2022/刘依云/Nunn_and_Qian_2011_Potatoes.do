*          ===================================
*
*              土豆对人口与城市化的贡献
*
*                    双重差分 (DID)      
*               Difference-in-Difference
*                作者：刘依云（中山大学）
*                liuyy376@mail2.sysu.edu.cn
*          ===================================

        
capture clear
capture log close
set more off
set scheme s2manual

* 系统诊断 

di "=== SYSTEM DIAGNOSTICS ==="
di "Stata version: `c(stata_version)'"
di "Updated as of: `c(born_date)'"
di "Edition:       `c(edition_real)'"
di "Processors:    `c(processors)'"
di "OS:            `c(os)' `c(osdtl)'"
di "Machine type:  `c(machine_type)'"
di "=========================="

* 路径设定

  global path "D:\Stata.v17.0\Homework_EF\DID-Potato"  //文件压缩路径
  global R    "$path\ref"      //参考文献 
  global D    "$path\data"     //数据
  global Out  "$path\out"       //结果：图形和表格
  global Orig "$path\Orig_dofile" //源dofile
  cd "$D"
  

*查看原文
  shellout "$path\Nunn-and-Qian-2011-The-Potato's-Contribution-to-Population-and-Urbanization.pdf"
  

* 日志文件

global logdir "${rootdir}/logs"
cap mkdir "$logdir"

local c_date = c(current_date)
local cdate = subinstr("`c_date'", " ", "_", .)  // repace all " " in "`c_date'" with "_"
local c_time = c(current_time)
local ctime = subinstr("`c_time'", ":", "_", .)

log using "$logdir/logfile_`cdate'-`ctime'-`c(username)'.log", name(ldi) replace text

* ssc install
if !missing("`ssc_packages'") {
    foreach pkg in `ssc_packages' {
        cap `pkg'
        if _rc == 199 {    // _rc the return code from the most recent capture command
            dis "Installing `pkg'"
            ssc install `pkg', replace
        }
        which `pkg'
    }
}

* net install pkgname, from(directory_or_url)

/*所有命令下载完成后运行。将命令包集合成一个新的库*/
mata: mata mlib index

*国家级别
  
  use "country_level_panel_for_web.dta"
  browse
  doedit "$Orig\Replication_country_level_results.do"
  
*===================================================
*-数据预处理: 
*   生成新变量，设置暂元
*===================================================

*-生成国家特征与时间固定效应的交互项

foreach x in 1000 1100 1200 1300 1400 1500 1600 1700 1750 1800 1850 1900{
	gen ln_wpot_`x'=ln_wpot*ydum`x'
	gen ln_all_`x'=ln_all*ydum`x'
	gen ln_nworld_`x'=ln_nworld*ydum`x'
	gen ln_oworld_`x'=ln_oworld*ydum`x'
	gen ln_tropical_`x'=ln_tropical*ydum`x'
	gen ln_rugged_`x'=ln_rugged*ydum`x'
	gen ln_elevation_`x'=ln_elevation*ydum`x'
	gen ln_disteq_`x'=ln_disteq*ydum`x'
	gen ln_dist_coast_`x'=ln_dist_coast*ydum`x'
	gen ln_export_`x'=ln_export*ydum`x'
	gen malaria_`x'=malaria*ydum`x'
	gen ln_spot_`x'=ln_spot*ydum`x'
	gen ln_silagemaize_`x'=ln_silagemaize*ydum`x'
	gen ln_maize_`x'=ln_maize*ydum`x'
	gen ln_cassava_`x'=ln_cassava*ydum`x'
	}

*********************************************
*** TABLE 2 - BASELINE FLEXIBLE ESTIMATES ***
           *** 基准灵活估计 ***
*********************************************	
**# Bookmark #5
	
*-给每个国家级别的特征变量生成其所有时期变量生成合并暂元以简化回归	
	
	local ln_potato_flexible "ln_wpot_1100 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850 ln_wpot_1900"
	local ydum_flexible "ydum1100 ydum1200 ydum1300 ydum1400 ydum1500 ydum1600 ydum1700 ydum1750 ydum1800 ydum1850 ydum1900"
	local ln_all_flexible "ln_all_1100 ln_all_1200 ln_all_1300 ln_all_1400 ln_all_1500 ln_all_1600 ln_all_1700 ln_all_1750 ln_all_1800 ln_all_1850 ln_all_1900"
	local ln_nworld_flexible "ln_nworld_1100 ln_nworld_1200 ln_nworld_1300 ln_nworld_1400 ln_nworld_1500 ln_nworld_1600 ln_nworld_1700 ln_nworld_1750 ln_nworld_1800 ln_nworld_1850 ln_nworld_1900"
	local ln_oworld_flexible "ln_oworld_1100 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850 ln_oworld_1900"
	local ln_tropical_flexible "ln_tropical_1100 ln_tropical_1200 ln_tropical_1300 ln_tropical_1400 ln_tropical_1500 ln_tropical_1600 ln_tropical_1700 ln_tropical_1750 ln_tropical_1800 ln_tropical_1850 ln_tropical_1900"
	local ln_rugged_flexible "ln_rugged_1100 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850 ln_rugged_1900"
	local ln_elevation_flexible "ln_elevation_1100 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850 ln_elevation_1900"
	local ln_disteq_flexible "ln_disteq_1100 ln_disteq_1200 ln_disteq_1300 ln_disteq_1400 ln_disteq_1500 ln_disteq_1600 ln_disteq_1700 ln_disteq_1750 ln_disteq_1800 ln_disteq_1850 ln_disteq_1900"
	local ln_dist_coast_flexible "ln_dist_coast_1100 ln_dist_coast_1200 ln_dist_coast_1300 ln_dist_coast_1400 ln_dist_coast_1500 ln_dist_coast_1600 ln_dist_coast_1700 ln_dist_coast_1750 ln_dist_coast_1800 ln_dist_coast_1850 ln_dist_coast_1900"
	local malaria_flexible "malaria_1100 malaria_1200 malaria_1300 malaria_1400 malaria_1500 malaria_1600 malaria_1700 malaria_1750 malaria_1800 malaria_1850 malaria_1900"
	local ln_export_flexible "ln_export_1100 ln_export_1200 ln_export_1300 ln_export_1400 ln_export_1500 ln_export_1600 ln_export_1700 ln_export_1750 ln_export_1800 ln_export_1850 ln_export_1900"
	local ln_spot_flexible "ln_spot_1100 ln_spot_1200 ln_spot_1300 ln_spot_1400 ln_spot_1500 ln_spot_1600 ln_spot_1700 ln_spot_1750 ln_spot_1800 ln_spot_1850 ln_spot_1900"
	local ln_silagemaize_flexible "ln_silagemaize_1100 ln_silagemaize_1200 ln_silagemaize_1300 ln_silagemaize_1400 ln_silagemaize_1500 ln_silagemaize_1600 ln_silagemaize_1700 ln_silagemaize_1750 ln_silagemaize_1800 ln_silagemaize_1850 ln_silagemaize_1900"
	local ln_maize_flexible "ln_maize_1100 ln_maize_1200 ln_maize_1300 ln_maize_1400 ln_maize_1500 ln_maize_1600 ln_maize_1700 ln_maize_1750 ln_maize_1800 ln_maize_1850 ln_maize_1900"
	local ln_cassava_flexible "ln_cassava_1100 ln_cassava_1200 ln_cassava_1300 ln_cassava_1400 ln_cassava_1500 ln_cassava_1600 ln_cassava_1700 ln_cassava_1750 ln_cassava_1800 ln_cassava_1850 ln_cassava_1900"


***土豆适宜地对于人口的影响***

* Col 1
xi: areg ln_population `ln_potato_flexible' i.year, a(isocode) cluster(isocode)
test (ln_wpot_1750=0) (ln_wpot_1800=0) (ln_wpot_1850=0) (ln_wpot_1900=0)
estadd local lnOldWorldCropsArea "N"
estadd local lnElevation "N"
estadd local lnRuggedness "N"
estadd local lnTropicalArea "N" 
estimates store t2c1 	
			
* Col 2
xi: areg ln_population `ln_potato_flexible' `ln_oworld_flexible' i.year, a(isocode) cluster(isocode)
test (ln_wpot_1750=0) (ln_wpot_1800=0) (ln_wpot_1850=0) (ln_wpot_1900=0)
estadd local lnOldWorldCropsArea "Y"
estadd local lnElevation "N"
estadd local lnRuggedness "N"
estadd local lnTropicalArea "N" 
est store t2c2

* Col 3
xi: areg ln_population `ln_potato_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
test (ln_wpot_1750=0) (ln_wpot_1800=0) (ln_wpot_1850=0) (ln_wpot_1900=0)
estadd local lnOldWorldCrops Area "Y"
estadd local lnElevation "Y"
estadd local lnRuggedness "Y"
estadd local lnTropicalArea "Y" 
est store t2c3



*** 土豆适宜地对于城镇化比例的影响 ***

* Col 4
xi: areg city_pop_share `ln_potato_flexible' i.year, a(isocode) cluster(isocode)
test (ln_wpot_1750=0) (ln_wpot_1800=0) (ln_wpot_1850=0) (ln_wpot_1900=0)
estadd local lnOldWorldCropsArea "N"
estadd local lnElevation "N"
estadd local lnRuggedness "N"
estadd local lnTropicalArea "N" 
estimates store t2c4 	
* Col 5
xi: areg city_pop_share `ln_potato_flexible' `ln_oworld_flexible' i.year, a(isocode) cluster(isocode)
test (ln_wpot_1750=0) (ln_wpot_1800=0) (ln_wpot_1850=0) (ln_wpot_1900=0)
estadd local lnOldWorldCropsArea "Y"
estadd local lnElevation "N"
estadd local lnRuggedness "N"
estadd local lnTropicalArea "N" 
estimates store t2c5 	
* Col 6
xi: areg city_pop_share `ln_potato_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
test (ln_wpot_1750=0) (ln_wpot_1800=0) (ln_wpot_1850=0) (ln_wpot_1900=0)
estadd local lnOldWorldCropsArea "Y"
estadd local lnElevation "Y"
estadd local lnRuggedness "Y"
estadd local lnTropicalArea "Y" 
estimates store t2c6

**# Bookmark #1

local s  "using Table02.rtf"  // 输出到word文档
local m  "t2c1 t2c2 t2c3 t2c4 t2c5 t2c6"        // 模型名称
local mt "FLEXIBLE ESTIMATES: THE RELATIONSHIP BETWEEN POTATO-SUITABLE LAND AREA AND POPULATION OR CITY POPULATION SHARE BY TIME PERIOD" //表格标题
esttab `m' `s', title(`mt') b(%6.3f) nostar nogap compress  ///
	         scalar(N r2 F) replace  ///
			 indicate("Time-FE =*year*" "lnOldWorldCropsArea = *`ln_oworld_flexible'*" "lnElevation = *`ln_elevation_flexible'*" "lnRuggedness = *`ln_rugged_flexible'*" "lnTropicalArea = *`ln_tropical_flexible'*") 

***Figure Ⅳ-a***	

xi: areg ln_population `ln_potato_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store mf1
coefplot mf1, keep(`ln_potato_flexible') ///
              rename(ln_wpot_* = " ") ///
			  title("ln Potato Area × Year indicators") /// 
			  xtitle("Year") ytitle("Coefficients and Confidence Intervals") ///
			  b1title("(a) ln Total Population") ///
			  vertical ci
graph export Figure4a.png			 

			 
***Figure Ⅳ-b***	
	
	xi: areg city_pop_share `ln_potato_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
	est store mf2
	coefplot mf2, keep(`ln_potato_flexible') ///
                  rename(ln_wpot_* = " ") ///
			      title("ln Potato Area × Year indicators") /// 
			      xtitle("Year") ytitle("Coefficients and Confidence Intervals") ///
				  b1title("(b) City Population Share") ///
			      vertical ci
graph export Figure4b.png

**************************************
*** TABLE 3 - ALTERNATIVE CUT-OFFS ***
**************************************

/* Locals for the control variables */
local ln_potato_flexible "ln_wpot_1100 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850 ln_wpot_1900"
local ydum_flexible "ydum1100 ydum1200 ydum1300 ydum1400 ydum1500 ydum1600 ydum1700 ydum1750 ydum1800 ydum1850 ydum1900"
local ln_all_flexible "ln_all_1100 ln_all_1200 ln_all_1300 ln_all_1400 ln_all_1500 ln_all_1600 ln_all_1700 ln_all_1750 ln_all_1800 ln_all_1850 ln_all_1900"
local ln_nworld_flexible "ln_nworld_1100 ln_nworld_1200 ln_nworld_1300 ln_nworld_1400 ln_nworld_1500 ln_nworld_1600 ln_nworld_1700 ln_nworld_1750 ln_nworld_1800 ln_nworld_1850 ln_nworld_1900"
local ln_oworld_flexible "ln_oworld_1100 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850 ln_oworld_1900"
local ln_tropical_flexible "ln_tropical_1100 ln_tropical_1200 ln_tropical_1300 ln_tropical_1400 ln_tropical_1500 ln_tropical_1600 ln_tropical_1700 ln_tropical_1750 ln_tropical_1800 ln_tropical_1850 ln_tropical_1900"
local ln_rugged_flexible "ln_rugged_1100 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850 ln_rugged_1900"
local ln_elevation_flexible "ln_elevation_1100 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850 ln_elevation_1900"
local ln_disteq_flexible "ln_disteq_1100 ln_disteq_1200 ln_disteq_1300 ln_disteq_1400 ln_disteq_1500 ln_disteq_1600 ln_disteq_1700 ln_disteq_1750 ln_disteq_1800 ln_disteq_1850 ln_disteq_1900"
local ln_dist_coast_flexible "ln_dist_coast_1100 ln_dist_coast_1200 ln_dist_coast_1300 ln_dist_coast_1400 ln_dist_coast_1500 ln_dist_coast_1600 ln_dist_coast_1700 ln_dist_coast_1750 ln_dist_coast_1800 ln_dist_coast_1850 ln_dist_coast_1900"
local malaria_flexible "malaria_1100 malaria_1200 malaria_1300 malaria_1400 malaria_1500 malaria_1600 malaria_1700 malaria_1750 malaria_1800 malaria_1850 malaria_1900"
local ln_export_flexible "ln_export_1100 ln_export_1200 ln_export_1300 ln_export_1400 ln_export_1500 ln_export_1600 ln_export_1700 ln_export_1750 ln_export_1800 ln_export_1850 ln_export_1900"

local ln_spot_flexible "ln_spot_1100 ln_spot_1200 ln_spot_1300 ln_spot_1400 ln_spot_1500 ln_spot_1600 ln_spot_1700 ln_spot_1750 ln_spot_1800 ln_spot_1850 ln_spot_1900"
local ln_silagemaize_flexible "ln_silagemaize_1100 ln_silagemaize_1200 ln_silagemaize_1300 ln_silagemaize_1400 ln_silagemaize_1500 ln_silagemaize_1600 ln_silagemaize_1700 ln_silagemaize_1750 ln_silagemaize_1800 ln_silagemaize_1850 ln_silagemaize_1900"
local ln_maize_flexible "ln_maize_1100 ln_maize_1200 ln_maize_1300 ln_maize_1400 ln_maize_1500 ln_maize_1600 ln_maize_1700 ln_maize_1750 ln_maize_1800 ln_maize_1850 ln_maize_1900"
local ln_cassava_flexible "ln_cassava_1100 ln_cassava_1200 ln_cassava_1300 ln_cassava_1400 ln_cassava_1500 ln_cassava_1600 ln_cassava_1700 ln_cassava_1750 ln_cassava_1800 ln_cassava_1850 ln_cassava_1900"

/* 1200--1500: Post = 1400, 1500 (Cols 1 & 2) */
preserve
drop ln_wpot_post post
drop if year<1200
drop if year>1500
tab year
gen post=0
replace post=1 if year==1400 | year==1500
gen ln_wpot_post=ln_wpot*post
* Total Population
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c1
* City Population Share
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c2

**# Bookmark #2
local s  "using Table03.rtf"  // 输出到word文档
local m  "t3c1 t3c2"      // 模型名称
local mt "THE IMPACT OF THE POTATO WITH ALTERNATIVE CUT-OFFS" //表格标题
esttab `m' `s', title(`mt') b(%6.3f) nostar nogap compress  ///
             keep(ln_wpot_post)    ///
	         scalar(N r2) replace  
restore


/* 1300--1600: Post = 1500, 1600 (Cols 3 & 4) */
preserve
drop ln_wpot_post post
drop if year<1300
drop if year>1600
tab year
gen post=0
replace post=1 if year==1500 | year==1600
gen ln_wpot_post=ln_wpot*post
* Total Population
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c3
* City Population Share
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c4

**# Bookmark #3
local s  "using Table03.rtf"  // 输出到word文档
local m  "t3c3 t3c4"      // 模型名称
esttab `m' `s',  b(%6.3f) nostar nogap compress  ///
             keep(ln_wpot_post)    ///
	         scalar(N r2) append  
restore

/* 1400--1700: Post = 1600, 1700 (Cols 5 & 6) */
preserve
drop ln_wpot_post post
drop if year<1400
drop if year>1700
tab year
gen post=0
replace post=1 if year==1600 | year==1700
gen ln_wpot_post=ln_wpot*post
* Total Population
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c5
* City Population Share
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c6
local s  "using Table03.rtf"  // 输出到word文档
local m  "t3c5 t3c6"      // 模型名称
esttab `m' `s',  b(%6.3f) nostar nogap compress  ///
             keep(ln_wpot_post)    ///
	         scalar(N r2) append  
restore

/* 1500--1800: Post = 1700, 1800 (Cols 7 & 8) */
preserve
drop ln_wpot_post post
drop if year<1500
drop if year>1800
tab year
gen post=0
replace post=1 if year==1700 | year==1750 | year==1800
gen ln_wpot_post=ln_wpot*post
* Total Population
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c7
* City Population Share
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c8

local s  "using Table03.rtf"  // 输出到word文档
local m  "t3c7 t3c8"      // 模型名称
esttab `m' `s',  b(%6.3f) nostar nogap compress  ///
             keep(ln_wpot_post)    ///
	         scalar(N r2) append  
restore

/* 1600--1900: Post = 1800, 1850, 1900 (Cols 9 & 10) */
preserve
drop ln_wpot_post post
drop if year<1600
drop if year>1900
tab year
gen post=0
replace post=1 if year==1800 | year==1850 | year==1900
gen ln_wpot_post=ln_wpot*post
* Total Population
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c9
* City Population Share
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c10
local s  "using Table03.rtf"  // 输出到word文档
local m  "t3c9 t3c10"      // 模型名称
esttab `m' `s',  b(%6.3f) nostar nogap compress  ///
             keep(ln_wpot_post)    ///
	         scalar(N r2) append  
restore

/* 1600--1900: post = 1750, 1850, 1900  (Cols 11 & 12) */
preserve
drop ln_wpot_post post
drop if year<1600
drop if year>1900
tab year
gen post=0
replace post=1 if year==1750 | year==1800 | year==1850 | year==1900
gen ln_wpot_post=ln_wpot*post
* Total Population
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c11
* City Population Share
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t3c12
local s  "using Table03.rtf"  // 输出到word文档
local m  "t3c11 t3c12"      // 模型名称
esttab `m' `s',  b(%6.3f) nostar nogap compress  ///
             keep(ln_wpot_post)    ///
	         scalar(N r2) append
restore

***************************************
*** TABLE 4 - BASELINE DD ESTIMATES ***
***************************************	

/* Locals for the control variables */
local ln_potato_flexible "ln_wpot_1100 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850 ln_wpot_1900"
local ydum_flexible "ydum1100 ydum1200 ydum1300 ydum1400 ydum1500 ydum1600 ydum1700 ydum1750 ydum1800 ydum1850 ydum1900"
local ln_all_flexible "ln_all_1100 ln_all_1200 ln_all_1300 ln_all_1400 ln_all_1500 ln_all_1600 ln_all_1700 ln_all_1750 ln_all_1800 ln_all_1850 ln_all_1900"
local ln_nworld_flexible "ln_nworld_1100 ln_nworld_1200 ln_nworld_1300 ln_nworld_1400 ln_nworld_1500 ln_nworld_1600 ln_nworld_1700 ln_nworld_1750 ln_nworld_1800 ln_nworld_1850 ln_nworld_1900"
local ln_oworld_flexible "ln_oworld_1100 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850 ln_oworld_1900"
local ln_tropical_flexible "ln_tropical_1100 ln_tropical_1200 ln_tropical_1300 ln_tropical_1400 ln_tropical_1500 ln_tropical_1600 ln_tropical_1700 ln_tropical_1750 ln_tropical_1800 ln_tropical_1850 ln_tropical_1900"
local ln_rugged_flexible "ln_rugged_1100 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850 ln_rugged_1900"
local ln_elevation_flexible "ln_elevation_1100 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850 ln_elevation_1900"
local ln_disteq_flexible "ln_disteq_1100 ln_disteq_1200 ln_disteq_1300 ln_disteq_1400 ln_disteq_1500 ln_disteq_1600 ln_disteq_1700 ln_disteq_1750 ln_disteq_1800 ln_disteq_1850 ln_disteq_1900"
local ln_dist_coast_flexible "ln_dist_coast_1100 ln_dist_coast_1200 ln_dist_coast_1300 ln_dist_coast_1400 ln_dist_coast_1500 ln_dist_coast_1600 ln_dist_coast_1700 ln_dist_coast_1750 ln_dist_coast_1800 ln_dist_coast_1850 ln_dist_coast_1900"
local malaria_flexible "malaria_1100 malaria_1200 malaria_1300 malaria_1400 malaria_1500 malaria_1600 malaria_1700 malaria_1750 malaria_1800 malaria_1850 malaria_1900"
local ln_export_flexible "ln_export_1100 ln_export_1200 ln_export_1300 ln_export_1400 ln_export_1500 ln_export_1600 ln_export_1700 ln_export_1750 ln_export_1800 ln_export_1850 ln_export_1900"

local ln_spot_flexible "ln_spot_1100 ln_spot_1200 ln_spot_1300 ln_spot_1400 ln_spot_1500 ln_spot_1600 ln_spot_1700 ln_spot_1750 ln_spot_1800 ln_spot_1850 ln_spot_1900"
local ln_silagemaize_flexible "ln_silagemaize_1100 ln_silagemaize_1200 ln_silagemaize_1300 ln_silagemaize_1400 ln_silagemaize_1500 ln_silagemaize_1600 ln_silagemaize_1700 ln_silagemaize_1750 ln_silagemaize_1800 ln_silagemaize_1850 ln_silagemaize_1900"
local ln_maize_flexible "ln_maize_1100 ln_maize_1200 ln_maize_1300 ln_maize_1400 ln_maize_1500 ln_maize_1600 ln_maize_1700 ln_maize_1750 ln_maize_1800 ln_maize_1850 ln_maize_1900"
local ln_cassava_flexible "ln_cassava_1100 ln_cassava_1200 ln_cassava_1300 ln_cassava_1400 ln_cassava_1500 ln_cassava_1600 ln_cassava_1700 ln_cassava_1750 ln_cassava_1800 ln_cassava_1850 ln_cassava_1900"

*** Total Population ***
* Col 1
xi: areg ln_population ln_wpot_post i.year, a(isocode) cluster(isocode)
est store t4c1
* Col 2
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' i.year, a(isocode) cluster(isocode)
est store t4c2
* Col 3
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t4c3
* Col 4
xi: areg ln_population ln_wpot_post `ln_all_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t4c4
* Col 5
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_spot_flexible' `ln_silagemaize_flexible' `ln_maize_flexible' `ln_cassava_flexible' i.year, a(isocode) cluster(isocode)
est store t4c5


*** City Population Share ***


* Col 6
xi: areg city_pop_share ln_wpot_post i.year, a(isocode) cluster(isocode)
est store t4c6
* Col 7
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' i.year, a(isocode) cluster(isocode)
est store t4c7
* Col 8
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t4c8
* Col 9
xi: areg city_pop_share ln_wpot_post `ln_all_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t4c9
* Col 10
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_spot_flexible' `ln_silagemaize_flexible' `ln_maize_flexible' `ln_cassava_flexible' i.year, a(isocode) cluster(isocode)
est store t4c10


local s  "using Table04.rtf"  // 输出到word文档
local m  "t4c1 t4c2 t4c3 t4c4 t4c5 t4c6 t4c7 t4c8 t4c9 t4c10"        // 模型名称
local mt "THE IMPACT OF THE POTATO: BASELINE ESTIMATES" //表格标题
esttab `m' `s', title(`mt') b(%6.3f) nostar nogap compress  ///
	         scalar(N r2) replace  ///
			 indicate("Time-FE =*year*" "lnOldWorldCropsArea = *`ln_oworld_flexible'*" "lnElevation = *`ln_elevation_flexible'*" "lnRuggedness = *`ln_rugged_flexible'*" "lnTropicalArea = *`ln_tropical_flexible'*" "lnAllCropsArea = *`ln_all_flexible'*" "lnMaizeArea = *`ln_maize_flexible'*" "lnSilageMaizeArea = *`ln_silagemaize_flexible'*" "lnSweetPotatoesArea = *`ln_spot_flexible'*" "lnCassavaArea = *`ln_cassava_flexible'*") 

			 
****************************************************
*** TABLE 5 - ROBUSTNESS TO ALTERNATIVE CUT-OFFS ***
****************************************************

/* Creating the alternative suitability measures */
gen non_int_wpot=ln(1+wpot_lr_vs_mars)  //衡量每个国家可以种植任意数量土豆的土地数量，即使土地只能产出最大可获得产量的一小部分。
gen non_ext_int_wpot=0
replace non_ext_int_wpot=1 if non_int_wpot>0
replace non_ext_int_wpot=ln(1+all_lr_vs_mars) if non_int_wpot>0   //如果该国境内有任何数量的土地可用于种植土豆，该指标等于可耕地总量。

preserve


*****************************************
*** Post = 1750+ (Baseline) - (Row 1) ***
*****************************************
gen ver2_ln_wpot_post=ln_wpot_ver2*post   //用于马铃薯的总土地面积的对数（版本2：中等投入强度；截点=边缘20%）
gen ver3_ln_wpot_post=ln_wpot_ver3*post  //用于马铃薯的总土地面积的对数（版本2：低投入强度；截点=中等40%）
for @ in any non_int_wpot non_ext_int_wpot: gen @_post=@*post

local ln_potato_flexible "ln_wpot_1100 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850 ln_wpot_1900"
	local ydum_flexible "ydum1100 ydum1200 ydum1300 ydum1400 ydum1500 ydum1600 ydum1700 ydum1750 ydum1800 ydum1850 ydum1900"
	local ln_all_flexible "ln_all_1100 ln_all_1200 ln_all_1300 ln_all_1400 ln_all_1500 ln_all_1600 ln_all_1700 ln_all_1750 ln_all_1800 ln_all_1850 ln_all_1900"
	local ln_nworld_flexible "ln_nworld_1100 ln_nworld_1200 ln_nworld_1300 ln_nworld_1400 ln_nworld_1500 ln_nworld_1600 ln_nworld_1700 ln_nworld_1750 ln_nworld_1800 ln_nworld_1850 ln_nworld_1900"
	local ln_oworld_flexible "ln_oworld_1100 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850 ln_oworld_1900"
	local ln_tropical_flexible "ln_tropical_1100 ln_tropical_1200 ln_tropical_1300 ln_tropical_1400 ln_tropical_1500 ln_tropical_1600 ln_tropical_1700 ln_tropical_1750 ln_tropical_1800 ln_tropical_1850 ln_tropical_1900"
	local ln_rugged_flexible "ln_rugged_1100 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850 ln_rugged_1900"
	local ln_elevation_flexible "ln_elevation_1100 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850 ln_elevation_1900"
*** Total Population ***
* Col1 
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c1
* Col 2
xi: areg ln_population ver2_ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c2
* Col 3
xi: areg ln_population non_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c3
* Col 4
xi: areg ln_population non_ext_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c4

*** City Population Share ***
* Baseline
* Col 1
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c5
* Col 2
xi: areg city_pop_share ver2_ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c6
* Col 3
xi: areg city_pop_share non_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c7
* Col 4
xi: areg city_pop_share non_ext_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r1c8
**# Bookmark #4

local s  "using Table05.rtf"  // 输出到word文档
local m  "t5r1c1 t5r1c2 t5r1c3 t5r1c4 t5r1c5 t5r1c6 t5r1c7 t5r1c8"        // 模型名称
local vars "ln_wpot_post ver2_ln_wpot_post non_int_wpot_post non_ext_int_wpot_post"
local mt "ROBUSTNESS TO THE USE OF ALTERNATIVE DEFINITIONS FOR POTATO SUITABILITY AND FOR THE POSTADOPTION TIME PERIOD" //表格标题
esttab `m' `s', title(`mt') b(%6.3f) nostar nogap compress  ///
	         keep(`vars')  ///
			 replace  
			   

restore
			 

******************************
*** Post = 1800+ - (Row 2) ***
******************************
preserve
drop post ln_wpot_post
gen post=0 if missing(year)!=1
replace post=1 if year>1750 & missing(year)!=1
gen ln_wpot_post=ln_wpot*post
gen ver2_ln_wpot_post=ln_wpot_ver2*post
gen ver3_ln_wpot_post=ln_wpot_ver3*post
for @ in any non_int_wpot non_ext_int_wpot: gen @_post=@*post

*** Total Population ***
* Col 1
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c1
* Col 2
xi: areg ln_population ver2_ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c2
* Col 3
xi: areg ln_population non_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c3
* Col 4
xi: areg ln_population non_ext_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c4
*** City Population Share ***
* Col 1
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c5
* Col 2
xi: areg city_pop_share ver2_ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c6
* Col 3
xi: areg city_pop_share non_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c7
* Col 4
xi: areg city_pop_share non_ext_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r2c8

local s  "using Table05.rtf"  // 输出到word文档
local m  "t5r2c1 t5r2c2 t5r2c3 t5r2c4 t5r2c5 t5r2c6 t5r2c7 t5r2c8"        // 模型名称
local vars "ln_wpot_post ver2_ln_wpot_post non_int_wpot_post non_ext_int_wpot_post"
esttab `m' `s', b(%6.3f) nostar nogap compress  ///
	         keep(`vars')  ///
			 append 

restore

******************************
*** Post = 1700+ - (Row 3) ***
******************************
preserve
drop post ln_wpot_post
gen post=0 if missing(year)!=1
replace post=1 if year>1600 & missing(year)!=1
gen ln_wpot_post=ln_wpot*post
gen ver2_ln_wpot_post=ln_wpot_ver2*post
gen ver3_ln_wpot_post=ln_wpot_ver3*post
rename ver2_ln_wpot_post ver2_ln_wpot_post
rename ver3_ln_wpot_post ver3_ln_wpot_post
for @ in any non_int_wpot non_ext_int_wpot: gen @_post=@*post

*** Total Population ***
* Col 1
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c1
* Col 2
xi: areg ln_population ver2_ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c2
* Col 3
xi: areg ln_population non_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c3
* Col 4
xi: areg ln_population non_ext_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c4
*** City Population Share ***
* Col 1
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c5
* Col 2
xi: areg city_pop_share ver2_ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c6
* Col 3
xi: areg city_pop_share non_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c7
* Col 4
xi: areg city_pop_share non_ext_int_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
est store t5r3c8

local s  "using Table05.rtf"  // 输出到word文档
local m  "t5r3c1 t5r3c2 t5r3c3 t5r3c4 t5r3c5 t5r3c6 t5r3c7 t5r3c8"        // 模型名称
local vars "ln_wpot_post ver2_ln_wpot_post non_int_wpot_post non_ext_int_wpot_post"
esttab `m' `s', b(%6.3f) nostar nogap compress  ///
	         keep(`vars')  ///
			 append 
restore



***********************************************
*** Table 6 - CONTROLLING FOR OTHER FACTORS ***
***********************************************
	local ln_potato_flexible "ln_wpot_1100 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850 ln_wpot_1900"
	local ydum_flexible "ydum1100 ydum1200 ydum1300 ydum1400 ydum1500 ydum1600 ydum1700 ydum1750 ydum1800 ydum1850 ydum1900"
	local ln_all_flexible "ln_all_1100 ln_all_1200 ln_all_1300 ln_all_1400 ln_all_1500 ln_all_1600 ln_all_1700 ln_all_1750 ln_all_1800 ln_all_1850 ln_all_1900"
	local ln_nworld_flexible "ln_nworld_1100 ln_nworld_1200 ln_nworld_1300 ln_nworld_1400 ln_nworld_1500 ln_nworld_1600 ln_nworld_1700 ln_nworld_1750 ln_nworld_1800 ln_nworld_1850 ln_nworld_1900"
	local ln_oworld_flexible "ln_oworld_1100 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850 ln_oworld_1900"
	local ln_tropical_flexible "ln_tropical_1100 ln_tropical_1200 ln_tropical_1300 ln_tropical_1400 ln_tropical_1500 ln_tropical_1600 ln_tropical_1700 ln_tropical_1750 ln_tropical_1800 ln_tropical_1850 ln_tropical_1900"
	local ln_rugged_flexible "ln_rugged_1100 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850 ln_rugged_1900"
	local ln_elevation_flexible "ln_elevation_1100 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850 ln_elevation_1900"
	local ln_disteq_flexible "ln_disteq_1100 ln_disteq_1200 ln_disteq_1300 ln_disteq_1400 ln_disteq_1500 ln_disteq_1600 ln_disteq_1700 ln_disteq_1750 ln_disteq_1800 ln_disteq_1850 ln_disteq_1900"
	local ln_dist_coast_flexible "ln_dist_coast_1100 ln_dist_coast_1200 ln_dist_coast_1300 ln_dist_coast_1400 ln_dist_coast_1500 ln_dist_coast_1600 ln_dist_coast_1700 ln_dist_coast_1750 ln_dist_coast_1800 ln_dist_coast_1850 ln_dist_coast_1900"
	local malaria_flexible "malaria_1100 malaria_1200 malaria_1300 malaria_1400 malaria_1500 malaria_1600 malaria_1700 malaria_1750 malaria_1800 malaria_1850 malaria_1900"
	local ln_export_flexible "ln_export_1100 ln_export_1200 ln_export_1300 ln_export_1400 ln_export_1500 ln_export_1600 ln_export_1700 ln_export_1750 ln_export_1800 ln_export_1850 ln_export_1900"
	local ln_spot_flexible "ln_spot_1100 ln_spot_1200 ln_spot_1300 ln_spot_1400 ln_spot_1500 ln_spot_1600 ln_spot_1700 ln_spot_1750 ln_spot_1800 ln_spot_1850 ln_spot_1900"
	local ln_silagemaize_flexible "ln_silagemaize_1100 ln_silagemaize_1200 ln_silagemaize_1300 ln_silagemaize_1400 ln_silagemaize_1500 ln_silagemaize_1600 ln_silagemaize_1700 ln_silagemaize_1750 ln_silagemaize_1800 ln_silagemaize_1850 ln_silagemaize_1900"
	local ln_maize_flexible "ln_maize_1100 ln_maize_1200 ln_maize_1300 ln_maize_1400 ln_maize_1500 ln_maize_1600 ln_maize_1700 ln_maize_1750 ln_maize_1800 ln_maize_1850 ln_maize_1900"
	local ln_cassava_flexible "ln_cassava_1100 ln_cassava_1200 ln_cassava_1300 ln_cassava_1400 ln_cassava_1500 ln_cassava_1600 ln_cassava_1700 ln_cassava_1750 ln_cassava_1800 ln_cassava_1850 ln_cassava_1900"

*** Total Population (Panel A) ***

* Baseline (Col 1)
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac1

* Latitude (Col 2)
xi: areg ln_population ln_wpot_post `ln_disteq_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac2

* Malaria (Col 3)
xi: areg ln_population ln_wpot_post `malaria_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac3

* Legal Origin (Col 4)

xi: areg ln_population ln_wpot_post i.year*legor_gbr i.year*legor_fra i.year*legor_deu i.year*legor_soc `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "Yes"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac4

* Identity of colonizer (Col 5)

xi: areg ln_population ln_wpot_post i.year*colony_esp i.year*colony_gbr i.year*colony_fra i.year*colony_prt colony_oeu `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "Yes"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac5

* Atlantic Trader (Col 6)
xi: areg ln_population ln_wpot_post i.year*attrade `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "Yes"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac6

* Roman (Col 7)
xi: areg ln_population ln_wpot_post i.year*roman `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "Yes" 
estadd local ProtestantIndicator "No"
est store t6Ac7

* Protestant (Col 8)
xi: areg ln_population ln_wpot_post i.year*protestant `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "Yes"
est store t6Ac8

* Distance from coast (Col 9)
xi: areg ln_population ln_wpot_post `ln_dist_coast_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac9

* Slave exports (Col 10)
xi: areg ln_population ln_wpot_post `ln_export_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Ac10

* All controls (Col 11)
xi: areg ln_population ln_wpot_post `ln_disteq_flexible' `malaria_flexible' i.year*legor_gbr i.year*legor_fra i.year*legor_deu i.year*legor_soc i.year*colony_esp i.year*colony_gbr i.year*colony_fra i.year*colony_prt colony_oeu i.year*attrade i.year*roman i.year*protestant `ln_dist_coast_flexible' `ln_export_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "Yes"
estadd local ColonialOriginIndicators "Yes"
estadd local AtlanticTraderIndicator "Yes"
estadd local RomanHeritageIndicator "Yes" 
estadd local ProtestantIndicator "Yes"
est store t6Ac11
**# Bookmark #5

local s  "using Table06.rtf"  // 输出到word文档
local m  "t6Ac1 t6Ac2 t6Ac3 t6Ac4 t6Ac5 t6Ac6 t6Ac7 t6Ac8 t6Ac9 t6Ac10 t6Ac11"        // 模型名称
local mt "ROBUSTNESS TO CONTROLLING FOR ADDITIONAL TIME- AND COUNTRY-VARYING FACTORS" //表格标题
esttab `m' , title(`mt') b(%6.3f) nostar nogap compress  ///
	         scalar(LegalOriginIndicators ColonialOriginIndicators AtlanticTraderIndicator RomanHeritageIndicator ProtestantIndicator N r2) replace  ///
			 keep(ln_wpot_post) ///
			 indicate("Time-FE =*year*" "Baselinecontrols = *`ln_oworld_flexible' `ln_elevation_flexible' `ln_rugged_flexible' `ln_rugged_flexible' `ln_tropical_flexible'*" "lnDistancefromEquator  = *`ln_disteq_flexible'*" "Malaria Index = *`malaria_flexible'*" "lnDistancefromCoast = *`ln_dist_coast_flexible'*" "lnSlaveExports = *`ln_export_flexible'*") 

*** City Population Share (Panel B) ***
* Baseline (Col 1)
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc1

* Latitude (Col 2)
xi: areg city_pop_share ln_wpot_post `ln_disteq_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc2
* Malaria (Col 3)
xi: areg city_pop_share ln_wpot_post `malaria_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc3
* Legal Origin (Col 4)
xi: areg city_pop_share ln_wpot_post i.year*legor_gbr i.year*legor_fra i.year*legor_deu i.year*legor_soc `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "Yes"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc4
* Identity of colonizer (Col 5)
xi: areg city_pop_share ln_wpot_post i.year*colony_esp i.year*colony_gbr i.year*colony_fra i.year*colony_prt i.year*colony_oeu `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "Yes"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc5
* Atlantic Trader (Col 6)
xi: areg city_pop_share ln_wpot_post i.year*attrade `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "Yes"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc6
* Roman (Col 7)
xi: areg city_pop_share ln_wpot_post i.year*roman `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "Yes" 
estadd local ProtestantIndicator "No"
est store t6Bc7
* Protestant (Col 8)
xi: areg city_pop_share ln_wpot_post i.year*protestant `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "Yes"
est store t6Bc8
* Distance from coast (Col 9)
xi: areg city_pop_share ln_wpot_post `ln_dist_coast_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc9
* Slave exports (Col 10)
xi: areg city_pop_share ln_wpot_post `ln_export_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "No"
estadd local ColonialOriginIndicators "No"
estadd local AtlanticTraderIndicator "No"
estadd local RomanHeritageIndicator "No" 
estadd local ProtestantIndicator "No"
est store t6Bc10
* All controls (Col 11)
xi: areg city_pop_share ln_wpot_post `ln_disteq_flexible' `malaria_flexible' i.year*legor_gbr i.year*legor_fra i.year*legor_deu i.year*legor_soc i.year*colony_esp i.year*colony_gbr i.year*colony_fra i.year*colony_prt i.year*colony_oeu i.year*attrade i.year*roman i.year*protestant `ln_dist_coast_flexible' `ln_export_flexible' `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local LegalOriginIndicators "Yes"
estadd local ColonialOriginIndicators "Yes"
estadd local AtlanticTraderIndicator "Yes"
estadd local RomanHeritageIndicator "Yes" 
estadd local ProtestantIndicator "Yes"
est store t6Bc11

local m  "t6Bc1 t6Bc2 t6Bc3 t6Bc4 t6Bc5 t6Bc6 t6Bc7 t6Bc8 t6Bc9 t6Bc10 t6Bc11"        // 模型名称
esttab `m' , b(%6.3f) nostar nogap compress  ///
	         scalar(LegalOriginIndicators ColonialOriginIndicators AtlanticTraderIndicator RomanHeritageIndicator ProtestantIndicator N r2) append  ///
			 keep(ln_wpot_post) ///
			 indicate("Time-FE =*year*" "Baselinecontrols = *`ln_oworld_flexible' `ln_elevation_flexible' `ln_rugged_flexible' `ln_rugged_flexible' `ln_tropical_flexible'*" "lnDistancefromEquator  = *`ln_disteq_flexible'*" "Malaria Index = *`malaria_flexible'*" "lnDistancefromCoast = *`ln_dist_coast_flexible'*" "lnSlaveExports = *`ln_export_flexible'*") 

*******************************
*** Table 7 - Continent FEs ***
*******************************
/* Locals for the control variables */
local ln_potato_flexible "ln_wpot_1100 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850 ln_wpot_1900"
local ydum_flexible "ydum1100 ydum1200 ydum1300 ydum1400 ydum1500 ydum1600 ydum1700 ydum1750 ydum1800 ydum1850 ydum1900"
local ln_all_flexible "ln_all_1100 ln_all_1200 ln_all_1300 ln_all_1400 ln_all_1500 ln_all_1600 ln_all_1700 ln_all_1750 ln_all_1800 ln_all_1850 ln_all_1900"
local ln_nworld_flexible "ln_nworld_1100 ln_nworld_1200 ln_nworld_1300 ln_nworld_1400 ln_nworld_1500 ln_nworld_1600 ln_nworld_1700 ln_nworld_1750 ln_nworld_1800 ln_nworld_1850 ln_nworld_1900"
local ln_oworld_flexible "ln_oworld_1100 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850 ln_oworld_1900"
local ln_tropical_flexible "ln_tropical_1100 ln_tropical_1200 ln_tropical_1300 ln_tropical_1400 ln_tropical_1500 ln_tropical_1600 ln_tropical_1700 ln_tropical_1750 ln_tropical_1800 ln_tropical_1850 ln_tropical_1900"
local ln_rugged_flexible "ln_rugged_1100 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850 ln_rugged_1900"
local ln_elevation_flexible "ln_elevation_1100 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850 ln_elevation_1900"
local ln_disteq_flexible "ln_disteq_1100 ln_disteq_1200 ln_disteq_1300 ln_disteq_1400 ln_disteq_1500 ln_disteq_1600 ln_disteq_1700 ln_disteq_1750 ln_disteq_1800 ln_disteq_1850 ln_disteq_1900"
local ln_dist_coast_flexible "ln_dist_coast_1100 ln_dist_coast_1200 ln_dist_coast_1300 ln_dist_coast_1400 ln_dist_coast_1500 ln_dist_coast_1600 ln_dist_coast_1700 ln_dist_coast_1750 ln_dist_coast_1800 ln_dist_coast_1850 ln_dist_coast_1900"
local malaria_flexible "malaria_1100 malaria_1200 malaria_1300 malaria_1400 malaria_1500 malaria_1600 malaria_1700 malaria_1750 malaria_1800 malaria_1850 malaria_1900"
local ln_export_flexible "ln_export_1100 ln_export_1200 ln_export_1300 ln_export_1400 ln_export_1500 ln_export_1600 ln_export_1700 ln_export_1750 ln_export_1800 ln_export_1850 ln_export_1900"

local ln_spot_flexible "ln_spot_1100 ln_spot_1200 ln_spot_1300 ln_spot_1400 ln_spot_1500 ln_spot_1600 ln_spot_1700 ln_spot_1750 ln_spot_1800 ln_spot_1850 ln_spot_1900"
local ln_silagemaize_flexible "ln_silagemaize_1100 ln_silagemaize_1200 ln_silagemaize_1300 ln_silagemaize_1400 ln_silagemaize_1500 ln_silagemaize_1600 ln_silagemaize_1700 ln_silagemaize_1750 ln_silagemaize_1800 ln_silagemaize_1850 ln_silagemaize_1900"
local ln_maize_flexible "ln_maize_1100 ln_maize_1200 ln_maize_1300 ln_maize_1400 ln_maize_1500 ln_maize_1600 ln_maize_1700 ln_maize_1750 ln_maize_1800 ln_maize_1850 ln_maize_1900"
local ln_cassava_flexible "ln_cassava_1100 ln_cassava_1200 ln_cassava_1300 ln_cassava_1400 ln_cassava_1500 ln_cassava_1600 ln_cassava_1700 ln_cassava_1750 ln_cassava_1800 ln_cassava_1850 ln_cassava_1900"
*** Total Population ***
* Baseline without Cont FEs (Col 1) 
xi: areg ln_population ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local ContinentFixedEffects "No"
est store t7c1
* No controls with Cont FEs (Col 2)
xi: areg ln_population ln_wpot_post i.year*cont_africa i.year*cont_asia i.year*cont_europe i.year*cont_oceania i.year, a(isocode) cluster(isocode)
estadd local ContinentFixedEffects "Yes"
est store t7c2
* Baseline with Cont FEs (Col 3)
xi: areg ln_population ln_wpot_post i.year*cont_africa i.year*cont_asia i.year*cont_europe i.year*cont_oceania `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local ContinentFixedEffects "Yes"
est store t7c3
*** City Population Share ***
* Baseline without Cont FEs (Col 4) 
xi: areg city_pop_share ln_wpot_post `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local ContinentFixedEffects "No"
est store t7c4
* No controls with Cont FEs (Col 5)
xi: areg city_pop_share ln_wpot_post i.year*cont_africa i.year*cont_asia i.year*cont_europe i.year*cont_oceania i.year, a(isocode) cluster(isocode)
estadd local ContinentFixedEffects "Yes"
est store t7c5
* Baseline with Cont FEs (Col 6)
xi: areg city_pop_share ln_wpot_post i.year*cont_africa i.year*cont_asia i.year*cont_europe i.year*cont_oceania `ln_oworld_flexible' `ln_elevation_flexible' `ln_tropical_flexible' `ln_rugged_flexible' i.year, a(isocode) cluster(isocode)
estadd local ContinentFixedEffects "Yes"
est store t7c6

local s  "using Table07.rtf"  // 输出到word文档
local m  "t7c1 t7c2 t7c3 t7c4 t7c5 t7c6"        // 模型名称
local mt "ROBUSTNESS TO USING WITHIN-CONTINENT VARIATION ONLY" //表格标题
esttab `m' `s', title(`mt') b(%6.3f) nostar nogap compress  ///
	         scalar(ContinentFixedEffects N r2) replace  ///
			 keep(ln_wpot_post) ///
			 indicate("Baselinecontrols = *`ln_oworld_flexible' `ln_elevation_flexible' `ln_rugged_flexible' `ln_tropical_flexible'*") 
			 
**************************************************************
**************************************************************
****************  VI.C City Populations  *********************
**************************************************************	 
**************************************************************
**************************************************************
clear all
capture clear
clear matrix
capture log close

set mem 100m
set matsize 5000

set more off

set scheme s2gmanual
*set scheme s1mono

use "city_level_panel_for_web.dta", clear
doedit "$Orig\Replication_city_level_results.do"

*===================================================
*-数据预处理: 
*   生成新变量，设置暂元
*===================================================

gen cont_europe=0
replace cont_europe=1 if continent=="Europe"
gen cont_africa=0
replace cont_africa=1 if continent=="Africa"
gen cont_asia=0
replace cont_asia=1 if continent=="Asia"

foreach x in 1000 1100 1200 1300 1400 1500 1600 1700 1750 1800 1850 1900{
	gen ln_wpot_`x'=ln_wpot*ydum`x'
	gen ln_oworld_`x'=ln_oworld*ydum`x'
	gen ln_tropical_`x'=ln_tropics*ydum`x'
	gen ln_rugged_`x'=ln_rugged*ydum`x'
	gen ln_elevation_`x'=ln_elevation*ydum`x'
	gen cont_europe_`x'=cont_europe*ydum`x'
	gen cont_africa_`x'=cont_africa*ydum`x'
	gen cont_asia_`x'=cont_asia*ydum`x'
	}

local ln_potato_flexible "ln_wpot_1100 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850 ln_wpot_1900"
local ln_oworld_flexible "ln_oworld_1100 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850 ln_oworld_1900"
local ln_tropical_flexible "ln_tropical_1100 ln_tropical_1200 ln_tropical_1300 ln_tropical_1400 ln_tropical_1500 ln_tropical_1600 ln_tropical_1700 ln_tropical_1750 ln_tropical_1800 ln_tropical_1850 ln_tropical_1900"
local ln_rugged_flexible "ln_rugged_1100 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850 ln_rugged_1900"
local ln_elevation_flexible "ln_elevation_1100 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850 ln_elevation_1900"
local cont_europe_flexible "cont_europe_1100 cont_europe_1200 cont_europe_1300 cont_europe_1400 cont_europe_1500 cont_europe_1600 cont_europe_1700 cont_europe_1750 cont_europe_1800 cont_europe_1850 cont_europe_1900"
local cont_africa_flexible "cont_africa_1100 cont_africa_1200 cont_africa_1300 cont_africa_1400 cont_africa_1500 cont_africa_1600 cont_africa_1700 cont_africa_1750 cont_africa_1800 cont_africa_1850 cont_africa_1900"
local cont_asia_flexible "cont_asia_1100 cont_asia_1200 cont_asia_1300 cont_asia_1400 cont_asia_1500 cont_asia_1600 cont_asia_1700 cont_asia_1750 cont_asia_1800 cont_asia_1850 cont_asia_1900"


* Conley SEs *
gen cutoff1=10	/* Closer than cutoff = 1, further = 0 */
gen cutoff2=10
gen constant=1

drop if missing(latitude)==1
drop if missing(longitude)==1
for @ in any ln_city_population ln_wpot_post year isocode continent: drop if missing(@)==1
for @ in any ln_rugged ln_elevation ln_tropics ln_oworld: drop if missing(@)==1

**************************************
*** TABLE 8 - City level estimates ***
**************************************

/* Column 1 - Baseline controls only */
* Conley SEs; 
num parms = 1 const + 694-1 city FE + 11 year FE + 1 wpot_post + 11*44 controls = 750 
xi: x_ols latitude longitude cutoff1 cutoff2 ln_city_population constant ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year i.city, xreg(750) coord(2)
* Clustered SEs
xi: areg ln_city_population ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year, absorb(city) cluster(isocode)
est store t8c1

/* Column 2 - Basline controls & continent x year FEs */
* Conley SEs; 
num parms = 1 const + 694-1 city FE + 11 year FE + 1 wpot_post + 11*4 controls + 11*(4-1) continent-year FE = 783 
* Had to do 2-step method
preserve
xi: reg ln_city_population `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year*i.continent i.city
predict ln_city_population_resid, resid
xi: reg cutoff1 cutoff2 ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year*i.continent i.city
predict ln_wpot_post_resid, resid
x_ols latitude longitude cutoff1 cutoff2 ln_city_population_resid ln_wpot_post_resid, xreg(2) coord(2)
restore 
* Clustered SEs
xi: areg ln_city_population ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year i.year*i.continent, absorb(city) cluster(isocode)
est store t8c2

******************************************************
*** Baseline controls regressions - Omiting Europe ***
******************************************************

for @ in any ln_rugged ln_elevation ln_tropics ln_oworld: drop if missing(@)==1

save "working.dta", replace

use "working.dta", clear
drop if continent=="Europe"

/* Column 3 - Baseline controls */
*Conley SEs; num parms = 1 const + 348-1 city FE + 11 year FE + 1 wpot_post + 11*4 controls = 404 
*preserve
*xi: reg ln_city_population `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.city i.year
*predict ln_city_population_resid, resid
*xi: reg ln_wpot_post       `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.city i.year
*predict ln_wpot_post_resid, resid
*x_ols latitude longitude cutoff1 cutoff2 ln_city_population_resid ln_wpot_post_resid, xreg(2) coord(2)
*restore
* Clustered SEs
xi: areg ln_city_population ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year, absorb(city) cluster(isocode)

/* Column 4 - Basline controls & continent x year FEs */
* Conley SEs; num parms = 1 const + 348-1 city FE + 11 year FE + 1 wpot_post + 11*4 controls + 11*2 continent-year FE = 426 --- Had to do 2-step method
*preserve
*xi: reg ln_city_population `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.city i.year `cont_asia_flexible' `cont_africa_flexible'
*predict ln_city_population_resid, resid
*xi: reg ln_wpot_post       `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.city i.year `cont_asia_flexible' `cont_africa_flexible'
*predict ln_wpot_post_resid, resid
*x_ols latitude longitude cutoff1 cutoff2 ln_city_population_resid ln_wpot_post_resid, xreg(2) coord(2)
*restore
* Clustered SEs
xi: areg ln_city_population ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' `cont_asia_flexible' `cont_africa_flexible', absorb(city) cluster(isocode)

***************************
*** Keeping only Europe ***
***************************

use "working.dta", clear
keep if continent=="Europe"

tab continent

/* Column 5 - Baseline controls */
* Conley SEs; num parms = 1 const + 350-1 city FE + 11 year FE + 1 wpot_post + 11*44 controls = 406
*xi: x_ols latitude longitude cutoff1 cutoff2 ln_city_population constant ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year i.city, xreg(404) coord(2)
* Clustered SEs
xi: areg ln_city_population ln_wpot_post `ln_oworld_flexible' `ln_tropical_flexible' `ln_rugged_flexible' `ln_elevation_flexible' i.year, absorb(city) cluster(isocode)

erase "working.dta"

***************************
***  Europe only City ***
***************************


use "Europe_only_city_level_panel_for_web.dta", clear

local ln_potato_flexible "ln_wpot_1000 ln_wpot_1200 ln_wpot_1300 ln_wpot_1400 ln_wpot_1500 ln_wpot_1600 ln_wpot_1700 ln_wpot_1750 ln_wpot_1800 ln_wpot_1850"
local ydum_flexible "ydum1000 ydum1200 ydum1300 ydum1400 ydum1500 ydum1600 ydum1700 ydum1750 ydum1800 ydum1850"
local ln_oworld_flexible "ln_oworld_1000 ln_oworld_1200 ln_oworld_1300 ln_oworld_1400 ln_oworld_1500 ln_oworld_1600 ln_oworld_1700 ln_oworld_1750 ln_oworld_1800 ln_oworld_1850"
local ln_tropics_flexible "ln_tropics_1000 ln_tropics_1200 ln_tropics_1300 ln_tropics_1400 ln_tropics_1500 ln_tropics_1600 ln_tropics_1700 ln_tropics_1750 ln_tropics_1800 ln_tropics_1850"
local ln_rugged_flexible "ln_rugged_1000 ln_rugged_1200 ln_rugged_1300 ln_rugged_1400 ln_rugged_1500 ln_rugged_1600 ln_rugged_1700 ln_rugged_1750 ln_rugged_1800 ln_rugged_1850"
local ln_elevation_flexible "ln_elevation_1000 ln_elevation_1200 ln_elevation_1300 ln_elevation_1400 ln_elevation_1500 ln_elevation_1600 ln_elevation_1700 ln_elevation_1750 ln_elevation_1800 ln_elevation_1850"

**************
* Conley SEs *
**************
gen cutoff1=5	/* Closer than cutoff = 1, further = 0 */
gen cutoff2=5
gen constant=1

********************************
*** Columns 6 & 7 of Table 8 ***
********************************

/* Baseline controls */
* Conley SEs
preserve
xi: areg ln_city_population i.year*ln_rugged i.year*ln_elevation i.year*ln_tropics i.year*ln_oworld, absorb(city)
predict ln_city_population_resid if e(sample)==1, resid
xi: areg ln_wpot_post i.year*ln_rugged i.year*ln_elevation i.year*ln_tropics i.year*ln_oworld, absorb(city)
predict ln_wpot_post_resid if e(sample)==1, resid
x_ols latitude longitude cutoff1 cutoff2 ln_city_population_resid constant ln_wpot_post_resid, xreg(2) coord(2)
restore
* Clustered SEs
xi: areg ln_city_population ln_wpot_post i.year*ln_rugged i.year*ln_elevation i.year*ln_tropics i.year*ln_oworld, absorb(city) cluster(isocode)


/* Baseline controls - country x year FEs */
* Conley SEs
preserve
xi: areg ln_city_population i.year*ln_rugged i.year*ln_elevation i.year*ln_tropics i.year*ln_oworld i.year*i.isocode, absorb(city)
predict ln_city_population_resid if e(sample)==1, resid
xi: areg ln_wpot_post i.year*ln_rugged i.year*ln_elevation i.year*ln_tropics i.year*ln_oworld  i.year*i.isocode, absorb(city)
predict ln_wpot_post_resid if e(sample)==1, resid
x_ols latitude longitude cutoff1 cutoff2 ln_city_population_resid constant ln_wpot_post_resid, xreg(2) coord(2)
restore
* Clustered SEs
xi: areg ln_city_population ln_wpot_post i.year*ln_rugged i.year*ln_elevation i.year*ln_tropics i.year*ln_oworld i.year*i.isocode, absorb(city) cluster(isocode)

***************************
***  France Heights ***
***************************


use "France_heights_for_web.dta", clear

gen AGE2=AGE*AGE

* For Conley SEs *
gen cutoff1=5	/* Closer than cutoff = 1, further = 0 */
gen cutoff2=5
gen constant=1

*************************
*** Baseline controls ***
*************************
* Conley SEs
preserve
xi: reg HEIGHT AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* i.BIRTHYR i.TOWNBIRT
predict HEIGHT_resid, resid
xi: reg wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* i.BIRTHYR i.TOWNBIRT
predict wpot_post1700_resid, resid
xi: x_ols latitude longitude cutoff1 cutoff2 HEIGHT_resid constant wpot_post1700_resid, xreg(2) coord(2)
restore
* Clustered SEs
xi: areg HEIGHT wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* i.BIRTHYR, absorb(TOWNBIRT) cluster(PROVINC)

/* Adding Region FEs */
* Conley SEs
preserve
xi: reg HEIGHT AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* i.BIRTHDEC*i.REGION i.BIRTHYR i.TOWNBIRT
predict HEIGHT_resid, resid
xi: reg wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* i.BIRTHDEC*i.REGION i.BIRTHYR i.TOWNBIRT
predict wpot_post1700_resid, resid
xi: x_ols latitude longitude cutoff1 cutoff2 HEIGHT_resid constant wpot_post1700_resid, xreg(2) coord(2)
restore
* Clustered SEs
xi: areg HEIGHT wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* i.BIRTHDEC*i.REGION i.BIRTHYR, absorb(TOWNBIRT) cluster(PROVINC)


**********************************
*** Regressions - All controls ***
**********************************
* Conley SEs
preserve
xi: reg HEIGHT AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* ln_latitude_dec* ln_dist_coast_dec* malaria_dec* i.BIRTHYR i.TOWNBIRT
predict HEIGHT_resid, resid
xi: reg wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* ln_latitude_dec* ln_dist_coast_dec* malaria_dec* i.BIRTHYR i.TOWNBIRT
predict wpot_post1700_resid, resid
xi: x_ols latitude longitude cutoff1 cutoff2 HEIGHT_resid constant wpot_post1700_resid, xreg(2) coord(2)
restore
* Clustered SEs
xi: areg HEIGHT wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* ln_latitude_dec* ln_dist_coast_dec* malaria_dec* i.BIRTHYR, absorb(TOWNBIRT) cluster(PROVINC)

/* Adding Region FEs */
* Conley SEs
preserve
xi: reg HEIGHT AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* ln_latitude_dec* ln_dist_coast_dec* malaria_dec* i.BIRTHDEC*i.REGION i.BIRTHYR i.TOWNBIRT
predict HEIGHT_resid, resid
xi: reg wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* ln_latitude_dec* ln_dist_coast_dec* malaria_dec* i.BIRTHDEC*i.REGION i.BIRTHYR i.TOWNBIRT
predict wpot_post1700_resid, resid
xi: x_ols latitude longitude cutoff1 cutoff2 HEIGHT_resid constant wpot_post1700_resid, xreg(2) coord(2)
restore
* Clustered SEs
xi: areg HEIGHT wpot_post1700 AGE AGE2 ow_dec* rugged_dec* elevation_dec* tropics_dec* ln_latitude_dec* ln_dist_coast_dec* malaria_dec* i.BIRTHDEC*i.REGION i.BIRTHYR, absorb(TOWNBIRT) cluster(PROVINC)

