# 聚类调整标准误：Stata代码汇总  
&emsp;  
> **作者** ：谢泽荟（中山大学）  
> **邮箱** ：<xiezh39@mail2.sysu.edu.cn>  

[TOC]  

## 1.引言
标准误在统计推断中发挥着至关重要的作用，直接影响着系数的显著性和置信区间，并最终影响到假设检验的结论。因此，正确地估计标准误在实证分析的过程中显得尤为重要。当干扰项满足 **独立同分布 (iid)** 条件时， OLS 所估计的标准误是无偏的。但是当误差项之间存在相关性时，OLS 所估计的标准误是有偏的，不能很好地反映估计系数的真实变异性 (Petersen, 2009)，故需要对标准误进行调整。在多种调整标准误的方式中，**聚类调整标准误(cluster)** 是一种有效的方法 (Petersen, 2009)。  

本文主要对要不要聚类和聚类到什么层面这两个问题进行简要讨论，并对Stata中聚类调整标准误的实操代码进行汇总。

## 2.要不要聚类？聚类到什么层面？
聚类调整标准误的基本思想是放宽了随机误差项 **独立同分布** 的假定，允许组内个体的干扰项之间存在相关性，但不同组个体的干扰项之间彼此不相关。  

尽管聚类后的标准误往往与聚类前存在较大差异，从而影响统计推断，但**并非所有情况都需要聚类调整标准误**。如果从大量人口中随机抽取单位样本，并且是否接受处理（treatment）在单位水平上是随机分配的，则不需要对最小二乘估计量的标准误进行聚类。即使结果变量存在组内相关性，聚类也不合适。在这种情况下，聚类可能会导致标准误过于保守（增大），置信区间过宽。例如，如果从人群中随机抽取工人，然后将其随机分配到职业培训计划中，则将标准误差聚类到行业、县或州级别可能会导致标准误差不必要地变大，对统计推断造成危害。  

当处理（treatment）的分配机制或抽样过程存在聚类性，则需要在该层面对标准误进行聚类。例如，某一政策的实施与否在于地级市层面的决策，且抽样过程也是以地级市为单位（尽管研究的个体单位可能是地级市中的企业），那么标准误就应该聚类到地级市层面。

针对面板数据的固定效应模型，聚类层面与固定效应模型并没有对应关系。一般而言聚类层级越高，对于相关性矩阵所施加的参数假设越少，结果越稳健。因此，如果本身控制了个体固定效应或者双向固定效应，为了结果更稳健，应该聚类到更高层面，例如村庄或县级。

## 3. Stata 代码汇总
### 3.1 一维/二维聚类标准误：基本操作
> 对标准误进行一维聚类调整：  
```stata
*-截面数据，在公司层面进行聚类，以下两种写法等价
  reg y x, cluster(id) 
  reg y x, vce(cluster id)  
  
*-面板数据，在公司层面进行聚类，以下三种写法等价
  xtset id year
  xtreg y x, fe cluster(id)  
  xtreg y x, fe vce(cluster id)
  xtreg y x, fe robust  // If you specify -xtreg, fe robust-, Stata will automatically, and without even telling you, use vce(cluster panel_variable) instead. 
```
> 以 `nlswork.dta` 为例，对 Stata 相关命令和结果予以说明。  
```stata
*-调入数据
  *copy http://www.stata-press.com/data/r9/nlswork.dta nlswork.dta, replace
  use nlswork.dta, clear
  
*-定义全局暂元
  global x "age grade"

*-回归结果
  reg ln_wage $x          //干扰项同方差
  est store m1
  reg ln_wage $x, robust  //干扰项异方差
  est store m2
  reg ln_wage $x, vce(cluster idcode)
  est store m3
```
>对标准误进行二维聚类调整,有以下几种方式：  
```stata
*-cluster2 (Petersen-2009, RFS) 
  cluster2 ln_wage $x, fcluster(idcode) tcluster(year) 
  *该命令没有帮助文件，所有功能都可以用 cgmreg 和 vce2way 代替
  *因此，建议日后不必使用该命令

*-cgmreg (CGM2011, Mitchell Petersen's -cluster2.ado- 的升级版)
  *需手动下载：https://sites.google.com/site/judsoncaskey/data
  *help cgmreg  
  cgmreg ln_wage $x, cluster(idcode year)	
  est store m4

*-vce2way (CGM2011, 支持 Panel data, xtreg 等命令)
  *ssc install vce2way
  *help vce2way
  vce2way reg ln_wage $x, cluster(idcode year)	
  est store m5
  
*-vcemway (Gu and Yoo-2019, 该命令在 vce2way 的基础上扩展到多维)
  *ssc install vcemway
  *help vcemway
   vcemway reg ln_wage $x, cluster(idcode year)	
   est store m6

*-结果对比
  local m  "m1 m2 m3 m4 m5 m6"
  local mt "OLS Robust 1Clus 2_cgmreg 2_vce2way 2_vcemway"
  esttab `m', mtitle(`mt') nogap b(%4.3f) se(%6.4f) brackets ///
	      star(* 0.1 ** 0.05 *** 0.01) s(N r2) compress
```  
### 3.2 二维聚类标准误：crhdreg
> 语法结构
```stata
*命令安装
ssc install crhdreg, replace

*语法结构
crhdreg depvar indepvarlist1 indepvarlist2 [if] [in] [, cluster1(varname) cluster2(varname) iv(varname) dimension(real) folds(real) resample(real) median alpha(real) tol(real) maxiter(real)]
```
· depvar：指定回归的被解释变量。  
· indepvarlist1：指定回归的内生解释变量。  
· indepvarlist2 ：指定回归的外生解释变量；通常是控制变量。  
· cluster1(varname)：设置聚类变量；在单向或双向聚类中构建第一个聚类维度。不调用这个选项会自动导致执行更高维的LS回归或无聚类的高维IV回归。  
· cluster2(varname)：设置聚类变量；在单向或双向聚类中构建第二个聚类维度。如果调用了cluster1而没有调用cluster2，那么命令会执行的只有一种聚类方式（以cluster1选项设置的变量为主）的高维LS回归或高维IV回归。  
· iv(varname)：设置indepvarlist1的工具变量；当这一选项不被调动时，该命令将执行高维LS回归。  
· dimension(real)：设置 indepvarlist1 中变量的数量，这些变量的系数将显示在输出表中。 默认值是 dimension(1) 。 它必须是一个正整数，不大于包含在 indepvarlist1 和 indepvarlist2 中的变量总数。  
· folds(real)：设置双重/偏置机器学习中交叉拟合的折叠次数K。在无聚类或单向聚类的情况下，默认值为folds(5)。 在双向聚类的情况下，默认值是folds(3)。它必须是一个大于1的正整数。  
· resample(real)：设置双重/偏倚机器学习的精细样本调整的重采样次数。 默认值是resample(10)。它必须是一个正整数。  
· median：设置指示器，表示有限样本调整使用重新抽样的估计值的中位数。 不调用这个选项会导致使用重新抽样的估计值的平均值。  
· alpha(real)：设置弹性网络算法（Elastic Net 是一种使用L1和L2先验作为正则化矩阵的线性回归模型；这种组合用于只有很少的权重非零的稀疏模型）中的惩罚权重。默认值是alpha(1)，弹性网络算法是 LASSO（最小绝对收缩和选择操作）。如果这个选项被设置为alpha(0)，那么弹性网络算法就变成了岭回归模型。此外，这个参数必须是一个介于0和1之间的实数。  
· tol(real)：设置公差作为弹性网络算法的数值解的停止标准。 默认值是tol(0.000001)。这个参数必须是严格的正实数。  
· maxiter(real)：设置弹性网数值解的最大迭代次数。 默认值是maxiter(1000)。这个参数必须是一个自然数。  

> Stata 实例  

以 Calvi et al. (2021) 使用是否加入美国退休金401k计划对个人净固定资产的影响进行举例。

401k计划是指美国1978年《国内税收法》新增的第401条k项条款的规定，指代一种由雇员、雇主共同缴费建立起来的完全基金式的养老保险制度。但是，个人选择养老保险会与家庭、年龄相关；相似家庭规模的人群更容易同时选择是否参与养老保险制度，同一年龄层的个体也更容易同时选择是否加入养老保险计划。如果只使用稳健性标准误，就有可能忽视个体之间的相关性，导致结果有偏；而如果只针对单一层面的相关性进行聚类，又有可能未能完全考虑另一层面的个体相关对回归结果造成的偏误。

因此，针对这种情况，Chiang et al.（2022）提出了双向稳健性标准误的估算方式，通过 `crhdreg` 命令估计加入401计划对个人净固定资产所造成的影响。
```stata
bcuse 401ksubs, clear  //导入数据

crhdreg nettfa p401k inc  //被解释变量是个人净固定资产（nettfa），解释变量是是否加入401计划（p401k），控制变量是个人年度收入
est store eq_LS   

crhdreg nettfa p401k inc, cluster1(grf)  //引入聚类在家庭规模变量（grf）的标准误
est store eq_c1

crhdreg nettfa p401k inc, cluster2(gra)   //引入聚类在年龄层面（age）的标准误
est store eq_c2

crhdreg nettfa p401k inc, cluster1(grf) cluster2(gra)   //时引入聚类在家庭规模层面（fsize）的标准误、以及聚类在年龄层面（age）的标准误
est store eq_c1c2

crhdreg nettfa p401k inc, iv(e401k) cluster1(grf) cluster2(gra)   //考虑到p401k可能存在内生性，假设e401k是合适的工具变量
est store eq_iv 

esttab eq_*   //回归结果汇总
------------------------------------------------------------
         (1)         (2)         (3)        (4)       (5)   
                                                      
------------------------------------------------------------
p401k  13.20***    11.22***    13.20***   12.01*    9.253   
      (6.98)      (5.63)      (6.98)     (2.09)    (1.61)   
------------------------------------------------------------
N       9275        9275        9275       9275      9275   
------------------------------------------------------------
t statistics in parentheses
* p<0.05, ** p<0.01, *** p<0.001

```

### 3.3 面板聚类标准误：wcbregress
在估计参数的聚类标准误差时，必须考虑总样本内不同样本群之间的相关性。但是，在许多程序中默认忽略这种相关性，这会使计算出的 OLS 标准误大大低于真实的 OLS 标准误。为了解决这个问题，应使用允许异方差或聚类间误差相关性存在的聚类稳健标准误 (CRVE)。  
需要注意的是，该稳健标准误使用的前提假设是样本中的样本簇数量要相当大，一般来说要超过 30 组。当使用 CRVE 方法但样本簇的数量较小 (小于 30) 时，如果使用传统的 Wald 检验来检验系数有效性，可能会产生过度拒绝的问题。为了解决这个问题，我们引入新命令 `wcbregress`。
`wcbregress` 可以选择使用 CRVE 方法进行参数的稳健性求解，同时还会返回用 **Wild Cluster Bootstrap** 方法对参数进行的有效性检验的结果。这个命令依赖于 Stata 内置的 `regress` 命令，因此 `regress` 的大多数选项都与 `wcbregress` 兼容。  

> 语法结构
```stata
*命令安装
ssc install wcbregress, replace

*语法结构
wcbregress depvar varlist [if] [in] [weight] [, options]
```
· depvar：被解释变量；  
· varlist：不进入选择模型的解释变量；  
`options` 具体如下：  
· group(varname)：用于生成各变量簇的变量名称；  
· robust：稳健地使用 CRVE 方法求解系数；  
· vce(vcetype)：获得稳健标准误；  
· nonconstant(varname)：进行无常数项回归；  
· hascons：使用用户提供的常数项进行回归；  
· tsscons：计算一系列常数项的平方和；  
· level(#)：设置置信区间，默认是 95%；  
· seed(#)：设置随机数种子；  
· detail：显示中间命令输出；  
· rep(#)：用于计算标准误差或置信区间的 bootstrap 复制数，默认是 500 。   

> Stata 实例  

我们以 Stata 自带的「auto.dta」数据为例，演示命令 wcbregress 的使用方法。相关设定如下：

· 假设价格 **price** 是行程英里数 **mpg** 、头部空间 **headroom** 、后备箱空间 **trunk** 、车重 **weight** 等变量的函数；  
· 以 1978 年维修记录 **rep78** 为分类依据；  
· 置信区间设定为 99%。
```stata
· clear all

. sysuse auto.dta, clear
(1978 Automobile Data)

. wcbregress price mpg headroom trunk weight, group(rep78) level(99)

Wild Cluster Boostrap in progress (200 replications)
(each dot . indicates one replication)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5 
..................................................    50
..................................................   100
..................................................   150
..................................................   200

Wild Cluster Bootstrap Linear regression
                                                Number of obs     = 74.000
                                                Replications      = 200.000
                                                R-squared         = 0.323
                                                Adj R-squared     = 0.283
                                                Root MSE          = 2497.033

----------------------------------------------------------------------------
 Panel A: Point Estimates and Wild Cluster Bootstrap Std. Err.   
----------------------------------------------------------------------------
          |              Bootstrap                        Normalized
    price |   Coef.      Std. Err.     z    P>|z|   [  99% Conf. Interval  ]
----------+-----------------------------------------------------------------
      mpg | -54.7915      66.9427    -0.82  0.413     -168.339        58.756
 headroom |-726.5434     243.1408    -2.99  0.003    -1150.295      -302.792
    trunk |  23.0425     104.3392    0.22   0.825     -150.260       196.345
   weight |   2.0119       1.1985    1.68   0.093       -0.080         4.104
    _cons |3114.9403    4283.5837    0.73   0.467    -4541.290     10771.170
----------------------------------------------------------------------------
 Panel B: Wild Cluster Bootstrap t-tests and Confidence Intervals  
----------------------------------------------------------------------------
          |  Bootstrap               [  99% Conf. Interval  ]
    price |   P>|t*|       [    Symmetric CI    ]   [    Asymmetric CI   ]
----------+-----------------------------------------------------------------
      mpg |   0.520          -177.168      67.585     -177.168      67.585
 headroom |   0.030         -1471.480      18.393    -1471.480      18.393
    trunk |   0.790          -336.623     382.708     -350.185     382.708
   weight |   0.500            -6.348      10.372       -4.879      10.372
    _cons |   0.560        -12662.700   18892.580   -13886.579   18892.580
----------------------------------------------------------------------------
```   

## 4.参考资料
[1] Abadie, A., S. Athey, G. W. Imbens, J. Wooldridge, 2022, When should you adjust standard errors for clustering? [PDF](http://www.google.com/search?q=When%20Should%20You%20Adjust%20Standard%20Errors%20for%20Clustering?)

> Note：产生如下推文列表的 Stata 命令为：  
> `lianxh 聚类`  
安装最新版 `lianxh` 命令：  
`ssc install lianxh, replace`    

· 专题：[Stata命令](https://www.lianxh.cn/blogs/43)  
    [Stata：双重机器学习-多维聚类标准误的估计方法-crhdreg](https://www.lianxh.cn/news/7519c2f054479.html)  
    [Stata：聚类分析-cluster](https://www.lianxh.cn/news/62dfdcb6eccc6.html)

· 专题：[数据处理](https://www.lianxh.cn/blogs/25)  
    Stata：[原始聚类自助法(wild cluster bootstrap)-boottest](https://www.lianxh.cn/news/c7f3ec4b585fd.html)

· 专题：[回归分析](https://www.lianxh.cn/blogs/32)  
    [聚类异质性：使用summclust进行统计推断](https://www.lianxh.cn/news/b7550f8ee074b.html)   
    [Stata：聚类标准误的纠结](https://www.lianxh.cn/news/c9dbc1acea75a.html)  
    [Stata：聚类调整标准误笔记](https://www.lianxh.cn/news/368a6a538b547.html)  
    [Stata：聚类调整后的标准误-Cluster-SE](https://www.lianxh.cn/news/a7a8e613b2699.html)  
    [小样本下OLS估计的纠偏聚类标准误](https://www.lianxh.cn/news/361d66f088624.html) 

· 专题：[面板数据](https://www.lianxh.cn/blogs/20)  
    [Stata：面板聚类标准误-自动确定最优聚类层级和数量-xtregcluster](https://www.lianxh.cn/news/31be9ab09c5ff.html)  
    [wcbregress：面板聚类标准误](https://www.lianxh.cn/news/5d4314cc25122.html)
