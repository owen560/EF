# Stata : plottabs命令介绍
[TOC]
&emsp;
> **作者** : 李其璋(中山大学)   
> **邮箱** : <3456899246@qq.com>    
## 1. 简介
本文介绍 Stata 中的一个高效绘图套件 `plottabs` ，包括 `plottabs` , `plotshares` , `plotbetas` , `plotmeans` 四个命令。

此套件的四个命令可以混合使用，比传统绘图命令可视化形式更丰富，图表表现力更强，能更直观地体现数据特征。`plottabs` 套件避免了本机绘图耗时过长的问题，且在大样本数据中速度比本机命令快 300 倍。此外，`plottabs` 套件支持用户在内存中储存多个绘图数据，方便随时调用。

四个命令功能如下：
* `plottabs` : 绘制条件频率或份额（ `tabulate oneway` 的可视化模拟）
* `plotshares` : 绘制条件分类频率或份额（ `tabulate twoway` 的可视化模拟）
* `plotbetas` : 绘制可视化回归系数估计（回归后）
* `plotmeans` : 绘制条件均值（ `mean y,over x` 的可视化模拟）
 
## 2. 命令介绍
命令安装：
```Stata
ssc install plottabs, replace
或
net install plottabs.pkg, replace
```
命令语法：
```Stata
plottabs varname [if] [in] [, options]
plotshares varname [if] [in] , over(groupvar) [options]
plotbetas varlist [if] [in] [, options]
plotmeans varname [if] [in] , over(groupvar) [options]
```
其中，`varname` 为需要可视化的变量，`varlist` 为回归中的变量集。`groupvar` 表示作为分组依据的变量。

`options` 包括：

**基本选项**
* `over(varname)` : 指定条件变量的另一种方法
* `output(output_type)` : 指定绘图类型：频率（默认）/份额/累计。将 `output_type` 设置为 `share` 会产生相对份额，而设置为 `cummulative` 会产生累积份额
* `graph(graph_type)` : 指定二维图表类型：线型图（默认）/条形图/连线图/散点图等
* `invert` ：倒转被分类变量在图表中的排列顺序
* `rgraph(rgraph_type)` ：指定置信区间区域的图表类型，包括 `rarea` （默认）、`bar`、`rcap`、`rspike` 等
* `ci(#,ci_options)` ：指定置信区间水平（`#` 为 0 到 100 的数，95 为默认值，`off` 表示不显示置信区间；`ci_options` 表示`rgraph_type` 的自定义选项，包括 `color`、`transparency` 等）

**存储/数据管理选项**
* `frame(frame_name)` : 指定存储输出数据的数据框名称（默认为 `frame_pt` ）
* `clear` : 清除存储在 `frame_name` 中的所有输出的数据
* `replace(#)` : 覆盖替换现有变量
* `plotonly` : 显示已存储在 `frame_name` 中的图表

**自定义选项**
* `command` : 输出用于显示图表的二维命令（更好地自定义）
* `global` : 将相同的自定义选项应用于 `frame_name` 中的所有绘图
* `plname(plot_name)` : 命名当前绘图（当同时使用多个图表时在图例中使用）
* `xshift(real)`       把绘制的变量沿着 x 轴移动 
* `yshift(real)`       把绘制的变量沿着 y 轴移动 
* `yzero` : 用于在 y 轴上显示零的缩略选项（并按 y 轴）
* twoway_options ：更改标题、图例、坐标轴、纵横比等
* connect_options ：更改图线外观或连线方式
* marker_options ：更改标记的外观（颜色、大小等）
* area_options ：更改图表区域的设置（仅在 `plotshares` 中适用）
* barlook_options ：更改条形图外观

**其他选项**
* `nodraw` : 不显示绘制的图表（在覆盖半成品图表时很有用）
* `times(real)` : 将绘图的数值乘以一个常数（对变量标准化处理时很有用）

## 3. Stata实操
### 3.1 plottabs 命令实操
#### 3.1.1 基础用法
这是 `plottabs` 最基本的用法，这里使用条形图描述了变量 **mpg** 的频数分布情况。代码和结果如下图。
```Stata
clear all
sysuse auto
plottabs mpg, graph(bar)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig1_plottabs基础用法_李其璋.png)

#### 3.1.2 比较两个子样本的累计份额
对于一个变量，我们有时候需要考察它的值落在某个区间内的样本量，以及它与总样本量的关系。这时使用 `output(cummulative)` 选项可达到目的。以下例子用阶梯折线考察了国内和国外两个子样本中变量 **mpg** 逐渐增大时其累计份额的变化情况。

若折线上某点的坐标为 (x,y) ，则意味着在从变量 **mpg** 的最小值到 x 这一区间内，**mpg** 的值出现的次数占总样本量的比为 y 。
```Stata
clear all
sysuse auto
plottabs mpg if foreign == 0, output(cummulative) connect(stairstep) plname(Domestic)
plottabs mpg if foreign == 1, output(cummulative) connect(stairstep) plname(Foreign)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig2_plottabs累计份额_李其璋.png)

#### 3.1.3 两种类型图表的合用
`plottabs` 也支持同时使用两种图表，并在同一个图表中显示，同时执行以下几行命令即可。以下例子把变量 **mpg** 的条形图和阶梯折线图同时呈现，还使用了 `scheme` 选项调整背景。
```Stata
ssc install schemepack
clear all
sysuse auto
plottabs mpg, graph(bar)
plottabs mpg, output(cummulative) connect(stairstep) legend(off) scheme(gg_tableau) yaxis(2)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig3_plottabs两种图表_李其璋.png)

当然，`plottabs` 也可以和 `plotmeans` 等其他三个命令合用，这一点在后面的例子中会看到。

#### 3.1.4 其他用法
* 隐藏图表

输入以下代码：
```Stata
clear all
sysuse auto
plottabs mpg if foreign == 0, nodraw plname(Domestic)
plottabs mpg if foreign == 1, nodraw plname(Foreign)
```
`nodraw` 选项表示命令已执行，图表储存在内存中，但不显示。
再输入以下指令，则显示此前未显示的图表：
```Stata
plottabs, plotonly
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig4_plottabs隐藏图表_李其璋.png)

* 替换图表

以下指令把上述第二个图表替换为另一个：
```Stata
plottabs mpg if headroom>3, replace(2)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig5_plottabs替换图表_李其璋.png)

* 替换自定义选项

```Stata
plottabs mpg, plotonly replace(2) color(pink) plname("Headroom > 3")
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig6_plottabs替换自定义选项_李其璋.png)

* 合并以上两个指令
```Stata
plottabs mpg if headroom>3, rep(2) col(pink) pln("Headroom > 3")
```

* 清除内存中的图表并绘制新图
```Stata
plottabs mpg, clear graph(bar)
```
结果同3.1.1

* 创建一个新的独立图表
```Stata
plottabs turn, graph(bar) frame(frame_hr)
```
新图表储存在内存中的独立位置。
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig7_plottabs新图_李其璋.png)

* 为内存中每个图表命名
```Stata
plottabs, plotonly name(fig1)
plottabs, plotonly name(fig2) frame(frame_hr)
```
#### 3.1.5 关于 frame 的说明
* 切换数据集

输入以下指令，得到图表。
```Stata
clear all
sysuse auto
plottabs mpg if foreign==0, out(cumm) connect(stairstep) pln(Domestic)
plottabs mpg if foreign==1, out(cumm) connect(stairstep) pln(Foreign) scheme(economist)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig8_plottabs返回图表_李其璋.png)

再输入以下指令：
```Stata
frame change frame_pt
browse      //查看 frame_pt 中图表的数据
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig9_plottabs查看数据_李其璋.png)

此时数据集被切换至图表处，并查看图表中的数据。若无 `frame` 步骤，则仅能查看整份 auto 数据。

* 查看自定义选项
```Stata
. frame change frame_pt_cust
. describe      //详细描述 frame_pt_cust 中的数据
. browse        //查看数据

Contains data
 Observations:             2                  
    Variables:             8                  
-------------------------------------------------------------------------------------------------
Variable      Storage   Display    Value
    name         type    format    label      Variable label
-------------------------------------------------------------------------------------------------
cust_out        str16   %16s                  Output type corresponding to _n-th plot
cust_gra        str4    %9s                   Graph type corresponding to _n-th plot
cust_opt        str18   %18s                  Graphing options specific to _n-th plot
cust_two        str17   %17s                  Two-way options applicable to the overall graph
cust_oth        str1    %9s                   Any other options specific to _n-th plot
cust_rgr        str1    %9s                   Graph type corresponding to the CIs of _n-th plot
cust_lci        str1    %9s                   Legend for the CIs of _n-th plot
cust_oci        str1    %9s                   Graphing options specific to the CIs of _n-th plot
-------------------------------------------------------------------------------------------------
Sorted by: 
     Note: Dataset has changed since last saved.
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig10_plottabs查看自定义选项_李其璋.png)

* 恢复默认数据集
```Stata
frame change default
```
注：当 `frame` 被定义后，每次添加新的图表，都要加上 `frame` 选项来切换，否则系统会用新的图表替换原有图表。
### 3.2 plotshares 命令实操
#### 3.2.1 基础用法
这是 `plotshares` 最基本的用法，以下描述了 nlsw88 数据中变量 **race** 按年龄 **age** 变化统计时，每个类别的相对份额情况，且三个类别相对份额之和为 1 。
```Stata
clear all
sysuse nlsw88
plotshares race, over(age)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig11_plotshares基础用法_李其璋.png)
#### 3.2.2 使用频数对比
以下使用具体频数来对比三个子样本中 **race** 随年龄变化的统计。
```Stata
clear all
sysuse nlsw88
plotshares race, over(age) output(stacked) graph(bar)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig12_plotshares频数_李其璋.png)
#### 3.2.3 综合应用
以下例子倒转了 **race** 子样本的排列顺序。同时使用 `plotmeans` 指令展示变量 **ttl_** 随年龄变化的条件均值，使用线图连接散点，再自定义其他选项。
```Stata
ssc install schemepack
clear all
sysuse nlsw88
plots race, over(age) invert
plotm ttl_, over(age) graph(connect) plname(Yrs of Exp) scheme(white_ptol) color(red) yaxis(2) yzero
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig13_plotshares综合应用_李其璋.png)
### 3.3 plotbetas 命令实操
#### 3.3.1 基础用法
以下探究 **ttl_exp** 对 **i.age** 回归时， **i.age** 的回归系数，并在图表中绘制 95% 置信区间。
```Stata
. clear all
. sysuse nlsw88
. reg ttl_exp i.age
. plotbetas i.age


      Source |       SS           df       MS      Number of obs   =     2,246
-------------+----------------------------------   F(12, 2233)     =      4.96
       Model |  1238.37396        12   103.19783   Prob > F        =    0.0000
    Residual |  46476.8864     2,233  20.8136526   R-squared       =    0.0260
-------------+----------------------------------   Adj R-squared   =    0.0207
       Total |  47715.2603     2,245  21.2540135   Root MSE        =    4.5622

------------------------------------------------------------------------------
     ttl_exp | Coefficient  Std. err.      t    P>|t|     [95% conf. interval]
-------------+----------------------------------------------------------------
         age |
         35  |     .72963   .6875776     1.06   0.289    -.6187282    2.077988
         36  |   .8667033   .6882568     1.26   0.208    -.4829868    2.216393
         37  |   1.409611   .6965742     2.02   0.043     .0436101    2.775611
         38  |   1.409422    .698391     2.02   0.044     .0398584    2.778985
         39  |   2.614319   .6940156     3.77   0.000     1.253336    3.975302
         40  |   1.914985   .7019801     2.73   0.006     .5383831    3.291587
         41  |   2.067868   .6974709     2.96   0.003     .7001091    3.435627
         42  |   2.219978   .7230462     3.07   0.002     .8020645    3.637891
         43  |   2.109318   .7203151     2.93   0.003     .6967608    3.521875
         44  |   2.786675   .7213886     3.86   0.000     1.372012    4.201337
         45  |   1.517426   .8121281     1.87   0.062    -.0751794    3.110031
         46  |   9.552189   3.286265     2.91   0.004     3.107735    15.99664
             |
       _cons |   10.84204   .6266661    17.30   0.000     9.613133    12.07095
------------------------------------------------------------------------------
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig14_plotbetas基础1_李其璋.png)

若使用箱线图绘制，可用以下指令：
```Stata
plotbetas i.age, clear graph(scatter) rgraph(rcap)  
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig15_plotbetas基础2_李其璋.png)
#### 3.3.2 对比两个子样本的回归系数
按 **union** 等于 0 或 1 分为两组分别进行回归，比较两组 **i.age** 的回归系数。
```Stata
. clear all
. sysuse nlsw88
. reg ttl_exp i.age if union==0
. plotbetas i.age, graph(connect) plname(Non-union)
. reg ttl_exp i.age if union==1
. plotbetas i.age, graph(connect) plname(Union)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig16_plotbetas对比_李其璋.png)
#### 3.3.3 自定义应用
以下自定义线型、置信区间颜色及透明度。
```Stata
clear all
sysuse nlsw88
reg ttl_exp i.age
plotbetas i.age, color(green) ci(, color(gold%35))
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig17_plotbetas自定义_李其璋.png)
### 3.4 plotmeans 命令实操
#### 3.4.1 基础用法
以下绘制 **ttl_exp** 关于年龄的条件均值，并绘制 95% 置信区间。
```Stata
clear all
sysuse nlsw88
plotmeans ttl_exp, over(age)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig18_plotmeans基础1_李其璋.png)

改用箱线图：
```Stata
plotmeans ttl_exp, over(age) graph(scatter) rgraph(rcap) clear
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig19_plotmeans基础2_李其璋.png)
#### 3.4.2 对比两个子样本的条件均值
按 **union** 等于 0 或 1 分为两组，对比 **ttl_exp** 在两组样本中的条件均值。
```Stata
clear all
sysuse nlsw88
plotmeans ttl_exp if union==0, over(age) graph(connect) plname(Non-union)
plotmeans ttl_exp if union==1, over(age) graph(connect) plname(Union)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig20_plotmeans对比_李其璋.png)
#### 3.4.3 综合应用
先用 `plottabs` 的条形图绘制 **age** 的分布，再用 `plotmeans` 的散点连线图绘制 **ttl_exp** 关于 **age** 的条件均值，两幅图在一个图表中显示。
```Stata
clear all
sysuse nlsw88
plottabs, over(age) graph(bar) plname(Frequencies) color(green%15)
plotmeans ttl_exp, over(age) graph(connect) plname(Yrs of Exp) yaxis(2)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig21_plotmeans综合_李其璋.png)
#### 3.4.4 自定义应用
以下自定义图线颜色、置信区间颜色及透明度。
```Stata
clear all
sysuse nlsw88
plotmeans ttl_exp, over(age) color(green) ci(, color(gold%35))
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/plottabs_Fig22_plotmeans自定义_李其璋.png)
## 4.结语
以上是 `plottabs` 绘图套件的大体介绍。在使用这四个指令的时候，我们应灵活运用 Stata 配置的各种画图选项，方能使图表的表现力更强，这也是可视化操作的灵魂所在。由于绘图的自定义选项繁多，本文未能穷尽指令的每一种变式，读者可结合实际问题进一步探索。
## 5.相关推文
Note：产生如下推文列表的 Stata 命令为：   
`lianxh 绘图`、`lianxh 面板数据`
安装最新版 `lianxh` 命令：    
`ssc install lianxh, replace` 
- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata绘图：circlepack-绘制圆堆图](https://www.lianxh.cn/news/2c586d5c5af52.html)
  - [Stata绘图：COVID-19数据可视化](https://www.lianxh.cn/news/7fd81b4dfa2d2.html)
  - [Stata绘图：复现组间均值差异图](https://www.lianxh.cn/news/5242699e2c424.html)
  - [Stata绘图：sunflower-向日葵图-克服散点重叠](https://www.lianxh.cn/news/04b0304d5a3be.html)
  - [Stata绘图：环形柱状图-实时全球新冠确诊人数](https://www.lianxh.cn/news/b33c479a0a6de.html)
  - [Stata绘图：峰峦图绘制 joy_plot](https://www.lianxh.cn/news/ae974713dbd40.html)
  - [Stata绘图：回归系数可视化-multicoefplot](https://www.lianxh.cn/news/3e052bd3291dd.html)
  - [Stata绘图：如何更高效的绘制图形](https://www.lianxh.cn/news/07bfc4c9db38b.html)
  - [Stata绘图：绘制桑基图-sankey_plot](https://www.lianxh.cn/news/5e628ee91f098.html)
  - [Stata绘图-可视化：组间差异比较散点图](https://www.lianxh.cn/news/b5fc0aeb1d7b7.html)
  - [Stata绘图：addplot-fabplot-多图层美化图片](https://www.lianxh.cn/news/0309ba204eb8e.html)
  - [Stata绘图：箱形图与小提琴图-vioplot](https://www.lianxh.cn/news/fe238b4d418c5.html)
  - [Stata绘图：太美了！羊皮卷风格图形](https://www.lianxh.cn/news/7d19377d49d52.html)
  - [Stata绘图：自定义绘图利器-palettes](https://www.lianxh.cn/news/4a9a119c6fbd8.html)
  - [史上最牛Stata绘图模版-schemepack：酷似R中的ggplot2-袁子晴](https://www.lianxh.cn/news/e76a8a7e3c6c4.html)
  - [Stata绘图：addplot-层层叠加轻松绘图](https://www.lianxh.cn/news/342e156beda06.html)
  - [常用科研统计绘图工具介绍](https://www.lianxh.cn/news/021856c11e46b.html)
  - [Stata绘图-组间差异可视化：不良事件火山图、点阵图](https://www.lianxh.cn/news/f9680c4be14fe.html)
  - [Stata绘图极简新模板：plotplain和plottig-T251](https://www.lianxh.cn/news/37ea99dab6efb.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-上篇](https://www.lianxh.cn/news/59bc1ff7d027a.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-下篇](https://www.lianxh.cn/news/180daa074ef27.html)
  - [Stata绘图：柱状图专题-T212](https://www.lianxh.cn/news/df9729c5de34d.html)
  - [Stata绘图：回归系数可视化-论文更出彩](https://www.lianxh.cn/news/324f5e3053883.html)
  - [Stata绘图：世行可视化案例-条形图-密度函数图-地图-断点回归图-散点图](https://www.lianxh.cn/news/96989b0de4d83.html)
  - [Stata绘图：随机推断中的系数可视化](https://www.lianxh.cn/news/ce93c41862c16.html)
  - [Stata绘图：用-bytwoway-实现快速分组绘图](https://www.lianxh.cn/news/4d72752d51859.html)
  - [Stata绘图：让图片透明——你不要掩盖我的光芒](https://www.lianxh.cn/news/ed0d5216d7eb7.html)
  - [Stata绘图：bgshade命令-在图形中加入经济周期阴影](https://www.lianxh.cn/news/b89fd3c601e31.html)
  - [Stata：图形美颜-自定义绘图模板-grstyle-palettes](https://www.lianxh.cn/news/8c1819ff61a8a.html)
  - [Stata绘图：多维柱状图绘制](https://www.lianxh.cn/news/01fab25337d76.html)
  - [Stata绘图全解：绘图语法-条形图-箱型图-散点图-矩阵图-直方图-点图-饼图](https://www.lianxh.cn/news/ae36a721cfe18.html)
  - [Stata绘图：绘制单个变量的时序图](https://www.lianxh.cn/news/f62630d968b9e.html)
- 专题：[Stata面板数据](https://www.lianxh.cn/blogs/20.html)
  - [Stata绘图：面板数据可视化-panelview](https://www.lianxh.cn/news/78c21ab215c46.html)