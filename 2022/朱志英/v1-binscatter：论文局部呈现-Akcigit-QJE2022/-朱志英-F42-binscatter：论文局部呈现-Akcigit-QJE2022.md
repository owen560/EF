&emsp;

&emsp;

> **作者**：朱志英 (中山大学)      
> **邮箱**：<zhuzhy28@mail2.sysu.edu.cn> 

> **Source**：Akcigit, Grigsby, Nicholas and Stantcheva (2022): Taxation and Innovation in the Twentieth Century, Quarterly Journal of Economics 137 (1): 329-385. [-pdf-](https://academic.oup.com/qje/article-abstract/137/1/329/6292271?redirectedFrom=fulltext&login=false)

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

<font size=3>

## 1. 内容简介
散点图能够直观表现出影响因素和预测对象之间的总体关系趋势，以醒目的图形方式，反映变量间的形态变化关系情况，以便于来模拟变量之间的关系。因此，它被广泛地应用于论文中。但是，当样本量太大或者 $x$ 与 $y$ 之间的关系不够明显时，散点图就会捉襟见肘。此时，分仓散点图 (binned scatterplot) 的作用就显现出来了。

本文将简要介绍分仓散点图，并介绍 Akcigit et al. (2022, QJE) 论文中对分仓散点图的应用，即 Akcigit et al. (2022, QJE) Figure 1 的绘制。

## 2. `binscatter`方法介绍

### 2.1 `binscatter`简介 

`binscatter` 是用于生成分仓散点图 (binned scatterplot) 的 Stata 命令。其生成的图像显示了给定 $x$ 情况下 $y$ 条件期望的非参数估计。该命令可以在控制其他变量或者固定效应的情况下，快速检测非线性、异常值、分布性问题和最佳拟合函数形式。此外，还可以很好地显示不同子组的异构关系。

### 2.2 `binscatter` 实现步骤

那么，`binscatter` 是如何实现的呢？

- 首先，`binscatter` 将散点划分成不同 bins ，不同的 bins 含有相同的样本量；
- 其次，计算每个 bin 内 $x$ 和 $y$ 的均值；
- 再次，创建这些数据点的散点图；
- 最后，绘制总体回归线 (采用原始数据，而非生成的散点) 。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig1_%E5%AE%9E%E7%8E%B0%E6%AD%A5%E9%AA%A4_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

### 2.3 `binscatter` 代码初探

首先，我们需要安装 `binsactter` 命令。

```stata
. ssc install binscatter, replace // 命令安装
```

可以查阅帮助文档进一步了解 `binscatter`。

```stata
. help binscatter//查阅帮助文档
```

接下来，我们以 nlsw88.dta 为例，绘制年龄和工资关系之间的图表，来感受一下 `binscatter` 的绘图效果。

```stata
. sysuse nlsw88, clear 
. binscatter wage age  //生成最基础的binscatter图像
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig2_%E5%9F%BA%E7%A1%80%E5%AE%9E%E7%8E%B0_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

可以看到，在一定年龄区间内，**wage** 和 **age** 之间呈现出明显的负向关系。

我们还可以对 bins 进行修改，`binscatter` 中默认的 bins 为20，我们在应用时，可以根据设计情况进行调整。如下图，即是将 bins 改为了30。

```stata
. binscatter wage age, n(30)  //更改bins为30个，默认bins为20个
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig3_%E6%94%B9%E5%8F%98bins_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

有时，我们除了关注变量间的线性关系，还想关注变量间的非线性关系 (如二次关系) ， `binscatter` 也可以帮助我们实现。

```stata
. binscatter wage age, line(qfit) // 进行二次拟合
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig4_%E4%BA%8C%E6%AC%A1%E6%8B%9F%E5%90%88_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

此外，我们还可以使用 `by` 进行分组绘图，查看不同组中变量间的关系。例如：研究不同种族中，年龄和工资的关系。我们发现，种族为其他的群体工资与年龄间关系的拟合直线较为陡峭。

```stata
. binscatter wage age, by(race) //按照种族进行分组绘图
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig5_%E5%88%86%E7%BB%84%E7%BB%98%E5%9B%BE_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

我们也可以使用 `controls` 加入控制变量。例如：在研究工资与年龄的关系时，我们将样本的工作经验设置为控制变量。

```stata
. binscatter wage age, controls(ttl_exp) //加入控制变量：工作经验
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig6_%E5%8A%A0%E5%85%A5%E6%8E%A7%E5%88%B6%E5%8F%98%E9%87%8F_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

另外，分组和控制变量可以结合起来。下图的例子中我们使用 `absorb` 命令，加入职业固定效应。

```stata
. binscatter wage age, by(race) absorb(occupation) //加入固定效应进行分组回归
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig7_%E5%9B%BA%E5%AE%9A%E6%95%88%E5%BA%94%E5%8A%A0%E5%88%86%E7%BB%84_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

`binscatter` 还可以帮助我们观察自变量和因变量在某些数值上是否存在断点。如下面的例子中，我们使用`rd` 命令观察工资与年龄的关系是否在 35 岁前后发生变化。

```stata
 . binscatter wage age, rd(35)  // 分段回归, 该命令对散点无影响, 但是生成分段回归曲线
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig8_%E6%96%AD%E7%82%B9%E5%9B%9E%E5%BD%92_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

&emsp;

## 3. 背景介绍

接下来，本文将简要介绍 Akcigit et al.(2022, QJE) 论文的主要内容及 Figure 1 在论文中发挥的作用。

### 3.1 论文介绍

Akcigit et al.(2022, QJE) 一文中，作者研究了 20 世纪美国公司税和个人税对创新的影响。他们建立了一个自 1920 年获得专利的发明人的微观数据库和一个包含公司税率和税基信息的州级层面的公司税数据库，并将其与州级个人所得税和其他经济变量关联起来。最终，他们发现，征税对创新数量有负面影响，也会迫使本地发明人外迁，但不会影响平均创新质量。州级税收弹性很大，并且与个人层面对创新产生的反应和跨州流动性的结果一致。公司税往往会特别影响公司发明者的创新生产和跨州流动性。个人所得税显着影响整体创新数量和发明人的流动性。

### 3.2 Figure 1 背景介绍

#### 3.2.1 目的

可视化呈现多元线性回归的系数，观察征税与创新数量之间的关系。

#### 3.2.2 主要变量说明

**mtr90_lag3**：个人所得税，采用滞后三年的第 90 分位点个人所得税税率表示
**top_corp_lag3**：公司所得税，采用滞后三年的头部公司所得税税率表示
**lnpat**：专利申请数，采用每年州申请的专利数的对数表示
**ln_inv**：发明者的数量，采用每年定居在州内的发明者的数量的对数表示
**real_gdp_pc**：人均实际GDP
**population_density**：人口密度

#### 3.2.3 命令语法

命令语法如下：

```stata
 . binscatter varlist [if] [in] [weight] [, options]
 ```

 其中，
 - `varlist` ：因变量和自变量；
 - `if` 和 `in`：添加限定条件；
 - `weight`: 以该变量作为权重进行回归；
 - `controls()`: 添加控制变量；
 - `absorb()`：使用固定效应；
 - `nquantiles()`: 设置生成多少组 bins；
 - `noaddmean`: 计算残差后不加回变量均值；
 - `nofastxtile`: 使用 `xtile` 。
  
#### 3.2.4 要点

计算散点值时，根据分位点进行分组，组内按照人口数量取加权。

   - 数据: state_data.dta
    - 删除存在离群值的州：Louisiana, Alaska, Hawaii, PR，以及州简称缺失的州
  - 时间范围: 1940-2000 年

&emsp;

## 4. Figure 1 复现

导入数据后，设置全局暂元。

```stata
  global cx "L.real_gdp_pc L.population_density rd_credit_lag3 i.statenum"
  global T_p "mtr90_lag3"    // 个人税
  global T_c "top_corp_lag3" // 公司税
```

 其中，**L.real_gdp_pc** 为滞后一年的人均实际  gdp，**L.population_density** 为滞后一年的人口密度，**rd_credit_lag3** 为滞后三年的研发支出抵免。

接下来，我们控制公司所得税，使用专利申请数对个人所得税进行回归。

  Figure 1 (A)

```stata

  *-画图：Figure 1 (A)
  binscatter lnpat    $T_p  [aw=pop1940],     ///
             controls($T_c $cx)               ///
             absorb(year) nquantiles(100)     ///
             noaddmean                        ///
             nofastxtile                      ///
             savegraph("Fig1A.gph") replace
```

其中， [aw=pop1940] 表示把 1940 年的人口数作为权重回归。作者控制公司所得税、滞后一年的人均实际 gdp 、滞后一年的人口密度、滞后三年的研发支出抵免和不同的州，并使用 `absorb` 加入年份的固定效应，将样本分为 100 组进行回归。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig9_1A%E5%A4%8D%E7%8E%B0_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

同理，控制个人所得税，使用专利申请数对公司所得税进行回归。

Figure 1 (B)

```stata
*-画图：Figure 1 (B)
  binscatter lnpat    $T_c  [aw=pop1940],     ///
             controls($T_p $cx)               ///
             absorb(year) nquantiles(100)     ///
             noaddmean                        ///
             nofastxtile                      ///
             savegraph("Fig1B.gph") replace

```

类似地，Figure 1 (B) 把 1940 年的人口数作为权重回归。作者控制个人所得税、滞后一年的人均实际 gdp 、滞后一年的人口密度、滞后三年的研发支出抵免和不同的州，并使用 `absorb` 加入年份的固定效应，将样本分为 100 组进行回归。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig10_1B%E5%A4%8D%E7%8E%B0_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

Figure 1 (C)

在Figure 1 (C)中，我们控制个人所得税，使用发明者数量对公司所得税进行回归。


```stata
*-画图：Figure 1 (C)
 .binscatter ln_inv    $T_p     [aw=pop1940],     ///
             controls($T_c $cx)               ///
             absorb(year) nquantiles(100)     ///
             noaddmean                        ///
             nofastxtile                      ///
             savegraph("Fig1C.gph") replace

```

把 1940 年的人口数作为权重回归，并控制个人所得税、滞后一年的人均实际 gdp 、滞后一年的人口密度、滞后三年的研发支出抵免和不同的州。使用 `absorb` 加入年份的固定效应，并将样本分为 100 组进行回归。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig11_1C%E5%A4%8D%E7%8E%B0_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

Figure 1 (D)

在Figure 1 (D) 中，我们控制公司所得税，使用发明者数量对个人所得税进行回归。

```stata
*-画图：Figure 1 (D)
  binscatter ln_inv    $T_c  [aw=pop1940],     ///
             controls($T_p $cx)               ///
             absorb(year) nquantiles(100)     ///
             noaddmean                        ///
             nofastxtile                      ///
             savegraph("Fig1D.gph") replace

```
把 1940年的人口数作为权重回归。作者控制公司所得税、滞后一年的人均实际 gdp 、滞后一年的人口密度、滞后三年的研发支出抵免和不同的州，并使用 `absorb` 加入年份的固定效应，将样本分为 100 组进行回归。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig12_1D%E5%A4%8D%E7%8E%B0_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

最后，我们将 4 张图片进行组合。

```stata
 graph combine Fig1A.gph Fig1B.gph Fig1C.gph Fig1D.gph, ///
          rows(2) imargin(zero) graphregion(fcolor(white))
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/binscatter%EF%BC%9A%E8%AE%BA%E6%96%87%E5%B1%80%E9%83%A8%E5%91%88%E7%8E%B0_Fig13_1_%E5%9B%BE%E7%89%87%E7%BB%84%E5%90%88_%E6%9C%B1%E5%BF%97%E8%8B%B1.png)

&emsp;

## 5. 参考资料
- 钟声，连享会推文，[Stata：分仓散点图绘制-binscatter-binscatter2](https://www.lianxh.cn/news/bd70ec133d209.html)
- Chen, Y., S. Shi, Y. Tang, 2019, Valuing the urban hukou in china: Evidence from a regression discontinuity design for housing prices, Journal of Development Economics, 141.[-pdf-](https://sci-hub.ee/10.1016/j.jdeveco.2019.102381)
- Beck, Thorsten, Ross Levine, and Alexey Levkov. 2010. “Big Bad Banks? The Winners and Losers from Bank Deregulation in the United States.” Journal of Finance 65 (5): 1637–67.[-pdf-](https://onlinelibrary.wiley.com/doi/10.1111/j.1540-6261.2010.01589.x)

&emsp;

## 6. 相关推文
> Note：产生如下推文列表的 Stata 命令为：   
> &emsp; `lianxh 边际, m`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace`

- 专题：[专题课程](https://www.lianxh.cn/blogs/44.html)
  - [⏩连享会公开课：实证研究中的数据可视化](https://www.lianxh.cn/news/9c86f40c6f17a.html)
- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata可视化：能用图形就不用表格](https://www.lianxh.cn/news/0edc0cc509220.html)
  - [Stata绘图：回归系数可视化-multicoefplot](https://www.lianxh.cn/news/3e052bd3291dd.html)
  - [Stata绘图-可视化：组间差异比较散点图](https://www.lianxh.cn/news/b5fc0aeb1d7b7.html)
  - [Stata绘图全解：绘图语法-条形图-箱型图-散点图-矩阵图-直方图-点图-饼图](https://www.lianxh.cn/news/ae36a721cfe18.html)
  - [Stata绘图-组间差异可视化：不良事件火山图、点阵图](https://www.lianxh.cn/news/f9680c4be14fe.html)
  - [Stata可视化：让他看懂我的结果！](https://www.lianxh.cn/news/01607de7be5e8.html)
  - [常用科研统计绘图工具介绍](https://www.lianxh.cn/news/021856c11e46b.html)
  - [Stata绘图：柱状图专题-T212](https://www.lianxh.cn/news/df9729c5de34d.html)
  - [Stata绘图：绘制一颗红心-姑娘的生日礼物](https://www.lianxh.cn/news/e6a97fe962611.html)
  - [Stata绘图：COVID-19数据可视化](https://www.lianxh.cn/news/7fd81b4dfa2d2.html)
  - [Stata绘图：唯美的函数图-自定义水平附加线和竖直附加线](https://www.lianxh.cn/news/c8d000f77cfc7.html)
  - [Stata绘图：复现组间均值差异图](https://www.lianxh.cn/news/5242699e2c424.html)
  - [Stata绘图：环形柱状图-实时全球新冠确诊人数](https://www.lianxh.cn/news/b33c479a0a6de.html)
  - [Stata可视化：能用图形就不用表格](https://www.lianxh.cn/news/0edc0cc509220.html)
  - [Stata绘图：绘制华夫饼图-waffle](https://www.lianxh.cn/news/beb993d5b649e.html)
  - [Stata绘图：绘制美观的散点图-superscatter](https://www.lianxh.cn/news/b8ca39e205d86.html)
  - [Stata绘图：如何更高效的绘制图形](https://www.lianxh.cn/news/07bfc4c9db38b.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;