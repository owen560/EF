&emsp;

> **作者**：柯有聪（中山大学）  
> **E-Mail:** <koyt@mail2.sysu.edu.cn> 

&emsp;

# Stata绘图：marimekko-正负异色柱形图

[toc]


# **1. 引言**

本文主要介绍了正负异色柱形图及其绘制方法，以及在Stata中的实现。

# **2. 正负异色柱形图**

柱形图，又称长条图、柱状统计图、条图、条状图、棒形图，是一种以长方形的长度为变量的统计图表。柱形图用来比较两个或以上的价值（不同时间或者不同条件），只有一个变量，通常利用于较小的数据集分析。长条图亦可横向排列，或用多维方式表达。

与柱形图相似，正负异色柱形图也是以长方形的长度为变量的统计图表。与之不同的是，正负异色柱形图可以分别将正值和负值互相颠倒显示，并辅以不同的颜色，如下图所示。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/stata正负异色柱形图_Fig1_正负异色柱形图_柯有聪.png)

本文介绍的命令所作的柱形图有别于一般的正负异色柱形图，通过该命令可以实现双变量的正负异色柱形图绘制。如下图所示，长方形的纵长度为change变量，而横长度为population变量，长方形的形状可以更直观的展示数据的特征而无需额外增加更多的柱形图。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/stata正负异色柱形图_Fig2_marimekko正负异色柱形图_柯有聪.png)

# **3. 代码介绍**

`marimekko` 是由 Asjad Naqvi 提供的绘制双变量正负异色柱形图的命令。

双变量正负异色柱形图实际上是一个马赛克图 ( Mosaic Plot ) ，又被称为 Marimekko Plot 或 Mekko Plot，这些名称以芬兰纺织品生产商 Marimekko 的名字命名，这源于他们的印花上的相似而独特的设计，`marimekko` 命令也因此得名。

## 3.1 安装方法

`marimekko` 是外部命令，可使用如下命令进行安装

```Stata 
ssc install marimekko
```

或进行查找安装

```Stata
findit marimekko
```

## 3.2 语法结构

```Stata
marikmekko y x [if] [in], label(varname) [,options]
```

## 3.3 主要选项

* `label(variable)` 表示以该变量定义标签，此项为必填项

* `sort(variable)` 表示以该变量定义排序顺序

* `reverse` 表示逆序

* `colorp(color)` 和 `colorn(color)` 表示 y 轴变量为正值和负值的条形图的颜色，可以填写色彩名或RGB值，列如 "255 255 0"

* `lcolor(color)` 或 `lc(value)` 表示外边框颜色，默认为白色

* `lwidth(color)` 或 `lw(value)` 表示外边框宽度，默认为 0.1

* `mlabsize(value)` 或 `mlabs(value)` 表示标签文字的粗细，默认为 1.6

* `mlabangle(value)` 或 `mlaba(value)` 表示标签文字的角度，默认为 0 （即与 x 轴平行）

* `mlabcolor(value)` 或 `mlabc(value)` 表示标签文字的颜色，默认为主题的默认值

* `mlabgap(value)` 或 `mlabg(value)` 表示标签与条形图之间的距离，默认为 0.5

* ` scheme(string)` 表示加载自定义主题，主题可参考 `schemepack` 命令

* 以下选项与 `graph twoway` 命令的选项相同

  共包含 `xlabel()`、`ylabel()`、`xtitle()`、`ytitle()`、`xsize()`、`ysize()`、`title()`、`subtitle()`、`note()` 和 `name()`，具体子选项可参考 `twoway_options` 命令


## 3.4 Stata实操范例

以中国第七次 ( 2020年 ) 人口普查数据为例，从 Gitee 仓库导入数据：

```Stata
use "https:\\gitee.com\arlionn\EF\2022\柯有聪\demo_marimekko_pop_grp_nmr_rgdp_rgdppc.dta?raw=true", clear
```

设置图表主题为 white_tableau，设置字体为 Arial Narrow：

```Stata
ssc install schemepack, replace
set scheme white_tableau
graph set window fontface "Arial Narrow"
```

将人口数据缩小一万倍：

```Stata
replace pop=pop/10000
```

绘制纵坐标为各省人口增长率，横坐标为各省人口总数的正负异色柱形图 ( 其中 **grp** 为各省人口增长率，**pop** 为各省人口总数，**region** 为省名 ) ：

```Stata
marimekko grp pop, label(region)             ///
    sort(grp)                                /// 将柱形图按照各省人口由小到大进行排序
	colorp(254 226 218) colorn(255 175 175)  /// 设置柱形图颜色
    lc(gs8) lw(0.1)                          /// 设置外边框颜色和宽度
    mlabs(2) mlaba(45) mlabg(0) mlabc(black) /// 设置标签文字的粗细、角度、颜色和距离
	ylabel(-20(5)25,labsize(2.5))            /// 设置纵坐标格式
	xlabel(,labsize(2.5))                    /// 设置横坐标格式
    ytitle("人口增长率/%", size(2.5))        /// 设置纵坐标标签
	xtitle("人口/万", size(2.5))             /// 设置横坐标标签
	xsize(3) ysize(2)                        // 设置图表比例
```

输出图表如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/stata正负异色柱形图_Fig3_2020人口增长率和人口总数正负异色柱形图_柯有聪.png)

绘制纵坐标为各省人口净迁移率、横坐标为各省人均地区生产总值的正负异色柱形图 ( 其中 **nmr** 为各省人口净迁移率，**rgdppc** 为各省人均地区生产总值 ) ：

```Stata
marimekko nmr rgdppc, label(region) ///                               
    sort(nmr) ///                                                  
	colorp(255 245 227) colorn(250 196 169) ///                         
    lc(gs8) lw(0.1) ///                                               
    mlabs(2) mlaba(45) mlabg(0) mlabc(black) ///                  
	ylabel(,labsize(2.5)) xlabel(,labsize(2.5)) ///                                         
    ytitle("人口净迁移率/%", size(2.5)) xtitle("人均地区生产总值/元", size(2.5)) ///     
	xsize(3) ysize(2)
```

输出图表如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/stata正负异色柱形图_Fig4_人口净迁移率和人均地区生产总值正负异色柱形图_柯有聪.png)

可以看到2020年北京、天津、上海、江苏、浙江、福建、广东等地主要人口迁入地的人口净迁移率率显著为正，特别是上海、浙江、北京、广东已成为人口高强度迁入地区，而且人口增长率也位居前列。

而作为东北三省的吉林、辽宁和黑龙江等地的人口净迁移率和人口增长率均为负增长。人均地区产值较高的上海、北京、广东、江苏等地人口净迁移率同样位居前列，不难发现省际人口迁出规模与迁入规模的分布比较集中，主要表现在人口向东部沿海等较发达地区迁移。

# **4. 结语**

本篇推文主要介绍了正负异色柱形图和 `marimekko` 命令的实际操作，`marimekko` 命令为传统柱形图的延申应用提供了帮助，并且极大简化了作图操作。与之类似，`spineplot` 命令提供了绘制马赛克图的方法，可以进一步美化正负异色柱形图的绘制。

# **5. 参考文献**
- [百度百科-柱形图](https://baike.baidu.com/item/%E6%9F%B1%E5%BD%A2%E5%9B%BE/10816534)
- [stata-marimekko](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fasjadnaqvi%2Fstata-marimekko)
- [Stata graphs: Mosaic (Marimekko) plots](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fasjadnaqvi%2Fstata-marimekko)