# pystacked: Stacking generalization and machine learning in Stata
&emsp;
> **作者**：冯乔 (中山大学)     
> **邮箱**：<fengq33@mail2.sysu.edu.cn>  

「**Source** [pystacked: Stacking generalization and machine learning in Stata](https://arxiv.org/pdf/2208.10896.pdf)」

## 概要：
机器学习中，堆叠 (stacking，又叫做meta ensembling) 是利用多个模型的输出集成起来产生新模型的一种模型集成技术。由于集成后模型的平滑特性，通常集成后的模型能够在性能上优于任何一个用于集成的基础模型。并且集成的模型侧重表现好的模型，不信任 (discredit) 表现不好的模型。因此，stacking 方法对于差别很大的基础模型，集成的有效性更佳。

&emsp;

 
pystacked 通过 scikit-learn (借助 sklearn.ensemble.StackingRegressor 和 sklearn.ensemble.StackingClassifier) 实现堆叠回归 (Wolpert，1992)，将多个有监督机器学习器组合成一个元学习器。目前支持的基分类器有线性回归、逻辑回归 (logit)、套索算法 (lasso)、岭回归 (ridge), 弹性网络 (elastic net), 线性支持向量机 (linear SVM)、梯度提升 (gradient boosting)、神经网络 (其中的MLP)。

&emsp;

pystacked 也可以使用单个基分类器。

&emsp;

pystacked 至少需要 Stata 16(或更高)，安装 Python 和 scikit-learn (0.24或更高)。

[[TOC]]

## 一、数学原理
### 1.1堆叠
本文主要介绍对于回归与二分类问题的堆叠方法，分成堆叠回归与堆叠分类两部分介绍。

&emsp;

#### 1.1.1堆叠回归
对于一个多变量的回归问题，堆叠的思路是用一个最终评估器 (1级) 来结合数个基分类器 (0级) 进行预测。当模型纳入较多且差别较大的基分类器，这种方法更能捕捉数据中的特征。对于基分类器来说，典型选择是正则化回归或集成方法，例如随机森林或梯度提升。

&emsp;

在堆叠的第一步，对于每个基分类器 $j$ ，我们得到交叉验证的预测值 $\hat{y}^{(j)}_i$ 。如果不使用交叉验证，堆叠会给过拟合的基分类器更高权重，因此交叉验证是必要的。

&emsp;

第二步，用实际值 $y_i$ 作为输出值，交叉验证中预测的 $\hat{y}^{(1)}_i$，$\hat{y}^{(2)}_i$，……，$\hat{y}^{(j)}_i$ 作为预测值，拟合出最终评估器。对于最终评估器来说，典型的选择是有约束的最小二乘法，限制堆叠权重为非负数并且和为 1 。这种限制有助于将最终评估器解释为基分类器的加权。

&emsp;

算法如下：

交叉验证：

把数据随机地分成大小相等的 $K$ 组(即$K$折)，每次取一组作为测试集，用其他 $K-1$ 组做训练集，对测试集进行预测。

接着，拟合最终评估器，默认是非负最小二乘法(NNLS)。
$min\sum_{i=1}^n({y_i-\sum_{j=1}^J{w_j\hat{y}^{(j)}_i}})^2$

$s.t. w_j\geq0,  \sum_{j=1}^Jw_j=1$

堆叠回归的预测值为 $\hat{y}^{*}_i=\sum_j{w_j\hat{y}^{(j)}_i}$。

&emsp;



#### 1.1.1堆叠分类
pystacked 支持用于二分类问题， $y_i$ 取 0 或 1 ，除了基分类器输出的是分类的概率，其他部分与堆叠回归相同。

&emsp;

### 1.2基分类器
接下来介绍基分类器可以使用的的算法，这些基分类器可以用于堆叠，也可以单独使用。线性回归、逻辑回归与回归树不做赘述，以下是其他的算法。

&emsp;

正则化回归对系数的大小施加惩罚来控制过度拟合。套索 (lasso) 惩罚系数的绝对大小，而岭回归 (ridge) 惩罚系数的平方和。两种方法都将系数往0进行缩小。弹性网结合了套索和岭回归惩罚。对于套索算法，pystacked 还支持选择 AIC 或 BIC 惩罚。使用 AIC 或 BIC 的优点是计算强度低于交叉验证。

&emsp;

随机森林依赖于大量的回归或决策树。随机森林的预测是单个树的平均。随机森林的重要参数是：树的数量 ((n_estimators())，单一树的最大深度 (max_depth()), 每片叶子的最小观测数 (min_samples_leaf()), 每次分组时考虑的特征数量 (max_features()) 和bootstrap样本的数量 (max_samples())。


&emsp;

梯度提升树同样依赖于大量的树。不同于随机森林，这些树根据当前模型的残差有顺序地进行拟合。除了树的数量 (n_estimators()) 是重要参数外，学习率 (learning_rate()) 决定了最新的树对与整体模型有多大影响.

&emsp;

支持向量机 (SVM) 通过一个超平面对观测值进行分类。超平面是基于最大距离(使得观测值离平面距离最大)来选择的，同时允许一定程度的分类错误。C参数 (C()) 控制着分类错误的频率和程度。超平面可以是线性的或者用核函数 (kernel) 拟合的。SVM 同样可以用于回归。

&emsp;

前馈神经网络由输入层-隐含层-输出层组成，每一个隐含层包含多个节点，通过激活函数将信号传输到下一层。重要参数是激活函数的选择 (activation()) , 隐含层的大小和数量 (hidden_layer_sizes())。

&emsp;

### 二、句法规则
#### 2.1句法规则1
第一个句法，使用 methods(string) 来选择基分类器，string 是基分类器的列表。最多可以选择10个基分类器，pipe*(string) 用于预测器的预处理。xvars*(predictors) 允许对分类器指定采用的变量列表。
```
pystacked depvar predictors [if]
[in], [,methods(string) cmdopt1(string)
cmdopt2(string) ... cmdopt10(string) pipe1(string) pipe2(string) ...
pipe10(string) xvars1(predictors) xvars2(predictors) ...
xvars10(predictors) general options ]
```

&emsp;

#### 2.2句法规则2
第二个句法更加灵活，对于基分类器的数量没有限制(虽然还要考虑计算的复杂程度)，基分类器通过 method(string) 添加，之后是对特定的分类器进行设置，用“｜｜”分隔。
```
pystacked depvar [indepvars] || method(string) opt(string) pipe(string) xvars(predictors) [|| method(string) opt(string) pipe(string) xvars(predictors) ... || ][if][in][, general options]  
```

&emsp;

#### 2.3预测结果输出
获得预测值：
```
predict type newname [if][in][, pr xb ]
```
获得每一个基分类器的拟合值：
```
predict type stub [if][in][, transform ]
```
输出表格(如果是回归问题，报告预测的均方预测误差MSPE，如果是分类问题，输出混淆矩阵)：
```
pystacked [, table holdout[(varname)] ]
```
输出图片展示模型性能：
```
pystacked [, graph[(options)] lgraph[(options)] histogram
holdout[(varname)] ]
```

#### 2.4分类器一览
| method()| type()|  description |
|----------|:-----:|------:|
|ols |regress |Linear regression |
|logit |class| Logistic regression |
|lassoic| regress| Lasso with AIC/BIC penalty |
|lassocv |regress |Lasso with CV penalty |
||class| Logistic lasso with CV penalty |
|ridgecv| regress| Ridge with CV penalty| 
||class| Logistic ridge with CV penalty |
|elasticcv |regress| Elastic net with CV penalty |
||class |Logistic elastic net with CV| 
|svm| regress| Support vector regression |
||class| Support vector classification| 
|gradboost| regress |Gradient boosting regression |
||class| Gradient boosting classification |
|rf |regress |Random forest regression|
||class| Random forest classification |
|linsvm |class| Linear SVC |
|nnet |regress| Neural net regression |
||class| Neural net classification |

#### 2.5数据预处理
sklearn使用pipelines来对输入数据进行预处理，可以填充缺失值、创建交互项、进行标准化。下图为可用的pipelines。

 
| pipe*() | Description| 
|----------|:-----:|
|stdscaler |Standardize to mean zero and unit variance (default for regularized regression)|
|nostdscaler| Overwrites default standardization for regularized regression|
|stdscaler0| Standardize to unit variance|
|sparse| Transform to sparse matrix |
|onehot |Create dummies from categorical variables|
|minmaxscaler| Scale to 0-1 range |
|medianimputer| Median imputation |
|knnimputer| KNN imputation |
|poly2| Add 2nd-order polynomials |
|poly3 |Add 3rd-order polynomials |
|interact| Add interactions |

&emsp;


### 三、应用
#### 3.1 单一基分类器
这里采用了加州房屋价格数据，以 75/25 的比例将数据随机划分为训练集和测试集。任务是通过一系列房价特征数据预测房屋价格。
首先准备数据：
```
. clear all
. use https://statalasso.github.io/dta/cal_housing.dta, clear
. set seed 42
. gen train=runiform()
. replace train=train<.75
(20,640 real changes made)
. replace medh = medh/10e3
variable medhousevalue was long now double
(20,640 real changes made)
. label var medh
```

&emsp;

采用 pystacked 的梯度提升回归树，并且保存样本外预测值：设置 type(reg)，method(gradboost)
```
. pystacked medh longi-medi if train, type(reg) methods(gradboost)
. predict double yhat_gb1 if !train
```

&emsp;

由于是单一基回归器，可见权重为1。
|Method|Weight|
|----------|:-----:|
|gradboost| 1.0000000|

&emsp;

#### 3.2堆叠回归
还以同样的数据为例，采用多个基分类器：
(1)线性回归
(2)套索+交叉验证惩罚
(3)套索+二阶多项式+交互作用
(4)默认参数的随机森林
(5)梯度提升，学习率= 0.01，树的数量= 1000
```
. pystacked medh longi-medi if train, ///
> type(regress) ///
> methods(ols lassocv lassocv rf gradboost) ///
> pipe3(poly2) cmdopt5(learning_rate(0.01) ///
> n_estimators(1000))

```

&emsp;

输出堆叠权重如下：
|Method|Weight|
|----------|:-----:|
|ols| 0.0000000|
|lassocv|0.0000000|
|lassocv| 0.4692388|
|rf |0.2505079|
|gradboost|0.2802533|

使用第二种句法规则，实现相同的功能：
```
. set seed 42
. pystacked medh longi-medi || ///
> m(ols) || ///
> m(lassocv) || ///
> m(lassocv) pipe(poly2) || ///
> m(rf) || ///
> m(gradboost) opt(learning_rate(0.01) n_estimators(1000)) ///
> if train, type(regress)
```
输出堆叠权重相同。

&emsp;

输出堆叠预测值与每个基分类器的预测值(使用 transf 选项)：
```
. predict double yhat, xb
. predict double ytrans, transf
. list yhat ytrans* if _n <= 5
```

&emsp;

绘图(横轴为观测值，纵轴为预测值，绘制散点图，红线为 45 度线)
```
. pystacked, graph holdout
 Number of holdout observations: 5192
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/pystacked_%E5%9B%BE1_%E5%86%AF%E4%B9%94.png)



#### 3.3堆叠分类
pystack 可用于二元的分类问题。这里采用了 UCI 机器学习库的垃圾邮件识别数据。首先，载入数据并且分成训练集和测试集。
```
. insheet using ///
> https://archive.ics.uci.edu/ml/machine-learning-databases/spambase/spambase.data, ///
> clear comma
(58 vars, 4,601 obs)
. set seed 42
. gen train=runiform()
. replace train=train<.75
(4,601 real changes made)
```

&emsp;

采用五个基分类器：
(1)逻辑回归+二次项+交互项
(2)梯度提升，600个分类树
(3)梯度提升，1000个分类树
(4)神经网络，两个隐含层，每层五个节点
(4)神经网络，一个隐含层，五个节点
```
pystacked v58 v1-v57 || ///
> m(logit) pipe(poly2) || ///
> m(gradboost) opt(n_estimators(600)) || ///
> m(gradboost) opt(n_estimators(1000)) || ///
> m(nnet) opt(hidden_layer_sizes(5 5)) || ///
> m(nnet) opt(hidden_layer_sizes(5)) || ///
> if train, type(class) njobs(8)
```

&emsp;

输出混淆矩阵：
```
. pystacked, table holdout
```

&emsp;

绘制 ROC 曲线(受试者工作特征曲线)，y轴为灵敏性，x轴为特异性，曲线下方的面积 (AUC) 是衡量分类效果的重要指标，面积为 0.5 为随机分类，识别能力为 0，面积越接近于 1 识别能力越强，面积等于 1 为完全识别。
```
. pystacked, graph(subtitle(Spam data)) ///
> lgraph(plotopts(msymbol(i) ///
> ylabel(0 1, format(%3.1f)))) ///
> holdout
Number of holdout observations: 1133
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/pystacked_%E5%9B%BE2_%E5%86%AF%E4%B9%94.png)


&emsp;

### 四、总结
pystacked 在 Stata 中调用 python 第三方包 sklearn 实现机器学习回归与分类，既可以实现单个分类器，也可实现堆叠回归与分类，并且也实现了模型相关评价指标的输出与可视化，使得解决机器学习问题更加方便快捷。


### 参考文献：
- Achim Ahrens, Christian B. Hansen. 2021. pystacked: Stacking generalization and machine learning in Stata. [-PDF-](https://arxiv.org/pdf/2208.10896.pdf)
