
# 事件研究法理论部分

注意： 
- 翻译时不要英文删除原文，整段保留
- 把握不准的地方用 `==标黄文字==` 格式标注出来，显示效果为：==标黄文字==

&emsp;

---

**目录**
[TOC]

---

&emsp;


>Source: Cowan, Arnold R. Eventus software, version 8.0. (Cowan Research LC, Ames, Iowa, 2005.), Eventus User's Guide. Appendix A: Technical Reference, [PDF](http://www.eventstudy.com/Eventus-Guide-8-Public.pdf) 


# A.1 Event Study Benchmarks
## Benchmarks using a separate estimation period
Event study benchmark models using a separate estimation period include the market model, the market-adjusted return model, the comparison period mean-adjusted return model, the raw return model, and, when the EVTSTUDY statement option TWOSTEP is in effect, the Fama-French three- and four-factor models and custom factor models. The application of each benchmark model requires two time series of return data for each security-event: an estimation period for estimating the benchmark parameters (including standard deviation), and an event period for computing and testing the abnormal returns. Typically the estimation period and event period do not overlap, in order to avoid potentially biasing the parameter estimates.

### Market model
Assume that security returns follow a single factor market model,
$$
R_{j t}=\alpha_j+\beta_j R_{m t}+\epsilon_{j t},
$$
where $R_{j t}$ is the rate of return of the common stock of the $j^{\text {th }}$ firm on day $t ; R_{m t}$ is the rate of return of a market index on day $t ; \epsilon_{j t}$ is a random variable that, by construction, must have an expected value of zero, and is assumed to be uncorrelated with $R_{m t}$, uncorrelated with $R_{k t}$ for $k \neq j$, not autocorrelated, and homoskedastic. $\beta_j$ is a parameter that measures the sensitivity of $R_{j t}$ to the market index. Define the abnormal return (or prediction error) for the common stock of the $j^{\text {th }}$ firm on day $t$ as:
$$
A_{j t}=R_{j t}-\left(\hat{\alpha}_j+\hat{\beta}_j R_{m t}\right),
$$
where the coefficients $\hat{\alpha}_j$ and $\hat{\beta}_j$ are ordinary least squares estimates of $\alpha_j$ and $\beta_j$.

The average abnormal return (or average prediction error) $A A R_t$ is the sample mean:
$$
A A R_t=\frac{\sum_{j=1}^N A_{j t}}{N},
$$
where $t$ is defined in trading days relative to the event date (e.g. $t=-60$ means 60 trading days before the event).

Over an interval of two or more trading days beginning with day $T_1$, and ending with $T_2$, the cumulative average abnormal return is
$$
C A A R_{T_1, T_2}=\frac{1}{N} \sum_{j=1}^N \sum_{t=T_1}^{T_2} A_{j t} .
$$
Over an interval of two or more trading days beginning with day $T_1$, and ending with $T_2$, the buy-and-hold abnormal return is
$$
\begin{aligned}
B_{H A R_{j, T_1, T_2}}=\\
{\left[\prod_{t=T_1}^{T_2}\left(1+R_{j t}\right)-1\right] } &-\left[\left(1+\hat{\alpha}_j\right)^{\left(T_2-T_1+1\right)}-1\right] \\
&-\hat{\beta}_j\left[\prod_{t=T_1}^{T_2}\left(1+R_{m t}\right)-1\right] .
\end{aligned}
$$
The average compounded abnormal return is is
$$
A C A R_{T_1, T_2}=\frac{1}{N} \sum_{j=1}^N B H A R_{j, T_1, T_2} .
$$
When the REQUEST statement option SHORT is in effect, Eventus computes the buy-and-hold raw return using the daily (or monthly, etc.) returns as reported by the database - that is, the long position returns - then uses the negative of the buy-and-hold raw return in any buy-and-hold abnormal return calculations.

### Market model with Scholes-Williams beta estimation
When the SW option appears on the EVTSTUDY statement, Eventus reports market model results using betas estimated by both ordinary least squares and the method of Scholes and Williams (1977). The Scholes-Williams beta estimator is
$$
\hat{\beta}_j^{\star}=\frac{\hat{\beta}_j^{-}+\hat{\beta}_j+\hat{\beta}_j^{+}}{1+2 \hat{\rho}_m},
$$
where $\hat{\beta}_j^{-}$is the OLS slope estimate from the simple linear regression of $R_{j t}$ on $R_{m t-1}, \hat{\beta}_j^{+}$is the OLS estimate from the regression of $R_{j t}$ on $R_{m t+1}$, and $\hat{\rho}_m$ is the estimated first-order autocorrelation of $R_m$. As in OLS, the intercept estimator forces the estimated regression line through the sample mean:
$$
\hat{\alpha}_j^{\star}=\overline{R_{j E s t}}-\hat{\beta}_j^{\star} \overline{R_{m E s t}} .
$$
$\overline{R_j}$ is the mean return of stock $j$ over the estimation period and $\overline{R_{m E s t}}$ is the mean market return over the estimation period. 


> footnote: 
`Eventus` applies the simplifying assumption that the use of Scholes-Williams estimates does not affect the formula for $s_{A_{j t}}^2$ below. Analytically this assumption is not strictly correct, but simulation results obtained by the author show that tests using the assumption are well specified.

### Market model with GARCH or EGARCH estimation
The EVTSTUDY statement option GARCH invokes a single factor market model with GARCH $(1,1)$ errors; the EGARCH option invokes exponential GARCH or EGARCH $(1,1)$ (Nelson, 1990) errors:
$$
R_{j t}=\alpha_j+\beta_j R_{m t}+\epsilon_{j t},
$$
where $\epsilon_{j t} \mid \Psi_{t-1} \sim\left(0, h_{j t}\right)$ and $\Psi_{t-1}$ denotes all information available at time $t-1$. The conditional variance in the GARCH case is
$$
h_{j t}=\omega_j+\delta_j h_{j t-1}+\gamma_j \epsilon_{j t-1}^2,
$$
with $\omega_j>0, \gamma_j>0, \delta_j \geq 0$, and $\gamma_j+\delta_j<1$. In the EGARCH case,
$$
\log h_{j t}=\omega_j+\delta_j \log h_{j t-1}+\gamma_j\left|z_{j t-1}\right|+\phi_j z_{j t-1}
$$
where $z_{j t}=\epsilon_{j t} / \sqrt{h_{j t}}$. The parameters are estimated by maximum likelihood.

### Fama-French three-factor model
The option combination FAMAFRENCH TWOSTEP selects the Fama-French (1993) three-factor model as the return-generating process using a separate estimation period. The model is:
$$
R_{j t}=\alpha+\beta_j R_{m t}+s_j S M B_t+h_j H M L_t+\epsilon_{j t} .
$$
where $R_{j t}$ is the rate of return of the common stock of the $j^{t h}$ firm on day $t$; $R_{m t}$ is the rate of return of a market index on day $t ; S M B_t$ is the average return on small market-capitalization portfolios minus the average return on three large market-capitalization portfolios; $H M L_t$ is the average return on two high book-to-market equity portfolios minus the average return on two low book-to-market equity portfolios; $\epsilon_{j t}$ is a random variable that, by construction, must have an expected value of zero, and is assumed to be uncorrelated with $R_{m t}$, uncorrelated with $R_{k t}$ for $k \neq j$, not autocorrelated, and homoskedastic. See Fama and French (1993) for a detailed description of $S M B_t$ and $H M L_t . \beta_j$ is a parameter that measures the sensitivity of $R_{j t}$ to the excess return on the market index; $s_j$ measures the sensitivity of $R_{j t}$ to the difference between small and large capitalization stock returns; and $h_j$ measures the sensitivity of $R_{j t}$ to the difference between value and growth stock returns.

Define the abnormal return (or prediction error) for the common stock of the $j^{\text {th }}$ firm on day $t$ as:
$$
A_{j t}=R_{j t}-\left(\hat{\alpha}_j+\hat{\beta}_j R_{m t}+\hat{s}_j S M B_t+\hat{h}_j H M L_t\right),
$$
where the coefficients $\hat{\alpha}_j, \hat{\beta}_j, \hat{s}_j$ and $\hat{h}_j$ are ordinary least squares estimates of $\alpha_j, \beta_j, s_j$ and $h_j$.

The average abnormal return, cumulative abnormal return, buy-and-hold abnormal return and related concepts are analogous to those defined in the market model section above.

### Fama-French-momentum four-factor model
The option combination FAMAFRENCH MOMENTUM TWOSTEP selects the FamaFrench (1993) three-factor model, augmented by the momentum factor as suggested by Carhart (1997), as the return-generating process using a separate estimation period. The model is:
$$
R_{j t}=\alpha+\beta_j R_{m t}+s_j S M B_t+h_j H M L_t+u_j U M D_t+\epsilon_{j t} .
$$
where $R_{j t}$ is the rate of return of the common stock of the $j^{\text {th }}$ firm on day $t$; $R_{m t}$ is the rate of return of a market index on day $t ; S M B_t$ is the average return on small market-capitalization portfolios minus the average return on three large market-capitalization portfolios; $H M L_t$ is the average return on two high book-to-market equity portfolios minus the average return on two low book-to-market equity portfolios; $U M D_t$ is the average return on two high prior return portfolios minus the average return on two low prior return portfolios. $\epsilon_{j t}$ is a random variable that, by construction, must have an expected value of zero, and is assumed to be uncorrelated with $R_{m t}$, uncorrelated with $R_{k t}$ for $k \neq j$, not autocorrelated, and homoskedastic. $\beta_j$ is a parameter that measures the sensitivity of $R_{j t}$ to the excess return on the market index; $s_j$ measures the sensitivity of $R_{j t}$ to the difference between small and large capitalization stock returns; $h_j$ measures the sensitivity of $R_{j t}$ to the difference between value and growth stock returns; and $u_j$ measures the sensitivity of $R_{j t}$ to the difference between high prior return stock returns and low prior return stock returns.

Define the abnormal return (or prediction error) for the common stock of the $j^{\text {th }}$ firm on day $t$ as:
$$
A_{j t}=R_{j t}-\left(\hat{\alpha}_j+\hat{\beta}_j R_{m t}+\hat{s}_j S M B_t+\hat{h}_j H M L_t+\hat{u}_j U M D_t\right)
$$
where the coefficients $\hat{\alpha}_j, \hat{\beta}_j, \hat{s}_j, \hat{h}_j$ and $\hat{u}_j$ are ordinary least squares estimates of $\alpha_j, \beta_j, s_j h_j$ and $u_j$.

The average abnormal return, cumulative abnormal return, buy-and-hold abnormal return and related concepts are analogous to those defined in the market model section above.

### Market adjusted returns model
Market adjusted returns are computed by subtracting the observed return on the market index for day $t, R_{m t}$, from the rate of return of the common stock of the $j^{\text {th }}$ firm on day $t$ :
$$
A_{j t}=R_{j t}-R_{m t} .
$$
The definitions of the average abnormal return, cumulative average abnormal return and average compounded abnormal return are analogous to those for market model abnormal returns above.

### Comparison period mean adjusted returns
Comparison period mean adjusted returns are computed by subtracting the arithmetic mean return of the common stock of the $j^{\text {th }}$ firm computed over the estimation period, $\overline{R_j}$, from its return on day $t$ :
$$
A_{j t}=R_{j t}-\overline{R_j} .
$$
The definitions of the average abnormal return, cumulative average abnormal return and average compounded abnormal return are analogous to those for market model abnormal returns above.

## A.benchmark model using combined estimation and event periods

In contrast to the two-step approach, the benchmark method in this section combines the estimation and event periods into a single time series for parameter estimation and testing.
Event parameter approach
Assume that security returns follow a conditional single factor market model,
$$
R_{j t}=\alpha_j+\beta_j R_{m t}+\sum_{t=T_p r e}^{T_p o s t} \gamma_{j k t} d_{j k t}+\epsilon_{j t},
$$
where $d_{j k t}$ is a dummy variable equal to one on the $k^{t h}$ day or month in the event period; $\gamma_{j k}$ is the estimated abnormal return of security $j$ on day or month $k$. As in the conventional market model approach, $\epsilon_{j t}$ is a mean zero error term that is assumed uncorrelated with $R_m$ and $d_{j k t}$. However, it is allowed to be potentially cross-correlated; it is not necessarily true that $E\left[\epsilon_{j t}, \epsilon_{i t}\right]=0$. In the case of cross-correlation, joint generalized least squares, also known as seemingly unrelated regressions (SUR) estimation, potentially can provide potentially more efficient estimation of the system of equations for a given sample. See Ingram and Ingram (1993) and Thompson (1985).

## Benchmarks with no separate estimation period

### Fama-French calendar time portfolio regressions

The EVTSTUDY statement option FAMAFRENCH, when neither the TWOSTEP option nor the IRATS option is used, invokes a calendar-time model using FamaFrench (1993) factors:
$$
R_{p t}-R_{f t}=\alpha+\beta\left(R_{m t}-R_{f t}\right)+s S M B_t+h H M L_t+\epsilon_{p t},
$$
where $R_{f t}$ is the one-month Treasury bill rate and other symbols are as defined above. A portfolio is formed for each date that includes each sample firm having the date in its event period. In an event study using monthly data, portfolios are formed monthly in calendar time. Portfolios are equally weighted unless the VALUEWEIGHTSAMPLE option appears on the EVTSTUDY statement. The regression is estimated on portfolio returns. The estimate of the average abnormal return is $\alpha$. If neither the GMM or WLS option is specified, Eventus uses ordinary least squares to estimate the model and test the null hypothesis $\alpha=0$.

If the MOMENTUM option is specified, the model includes a fourth factor as described in the section "Fama-French-momentum four-factor model" on $76 .$

### Ibbotson's "Returns Across Time and Securities"
The EVTSTUDY statement option IRATS invokes a simplified version of the Returns across Time and Securities, or RATS, model (Ibbotson, 1975):
$$
R_{j t}=\alpha_t+\beta_t R_{m t}+\epsilon_{j t},
$$
Unlike the conventional market model, the RATS regression is estimated for each period in event time (day, month, etc.) The estimate of the mean abnormal return is $\alpha_t$. Eventus uses ordinary least squares to estimate the model and test the null hypothesis $\alpha_t=0$. For a window, Eventus reports the sum of the mean abnormal returns as the mean cumulative abnormal return and tests its significance assuming time-series independence. Therefore, the denominator of the test statistic for a window is the square root of the sum of the squares of the test-statistic denominators for the individual days or months that make up the window.

### Fama-French factors with Ibbotson's "Returns Across Time and Securities"

The EVTSTUDY statement option combination FAMAFRENCH IRATS invokes a modified version of the Returns across Time and Securities, or RATS, model (Ibbotson, 1975) using Fama-French (1993) factors:
$$
R_{j t}-R_{f t}=\alpha_t+\beta_t\left(R_{m t}-R_{f t}\right)+s_t S M B_t+h_t H M L_t+\epsilon_{j t} .
$$
The regression is estimated for each period in event time (day, month, etc.) The estimate of the abnormal return is $\alpha_t$. Eventus uses ordinary least squares to estimate the model and test the null hypothesis $\alpha_t=0$.




%todo test

# A.2 Event Study Test Statistics

## Patell test

The PATELL option on the EVTSTUDY statement invokes the Patell (1976) test. It is also the default when the user does not select another parametric test and the event study does not use a multi-factor model or buy-and-hold returns. The literature also refers to the Patell test as a standardized abnormal return test or a test assuming cross-sectional independence. Many published studies use the Patell test (see, for example, Linn and McConnell, 1983; Schipper and Smith, 1986; and Haw, Pastena and Lilien, 1990).
Event studies centered on a single date
Under the null hypothesis, each $A_{j t}$ has mean zero and variance $\sigma_{A_{j t}}^2$. The maximum likelihood estimate of the variance is,
$$
s_{A_{j t}}^2=s_{A_j}^2\left[1+\frac{1}{M_j}+\frac{\left(R_{m t}-\overline{R_{m E s t}}\right)^2}{\sum_{k=E_1}^{E_2}\left(R_{m k}-\overline{R_{m E s t}}\right)^2}\right]
$$
where
$$
s_{A_j}^2=\frac{\sum_{k=E_1}^{E_2} A_{j k}^2}{M_j-2},
$$

$R_{m t}$ is the observed return on the market index on day $t, \overline{R_{m E s t}}$ is the mean market return over the estimation period and $M_j$ is the number of nonmissing trading day returns in the interval $E_1$ through $E_2$ used to estimate the parameters for firm $j$.

Define the standardized abnormal return (or standardized prediction error) as
$$
S A R_{j t}=\frac{A_{j t}}{s_{A_{j t}}} .
$$
Under the null hypothesis, each $S A R_{j t}$ follows a Student's $t$ distribution with $M_j-2$ degrees of freedom. Summing the $S A R_{j t}$ across the sample, we obtain
$$
T S A R_t=\sum_{j=1}^N S A R_{j t} .
$$
The expected value of $T S A R_t$ is zero. The variance of $T S A R_t$ is
$$
Q_t=\sum_{j=1}^N \frac{M_j-2}{M_j-4}
$$
The test statistic for the null hypothesis that $C A A R_{T_1, T_2}=0$ is
$$
Z_{T_1, T_2}=\frac{1}{\sqrt{N}} \sum_{j=1}^N Z_{T_1, T_2}^j,
$$
where
$$
Z_{T_1, T_2}^j=\frac{1}{\sqrt{Q_{T_1, T_2}^j}} \sum_{t=T_1}^{T_2} S A R_{j t},
$$
and
$$
Q_{T_1, T_2}^j=\left(T_2-T_1+1\right) \frac{M_j-2}{M_j-4} .
$$
Under cross-sectional independence of the $Z_{T_1, T_2}^j$ and other conditions (see Patell, 1976), $Z_{T_1, T_2}$ follows the standard normal distribution under the null hypothesis.

The Patell test without the SERIAL option assumes that abnormal returns are serially uncorrelated. If abnormal returns are serially uncorrelated, the variance of $C A R_j$ is the sum of the variances of the daily abnormal returns:
$$
s_{C A R_{T_{1 j}, T_{2 j}}^2}^2=s_{A_j}^2\left[L_j+\frac{L_j}{M_j}+\frac{\sum_{t=T_{1 j}}^{T_{2 j}}\left(R_{m t}-\overline{R_{m E s t}}\right)^2}{\sum_{k=1}^{M_j}\left(R_{m k}-\overline{R_{m E s t}}\right)^2}\right] \text {. }
$$
Eventus reports a precision-weighted cumulative average abnormal return when the Patell or other standardized abnormal return test is selected. The precision-weighted average is constructed using the relative weights implied by the definition of $Z_{T_1, T_2}$. Thus, the precision-weighted average will always have the same sign as the corresponding $Z_{T_1, T_2}$. The formula for the precisionweighted average is
$$
\operatorname{PWCAAR}_{T_1, T_2}=\sum_{j=1}^N \sum_{t=T_1}^{T_2} w_j A_{j t},
$$
where
$$
w_j=\frac{\left(\sum_{t=T_1}^{T_2} s_{A_{j t}}^2\right)^{-\frac{1}{2}}}{\sum_{i=1}^N\left(\sum_{t=T_1}^{T_2} s_{A_{i t}}^2\right)^{-\frac{1}{2}}} .
$$
The precision-weighted CAAR fulfills the reporting needs for which researchers sometimes report an average standardized cumulative abnormal return (average $S C A R$ ). The precision-weighted $C A A R$, as a weighted average of the original CARs, preserves the portfolio interpretation that $C A A R$ offers but average $S C A R$ does not.

### TWIN event studies (two firm-specific event dates)
The major difference between TWIN and single date event studies is that TWIN cumulates returns over intervals of security-specific length. Instead of defining a window for return cumulation with reference to a single event date, the window is defined as the period between two event dates. The number of trading days between the two event dates varies from firm to firm.
Let the cumulative abnormal return for firm $j$ be
$$
C A R_{T_{1 j}, T_{2 j}}=\sum_{t=T_{1 j}}^{T_{2 j}} A_{j t},
$$


where $T_{1 j}, T_{2 j}$ are the two event dates specific to firm $j$. Let $L_j$ be the length of the event period in trading days,
$$
L_j=T_{2 j}-T_{1 j}+1 .
$$
The $z$ statistic for testing the significance of $C A R_{T_{1 j}, T_{2 j}}$ is
$$
z_j=\frac{\sum_{t=T_{1 j}}^{T_{2 j}} S A R_{j t}}{\left(L_j \frac{M_j-2}{M_j-4}\right)^{\frac{1}{2}}} .
$$
Assuming cross-sectional and time-series independence, the test statistic for
$$
C A A R=\frac{1}{N} \sum_{j=1}^N C A R_{T_{1 j}, T_{2 j}}
$$
is
$$
z_{C A A R}=N^{-\frac{1}{2}} \sum_{j=1}^N z_j .
$$

### Correction for correlation of abnormal returns
When the SERIAL option appears on the EVTSTUDY statement, Eventus uses a corrected version of the Patell test. The correction affects only windows, not single-period test statistics. Following Mikkelson and Partch (1988), the corrected test statistic for the null hypothesis that $C A A R=0$ is
$$
z_{C A A R}=N^{-\frac{1}{2}} \sum_{j=1}^N \frac{C A R_{T_{1 j}, T_{2 j}}}{s_{C A R_{T_{1 j}, T_{2 j}}}},
$$
where
$$
s_{C A R_{T_{1 j}, T_{2 j}}^2}^2=s_{A_j}^2\left\{L_j\left[1+\frac{L_j}{M_j}+\frac{\left(\sum_{t=T_{1 j}}^{T_{2 j}} R_{m t}-L_j \overline{R_{m E s t}}\right)^2}{\sum_{k=1}^{M_j}\left(R_{m k}-\overline{R_{m E s t}}\right)^2}\right]\right\}
$$

For an event study centered on a single event date, $T_{1 j}, T_{2 j}$ (and $L_j$ if there is no missing return) are equal across firms and the subscript $j$ can be dropped from them.

The corrected test accounts for the fact that within the window, the abnormal returns for each stock are serially correlated. The serial correlation occurs because all the abnormal returns are functions of the same market model intercept and slope estimators. Applications of the corrected test in addition to Mikkelson and Partch (1988) include Mais, Moore and Rogers (1989), Cowan, Nayar and Singh (1990), Mann and Sicherman (1991) and Lee (1992). Simulation evidence of the properties of the corrected and uncorrected Patell tests appears in Karafiath and Spencer (1991, using Monte Carlo experiments) and Cowan (1993, using sampling experiments with CRSP data). Both papers report that the bias in the uncorrected test is small in event windows shorter than 60 days but serious in event windows longer than 100 days. Mikkelson and Partch (1988) acknowledge Craig Ansley for the original derivation of the corrected test statistic in an event study context. For other derivations and discussion, see Cantrell, Maloney and Mitchell (1989) and Sweeney (1991).

When the SERIAL and STDALL options both appear on the EVTSTUDY statement, Eventus uses the following definitions for the standardized abnormal return tests with non-market model abnormal returns. For comparison period mean adjusted returns,
$$
s_{C A R_{T_{1 j}, T_{2 j}}^2}^2=s_{A_j}^2\left(L_j+\frac{L_j^2}{M_j}\right) .
$$
For raw returns and market-adjusted returns, there is no estimation of the mean. Instead, the mean is assumed to be equal to a known constant with probability one. The constant is zero for raw returns and is the realized market index return for market-adjusted returns. Thus,
$$
s_{C A R_{T_{1 j}, T_{2 j}}}^2=s_{A_j}^2\left(L_j\right) .
$$

### Standardized cross-sectional test
Eventus uses the standardized cross-sectional test for market model abnormal returns when you specify STDCSECT on the EVTSTUDY statement. Boehmer, Musumeci and Poulsen (1991) introduce the test and report its empirical properties. The test is the same as the Patell test described above except that there is a final empirical cross-sectional variance adjustment in place of the analytical variance of the total standardized prediction error. For additional discussion of event-date variance increases and related tests, see Sanders and Robins (1991).
For day $t$ in the event period, the test statistic is
$$
z_t=\frac{T S A R_t}{N^{\frac{1}{2}}\left(s_{S A R_{\bullet t}}\right)},
$$
where
$$
s_{S A R_{\boldsymbol{\bullet} t}}^2=\frac{1}{N-1} \sum_{i=1}^N\left(S A R_{i t}-\frac{1}{N} \sum_{j=1}^N S A R_{j t}\right)^2
$$
Eventus extends the cross-sectional standardized test to multiperiod windows using the correction for serial correlation described above. Thus, the STDCSECT option implies the SERIAL option. Define the standardized cumulative abnormal return for stock $j$ as
$$
S C A R_{T_{1 j}, T_{2 j}}=\left(C A R_{T_{1 j}, T_{2 j}} / s_{C A R_{T_{1 j}, T_{2 j}}}\right)
$$
where $s_{C A R_{T_{1 j}, T_{2 j}}}$ is as defined in equation A.31. Then the standardized crosssectional test statistic for the null hypothesis that $C A A R=0$ is
$$
z_t=\frac{\sum_{i=1}^N S C A R_{T_{1 j}, T_{2 j}}}{N^{\frac{1}{2}} s_{S C A R_{\bullet t}}},
$$
where
$$
s_{S C A R \bullet t}^2=\frac{1}{N-1} \sum_{i=1}^N\left(S C A R_{T_{1 i}, T_{2 i}}-\frac{1}{N} \sum_{j=1}^N S C A R_{T_{1 j}, T_{2 j}}\right)^2
$$

### Time-series standard deviation test
The EVTSTUDY statement option CDA invokes the time-series standard deviation test. The test is also called the "crude dependence adjustment" test (Brown and Warner, 1980). Unlike the standardized abnormal return test, the time series standard deviation test uses a single variance estimate for the entire portfolio. Therefore, the time series standard deviation test does not take account of unequal return variances across securities. On the other hand, it avoids the potential problem of cross-sectional correlation of security returns. The estimated variance of $A A R_t$ is
$$
\hat{\sigma}_{A A R}^2=\frac{\sum_{t=E_1}^{E_2}\left(A A R_t-\overline{A A R}\right)^2}{M-2},
$$
where the market model parameters are estimated over the estimation period of $M=E_2-E_1+1$ days and
$$
\overline{A A R}=\frac{\sum_{t=E_1}^{E_2} A A R_t}{M} .
$$
The portfolio test statistic for day $t$ in event time is
$$
\mathrm{t}=\frac{A A R_t}{\hat{\sigma}_{A A R}} .
$$
Assuming time-series independence, the test statistic for $C A A R_{T_1, T_2}$ is
$$
\mathrm{t}=\frac{C A A R_t}{\left(T_2-T_1+1\right)^{\frac{1}{2}} \hat{\sigma}_{A A R}} .
$$
Many studies use the time series standard deviation test (for example, see Dopuch, Holthausen and Leftwich, 1986 and Brickley, Dark and Weisbach, 1991)

### Cross-sectional standard deviation test
When the CSECTERR option appears on the EVTSTUDY statement, Eventus substitutes a daily cross-sectional standard deviation for the portfolio timeseries standard deviation in the non-standardized tests. The portfolio test statistic for day $t$ in event time is
$$
\mathrm{t}=\frac{A A R_t}{\hat{\sigma}_{A A R_t} / \sqrt{N}},
$$
where
$$
\hat{\sigma}_{A A R_t}^2=\frac{1}{N-1} \sum_{i=1}^N\left(A_{i t}-\frac{1}{N} \sum_{j=1}^N A_{j t}\right)^2 .
$$

The estimated variance of $C A A R_{T_1, T_2}$ is
$$
\hat{\sigma}_{C A A R_{T_1, T_2}^2}^2=\frac{1}{N-1} \sum_{i=1}^N\left(C A R_{i, T_1, T_2}-\frac{1}{N} \sum_{j=1}^N C A R_{j, T_1, T_2}\right)^2
$$
The test statistic for $C A A R_{T_1, T_2}$ is
Brown and Warner (1985) report that the cross-sectional test is wellspecified for event date variance increases but not very powerful. Boehmer, Musumeci and Poulsen (1991) report that the standardized cross-sectional test (see above) is more powerful and equally well specified. Cowan (1992) reports that the generalized sign test (see below) also is well specified for event date variance increases and more powerful than the cross-sectional test.

### Skewness-adjusted transformed normal test

The transformed normal test (Hall, 1992) can be applied to mean abnormal return and to window mean cumulative or compounded abnormal return. Assume that it is applied to average compounded abnormal return. Estimate the cross-sectional standard deviation by
$$
\hat{\sigma}_{B H A R}=\left[\frac{1}{N-1} \sum_{i=1}^N\left(B H A R_{i, T_1, T_2}-A C A R_{T_1, T_2}\right)^2\right]^{\frac{1}{2}}
$$
and the skewness by
$$
\hat{\gamma}=\frac{N}{(N-1)(N-2)} \sum_{i=1}^N\left(B H A R_{i, T_1, T_2}-A C A R_{T_1, T_2}\right)^3 \hat{\sigma}_{B H A R}^{-3}
$$
Define
$$
S=\frac{A C A R_{T_1, T_2}}{\hat{\sigma}_{B H A R} / \sqrt{N}} .
$$
The skewness-adjusted transformed normal test statistic is
$$
t_1=S+\frac{1}{3} \hat{\gamma} S^2+\frac{1}{27} \hat{\gamma}^2 S^3+\frac{1}{6 N} \hat{\gamma} .
$$
See Hall (1992) for derivation, simulation evidence and discussion.

Generalized sign test
For each trading day or month in the event period, and for each window, Eventus reports the number of securities with positive and negative average abnormal returns (cumulative or compounded abnormal returns for windows). Also reported is a test statistic (in the default output format) and significance level symbols for the generalized sign test. The null hypothesis for the generalized sign test is that the fraction of positive returns is the same as in the estimation period. For example, if $46 \%$ of market adjusted returns are positive in the estimation period, while $60 \%$ of firms have positive market adjusted returns on event day $-1$, Eventus reports whether the difference between $60 \%$ and $46 \%$ is significant at the five percent, one percent, or onetenth of one percent level. The actual test uses the normal approximation to the binomial distribution. For examples of the generalized sign test in the literature, see Sanger and Peterson (1990), Singh, Cowan and Nayar (1991), binomial sign test.) For a more detailed explanation of the generalized sign test, see Sprent (1989) and Cowan (1992).

### Rank test

Corrado (1989) describes the rank test for a one-day event window. The ranks of the abnormal returns of different days are dependent by construction. However, the effect of ignoring the dependence should be negligible for short event windows. Eventus extends the rank test to multiple day windows by assuming that the daily return ranks within the window are independent.

The rank test procedure treats the combined estimation period and event period as a single set of returns, and assigns a rank to each daily (or monthly, etc.) return for each firm.\footnote{%
      Eventus does not require that the estimation and event periods be contiguous. The estimation and event period returns used for the other computations in the current event study are used for the rank test also.}

Let $K_{j t}$ represent the rank of abnormal return $A_{j t}$ in the sample of $M_j+L_j$ abnormal returns of stock $j$ . $L_j$ is the number of non-missing returns of stock $j$ in the event period; if there are no missing returns, $L_j=L=$ POST $-$ PRE $+1$ and $M_j=M=$ ESTLEN. Rank one signifies the smallest abnormal return. The mean (and median) rank across the combined estimation and event period is
$$
\widetilde{K}=\frac{M+L+1}{2} .
$$
The rank test statistic for the event window composed of days $T_1$ through $T_2$ is
where
$$
z_r=(L)^{\frac{1}{2}}\left\{\frac{\overline{K_{T_1, T_2}}-\widetilde{K}}{\left[\sum_{t=1}^{M+L}\left(\overline{K_t}-\widetilde{K}\right)^2 /(M+L)\right]^{\frac{1}{2}}}\right\}
$$
$$
\overline{K_{T_1, T_2}}=\frac{1}{L} \sum_{t=T_1}^{T_2} \frac{1}{N} \sum_{j=1}^N K_{j t}
$$
is the average rank across the $N$ stocks and $L=T_2-T_1+1$ days of the event window and $\overline{K_t}=(1 / N) \sum_{j=1}^N K_{j t}$ is the average rank across $N$ stocks on day $t$ of the $M+L$ day combined estimation and event period. The expected rank still is $\widetilde{K}$ for event windows shorter than $L$ days, because the full $M+L$ day set of returns is used for the assignment of ranks.
Jackknife test
The discussion in this subsection is adapted from Giaccotto and Sfiridis (1996). The jackknife test incorporates the standardized abnormal return for each stock $j$, computed using the event period sample standard deviation. The standardized abnormal return for day $t$ is
$$
\hat{\theta}=\frac{A_{j t}}{\tilde{\sigma}_{A_{j t}}}
$$
where
$$
\tilde{\sigma}_{A_{j t}}=\left\{\sum_{t=T_1}^{T_2} \frac{\left(A_{j t}-\overline{A_j}\right)^2}{L_j}\right\}^{\frac{1}{2}}
$$
and $\overline{A_j}$ is the mean abnormal return of stock $j$ during the event period of $L$ days. If there is an event-induced, transient variance change on day $t$, then $\tilde{\sigma}_{A_{j t}}$ is a biased estimator of $\sigma_{A_{j t}}$ and $\hat{\theta}$ is a biased statistic. Giaccotto and Sfiridis propose reducing the bias by jackknifing the $\hat{\theta}$ values. The first step of the jackknife is to sequentially delete one abnormal return $A_{j T_s}$ from equation A.43 and re-compute $\tilde{\sigma}_{A_{j t}}$, using the new value in turn to re-compute $\hat{\theta}$ using equation A.42. Call the latter value $\hat{\theta}_{(-s)}$. The next step is to form pseudo-values
$$
\theta_{(-s)}=\left(L_j\right) \hat{\theta}-\left(L_j-1\right) \hat{\theta}_{(-s)}
$$
The jackknife estimator for stock $j$ on day $t$ is the mean of the pseudo-values,
$$
\theta_{j t}=\frac{1}{L_j} \sum_{t=T_1}^{T_2} \theta_{(-s)}
$$
To gain efficiency, the estimates are averaged across the sample of stocks:
$$
\bar{\Theta}_t=\frac{1}{N} \sum_{j=1}^N \theta_{j t}
$$
Finally, the jackknife test statistic for the sample of stocks on day $t$ is
$$
t_{\text {Jackknife }}=\frac{\bar{\Theta}_t}{S_{\text {Jackknife, } t} / \sqrt{N}}
$$
where
$$
S_{\text {Jackknife }, t}=\left[\frac{1}{N-1} \sum_{i=1}^N\left(\theta_{j t}-\bar{\Theta}_t\right)^2\right]^{\frac{1}{2}}
$$
The distribution of $t_{\text {Jackknife }}$ under the null hypothesis is approximately normal with mean zero and unit variance.

To test the significance of the cumulative average abnormal return over the window from date $T_1$ through date $T_2$, define
$$
\hat{\theta}_{T_1, T_2}=\frac{\sum_{t=T_1}^{T_2} A_{j t}}{\left(T_2-T_1+1\right)^{\frac{1}{2}} \tilde{\sigma}_{A_{j t}}}
$$
Sequentially delete one abnormal return $A_{j T_s}$ from equation A.43 and recompute $\tilde{\sigma}_{A_{j t}}$, using the new value in turn to re-compute $\hat{\theta}$ using equation A.48. Call the latter value $\hat{\theta}_{(-s), T_1, T_2}$. Form pseudo-values
$$
\theta_{(-s), T_1, T_2}=\left(L_j\right) \hat{\theta}_{T_1, T_2}-\left(L_j-1\right) \hat{\theta}_{(-s), T_1, T_2}
$$

The jackknife estimator for stock $j$ in window $\left(T_1, T_2\right)$ is the mean of the pseudo-values,
$$
\theta_{j, T_1, T_2}=\frac{1}{L_j} \sum_{t=T_1}^{T_2} \theta_{(-s)}
$$
The estimates are averaged across the sample of stocks:
$$
\bar{\Theta}_{T_1, T_2}=\frac{1}{N} \sum_{j=1}^N \theta_{j, T_1, T_2}
$$
The jackknife test statistic for the sample of stocks in window $\left(T_1, T_2\right)$ is
$$
t_{\text {Jackknife }}=\frac{\bar{\Theta}_{T_1, T_2}}{S_{\text {Jackknife }, T_1, T_2} / \sqrt{N}}
$$
where
$$
S_{J a c k k n i f e, T_1, T_2}=\left[\frac{1}{N-1} \sum_{i=1}^N\left(\theta_{j, T_1, T_2}-\bar{\Theta}_{T_1, T_2}\right)^2\right]^{\frac{1}{2}}
$$

# A.5 Missing Returns
CRSP codes any missing return on its files as an integer strictly less than $-1.0$. Eventus internally converts each of these CRSP missing return codes to the SAS special missing value. Special missing values work the same way as the regular SAS missing value, ., in all arithmetic operations.

When an estimation period contains a sequence of one or more missing values, Eventus does not use the first succeeding non-missing return. The reason is that the first non-missing return is a multi-period return. Permitting multi-period returns could have unexpected consequences for parameter estimates. The first non-missing return following a sequence of missing estimation-period returns is replaced by the special missing value .N.

When a sequence of one or more returns is missing in the event period, Eventus adjusts the abnormal return computation procedure to account for the multi-day character of the first post-missing return. For example, if the number of missing days is $q$, the market model abnormal return for the first post-missing day, $t$, is
$$
A_{j t}=R_{j t}-\left[(q+1) \hat{\alpha}_j+\hat{\beta}_j \sum_{h=0}^q R_{m(t-h)}\right],
$$
while the maximum likelihood estimate of the variance of $A_{j t}$ is,
$$
s_{A_{j t}}^2=s_{A_j}^2\left\{(q+1)\left(1+\frac{1}{M_j}\right)+\frac{\sum_{h=0}^q\left(R_{m(t-h)}-\overline{R_{m E s t}}\right)^2}{\sum_{k=E_2}^{E_1}\left(R_{m k}-\overline{R_{m E s t}}\right)^2}\right\} .
$$
