&emsp;

>**作者**：尚佳雪 (中山大学)  
>**邮箱** <shangjx5@mail2.sysu.edu.cn>

**目录**
[TOC]

本推文介绍了 2022 年 Jonathan Mellon 在 SSRN 的 ***Rain, Rain, Go Away: 192 Potential Exclusion-Restriction Violations for Studies Using Weather as an Instrumental Variable*** 这篇文章

> **Source**: Mellon, Jonathan, Rain, Rain, Go Away: 192 Potential Exclusion-Restriction Violations for Studies Using Weather as an Instrumental Variable (July 18, 2022). [-PDF-](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3715610)
> 


## 1. 背景介绍

**内生性** 是研究中面临的最普遍的挑战之一，两个变量 $X$ 和 $Y$ 的直接因果关系可以由图 1 中的 " Native model " 板块来表示，这是一个**有向无环图** ( Directed Acyclic Graph , **DAG** ) 。但这样的关系通常是不可靠的,如图 1 中的 " Endogeneity Critique " 板块所示,变量 $U$ 会同时影响 $X$ 与 $Y$,从而违反了内生性的假设。

我们通常会使用**工具变量 ( IV )** 来应对内生性问题,如图1中的 " Instrumental Variable " 板块所示， $W$ 为工具变量，能够排除掉 $X$ 中与 $U$ 相关的因素，通常我们使用两阶段最小二乘法 ( 2SLS ) 来估计。

任何工具变量估计需要满足**识别假设**，其中最重要假设之一是**排他性约束假设**，即工具变量 $W$ 仅通过 $X$ 来与 $Y$ 相关联,即**从 $W$ 到 $Y$ 没有其他因果路径** ，但这样的假设在研究中真的实现了吗？事实上并非如此，如图 1 的 " Exclusion restriction violation 2 " 板块所示， $W$ 有可能通过 $Z$ 变量来与 $Y$ 发生因果关系。这就违反了排他性约束假设。

违反排他性约束假设会对估计结果造成干扰，Bartels ( 1991 ) 表明即使是微弱地违反工具变量估计假设也会极大造成 IV 回归有偏，甚至有时还不如 OLS 回归。  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig1_工具变量和内生性_尚佳雪.jpg)

一个广泛使用的工具变量是**天气 ( Weather )** 。天气被广泛作为外生变量是有原因的，首先天气本质上是随机的，其次它只通过特定的狭窄因果路径与人类的活动结果相关。

但是天气做为工具变量也受到了很多质疑与批评。Sarsons ( 2015 ) 指出降雨量对经济增长的 IV估计可能违反了排他性约束假设，因为她发现，即使收入不受影响,降雨也会预测到印度的冲突。Gallen and Raymond ( 2019 ) 将降雨量作为几个过度使用的工具变量之一。Cooperman ( 2017 ) 和 Betz, Cook and Hollenbach ( 2019 ) 指出，降雨和许多其他工具存在空间相互依赖性，但这在实际研究中很少被充分考虑。

因此本文收集了 156 篇使用天气作为工具变量的文献和另外 132 篇研究天气对其他变量直接影响的文献。总的来说，作者保守地确定了社会科学家感兴趣的 **192 个变量** ，现有的研究已经通过许多不同的因果途径将这些变量与天气联系起来。

通过这些文献,作者使用天气对可能的 $Z$ 变量的第一阶段 IV 估计，对天气 IV 回归的简化形式方程进行**敏感性分析** ( Cinelli and Hazlett 2020a, 2020b ) ，这样就能**测算 $Z$ 变量和因变量之间必须存在多大的关系，才能使 IV 估计变得不显著，推翻论文的已有结果**。

本文结论发现，在很多已有文献中，天气作为工具变量并不符合 DAG 基本假设，即存在违反排他性约束假设，这表明他们的研究结果可能是有偏甚至是错误的。此外，本文还提供了一种系统列举现有文献存在的潜在违反排他性约束的步骤，见 **4.3**。

## 2. 不同研究使用同一个工具变量意味着什么？——违反排他性约束假设的本质原因

我们来举一个例子。研究 1 将 $W$ 作为 $X_1$ 预测 $Y_1$ 结果的工具变量，研究 2 将 $W$ 作为 $X_2$ 预测 $Y_2$ 结果的工具变量，图 2 显示了三种可能的情况。图 2 的第 1 个板块 **" Best case "** 显示了最好的情况，即 **$X_2$ 对 $Y_1$ 没有因果关系**。在这种情况下，研究 1 的 IV 估计值被确定为因果关系，不存在违背排他性约束假设。

然而,一种可能的情况是, **$X_2$ 对 $Y_1$ 也有因果关系**，在这种情况下，研究 1 的 IV 估计就**违反了排他性约束假设**，因为除了 $X_1$， $W$ 还可以通过 $X_2$ 影响 $Y$ 。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig2_多个研究使用同一工具变量的不同情形_尚佳雪.jpg)

在实际中，**不同研究违反排他性约束假设的程度会有很大不同**，因此因果估计中的结果偏差也会相应变化。如果 $X_2$ 对 $Y_1$ 的影响足够小，总体偏差可能不太大。同样，如果 $U_2$ 对 $X_2$ 和 $Y_1$ 的影响足够小，对 $X_2$ 施加条件能不会大幅偏离估计值 (Gallen and Raymond, 2019 ) 。本文就是使用敏感性分析来说明违反排他性约束的造成的偏差大小。

## 3. 方法

本节概述了收集将 "天气" 作为工具变量的研究、识别可能的违反排他性约束的情况以及可能的测量 IV 估计的敏感性的方法。

### 3.1 可能的排他性约束变量 (工具变量)

作者收集了在谷歌学术搜索 " weather " 工具变量的前 500 个结果，又添加了搜索 "rain" 工具变量的前 100 个结果，以及作者之前确定的一些研究以及最初搜索中引用的相关研究，共收集了 156 项以天气做为工具变量的研究，以及另外 132 项使用天气作为感兴趣的独立变量的研究。

本文尽可能广泛地确定天气作为 IV 研究的潜在违反排他性约束的情况。在研究 A 中，使用 $W$ 作为 $X$ 对 $Y$ 影响的 IV ，作者将其**潜在的违反排他性约束情况定义为 $W → Z$ 的因果关系**。若 $W → Z$ 这一联系存在，则排他性约束成立要求研究 A 有一个新的识别假设：没有 $Z → Y$ 因果关系。

一个值得注意的一点是，已有研究使用各种天气现象作为工具变量，尽管天气类型不同，但**不同的天气类型是复杂因果网络的一部分，它们通常有很大的相关性**。例如，$rain→Z$的因果关系也意味着温度和 $Z$、风速和 $Z$ 等等之间某种形式的因果关系，虽然这种关系的强度可能会减弱。

在**不同的时间频率与经济发展水平**情况下，将天气作为工具变量来测量因果关系也可能会有不同的结果：

- 时间频率

    天气可以在不同的时间频率上进行测量。一些研究着眼于降雨量的每日变化，而另一些研究则侧重于降雨量的年变化。**如果天气的每日变化影响了一个变量，那么这种影响可能会在更长的时期内累积起来影响这个变量**。因此，长期的天气 IV 研究相对容易受到所有其他短期天气 IV 研究的影响，从而发生违反排他性约束假设情况。**然而，反过来则不一定**。如果短期的研究控制了长期的天气变量，那么就有可能消除通过长期天气起作用的违反排他性约束情况。因此，作者将这些研究分为短期研究和长期研究，短期研究中，关注天气的月变化或更频繁，反之则是长期研究。

- 经济发展水平

    较贫穷的国家倾向于以农业和原材料开采为基础的初级经济，而较富裕的国家则拥有更加多样化的经济。因此，**在不同经济发展环境中，天气的影响可能大不相同**。为了缓解这一问题，作者根据世界银行中国家目前的发展水平对每项研究涵盖的国家进行分类，将高或中高收入国家归入高收入类别，将中低收入国家归入低收入类别。

### 3.2 天气IV的估计值有多大偏差？

违反排他性约束假设并不一定就意味着IV估计将有很大偏差，因此需要测算Z是否严重违反了排他性约束假设，从而改变IV研究的结论。**违反排他性约束假设将在多大程度上改变天气IV研究的结论取决于两个量**： $W$ 与 $Z$ 的 $Partial R^2$  、$Z$ 与 $Y$ 的 $Partial R^2$ ，即 $R^2_{Z～W|K}$ 和  $R^2_{Y～Z|W,K}$  ，其中 $K$ 是外生变量。$Partial R^2$ 来自于收集到的文献中天气和各种 $X$ 变量之间关系的第一阶段估计，$F$ 统计量  ( $F_{X,W|K}$ )   衡量了天气和 $X$ 变量之间的关系强度。

这样，**估计系数的偏差**就表示为：           


$$bias=SE\sqrt{\frac{R^2_{Y～Z|W,K}R^2_{Z～W|K}}{1-R^2_{Z～W|K}}(n-p-1)}$$
其中，$n$ 是样本大小，$p$ 是变量个数。

**矫正后的标准误**为： 

$$SE_{corrected}=SE\sqrt{\frac{1-{R^2_{Y～Z|W,K}}}{1-{R^2_{Z～W|K}}}·\frac{n-p-1}{n-p-2}}$$ 


### 3.3 不同文献间的R<sup>2 </sup>

对于回归第一阶段的 $R^2_{Z～W|K}$ ，作者提出 **5 个要求**：

 1. 研究 B 必须具有与研究 A 相同的时间频率;
 2. 研究 A 、B 必须具有相似经济发展水平;
 3. 研究 A 中使用的天气变量也必须用于研究 B;
 4. 研究 A 、B 中模型的方差相同;
 5. 研究 A 不能将研究 B 中的 Z 变量作为控制变量。
  
前两点已在 **3.1** 中阐释，接下来阐释 第 3、4 点 。

- **3. 研究 A 中使用的天气变量也必须用于研究 B**：
不同研究中使用的天气工具变量差异很大。表 1 显示使用每个天气变量的被使用次数 (有些研究使用多种天气工具变量) 。降雨量 (Rainfall) 是最常用的天气变量，其次是温度 (Temperature) 。不同类型的天气与特定 $X$ 变量的关系强度不同。一项表明降雨量和 $X$ 之间有很强联系的研究并不意味着 $X$ 与阳光强度或降雪量也有很强的关系。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig5_不同天气变量_尚佳雪.jpg)

- **4. 研究 A 、B 中模型的方差相同**：
  模型的方差有差异。一些天气 IV 研究使用单个时间点的个人水平数据，而其他研究使用一段时间内的地区水平数据，这两类研究的方差不同。不同的方差确实会使转换拟合优度变得困难。表 2 显示了天气 IV 简化形式和 $F$ 统计所基于的方差水平数量。最常见的是国家内部的差异。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig6_方差_尚佳雪.jpg)

## 4. 结果

### 4.1 天气IV中的排他性约束假设违例

在图 1 的第三个板块 " Instrumental Variable " 中，天气只通过感兴趣的单个 $X$ 变量对 $Y$ 有影响 ( DAG )。但**对 288 项研究的整理发现，天气通过 123 个不同的变量进入因果网络，并间接影响社会科学家感兴趣的 192 个变量,这些变量中的每一个都表示对图 1 中 DAG 的潜在排他性约束假设违例**。

在因果网络中最常见的天气类型是 " 降雨量 "，下图显示了 191 篇文献中包含" 降雨量 "的文献构成的因果网络。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig7_降雨量结果_尚佳雪.jpg)

图中圆圈大小表示文献中出现的次数，绿色圆圈表示天气变量，橙色圆圈表示内生变量，紫色圆圈表示结果变量，**可以看到明显违背排他性约束假设的现象**。

下图显示了按国家经济水平和时间频率分类的天气 IV 研究 (所有类型的天气) 的因果网络。其中针对低收入国家并使用天气短期变化的研究相对较少，只有 11 个变量与天气相关。另外三个板块显示了一个需要考虑潜在排他性约束违例的网络，高收入-短期有 74 个变量，低收入-长期有 83 个变量，高收入-长期有 119 个变量。对于绝大多数研究，存在违反排他性约束假设的情况。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig8_高收入_尚佳雪.jpg)![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig9_低收入_尚佳雪.jpg)

### 4.2 那些变量与天气 (Weather) 有关

在对高收入和低收入国家进行的天气 IV 研究中，使用最广泛的内生变量是 **收入**，无论是个人收入、家庭收入还是总收入。收入是一个非常重要的变量，因此对于其他使用天气 IV 的研究来说，就存在很大的违反排他性约束假设的概率。

大多数人认为"直升机飞行条件"和"海盗袭击"不可能违反排他性约束假设，但结果可能并非如此，有多条路线表明存在这样的违例。

第一条因果路线是 **情绪 (mood)** ，几项研究称天气和情绪之间存在联系 ( Meier, Schmid and Stutzer 2019；Khanthavit 2017；Jacobsen and Marquering 2008；Duhaime and Moulton 2018；Bassi 2013；Guven and Hoxha 2015 )。Guven and Hoxha ( 2015 )表示情绪被认为与决策速度、投资决策、通胀预期、风险规避、财务决策和预期寿命有关。很难想象一个变量不会受到这些变量中至少一个变量的因果影响或直接受到情绪的影响。

第二条因果路径是 **污染 (pollution)** ，多种天气变量对污染进行了工具变量估计，包括风速 ( Peet 2020；Zheng et al. 2019； Bondy，Roth and Sager 2020 )、风向( Luechinger 2014；Fan and Wang 2020 )，大气逆温( Bondy，Roth，and Sager 2020；Sager 2019 )，以及降雨量 ( Peet 2020；Fontenla，Goodwin，and Gonzalez 2019 )。反过来，污染与犯罪( Bondy，Roth，and Sager 2020 )、心理健康 ( Gu et al. 2020 )、死亡率( Fan and Wang 2020 )、道路事故( Sager 2019 )、零售 ( H. Kang，Suh，and Yu 2019 )、房价( Fontenla，Goodwin，and Gonzalez 2019 )、流动性( Cui et al. 2019 )、认知( Peet 2020 )和情绪( Zheng et al. 2019 )相关联。很难认为任何社会科学研究变量都不会受到这些因素的影响。

第三条因果路径是 **肤色 (skin tone)**。Katz等 ( 2020 ) 表明一些工人暴露在阳光下时皮肤变得更黑，而其他人则不会晒黑，在阳光明媚的时期，晒太阳的人和不晒太阳的人在劳动力市场上的差距扩大了。鉴于色彩主义无处不在，肤色与天气的混合效应是因果网络中的的一个主要切入点。

此外，还有数百个其他变量，涉及范围很广，包括自闭症 ( autism )、政府支出 ( government spending )、无人机袭击 ( drone strikes )、全球变暖信念 ( global warming beliefs )、电力消费 ( alcohol consumption )、回收 ( recycling )、疟疾 ( malaria )等等。

### 4.3 敏感性分析

下图显示了敏感性分析结果。其中的阴影与数字表示要多强能够使已有文献的结果不再显著，越小的数值表明 $Z$ 和 $Y$ 只需要非常微弱的关系就能够使研究结果变得不显著。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig10_敏感性分析_尚佳雪.jpg)

虽然一些研究可能违反了排他性约束假设，但它们的**结果对 $Z$ 变量和因变量之间的关系是稳健的**。例如，Collins和Margo ( 2007 ) 使用降雨量作为暴力抗议的工具变量，并评估暴力抗议对房地产价格的影响。总共有 10 个与第一阶段相关的变量可能会影响这一估计，包括财产犯罪、暴力犯罪、情绪和交通，即使它们与房地产价格完全相关，也不能使结果变得不显著。移民可能使结果变得不显著，但这需要短期移民来解释35%的房地产价格变化，这几乎是难以置信的。虽然还有许多其他潜在的违反排他性约束假设，但结果是相对稳健的。

另一方面，**一些研究只需要排他性约束变量和因变量之间的微小关系就能够推翻结果，使结果变得不显著**。Moreno-Medina ( 2021 ) 通过使用一年中周日上午 9 点至下午 1 点之间下雨的周数作为是否去教堂的工具变量，来考察去教堂对各种类型犯罪的影响。根据第一阶段结果，存在五种相关的违反排他性约束假设的情况:移民、侵蚀、收入、作物产量和农业生产率。其中，这些变量中的任何一个都只需要解释 " alcohol arrests " 中方差的0.05%，就可以使结果不显著。

**大多数的研究至少有一个使结果不显著的排他性约束变量**。这些敏感度分析结果仅涵盖了一小部分可能的排他性约束假设违例，因此，**这仅代表了天气IV估计敏感度分析的下限，是保守的分析**。

### 4.4 系统搜索排他性约束违例

我们应该如何避免使用已知的违反排他性约束假设的工具变量呢? 作者给出了一套搜索程序步骤，如下。
>![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/天气工具变量_Fig11_搜索违法排他性约束情况_尚佳雪.jpg)

## 5.结语

本推文介绍了 **Rain, Rain, Go Away: 192 Potential Exclusion-Restriction Violations for Studies Using Weather as an Instrumental Variable** 这篇文章，阐释工具变量作用机制、DAG 图形、以及各种违反排他性约束的情形，并总结系统搜索排他性约束违例的步骤，对大家进一步理解 IV 及其识别条件有一定帮助。

## 6. 参考资料

- 曹昊煜，连享会推文，[A-理论部分：控制变量！控制变量！Good-Controls-Bad-Controls](https://www.lianxh.cn/news/c548e33fbb649.html)
- 李烨阳，连享会推文，[B-Stata模拟：控制变量！控制变量！Good-Controls-Bad-Controls](https://www.lianxh.cn/news/a54246bf82745.html)
- 陈卓然，连享会推文，[因果推断：混杂因素敏感性分析实操(下)-tesensitivity](https://www.lianxh.cn/news/5f38356e6485c.html)
- 陈卓然，连享会推文，[敏感性分析B-Stata实操：控制变量内生时的系数敏感性分析-regsensitivity](https://www.lianxh.cn/news/b96bf11d5d81a.html)
- 陈卓然，连享会推文，[敏感性分析A-理论基础：控制变量内生时的系数敏感性分析-regsensitivity](https://www.lianxh.cn/news/212015484d17c.html)

## 7. 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
>  &emsp; ` lianxh 外生 内生 排他性`  
>  安装最新版 `lianxh` 命令：    
>  &emsp; `ssc install lianxh, replace` 
- 专题：[专题课程](https://www.lianxh.cn/blogs/44)
  - [助教入选名单公布：因果推断-内生性专题](https://www.lianxh.cn/news/29f91eb09cfde.html)
- 专题：[面板数据](https://www.lianxh.cn/blogs/20)
  - [Stata：被忽视的严格外生性假设](https://www.lianxh.cn/news/cc3b463f4395d.html)
- 专题：[IV-GMM](https://www.lianxh.cn/blogs/38)
  - [IV专题- 内生性检验与过度识别检验](https://www.lianxh.cn/news/cc3f92710dd7a.html)
  - [Stata：当工具变量小于内生变量时，该如何估计？-mmeiv](https://www.lianxh.cn/news/0ae09a74e477f.html)
  - [IV：工具变量不满足外生性怎么办？](https://www.lianxh.cn/news/c66c145337ffa.html)
  - [IV：可以用内生变量的滞后项做工具变量吗？](https://www.lianxh.cn/news/c45f6113e2c2a.html)
  - [IV专题: 内生性检验与过度识别检验](https://www.lianxh.cn/news/cbb06931b699e.html)
  - [IV-估计：工具变量不外生时也可以用！](https://www.lianxh.cn/news/9ec69d0779fb4.html)
- 专题：[内生性-因果推断](https://www.lianxh.cn/blogs/19)
  - [内生性！内生性！解决方法大集合](https://www.lianxh.cn/news/224e2b4e170e4.html)
  - [第三种内生性：衡量偏误(测量误差)如何解决？-eivreg-sem](https://www.lianxh.cn/news/18b0a37d8391d.html)
  - [敏感性分析B-Stata实操：控制变量内生时的系数敏感性分析-regsensitivity](https://www.lianxh.cn/news/b96bf11d5d81a.html)
  - [敏感性分析A-理论基础：控制变量内生时的系数敏感性分析-regsensitivity](https://www.lianxh.cn/news/212015484d17c.html)
  - [Stata：内生转换模型及其应用-movestay-mspredict](https://www.lianxh.cn/news/3b3bfdad7e106.html)
  - [Stata：内生变量与工具变量非线性关系处理-discretize](https://www.lianxh.cn/news/3c3abdf6a6f68.html)
  - [内生性：来源及处理方法-幻灯片下载](https://www.lianxh.cn/news/b770b35f48e17.html)
  - [locmtest：非线性模型的内生性检验](https://www.lianxh.cn/news/f118ef34fd52f.html)
  - [Stata：内生变量的交乘项如何处理？](https://www.lianxh.cn/news/603e0b458bc07.html)
  - [第三种内生性：衡量偏误(测量误差)如何检验-dgmtest？](https://www.lianxh.cn/news/44fa0eb361042.html)
  - [工具变量-IV：排他性约束及经典文献解读](https://www.lianxh.cn/news/d4e7734f4d10d.html)
- 专题：[交乘项-调节-中介](https://www.lianxh.cn/blogs/21)
  - [内生变量的交乘项如何处理？](https://www.lianxh.cn/news/1f6e3f51c4991.html)
  - [Stata：内生变量和它的交乘项](https://www.lianxh.cn/news/9056df610bb6c.html)
- 专题：[分位数回归](https://www.lianxh.cn/blogs/48)
  - [Stata：面板分位数模型估计及内生性初探](https://www.lianxh.cn/news/c823976859a77.html)
