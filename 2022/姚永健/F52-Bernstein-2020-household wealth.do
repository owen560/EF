cd D:\software\code\论文复现\data
use replication-main.dta,clear

*-----------------------*
*-Section One:Data      
*-----------------------*

*----------------------------------
*-Table One: key variables————Summary Description
*----------------------------------
*-Panel A :Patent Measures (2008–2012)
label variable f_ln_numpats "Log(Number of Patents)"
label variable f_ln_cites   	"Log(Normalized Citations Per Patent)"
label variable f_ln_top10  	   "Log(Number of Top Cited Patents)"
label variable f_ln_generality "Log(Normalized Generality Per Patent)"
label variable f_ln_original~y  "Log(Normalized Originality Per Patent)"
label variable f_ln_newclass   "New Technology Indicator"
label variable f_ln_explorat~e "Log(Number of Exploratory Patents)"

local y_list = "f_ln_numpats f_ln_cites f_ln_top10 f_ln_generality f_ln_original~y f_ln_newclass f_ln_explorat~e"
tabstat `y_list' ,stat(count mean sd) column(stat)  //需要与定义暂元的命令一起执行

foreach v of local y_list{
	kdensity `v'
}

*-Panel B:Patent Measure Correlation Matrix (2008–2012)
correlate f_ln_cites f_ln_top10 f_ln_generality f_ln_original~y f_ln_newclass f_ln_explorat~e

*-Panel C: Worker Characteristics (2007)
label variable ba "=1 if at least with a BA degree"
label variable ma "=1 if at least with a MA Degree"
label variable phd "=1 if with a PhD Degree"
label variable mba "=1 if with a MBA Degree"
label variable jd "=1 if with a JD Degree"
label variable md "=1 if with a MD Degree"
label variable expjob "the experience of working at a job"
label variable tenure "Tenure at Firm"
label variable management "=1 if with a senior position"

tabstat ba ma phd mba jd md age expjob tenure management,stat(count mean sd) column(stat)

*-Panel D: Worker House Characteristics (2007)
label variable yearsown "Years Owned House"
label variable building_square_feet "Square Footage"
label variable homeage "Age of House"
label variable l_hpi_change "pecentage change in house price in Pre period(2004-2007)"
label variable f_hpi_change "pecentage change in house price in Post period(2007-2012)"

tabstat yearsown building_square_feet homeage l_hpi_change f_hpi_change,stat(count mean sd) column(stat)

*--------------------------------------------------
*-Table Two: Key Regions
*--------------------------------------------------
sort rank_cbsa
label define cbsa_name 1 "San Jose-Sunnyvale-Santa Clara, CA" 2 "San Francisco-Oakland-Fremont, CA" 3 "New York-Northern New Jersey-Long Island, NY-NJ-PA" 4 "Seattle-Tacoma-Bellevue, WA" 5 "Boston-Cambridge-Quincy, MA-NH" 6 "Chicago-Joliet-Naperville, IL-IN-WI" 7 "Los Angeles-Long Beach-Anaheim, CA" 8 "San Diego-Carlsbad-San Marcos, CA" 9 "Detroit-Warren-Livonia, MI" 10 "Philadelphia-Camden-Wilmington, PA-NJ-DE-MD" 11 "Minneapolis-St. Paul-Bloomington, MN-WI" 12 "Austin-Round Rock-San Marcos, TX" 13 "Dallas-Fort Worth-Arlington, TX" 14 "Houston-Sugar Land-Baytown, TX" 15 "Portland-Vancouver-Hillsboro, OR-WA" 16 "Washington-Arlington-Alexandria, DC-VA-MD-WV" 17 "Atlanta-Sandy Springs-Marietta, GA" 18 "Raleigh-Cary, NC" 19 "Phoenix-Mesa-Glendale, AZ" 20 "Cincinnati-Middletown, OH-KY-IN"
label values rank_cbsa cbsa_name //label the core CBSAs areas' name(top 20) with the number
gen core_cbsa = (rank_cbsa>=1 & rank_cbsa<=20)

tabstat percent_inventors if core_cbsa==1,by (rank_cbsa) stat(mean)
tabstat mean_f_hpi_change if core_cbsa==1,by (rank_cbsa) stat(mean)
tabstat sd_f_hpi_change if core_cbsa==1,by (rank_cbsa) stat(mean)


*--------------------------------------------------------*
*-Section Two: Baseline Regression and Section Concerns  *
*--------------------------------------------------------*

*--------------------------------------------------
*-Table Three: Baseline Results 
*--------------------------------------------------
*-Panel A-Quantity and Quality of Innovation:Log(Number of Patents),Log(Normalized Citations Per Patent),Log(Number of Top Cited Patents)
*-Panel B-Originality and Generality:Log(Normalized Generality Per Patent),Log(Normalized Originality Per Patent)
*-Panel C-Exploratory Nature:New Technology Indicator, Log(Exploratory Patents Post)

*-Regression One:without including the Change of House Price in Pre Period
foreach v in ln_numpats ln_cites ln_top10 ln_generality ln_originality ln_newclass ln_explorative {
	reghdfe f_`v' f_hpi_change l_`v', vce(cluster zip_id firm_id) absorb(firm_id rank_cbsa)
}

*-Regression Two: including the Change of House Price in Pre Period (l_hpi_change)
foreach v in ln_numpats ln_cites ln_top10 ln_generality ln_originality ln_newclass ln_explorative {
	reghdfe f_`v' f_hpi_change l_hpi_change l_`v', vce(cluster zip_id firm_id) absorb(firm_id rank_cbsa)
}

*----------------------------------------------------
*-Table Four: Three-Way Fixed Effects
*----------------------------------------------------
global y_inovation "ln_numpats ln_cites ln_top10 ln_generality ln_originality ln_newclass ln_explorative"

*--------------------
*Panel A  Full Sample
*--------------------
*-Panel A Row1  Firm × CBSA FE
*-Panel A Row2  Firm × CBSA × Tech Class FE
*-Panel A Row3  Firm × CBSA × Neighborhood Income Q. FE
*-Panel A Row4  Firm × CBSA × Family Neighborhood Q. FE
*-Panel A Row5  Firm × CBSA × Urban Neighborhood Q. FE
*-Panel A Row6  Firm × CBSA × Neighborhood Price Level Q. FE
*-Panel A Row7  Firm × CBSA × Square Footage Q. FE
*-Panel A Row8  Firm × CBSA × Experience Q. FE

global fe_A "firmXcbsa firmXcbsaXclass firmXcbsaXincomeQ4 firmXcbsaXkidsQ4 firmXcbsaXurbanQ4 firmXcbsaXpriceQ4 firmXcbsaXsqftQ4 firmXcbsaXexppatQ4"

foreach i in $fe_A{
	foreach v in $y_inovation{
	reghdfe f_`v' f_hpi_change l_hpi_change l_`v', vce(cluster zip_id firm_id) absorb(`i')
	}
}

*-----------------------
*Panel B LinkedIn Sample
*-----------------------
global fe_B "firmXcbsa firmXcbsaXageQ4 firmXcbsaXeduc firmXcbsaXmanagement"

foreach i in $fe_B{
	foreach v in $y_inovation{
	reghdfe f_`v' f_hpi_change l_hpi_change l_`v' if linkedin==1, vce(cluster zip_id firm_id) absorb(`i')
	}
}

*--------------------------------------------------------------
*-Table Five: Baseline Specification with Additional Controls  
*--------------------------------------------------------------

*-More Controls
*-house characteristics:log square footage,family sizes, total rooms
*-Inventors characteristics (Both linear and quadratic): job tenure, age, years of patenting experience
*-zip code characteristics:log median income, log house price level in 2007, child percentage, urban percentage
*-Edu characteristics:education fixed effects 
*-Additional FE:job seniority fixed effects and functional role fixed effects,technology class fixed effects

global controls_house "familysize ln_sqfeetF ln_sqfeetM total_roomsF total_roomsM "
global controls_zip "ln_medianincome99 l_ln_price ratiokids99 ratiourban99"
global controls_inventor "c.age##c.age c.expjob##c.expjob c.exppat##c.exppat c.tenure##c.tenure "
global controls_edu "ba phd ma mba jd"

foreach v in $y_inovation{
	reghdfe f_`v' f_hpi_change l_hpi_change l_`v' $controls_house $controls_zip $controls_inventor  $controls_edu if linkedin==1,vce(cluster zip_id firm_id) absorb(firmXcbsa seniority_code function_code inv_class)
}

*------------------------------------------------
*-Table Six: Single Establishment Subsample
*------------------------------------------------
foreach v in $y_inovation{
	reghdfe f_`v' f_hpi_change l_hpi_change l_`v' if num_establishments==1,vce(cluster zip_id firm_id) absorb(firm_id rank_cbsa)
}

*------------------------------*
*-Section Three: Robust Test   *
*------------------------------*

*-----------------------------------------------
*-Table Seven: Value of Innovative Output
*-----------------------------------------------
label variable f_ln_xi "log of one plus the total value of the patents a worker applied for in the post-crisis period"
label variable l_ln_xi "log of one plus the total value of the patents a worker applied for in the pre-crisis period"
*-Column 1
reghdfe f_ln_xi f_hpi_change l_ln_xi,vce (cluster zip_id firm_id) absorb(firmXcbsa)
*-Column 2
reghdfe f_ln_xi f_hpi_change l_hpi_change l_ln_xi,vce (cluster zip_id firm_id) absorb(firmXcbsa)
*-Column 3
reghdfe f_ln_xi f_hpi_change l_ln_xi,vce (cluster zip_id firm_id) absorb(firmXcbsaXclass)
*-Column 4
reghdfe f_ln_xi f_hpi_change l_hpi_change l_ln_xi,vce (cluster zip_id firm_id) absorb(firmXcbsaXclass)

*---------------------------
*-Table Eight:Job Changes
*---------------------------
global firm_change "f_firm_change1 f_firm_change2 f_firm_change3 f_firm_change4 f_firm_change5"
global position_change "f_position_change1 f_position_change2 f_position_change3 f_position_change4 f_position_change5"
global anyjob_change " f_anyjob_change1 f_anyjob_change2 f_anyjob_change3 f_anyjob_change4 f_anyjob_change5 "

*-Panel A:Firm Changes
foreach v in $firm_change{
	reghdfe `v' f_hpi_change l_hpi_change, vce(cluster zip_id firm_id) absorb(firmXcbsa)
}
*-Panel B:Position Change Within Firm
foreach v in $position_change{
	reghdfe `v' f_hpi_change l_hpi_change, vce(cluster zip_id firm_id) absorb(firmXcbsa)
}
*-Panel C: Firm Change or Position Change Within Firm
foreach v in $anyjob_change{
	reghdfe `v' f_hpi_change l_hpi_change, vce(cluster zip_id firm_id) absorb(firmXcbsa)
}

*---------------------------------------------------------
*-Table Nine: Workers Who Remain in Same Firm and Position
*---------------------------------------------------------
*-How to define "remaining the same firm and position all the time"?
*-The variable "f_anyjob_change5"==0

foreach v in $y_inovation{
	reghdfe f_`v' f_hpi_change l_hpi_change l_`v' if f_anyjob_change5==0, vce(cluster zip_id firm_id) absorb(firmXcbsa)
}

*------------------------------------
*-Table Ten:Firm-Level Aggregation
*------------------------------------
use replication-firmlevel, clear
*-Panel A: Baseline Outcomes
foreach v in $y_inovation{
	reghdfe f_`v' f_hpi_change l_`v', absorb(firm_cbsa) vce(cluster firm_cbsa)
}

*-Panel B: Total Outcomes
foreach v in ln_total_cites ln_total_generality ln_total_originality {
	reghdfe f_`v' f_hpi_change l_`v', absorb(firm_cbsa) vce(cluster firm_cbsa)
}



*--------------------------------------------------*
*-Section Four: Discussion of Potential Mechanisms *
*--------------------------------------------------*

*---------------------------------------------------
*-Table Eleven:Leverage and Delinquency/Foreclosure
*---------------------------------------------------
use replication-delinquency1, clear

*-Panel B-part1: IRS income,HMDA income, each years'Delinquency/Foreclosure Rate
tabstat irs_income2002,stat(mean) by(irs_income2002Q10) column(stat)
tabstat hmda_income2002,stat(mean) by(irs_income2002Q10) column(stat)
forval time=2006/2009 {
	tabstat delinquent if year==`time' [aweight=alive], by(irs_income2002Q10) statistics(mean) columns(statistics) 
}
tabstat delinquent_ratio, by(irs_income2002Q10) statistics(mean) columns(statistics) nototal 

use replication-delinquency2, clear
tabstat irs_income2002, statistics(mean) columns(statistics) 
tabstat hmda_income2002, statistics(mean) columns(statistics) 
forval time=2006/2009 {
	tabstat delinquent if year==`time' [aweight=alive],  statistics(mean) columns(statistics) 
}
tabstat delinquent_ratio,  statistics(mean) columns(statistics) 


*-------------------------------------------------
*-Table Twelve:Housing Prices Effects in 2002
*-------------------------------------------------
use replication-2002, clear
*-New define: pre-period is defined as 1999 to 2001, The post-period is defined as 2002 to 2006.

foreach v in $y_inovation{
	reghdfe f_`v' f_hpi_change l_hpi_change l_`v', vce(cluster zip_id firm_id) absorb(firmXcbsa)
}

*----------------------------------------------
*-Table Thirteen:House Ownership Duration
*----------------------------------------------
*-How to exploit the timing of when workers bought their houses?
*-Interacting house price shocks with this indicator(purchased_pre2004),which equals to one if the worker bought their house prior to 2004.

use  replication-main, clear
foreach v in $y_inovation{
	reghdfe f_`v' l_`v' c.f_hpi_change##i.purchased_pre2004,vce(cluster zip_id firm_id) absorb(firm_id zip_id)
}


*--------------------------------
*-Table Fourteen:Labor Market
*--------------------------------
*-How to measure workers' outside labor market opportunities?
*-classify workers as specializing inwidely used technologies or narrowly used technologies (variable:popular_tech)

foreach v in $y_inovation {	
	reghdfe  f_`v'  l_`v' c.f_hpi_change##i.popular_tech, vce(cluster zip_id firm_id) absorb(firm_id zip_id)
}