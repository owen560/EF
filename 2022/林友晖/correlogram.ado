*! correlogram version 1.0 3Jan2023
*! by Youhui Lin <linyh78@mail2.sysu.edu.cn>

capture program drop correlogram
program define correlogram
	version 17 
	syntax varlist(min=3 numeric) [if] [in]   ///
	            [, CORROption(string) color(namelist) TItle(string) note(string) PSize(integer 15)] 
	

	//check needed packages

	cap which colorpalette.ado
	if _rc {
		*di as error "colorpalette.ado required: {stata ssc install palettes, replace}"
		di as error "colorpalette.ado required: {stata search gr0075} and click to install"
		exit 199
	}
	
	
	cap which colrspace.sthlp
	if _rc {
		di as error "-colrspace- required: {stata ssc install colrspace, replace}"
		exit 199
	}
	
	
	cap which labmask.ado
	if _rc {
		di as error "labmask.ado required: {stata ssc install labutil, replace}"
		exit 199
	}
	
	cap which corsp.ado
	if _rc {
		di as error "corsp.ado required: {stata ssc install corsp, replace}"
		exit 199
	}
	
	cap which scheme-white_tableau.scheme
	if _rc {
		di as error "-schemepack- required: {stata ssc install schemepack, replace}"
		exit 199
	}
	
	
	//check if "corroption" is specified correctly:
	if ("`corroption'" != "pearson" & "`corroption'" != "p" & "`corroption'" != "spearman" & "`corroption'" != "s" & "`corroption'" != "both" & "`corroption'" != "b" &"`corroption'" != "") {
				di as err ///
				"option corroption can only be one of the following: pearson(p), spearman(s) and both(b)"
				di as err ///
				"corroption(pearson) in default"
				exit 198
			}

			
			
			
preserve
	quietly { 
	local countn : word count `varlist'
	
	* Use correlation command
	
**** Pearson Correlation Coefficient
	if ("`corroption'" == "pearson" | "`corroption'" == "p" | "`corroption'" == "" ){
		correlate `varlist' `if' `in'
		matrix C = r(C)
		local rnames : rownames C
	* Now to generate a dataset from the Correlation Matrix
	clear
		
	* For no diagonal and total count
	local tot_rows : display `countn' * `countn'
	set obs `tot_rows'
	
	generate corrname1 = ""
	generate corrname2 = ""
	generate y = .
	generate x = .
	generate corr = .
	generate abs_corr = .
	
	local row = 1
	local y = 1
	local rowname = 2
		
	foreach name of local varlist {
		forvalues i = `rowname'/`countn' { 
			local a : word `i' of `varlist'
			replace corrname1 = "`name'" in `row'
			replace corrname2 = "`a'" in `row'
			replace y = `y' in `row'
			replace x = `i' in `row'
			replace corr = round(C[`i',`y'], .01) in `row'
			replace abs_corr = abs(C[`i',`y']) in `row'
			
			local ++row
			
		}
		
		local rowname = `rowname' + 1
		local y = `y' + 1
	
	}
		
	drop if missing(corrname1)
	replace abs_corr = 0.1 if abs_corr < 0.1 & abs_corr > 0.04

	* Choose the palette
	if "`color'"=="" {
		colorpalette HCL pinkgreen, n(10) nograph intensity(0.65)
	}
	else {
		capture colorpalette `color', n(10) nograph 
		
		if _rc !=0 {
			dis as error "color() must be a palette."
			exit 3498
		}
	}
	
	
	generate colorname = ""
	local col = 1
	forvalues colrange = -1(0.2)0.8 {
		replace colorname = "`r(p`col')'" if corr >= `colrange' & corr < `=`colrange' + 0.2'
		replace colorname = "`r(p10)'" if corr == 1
		local ++col
	}	
	
	
	* Plotting
	* Saving the plotting code in a local 
	
	forvalues i = 1/`=_N' {
		
		local slist "`slist' (scatteri `=y[`i']' `=x[`i']' "`: display %3.2f corr[`i']'", mlabposition(0) msize(`=abs_corr[`i']*`psize'') mcolor("`=colorname[`i']'"))"
	
	}

		
		* Gather Y axis labels
	labmask y, val(corrname1)
	labmask x, val(corrname2)
	
	levelsof y, local(yl)
	foreach l of local yl {
		local ylab "`ylab' `l'  `" "`:lab (y) `l''" "'"	
		
	}	

	* Gather X Axis labels
	levelsof x, local(xl)
	foreach l of local xl {
		local xlab "`xlab' `l'  `" "`:lab (x) `l''" "'"	
		
	}	

	
	* Plot all the above saved locals
	twoway `slist',  title(`title', size(3) pos(11)) ///
			note(`note', size(2) margin(t=5)) ///
			xlabel(`xlab', labsize(2.5)) ylabel(`ylab', labsize(2.5)) ///
			xscale(range(1.8 )) yscale(range(0.8)) ///
			ytitle("") xtitle("") ///
			legend(off) ///
			aspect(1) ///  
			scheme(white_tableau) 
	}

**** Spearman's rank correlation coefficient	
	else if ("`corroption'" == "spearman" | "`corroption'" == "s") {
		spearman `varlist'  `if' `in'
		matrix C = r(Rho)
		local rnames : rownames C
	* Now to generate a dataset from the Correlation Matrix
	clear
		
	* For no diagonal and total count
	local tot_rows : display `countn' * `countn'
	set obs `tot_rows'
	
	generate corrname1 = ""
	generate corrname2 = ""
	generate y = .
	generate x = .
	generate corr = .
	generate abs_corr = .
	
	local row = 1
	local y = 1
	local rowname = 2
		
	foreach name of local varlist {
		forvalues i = `rowname'/`countn' { 
			local a : word `i' of `varlist'
			replace corrname1 = "`name'" in `row'
			replace corrname2 = "`a'" in `row'
			replace y = `y' in `row'
			replace x = `i' in `row'
			replace corr = round(C[`i',`y'], .01) in `row'
			replace abs_corr = abs(C[`i',`y']) in `row'
			
			local ++row
			
		}
		
		local rowname = `rowname' + 1
		local y = `y' + 1
	
	}
		
	drop if missing(corrname1)
	replace abs_corr = 0.1 if abs_corr < 0.1 & abs_corr > 0.04

	* Choose the palette
	if "`color'"=="" {
		colorpalette HCL pinkgreen, n(10) nograph intensity(0.65)
	}
	else {
		capture colorpalette `color', n(10) nograph 
		
		if _rc !=0 {
			dis as error "color() must be a palette."
			exit 3498
		}
	}
	
	generate colorname = ""
	local col = 1
	forvalues colrange = -1(0.2)0.8 {
		replace colorname = "`r(p`col')'" if corr >= `colrange' & corr < `=`colrange' + 0.2'
		replace colorname = "`r(p10)'" if corr == 1
		local ++col
	}	
	
	
	* Plotting
	* Saving the plotting code in a local 
	
	forvalues i = 1/`=_N' {
		
		local slist "`slist' (scatteri `=y[`i']' `=x[`i']' "`: display %3.2f corr[`i']'", mlabposition(0) msize(`=abs_corr[`i']*`psize'') mcolor("`=colorname[`i']'"))"
	
	}
	
	* Gather Y axis labels
	labmask y, val(corrname1)
	labmask x, val(corrname2)
	
	levelsof y, local(yl)
	foreach l of local yl {
		local ylab "`ylab' `l'  `" "`:lab (y) `l''" "'"	
		
	}	

	* Gather X Axis labels
	levelsof x, local(xl)
	foreach l of local xl {
		local xlab "`xlab' `l'  `" "`:lab (x) `l''" "'"	
		
	}	
	

	
	* Plot all the above saved locals
	twoway `slist',  title(`title', size(3) pos(11)) ///
			note(`note', size(2) margin(t=5)) ///
			xlabel(`xlab', labsize(2.5)) ylabel(`ylab', labsize(2.5)) ///
			xscale(range(1.8 )) yscale(range(0.8)) ///
			ytitle("") xtitle("") ///
			legend(off) ///
			aspect(1) ///  
			scheme(white_tableau) 
	}
	
**** Pearson Correlation Coefficient and Spearman's rank correlation coefficient
	else if ("`corroption'" == "both" | "`corroption'" == "b") {
		corsp `varlist'  `if' `in'
		matrix C = r(R)
		local rnames : rownames C
	* Now to generate a dataset from the Correlation Matrix
	clear
		
	* For no diagonal and total count
	local tot_rows : display `countn' * `countn'
	set obs `tot_rows'
	
	generate corrname1 = ""
	generate corrname2 = ""
	generate y = .
	generate x = .
	generate corr = .
	generate abs_corr = .
	generate spearman_corr = .
	generate abs_spearman_corr = .
	
	local row = 1
	local y = 1
	local rowname = 1
		
	foreach name of local varlist {
		forvalues i = `rowname'/`countn' { 
			local a : word `i' of `varlist'
			replace corrname1 = "`name'" in `row'
			replace corrname2 = "`a'" in `row'
			replace y = `y' in `row'
			replace x = `i' in `row'
			replace corr = round(C[`i',`y'], .01) in `row'
			replace abs_corr = abs(C[`i',`y']) in `row'
			replace spearman_corr = round(C[`y',`i'], .01) in `row'
			replace abs_spearman_corr = abs(C[`y',`i']) in `row'
			
			local ++row
			
		}
		
		local rowname = `rowname' + 1
		local y = `y' + 1
	
	}
		
	drop if missing(corrname1)
	replace abs_corr = 0.1 if abs_corr < 0.1 & abs_corr > 0.04
	replace abs_spearman_corr = 0.1 if abs_spearman_corr < 0.1 & abs_spearman_corr > 0.04
	
	* Choose the palette
	if "`color'"=="" {
		colorpalette HCL pinkgreen, n(10) nograph intensity(0.65)
	}
	else {
		capture colorpalette `color', n(10) nograph 
		
		if _rc !=0 {
			dis as error "color() must be a palette."
			exit 3498
		}
	}
	
	
	generate colorname = ""
	generate colorname1 = ""
	local col = 1
	forvalues colrange = -1(0.2)0.8 {
		replace colorname = "`r(p`col')'" if corr >= `colrange' & corr < `=`colrange' + 0.2'
		replace colorname = "`r(p10)'" if corr == 1
		replace colorname1 = "`r(p`col')'" if spearman_corr >= `colrange' & spearman_corr < `=`colrange' + 0.2'
		replace colorname1 = "`r(p10)'" if spearman_corr == 1
		
		local ++col
	}	
	
	
	* Plotting
	* Saving the plotting code in a local 
	forvalues i = 1/`=_N' {
		if `=y[`i']' != `=x[`i']'  {
			local slist "`slist' (scatteri `=y[`i']' `=x[`i']' "`: display %3.2f corr[`i']'", mlabposition(0) msize(`=abs_corr[`i']*`psize'') mcolor("`=colorname[`i']'") )"
			local slist "`slist' (scatteri `=x[`i']' `=y[`i']' "`: display %3.2f spearman_corr[`i']'", mlabposition(0) msize(`=abs_spearman_corr[`i']*`psize'') mcolor("`=colorname[`i']'") )"
		}
	}
	
	* Gather Y axis labels
	labmask y, val(corrname1)
	labmask x, val(corrname2)
	
	levelsof y, local(yl)
	foreach l of local yl {
		local ylab "`ylab' `l'  `" "`:lab (y) `l''" "'"	
		
	}	

	* Gather X Axis labels
	levelsof x, local(xl)
	foreach l of local xl {
		local xlab "`xlab' `l'  `" "`:lab (x) `l''" "'"	
		
	}	
		
	* Plot all the above saved locals
	twoway `slist',  title(`title', size(3) pos(11)) ///
					note(`note', size(2) margin(t=5)) ///
					xlabel(`xlab', labsize(2.5)) ylabel(`ylab', labsize(2.5)) ///
					xscale(range(0.8)) yscale(range(0.8)) ///
					ytitle("") xtitle("") ///
					legend(off) ///
					aspect(1) ///  
					scheme(white_tableau) 
	
	}
	

	}
	
	
restore



end

