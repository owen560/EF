{smcl}
{hline}
help {hi:correlogram}
{hline}

{title:Title}

{p2colset 5 14 22 2}{...}
{p2col :{hi:correlogram} {hline 2}} Correlation coefficient visualization{p_end}
{p2colreset}{...}

{title:Syntax}

{p 8 14 4}{cmd:correlogram varname} 
{ifin} {cmd:,}
{cmdab:corro:ption:(}{it:string}{cmd:)}
{cmdab:p:size:(}{it:#}{cmd:)}
{cmdab::color:(}{it:argument}{cmd:)}
{cmdab:ti:tle:(}{it:tinfo}{cmd:)}
{cmdab::note:(}{it:tinfo}{cmd:)}

{synoptset 24 tabbed}{...}
{synopthdr}
{synoptline}

{synopt :{opt corro:ption(#)}}can be {cmd:{ul:p}earson(default)}, {cmd:{ul:s}pearman}, and {cmd:{ul:b}oth}. {cmd:{ul:corro}ption({ul:p}earson)} plots Pearson Correlation Coefficient; {cmd:{ul:corro}ption({ul:s}pearman)} plots Spearman's rank correlation coefficient; {cmd:{ul:corro}ption({ul:b}oth)} plots Pearson Correlation Coefficient and Spearman's rank correlation coefficient in the same graph.{p_end}
{synopt :{opt p:size(#)}}set the size of the point. Default is 15.{p_end}
{synopt :{opt color(argument)}}set the {it:{help colorpalette}} of the graph. Defult is {cmd:HCL pinkgreen}.It's worth mentioning that one of the blind friendly options is CET CBD1.{p_end}
{synopt :{opt ti:tle(tinfo)}}set the overall title.{p_end}
{synopt :{opt note(tinfo)}}set the note about the graph.{p_end}



{title:Description}

{dlgtab:Introduction}



{p 4 4 2}
Visualize the correlation coefficient with images. Pearson Correlation Coefficient, Spearman's rank correlation coefficient, and both can be chosen.

{title:Examples}

{dlgtab:basic setting: the auto.dta data}

{p 8 6 2}cap program drop correlogram{p_end}
{p 8 6 2}sysuse auto, clear{p_end}
	
{dlgtab:Pearson Correlation Coefficient}

{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length{p_end}
{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(p){p_end}
{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(pearson) title("Correlogram of Auto Dataset Cars") note("Dataset Used: Sysuse Auto") {p_end}
{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(pearson) psize(10) {p_end}

{dlgtab:Spearman's rank correlation coefficient}

{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(s){p_end}
{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(spearman){p_end}
{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(spearman) color(CET CBD1){p_end}

{dlgtab:Pearson Correlation Coefficient and Spearman's rank correlation coefficient}

{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(b){p_end}
{p 8 6 2}correlogram price mpg rep78 headroom trunk weight length, corro(both){p_end}

{title:Acknowledgements}

{p 4 4 2}

Thanks to {browse "https://github.com/fahad-mirza/correlogram_stata":Fahad Mirza} for the license of his software.  {break}

{title:Author}

{phang}
{cmd:Youhui,Lin} Department of Finance, Lingnan College, Sun Yat-Sen University.{break}
E-mail: {browse "linyh78@mail2.sysu.edu.cn":linyh78@mail2.sysu.edu.cn}. {break}
{p_end}


{title:For problems and suggestions}

{p 4 4 2}
Any problems or suggestions are welcome, please Email to
{browse "linyh78@mail2.sysu.edu.cn":linyh78@mail2.sysu.edu.cn}. 

