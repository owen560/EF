&emsp;

> **作者**：林友晖 (中山大学岭南学院)   
> **邮箱**：<linyh78@mail2.sysu.edu.cn>

&emsp;

&emsp;

> **编者按**：本文参考自「[fahad-mirza/correlogram_stata](https://github.com/fahad-mirza/correlogram_stata)」，特此致谢！   

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;
## 1. 引言
在建立模型时，我们往往需要通过相关性对变量进行初步筛选，即模型中的控制变量和因变量之间应该具有一定的相关关系。通过 `correlate`  `pwcorr` 等命令，我们可以轻松实现上述的相关性分析，但是如何用图像更加直观地将结果展示出来呢？

本文参考`GitHub`项目 [correlogram_stata](https://github.com/fahad-mirza/correlogram_stata) 提供的代码来说明如何使“相关系数可视化”。

&emsp;

## 2. 准备工作

### 2.1 schemepack 安装及介绍

```stata
ssc install schemepack, replace
```
由于stata自带的绘图模板有限，因此我们可以利用作者 [Asjad Naqvi](https://github.com/asjadnaqvi/Stata-schemes) 推出的 `schemepack` 去绘制更加美观的图案。

设定绘图模板的形式如下：
```stata
*1
. set scheme white_tableau  //在绘图之前设定绘图模板
*2
. set scheme white_tableau，perm  //永久设定绘图模板
*3
. twoway (scatter var2 date if group==1), scheme(white_tableau)  //在twoway选项scheme()中指定绘图模板
```

其中，模板 `white_tableau` 可以按需替换，有30种绘图模板可供选择，具体介绍可见： [史上最牛Stata绘图模版-schemepack：酷似R中的ggplot2](https://www.lianxh.cn/news/e76a8a7e3c6c4.html)

### 2.2 palettes 安装及介绍
```stata
. ssc install palettes, replace
```
`palettes` 为调色板工具，具体命令为：
```stata
* 语法1：从一个或者多个调色板中检索/显示颜色，并返回至r()中
. colorpalette [argument] [, palette_options macro_options graph_options ]
* 语法2：浏览多个调色板，但不在r()中返回颜色
. colorpalette [, palette_options graph_options ] : pspec [ / pspec / ... ]
```
语法1的 `argument` 语法如下：
```stata
palette [[, palette_options] / [ palette [, palette_options] / ... ]]
``` 
语法2的 `pspec` 语法如下：
```stata
palette [, palette_options]
``` 

由于我们需要利用返回值 `r()`，以便在绘图时调用调色板，因此选用语法1。

下面介绍一下本文利用语法1绘图时主要利用到的几个选项：
- 调色板选项 `palette options` :
  -  `n(#)`：指定调色板的颜色数量
  -  `intensity(numlist)`：设置颜色强度，数值范围为0到255。其中0到1的值使颜色变淡，大于1的值使颜色变深
- 语法1的附加图形选项：
  -  `nograph`：禁止生成调色板预览图

另外，语法1的返回值 `r()` 储存着以下内容：
- **Scalars:**
  - `r(n)`：返回的颜色数量
- **Macros:**
  - `r(ptype)`：颜色
  - `r(pname)`：调色板名称
  - `r(p)`：以空格分隔的颜色列表
  - `r(p#)`：第#个颜色
  - ......


**Note**：
- 若出现错误「colrspace_library_palettes.sthlp not found」，则需要安装 `ssc install colrspace, replace`

- 如果想要了解更多，可以 `help colorpalette` 或者参考连享会往期推文：[Stata绘图：自定义绘图利器-palettes](https://mp.weixin.qq.com/s/eJ0uA1o0zfj_r1wiMXXPIw)

### 2.3 labutil 安装及介绍
```stata
. ssc install labutil, replace
```
利用 `labmask` 可以将字符变量定义为另一个变量的值标签。语法如下：
```stata
labmask varname [if exp] [in range] , values(varname) [ lblname(lblname) decode ]
```
**Options**
- `values(varname)`：指定一个变量，其值(默认)或值标签(可选)将被用作 **varname** 的值标签。这是一个必选项。
- `lblname(lblname)`：指定要定义的值标签的标签名为 `lblname`。默认情况下，它们将具有与 **varname** 相同的名称。
- `decode`：指定变量的值标签应该用作 **varname** 的值标签。默认使用 **varname** 的值。



&emsp;

## 3. 实例演示
### 3.1 数据处理
#### 3.1.1 导入数据并提取相关系数矩阵中的数据
本节采用 Stata 内置的 `auto.dta` 来演示。

首先，导入原始数据。
```stata
. sysuse auto, clear
```
在此，我们利用数据集中的7个变量 **price**、**mpg**、**trunk**、**length**、**turn**、**foreign** 作为示范。
```stata
. local var_corr price mpg trunk weight length turn foreign  //存放变量名称，暂元取名为var_corr
. local countn : word count `var_corr'  //存放变量个数
```
“相关系数可视化”的关键在于对相关系数矩阵的数据、列名和行名进行提取，之后所有的操作皆是基于该矩阵。
```stata
* 计算相关系数矩阵
. quietly correlate `var_corr'
. matrix C = r(C)
. mat list C  //矩阵如下

symmetric C[7,7]
              price         mpg       trunk      weight      length        turn     foreign
  price           1
    mpg  -.46859669           1
  trunk   .31433161  -.58158503           1
 weight   .53861146  -.80717486   .67220573           1
 length   .43183124  -.79577944   .72659561   .94600864           1
   turn   .30961742  -.71918633   .60105949   .85744287   .86426115           1
foreign   .04871947   .39339742  -.35943477   -.5928299  -.57019759   -.6310965           1
```
利用暂元存放矩阵的行名，在后续作图中作为横轴和纵轴的名称。
```stata
. local rnames : rownames C  //存放行名
. dis "`rnames'"
price mpg trunk weight length turn foreign
```	
#### 3.1.2 生成新数据集以存放作图需要的数据
接下来，清除原数据集，并利用相关系数矩阵生成新的数据集。
```stata
* 现在从相关系数矩阵中生成变量
. clear
		
* 生成7*7个观察值
. local tot_rows : display `countn' * `countn'
. set obs `tot_rows'
```
从相关系数矩阵中生成相关系数变量 **corr**、**abs_corr**，同时生成相关系数对应的两个变量。
```stata
* 生成字符型变量 corrname1、corname2，和数值型变量 y、x、corr、abs_corr
. generate corrname1 = ""
. generate corrname2 = ""
. generate y = .
. generate x = .
. generate corr = .
. generate abs_corr = .
		
. local row = 1
. local y = 1
. local rowname = 2
			
. foreach name of local var_corr {
	forvalues i = `rowname'/`countn' { 
		local a : word `i' of `var_corr'
		replace corrname1 = "`name'" in `row'
		replace corrname2 = "`a'" in `row'
		replace y = `y' in `row'
		replace x = `i' in `row'
		replace corr = round(C[`i',`y'], .01) in `row'
		replace abs_corr = abs(C[`i',`y']) in `row'

		local ++row			
	}
	local rowname = `rowname' + 1
	local y = `y' + 1		
	}

. drop if missing(corrname1)  //去除多余的观察值
. replace abs_corr = 0.1 if abs_corr < 0.1 & abs_corr > 0.04
. list  //数据结构如下
     +-----------------------------------------------+
     | corrna~1   corrna~2   y   x   corr   abs_corr |
     |-----------------------------------------------|
  1. |    price        mpg   1   2   -.47   .4685967 |
  2. |    price      trunk   1   3    .31   .3143316 |
  3. |    price     weight   1   4    .54   .5386115 |
  4. |    price     length   1   5    .43   .4318312 |
  5. |    price       turn   1   6    .31   .3096174 |
     |-----------------------------------------------|
  6. |    price    foreign   1   7    .05         .1 |
  7. |      mpg      trunk   2   3   -.58    .581585 |
  8. |      mpg     weight   2   4   -.81   .8071749 |
  9. |      mpg     length   2   5    -.8   .7957795 |
 10. |      mpg       turn   2   6   -.72   .7191863 |
     |-----------------------------------------------|
 11. |      mpg    foreign   2   7    .39   .3933974 |
 12. |    trunk     weight   3   4    .67   .6722057 |
 13. |    trunk     length   3   5    .73   .7265956 |
 14. |    trunk       turn   3   6     .6   .6010595 |
 15. |    trunk    foreign   3   7   -.36   .3594348 |
     |-----------------------------------------------|
 16. |   weight     length   4   5    .95   .9460086 |
 17. |   weight       turn   4   6    .86   .8574429 |
 18. |   weight    foreign   4   7   -.59   .5928299 |
 19. |   length       turn   5   6    .86   .8642612 |
 20. |   length    foreign   5   7   -.57   .5701976 |
     |-----------------------------------------------|
 21. |     turn    foreign   6   7   -.63   .6310965 |
     +-----------------------------------------------+

* 其中 y 和 corrname1 ，以及 x 和 corrname2 的对应关系如下：
. list corrname1 y corrname2 x
     +-----------------------------+
     | corrna~1   y   corrna~2   x |
     |-----------------------------|
  1. |    price   1        mpg   2 |
  2. |    price   1      trunk   3 |
  3. |    price   1     weight   4 |
  4. |    price   1     length   5 |
  5. |    price   1       turn   6 |
     |-----------------------------|
  6. |    price   1    foreign   7 |
  7. |      mpg   2      trunk   3 |
  8. |      mpg   2     weight   4 |
  9. |      mpg   2     length   5 |
 10. |      mpg   2       turn   6 |
     |-----------------------------|
 11. |      mpg   2    foreign   7 |
 12. |    trunk   3     weight   4 |
 13. |    trunk   3     length   5 |
 14. |    trunk   3       turn   6 |
 15. |    trunk   3    foreign   7 |
     |-----------------------------|
 16. |   weight   4     length   5 |
 17. |   weight   4       turn   6 |
 18. |   weight   4    foreign   7 |
 19. |   length   5       turn   6 |
 20. |   length   5    foreign   7 |
     |-----------------------------|
 21. |     turn   6    foreign   7 |
     +-----------------------------+
```

### 3.2 绘制图像

#### 3.2.1 设置图像颜色

利用 `colorpalette` 设置图像颜色，并利用返回值 `r(p#)` 对不同区间中的相关系数 **corr** 定义不同的颜色。
```stata
* colorpalette HCL pinkgreen, n(10) nograph intensity(0.65)
. colorpalette CET CBD1, n(10) nograph  //该颜色组合对色盲友好。此处对应着最后相关系数图的图像颜色。
. generate colorname = ""
. local col = 1
. forvalues colrange = -1(0.2)0.8 {
	replace colorname = "`r(p`col')'" if corr >= `colrange' & corr < `=`colrange' + 0.2'
	replace colorname = "`r(p10)'" if corr == 1
	local ++col
  }	
. list corr colorname  //不同区间的corr对应不同的颜色，colorname为不同颜色对应的RGB值
     +--------------------+
     | corr     colorname |
     |--------------------|
  1. | -.47   160 185 249 |
  2. |  .31   221 206 167 |
  3. |  .54   205 185 121 |
  4. |  .43   205 185 121 |
  5. |  .31   221 206 167 |
     |--------------------|
  6. |  .05   234 228 215 |
  7. | -.58   160 185 249 |
  8. | -.81    58 144 254 |
  9. |  -.8    58 144 254 |
 10. | -.72   119 164 252 |
     |--------------------|
 11. |  .39   221 206 167 |
 12. |  .67    187 164 73 |
 13. |  .73    187 164 73 |
 14. |   .6    187 164 73 |
 15. | -.36   194 207 246 |
     |--------------------|
 16. |  .95     168 144 8 |
 17. |  .86     168 144 8 |
 18. | -.59   160 185 249 |
 19. |  .86     168 144 8 |
 20. | -.57   160 185 249 |
     |--------------------|
 21. | -.63   119 164 252 |
     +--------------------+
```

 调色板颜色组合的预览图如下图所示：
  ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20230102171640.png)
  ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20230102150838.png)  

#### 3.2.2 保存绘图命令

```stata
* 利用暂元保存绘图命令
. forvalues i = 1/`=_N' {
	local slist "`slist' (scatteri `=y[`i']' `=x[`i']' "`: display %3.2f corr[`i']'", mlabposition(0) msize(`=abs_corr[`i']*15') mcolor("`=colorname[`i']'"))"
  }
```
**关键命令：**
```stata
twoway scatteri immediate_values [, options]
```
`scatteri` 为 `twoway scatter` 的即时版本，能够通过坐标绘制数据点。

**语法要点：**
- **Immediate_values** 
  - **语法：** 
   
    `#_y #_x [(#_clockposstyle)] ["text for label"]` 
   
    其中 `#_y`、`#_X` 分别为y轴和x轴的坐标; `(#_clockposstyle)` 为位置选择，默认为0，即位于中心； `"text for label"` 为标签文本。
  
  - **示例：** 
    在(1,1)和(2,2)处绘制数据点，并分别在3点钟方向和9点钟方向标注 First_point 与 Second_point 。
    
```stata
    twoway scatteri 1 1 (3) "First_point" 2 2 (9) "Second_point"
```


- **Options**
  - `msize(markersizestylelist)`：散点的大小。此处设定为相关系数绝对值的15倍，从而使得相关系数绝对值越大时，散点越大。
  - `mcolor(colorstylelist)`：散点的颜色与不透明度。
  - `mlabposition(clockposlist)`：标签的位置。

#### 3.2.3 保存横纵轴标签

```stata	
* 保存纵轴标签
. labmask y, val(corrname1)
. labmask x, val(corrname2)
	
. levelsof y, local(yl)
. foreach l of local yl {
	local ylab "`ylab' `l'  `" "`:lab (y) `l''" "'"		
  }	

* 保存横轴标签
. levelsof x, local(xl)
. foreach l of local xl {
	local xlab "`xlab' `l'  `" "`:lab (x) `l''" "'"	
  }		
```
#### 3.2.4 绘图
```stata
* 利用上述保存的暂元绘制图像
. twoway `slist', title("Correlogram of Auto Dataset Cars", size(3) pos(11)) ///
	note("Dataset Used: Sysuse Auto", size(2) margin(t=5)) ///
	xlabel(`xlab', labsize(2.5)) ylabel(`ylab', labsize(2.5)) ///
	xscale(range(1.75 )) yscale(range(0.75 )) ///
	ytitle("") xtitle("") ///
	legend(off) ///
	aspect(1) ///
	scheme(white_tableau)

* 以 PNG 格式输出图像		
. graph export "~/Desktop/correlogram_stata_cbf.png", as(png) width(1920) replace 
```
### 3.3 图像展示
```stata
. colorpalette HCL pinkgreen, n(10) nograph intensity(0.65)
```
当 3.2.1 中颜色设定为 `HCL pinkgreen` 时，最后的图像如下图所示：
 ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/correlogram_stata.png)

```stata
. colorpalette CET CBD1, n(10) nograph
```
当 3.2.1 颜色设定为 `CET CBD1` 时，最后的图像如下图所示：
 ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/correlogram_stata_cbf.png)

&emsp;

## 4. 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
> &emsp; `lianxh 可视化, m`  
> 安装最新版 `lianxh` 命令：    .
> 
> &emsp; `ssc install lianxh, replace` 

- 专题：[专题课程](https://www.lianxh.cn/blogs/44.html)
  - [⏩连享会公开课：实证研究中的数据可视化](https://www.lianxh.cn/news/9c86f40c6f17a.html)
- 专题：[数据处理](https://www.lianxh.cn/blogs/25.html)
  - [Stata：边际处理效应及其可视化-mtefe-T309](https://www.lianxh.cn/news/edd2a0cf9fa6a.html)
  - [Stata: 约翰霍普金斯大学 COVID-19 疫情数据处理及可视化](https://www.lianxh.cn/news/ca5082adbf97f.html)
- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata可视化：能用图形就不用表格](https://www.lianxh.cn/news/0edc0cc509220.html)
  - [Stata绘图：回归系数可视化-multicoefplot](https://www.lianxh.cn/news/3e052bd3291dd.html)
  - [Stata绘图-可视化：组间差异比较散点图](https://www.lianxh.cn/news/b5fc0aeb1d7b7.html)
  - [Stata可视化：biplot一图看尽方差、相关性和主成分](https://www.lianxh.cn/news/3173ebd034f12.html)
  - [Stata绘图-组间差异可视化：不良事件火山图、点阵图](https://www.lianxh.cn/news/f9680c4be14fe.html)
  - [forest-森林图：分组回归系数可视化](https://www.lianxh.cn/news/472768848cd8d.html)
  - [Stata绘图：回归系数可视化-论文更出彩](https://www.lianxh.cn/news/324f5e3053883.html)
  - [Stata绘图：世行可视化案例-条形图-密度函数图-地图-断点回归图-散点图](https://www.lianxh.cn/news/96989b0de4d83.html)
  - [Stata绘图：随机推断中的系数可视化](https://www.lianxh.cn/news/ce93c41862c16.html)
- 专题：[结果输出](https://www.lianxh.cn/blogs/22.html)
  - [Stata可视化：让他看懂我的结果！](https://www.lianxh.cn/news/01607de7be5e8.html)
- 专题：[回归分析](https://www.lianxh.cn/blogs/32.html)
  - [Stata：在线可视化模拟-OLS-的性质](https://www.lianxh.cn/news/555995cc140a8.html)
- 专题：[Python-R-Matlab](https://www.lianxh.cn/blogs/37.html)
  - [Python 调用 API 爬取百度 POI 数据小贴士——坐标转换、数据清洗与 ArcGIS 可视化](https://www.lianxh.cn/news/a72842993b22b.html)
- 专题：[工具软件](https://www.lianxh.cn/blogs/23.html)
  - [知乎热议：有哪些一用就爱上的可视化工具？](https://www.lianxh.cn/news/67e8b9c15a0e9.html)
- 专题：[其它](https://www.lianxh.cn/blogs/33.html)
  - [数据可视化：带孩子们边玩边学吧](https://www.lianxh.cn/news/5a0f12c4ea08a.html)

