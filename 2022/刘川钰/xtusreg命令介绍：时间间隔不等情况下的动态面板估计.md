# xtusreg命令介绍：时间间隔不等情况下的动态面板估计

[TOC]

>**作者:** 刘川钰 (中山大学)  
>**E-mail:** <liuchy85@mail2.sysu.edu.cn>  

>**备注:** 本推文部分摘译自以下论文，特此致谢！  
>**Source:** Sasaki, Y., Y. Xin, 2022, xtusreg: Software for dynamic panel regression under irregular time spacing, The Stata Journal, 22 (3): 713-724. [-PDF-](http://sage.cnpereading.com/paragraph/download/?doi=10.1177/1536867X221124567)

## 1. 背景介绍

估计固定效应动态面板回归模型的传统命令 `xtabond` 要求模型的时间间隔必须是**三个连续的时间段**或**两对两个连续的时间段**。但是现实研究中有许多数据的观测都不满足上述要求，所以我们需要借助**不规则时间间隔的面板数据估计方法**来实现更多动态面板回归模型的参数估计。

本推文将要介绍的新命令 `xtusreg` 就是基于时间间隔不相等的面板数据对其固定效应模型进行回归参数的估计，为了对此命令的应用场景有更清晰的认识，下面对命令涉及的一些背景知识进行介绍：

### 1.1 不等时间间隔

例如，现在有这样一份数据，显示有学者分别在**1966年、1967年、1969年、1971年、1976年、1981年和1990年**对 National Longitudinal Survey Original Cohorts Older Men 成员进行了个人访谈，并收集了他们的收入、年龄、受教育年限和是否为白种人等信息。这个例子的年份列表中既没有三个连续的时间段，也没有两对两个连续的时间段。这个调查的年份数据出现了**时间间隔不等**的情况，我们无法用传统的方法 `xtabond` 对其回归模型进行估计。

Sasaki & Xin (2017) 的研究表明，即使面板数据表现出不规则、不相等的时间间隔，只要数据存在**两对两个连续的时间间隔**，那么仍然可以估计出固定效应的动态面板回归模型的参数，这是对传统的**两对两个连续的时间段**要求的放松及扩展。

### 1.2 实例说明

在 National Longitudinal Survey Original Cohorts Older Men 的例子中，1966年与1966年的时间差为0,1966年与1967年的时间差为1,1967年与1969年的时间差为2,1966年与1969年的时间差为3。因此，在这个面板数据集中有两对连续的时间间隔：{0,1}和{2,3}，可以通过 `xtusreg` 命令实现参数估计。


## 2.xtusreg命令介绍

### 2.1 理论介绍

我们考虑如下模型：
$$
y_{i t}=\gamma y_{i, t-1}+\beta x_{i t}+\alpha_i+\varepsilon_{i t}\quad(1)
$$
其中$y_{i t}$表示观察到的状态变量，$x_{i t}$表示观察到的协变量，$\alpha_i$表示未观察到的个体固定效应，$\varepsilon_{i t}$表示未观察到的特殊冲击。我们重点关注$\beta$和$\gamma$变量。
Sasaki & Xin (2017) 的研究提出了时间间隔不等情况下的阐述估计方法，具体讨论可分为以下步骤：

**Step 1:定义辅助随机变量**

我们将个体i的特定的固定效应 $\alpha_i$ 的期望 $E\left(\cdot \mid \alpha_i\right)$ 简记为 $E_i(\cdot):$ ，即$E_i(\cdot):=E\left(\cdot \mid \alpha_i\right)$。
然后我们对于任意的时间 $t$ 和 时间间隔 $\tau$ ，定义如下辅助随机变量：
$$
\begin{aligned}
Z_{i \tau} &:=E_i\left(y_{i t} y_{i, t+\tau}\right) & z_{i \tau} &:=E_i\left(x_{i t} x_{i, t+\tau}\right) \\
\zeta_{i \tau} &:=E_i\left(y_{i t} x_{i, t+\tau}\right) & \zeta_{i,-\tau} &:=E_i\left(x_{i t} y_{i, t+\tau}\right)
\end{aligned}
$$

**Step 2：定义时间差距的集合**

设T是研究人员观察面板数据$\left\{\left(y_{i t}, x_{i t}\right)\right\}_{i=1}^N$的时间间隔不等的时间段集T。
通过以下方法定义调查时间差距的集合：

$$
\mathcal{T}=\left\{\left|t_1-t_2\right|: t_1, t_2 \in T\right\}
$$

以及与时间差距相关的调查年的集合：    

$$
T(\tau)=\{t \in T: t+\tau \in T\}
$$

其中任意时间间隔$\tau \in \mathcal{T}$，并且令$T(\tau)=\emptyset$ ，如果 $\tau \notin \mathcal{T}$。

**Step 3：基于时间间距结构US spacing的讨论**

如果时间间隔不相等的面板数据对于自然数集合中的某个间隙$\Delta T$满足 $T(1) \neq \emptyset, T(\Delta t) \neq \emptyset$ 以及 $T(\Delta t+1) \neq \emptyset$时，根据 Sasaki & Xin (2017) 称其间距结构为"**US spacing**"。

在"**US spacing**"间距结构下，$(\gamma, \beta)^{\prime}$可以被定义为：

$$
\left(\begin{array}{l}
\gamma \\
\beta
\end{array}\right)=\frac{1}{|\Delta|}\left(\begin{array}{c}
\left(z_{\Delta t+1}-z_1\right)\left(Z_{\Delta t+1}-Z_1\right)+\left(\zeta_1-\zeta_{\Delta t+1}\right)\left(\zeta_{-(\Delta t+1)}-\zeta_{-1}\right) \\
\left(\zeta_0-\zeta_{-\Delta t}\right)\left(Z_{\Delta t+1}-Z_1\right)+\left(Z_{\Delta t}-Z_0\right)\left(\zeta_{-(\Delta t+1)}-\zeta_{-1}\right)
\end{array}\right)\quad(2)
$$

其中，$|\Delta|:=\left(Z_{\Delta t}-Z_0\right)\left(z_{\Delta t+1}-z_1\right)-\left(\zeta_{-\Delta t}-\zeta_0\right)\left(\zeta_{\Delta t+1}-\zeta_1\right)$，同时假设$|\Delta|\neq 0$成立。

**Step 4：估计辅助随机变量**

给定识别结果后，取对应样本来获得估计量。首先对于辅助随便变量可以通过以下方法进行估计：

$$
\hat{Z}_\tau=\frac{1}{N} \sum_{i=1}^N \bar{Z}_{i \tau} \quad \hat{z}_\tau=\frac{1}{N} \sum_{i=1}^N \bar{z}_{i \tau} \quad \hat{\zeta}_\tau=\frac{1}{N} \sum_{i=1}^N \bar{\zeta}_{i \tau} \quad \hat{\zeta}_{-\tau}=\frac{1}{N} \sum_{i=1}^N \bar{\zeta}_{i,-\tau}
$$

在时不变矩假设下，$\bar{Z}_{i \tau},\bar{z}_{i \tau},\bar{\zeta}_{i \tau}$ 和 $\bar{\zeta}_{i,-\tau}$ 可由以下形式表示：

$$
\begin{aligned}
\bar{Z}_{i \tau} &=\sum_{t \in T(\tau)} a_t^\tau y_{i t} y_{i, t+\tau} & \bar{z}_{i \tau} &=\sum_{t \in T(\tau)} b_t^\tau x_{i t} x_{i, t+\tau} \\
\bar{\zeta}_{i \tau} &=\sum_{t \in T(\tau)} c_t^\tau y_{i t} x_{i, t+\tau} & \bar{\zeta}_{i,-\tau} &=\sum_{t \in T(\tau)} d_t^\tau x_{i t} y_{i, t+\tau}
\end{aligned}
$$

其中，$a^\tau=\left(a_t^\tau\right)_{t \in T(\tau)}, b^\tau=\left(b_t^\tau\right)_{t \in T(\tau)}, c^\tau=\left(c_t^\tau\right)_{t \in T(\tau)}$ 和 $d^\tau=\left(d_t^\tau\right)_{t \in T(\tau)}$ 满足 $\sum_{t \in T(\tau)} a_t^\tau=1, \sum_{t \in T(\tau)} b_t^\tau=1, \sum_{t \in T(\tau)} c_t^\tau=1$ 和 $\sum_{t \in T(\tau)} d_t^\tau=1$条件。

`xtusreg` 命令使用简单的算术平均数来识别公式的对应样本产生**显式估计量**：

$$
\left(\begin{array}{l}
\hat{\gamma} \\
\hat{\beta}
\end{array}\right)=\frac{1}{|\hat{\Delta}|}\left(\begin{array}{c}
\left(\hat{z}_{\Delta t+1}-\hat{z}_1\right)\left(\hat{Z}_{\Delta t+1}-\hat{Z}_1\right)+\left(\hat{\zeta}_1-\hat{\zeta}_{\Delta t+1}\right)\left(\hat{\zeta}_{-(\Delta t+1)}-\hat{\zeta}_{-1}\right) \\
\left(\hat{\zeta}_0-\hat{\zeta}_{-\Delta t}\right)\left(\hat{Z}_{\Delta t+1}-\hat{Z}_1\right)+\left(\hat{Z}_{\Delta t}-\hat{Z}_0\right)\left(\hat{\zeta}_{-(\Delta t+1)}-\hat{\zeta}_{-1}\right),
\end{array}\right)\quad(3)
$$

其中，$|\hat{\Delta}|=\left(\hat{Z}_{\Delta t}-\hat{Z}_0\right)\left(\hat{z}_{\Delta t+1}-\hat{z}_1\right)-\left(\hat{\zeta}_{-(\Delta t)}-\hat{\zeta}_0\right)\left(\hat{\zeta}_{\Delta t+1}-\hat{\zeta}_1\right)
$

**Step 5：估计参数$\theta$**

以上的过程只关注**恰好识别**的情况，但通用的GMM满足以下条件：

$$
E\left(\boldsymbol{g}\left(\boldsymbol{w}_i, \boldsymbol{\theta}_0\right)\right)=\mathbf{0}
$$

其中，$\boldsymbol{\theta}=(\gamma,\beta)$ ，$\boldsymbol{w}_i=\left(x_{i t}, y_{i t}\right)_{t \in T}$，距函数 $\boldsymbol{g}=(\boldsymbol{w}_i, \boldsymbol{\theta})$的行包括：

$$
\begin{aligned}
g_{1, t t^{\prime} t^{\prime \prime} t^{\prime \prime \prime}}\left(\boldsymbol{w}_i, \boldsymbol{\theta}\right)=& {\left[y_{i, t^{\prime \prime \prime}} y_{i, t^{\prime \prime \prime}+\Delta t+1}-y_{i, t^{\prime}} y_{i, t^{\prime}+1}\right] } \\
&-\gamma\left[y_{i, t^{\prime \prime}} y_{i, t^{\prime \prime}+\Delta t}-y_{i t} y_{i, t}\right]-\beta\left[y_{i, t^{\prime \prime \prime}} x_{i, t^{\prime \prime \prime}+\Delta t+1}-y_{i, t^{\prime}} x_{i, t^{\prime}+1}\right] \\
g_{2, t t^{\prime} t^{\prime \prime} t^{\prime \prime \prime}}\left(\boldsymbol{w}_i, \boldsymbol{\theta}\right)=& {\left[x_{i, t^{\prime \prime \prime}} y_{i, t^{\prime \prime \prime}+\Delta t+1}-x_{i, t^{\prime}} y_{i, t^{\prime}+1}\right] } \\
&-\gamma\left[x_{i, t^{\prime \prime}} y_{i, t^{\prime \prime}+\Delta t}-x_{i t} y_{i, t}\right]-\beta\left[x_{i, t^{\prime \prime \prime}} x_{i, t^{\prime \prime \prime}+\Delta t+1}-x_{i, t^{\prime}} x_{i, t^{\prime}+1}\right]
\end{aligned}
$$

其中，$\left(t, t^{\prime}, t^{\prime \prime}, t^{\prime \prime \prime}\right) \in T(0) \times T(1) \times T(\Delta t) \times T(\Delta t+1)$。

`xtusreg` 命令可以基于这些矩条件限制实现GMM，因此它可以处理**恰好识别**和**过度识别**的情况。具体的估计方法如下：

$$
\hat{\boldsymbol{\theta}}=\arg \min _{\boldsymbol{\theta} \in \Theta}\left[\frac{1}{N} \sum_{i=1}^N \boldsymbol{g}\left(\boldsymbol{w}_i, \boldsymbol{\theta}\right)\right]^{\prime} \boldsymbol{W}_N\left[\frac{1}{N} \sum_{i=1}^N \boldsymbol{g}\left(\boldsymbol{w}_i, \boldsymbol{\theta}\right)\right]\quad(4)
$$

其中，$\boldsymbol{W}_N$ 是一个权重矩阵。`xtusreg` 命令在第一阶段使用单位矩阵，在第二阶段中使用估计的最优权重矩阵。在恰好识别的情况下，GMM估计量与方程(3)表示的显示估计量一致。

即使使用了GMM的理论框架，我们也要强调，为了命令的运行，面板数据的数据结构依然需要是"US间距"。

基于渐近正态性计算GMM估计量的方差矩阵：

$$
\sqrt{N}\left(\hat{\boldsymbol{\theta}}-\boldsymbol{\theta}_0\right) \stackrel{d}{\rightarrow} N\left(0,\left(\boldsymbol{G}^{\prime} \boldsymbol{W} \boldsymbol{G}\right)^{-1} \boldsymbol{G}^{\prime} \boldsymbol{W} \boldsymbol{S} \boldsymbol{W} \boldsymbol{G}\left(\boldsymbol{G}^{\prime} \boldsymbol{W} \boldsymbol{G}\right)^{-1}\right)
$$

这里，$\boldsymbol{S}$ 是 $\boldsymbol{g}=(\boldsymbol{w}_i, \boldsymbol{\theta})$ 的方差矩阵，而 $\boldsymbol{G}$ 可写成以下形式：

$$
\boldsymbol{G}=E\left[\begin{array}{cc}
\vdots & \vdots \\
-\left[y_{i, t^{\prime \prime}} y_{i, t^{\prime \prime}+\Delta t}-y_{i t} y_{i, t}\right] & -\left[y_{i, t^{\prime \prime \prime}} x_{i, t^{\prime \prime \prime}+\Delta t+1}-y_{i, t^{\prime}} x_{i, t^{\prime}+1}\right] \\
\vdots & \vdots \\
-\left[x_{i, t^{\prime \prime}} y_{i, t^{\prime \prime}+\Delta t}-x_{i t} y_{i, t}\right] & -\left[x_{i, t^{\prime \prime \prime}} x_{i, t^{\prime \prime \prime}+\Delta t+1}-x_{i, t^{\prime}} x_{i, t^{\prime}+1}\right] \\
\vdots & \vdots
\end{array}\right]
$$

本文介绍的 `xtusreg` 命令并不使用两对连续时间间隔的所有组合。相反，我们使用**两对最小的两个连续时间间隔**。因为，在实践中较大的时间间隔往往导致有限样本的较大偏差。

### 2.2 语法结构

`xtusreg` 是 Sasaki & Xin (2017) 根据以上估计方法所编写的 Stata 新命令。
`xtusreg` 是一个 `eclass` 的命令，可以通过以下方法下载：
```stata
. ssc install xtusreg
```
`xtusreg`语法结构如下：

```stata
xtusreg depvar [indepvars] [if ] [in] ///
        [, onestep stationary sweight(var) gamma(real) beta(real)]
```
* `depvar`：被解释变量；
* `indepvars`：进入回归模型的解释变量；
* `onestep`：设置一阶段GMM估计的指标。默认情况下，不调用此选项将导致两阶段有效的GMM估计。如果参数是恰好识别的，此选项不会对结果产生影响；
* `stationary`：设置不执行变量位置-规模标准化的指标。默认情况下，不调用此选项将导致位置-规模标准化；
* `sweight(var)`：设置采样权重。默认情况下，所有观察值权重一样；
* `gamma(real)`：设置GMM估计中数值优化的自回归系数的初始值。默认情况下，初始值为$gamma(0)$；
* `beta(real)`：设置GMM估计中数值优化的回归系数的初始值。默认情况下，初始值为$beta(0)$。

详情请参考 `help xtusreg`

## 3.Stata实例

下面我们用NLS Original Cohorts Older Men的真实数据来检验此命令。这是一个 Stata 官方在介绍 `xtusreg` 命令时所使用的数据集。
```stata
. use "NLS_Originl_Cohort.dta"
/* Notes:
数据中每一个个体为一名男性，包括了其被采访时间，年龄，收入的对数，受教育年限，是否为白种人等信息。
    year：被采访的年份
    logincome：收入的对数
    age:年龄
    edu:受教育年限
    white:是否为白种人
*/
```
我们先分别通过 **id** 和 **year** 作为个体和时间的指标设置了面板数据结构：
```stata
. xtset id year 

Panel variable: id (strongly balanced)
 Time variable: year, 65 to 68, but with gaps
         Delta: 1 unit

```
设置完面板数据结构后，我们首先对 $y=logincome$ 进行简单的自回归：
```stata
. xtusreg logincome 

--------------------------------
GMM Estimation
--------------------------------
Iteration 0:   f(p) =   .0144283  
Iteration 1:   f(p) =  2.862e-19  
Iteration 2:   f(p) =  2.375e-29  

Balanced Portion of Panel Data
------------------------------------------------------------------------------
Number of observations:          8994 
Number of cross-section units:   2998 
Number of time periods:          3 
List of time periods:            65, 66, 68
------------------------------------------------------------------------------
L1 = Autoregressive Coefficient (gamma)
------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
          L1 |   .4775666   .1029879     4.64   0.000      .275714    .6794192
------------------------------------------------------------------------------

```
由上述的回归结果，我们可以发现自回归参数 $\gamma$ 的估计值是显著为正的，这表明收入的对数是正自相关的，并且遵循一个平稳过程。
接着我们将 **age** 作为控制变量 $x$ 加入回归方程：
```stata
. xtusreg logincome age

--------------------------------
GMM Estimation
--------------------------------
Iteration 0:   f(p) =  .01461312  
Iteration 1:   f(p) =   .0004497  
Iteration 2:   f(p) =   .0004497  (backed up)

Balanced Portion of Panel Data
------------------------------------------------------------------------------
Number of observations:          8994 
Number of cross-section units:   2998 
Number of time periods:          3 
List of time periods:            65, 66, 68
------------------------------------------------------------------------------
L1 = Autoregressive Coefficient (gamma)
------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
          L1 |   .4626891   .1014739     4.56   0.000     .2638038    .6615743
         age |  -.0000174   .0000626    -0.28   0.781    -.0001401    .0001052
------------------------------------------------------------------------------

```
上表的回归结果显示，控制因素 $x$ 的加入对自回归参数 $\gamma$ 的点估计有影响，但统计显著性较没加入之前有提高，且 **age** 的系数 $\beta$ 也显著。
除此之外，**edu** 和 **white** 也属于控制变量 $x$ 的范畴，但这两个变量通常不随时间而改变，无法将其纳入控制变量中进行回归分析。故为了观察到人种的异质性，我们可以对每个种类进行分组回归：
```stata
. xtusreg logincome if !white

--------------------------------
GMM Estimation
--------------------------------
Iteration 0:   f(p) =  .00765154  
Iteration 1:   f(p) =  2.534e-21  
Iteration 2:   f(p) =  4.965e-30  

Balanced Portion of Panel Data
------------------------------------------------------------------------------
Number of observations:          2853 
Number of cross-section units:   951 
Number of time periods:          3 
List of time periods:            65, 66, 68
------------------------------------------------------------------------------
L1 = Autoregressive Coefficient (gamma)
------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
          L1 |   .2597244   .1276535     2.03   0.042     .0095281    .5099206
------------------------------------------------------------------------------

. xtusreg logincome if white

--------------------------------
GMM Estimation
--------------------------------
Iteration 0:   f(p) =  .02699349  
Iteration 1:   f(p) =  1.707e-23  
Iteration 2:   f(p) =  7.519e-29  

Balanced Portion of Panel Data
------------------------------------------------------------------------------
Number of observations:          6141 
Number of cross-section units:   2047 
Number of time periods:          3 
List of time periods:            65, 66, 68
------------------------------------------------------------------------------
L1 = Autoregressive Coefficient (gamma)
------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
          L1 |   .6318455    .144441     4.37   0.000     .3487464    .9149446
------------------------------------------------------------------------------

```
最后，和 **white** 一样，我们对 **educ** 的两个类别进行了分组回归：
```stata
. xtusreg logincome if edu < 12

--------------------------------
GMM Estimation
--------------------------------
Iteration 0:   f(p) =  .01558785  
Iteration 1:   f(p) =  1.981e-20  
Iteration 2:   f(p) =  1.098e-28  

Balanced Portion of Panel Data
------------------------------------------------------------------------------
Number of observations:          5766 
Number of cross-section units:   1922 
Number of time periods:          3 
List of time periods:            65, 66, 68
------------------------------------------------------------------------------
L1 = Autoregressive Coefficient (gamma)
------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
          L1 |   .4015673   .1041893     3.85   0.000       .19736    .6057745
------------------------------------------------------------------------------

. xtusreg logincome if edu >= 12

--------------------------------
GMM Estimation
--------------------------------
Iteration 0:   f(p) =  .02754738  
Iteration 1:   f(p) =  5.608e-19  
Iteration 2:   f(p) =  4.824e-27  

Balanced Portion of Panel Data
------------------------------------------------------------------------------
Number of observations:          3228 
Number of cross-section units:   1076 
Number of time periods:          3 
List of time periods:            65, 66, 68
------------------------------------------------------------------------------
L1 = Autoregressive Coefficient (gamma)
------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
          L1 |   .7372661   .2292074     3.22   0.001     .2880279    1.186504
------------------------------------------------------------------------------

```
由分组回归的结果可以发现，那些具有12年或以上受教育经历的个体的自回归参数 $\gamma$ 更大，这意味着 **logincome** 对受教育年限更高的人群更持久。

由此，新命令 `xtusreg` 可以帮助研究人员使用时间间隔不相等的动态面板数据进行研究。

## 4.结语

估计固定效应动态面板回归模型的传统方法需要对**三个连续的时间段**或**两对两个连续的时间段**进行观测。但实际研究中，通常观测值之间的时间间隔并不相等，而在本文中，我们介绍的 `xtusreg` 命令正是解决这类情况的估计方法。Sasaki & Xin (2017) 将观测时间的要求拓展为**两对两个连续的时间间隔**。通过 `xtusreg` 命令，我们可以实现在**时间间隔不等情况**下执行固定效应动态面板回归的估计和推断。但是当前 `xtusreg` 只适用于面板数据中时间间隔不等的特定形式，即"**US spacing**"。

## 5.参考资料

- Sasaki, Y., Y. Xin, 2022, xtusreg: Software for dynamic panel regression under irregular time spacing, The Stata Journal, 22 (3): 713-724. [-PDF-](http://sage.cnpereading.com/paragraph/download/?doi=10.1177/1536867X221124567)
- Sasaki, Y. and Y. Xin (2017) Unequal Spacing in Dynamic Panel Data: Identification and Estimation. Journal of Econometrics, 196 (2), pp. 320-330. [-PDF-](https://www.sciencedirect.com/science/article/pii/S0304407616301932)
- 姚旭生，连享会推文，[动态面板数据模型与xtabond2应用](https://www.lianxh.cn/news/b4ee03b676d55.html)

## 6.相关推文

Note：产生如下推文列表的 Stata 命令为：   
`lianxh 面板数据`  
安装最新版 `lianxh` 命令：    
`ssc install lianxh, replace` 
- 专题：[Stata程序](https://www.lianxh.cn/blogs/26.html)
  - [Stata：动态面板数据模型与xtabond2应用](https://www.lianxh.cn/news/b4ee03b676d55.html)
  - [Stata实操陷阱：动态面板数据模型](https://www.lianxh.cn/news/cc6c5ea80d70c.html)
  - [Stata: 面板数据模型一文读懂](https://www.lianxh.cn/news/bf27906144b4e.html)