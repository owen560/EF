from bs4 import BeautifulSoup
from selenium import webdriver
import pandas as pd
def get_url(DOI):
    url = f'''https://doi.org/{DOI}'''
    return url

scholar_link = []
title = []
JR = []
page = []
symbol = ", "
output = []
output_temp = []
DOI = []
pdf_link = []
def get_citation():
    # browser = webdriver.Chrome(executable_path=r'/Users/xieluozheng/Downloads/chromedriver')
    browser = webdriver.Chrome()
    browser.get(url)
    data = browser.page_source
    soup = BeautifulSoup(data, 'lxml')
    references = soup.select('.citation .citation-content')
    googlescholar = soup.select('.citation .external-links')
    doi_d = soup.select('.citation-content a')
    reference_n = len(references)
    for j in range(reference_n):
        if len(references[j]) != 5:
            # references[j].decompose()
            references[j].extract()
    references = soup.select('.citation .citation-content')
    googlescholar = soup.select('.citation .external-links')
    doi_d = soup.select('.citation-content a')
    reference_n = len(references)
    print(len(references))
    print(len(doi_d))
    print(len(googlescholar))
    n = 0
    for i in range(0, reference_n):
        temp = references[i]
        # print(len(temp))
        # print(i)
        # print(temp)
        jr_d = str(temp.contents[1])
        jr_d = jr_d.strip("<em>")
        jr_d = jr_d[:-2]
        title.append(str(temp.contents[0]))
        JR.append(str(jr_d))
        page.append(str(temp.contents[2]))
        output_temp = title[i] + symbol + JR[i] + symbol + page[i]
        output.append(output_temp)

        temp_g = googlescholar[i]
        temp_g = temp_g.select('a')[1]
        temp_g = str(temp_g.attrs['href'])
        temp_g = f'''[-GooglScholar-]({temp_g})'''
        scholar_link.append(temp_g)

        doi_d1 = doi_d[i]
        doi_d1 = str(doi_d1.attrs['href'])[16:]
        DOI.append(doi_d1)
        pdf_d = f'''[-PDF-](https://sci-hub.wf/{doi_d1})'''
        pdf_link.append(pdf_d)

        # else:
        #     n += 1
        #     title.append('NaN')
        #     JR.append('NaN')
        #     page.append('NaN')
        #     output.append('NaN')
        #     scholar_link.append('NaN')
        #     pdf_link.append('NaN')
        #     DOI.append('NaN')
        #     print(i, n, doi_d1)
        #     # print(i,'*')
        #     # print(len(temp))
    return

if __name__=="__main__":
    url = get_url(input('DOI:'))
    aa = get_citation()

