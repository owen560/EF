import openpyxl
import requests
import json
from lxml import etree
from bs4 import BeautifulSoup
from selenium import webdriver
import pandas as pd

# a = input('文章DOI:')
a = 'https://doi.org/10.1177/1536867X221140943'
headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'}
# response = requests.get(a, headers=headers)
# r = response.text
# html = etree.HTML(r, etree.HTMLParser())
# print(r)
# browser = webdriver.Chrome(executable_path=r'/Users/xieluozheng/Downloads/chromedriver')
browser = webdriver.Chrome()
browser.get(a)
data = browser.page_source
soup = BeautifulSoup(data,'lxml')
# print(soup.citation-content.string)
# print(soup.find_all(attrs={'class': "citation-content"}))
references = soup.select('.citation .citation-content')
googlescholar = soup.select('.citation .external-links')
doi_d = soup.select('.citation-content a')
reference_n = len(references)
# print(type(references))
# print(type(references[0]))
scholar_link = []
title = []
JR = []
page = []
symbol = ", "
output = []
output_temp = []
DOI = []
pdf_link = []
for i in range(reference_n):
    temp = references[i]
    jr_d = str(temp.contents[1])
    jr_d = jr_d.strip("<em>")
    jr_d = jr_d[:-2]
    title.append(str(temp.contents[0]))
    JR.append(str(jr_d))
    page.append(str(temp.contents[2]))
    output_temp = title[i] + symbol +JR[i] +symbol +page[i]
    output.append(output_temp)
    
    temp_g = googlescholar[i]
    temp_g = temp_g.select('a')[1]
    temp_g = str(temp_g.attrs['href'])
    temp_g = f'''[-GooglScholar-]({temp_g})'''
    scholar_link.append(temp_g)

    doi_d1 = doi_d[i]
    doi_d1 = str(doi_d1.attrs['href'])[16:]
    DOI.append(doi_d1)
    pdf_d = f'''[-PDF-](https://sci-hub.wf/{doi_d1})'''
    pdf_link.append(pdf_d)
print(references[0])
print(type(references[0]))
'''
print(len(DOI))
print(type(DOI))
print(DOI)
print(pdf_link)
result = pd.DataFrame(columns=['Title', 'DOI', 'scholar_link', 'PDF'])
result['Title'] = pd.Series(output)
result['DOI'] = pd.Series(DOI)
result['scholar_link'] = pd.Series(scholar_link)
result['PDF'] = pd.Series(pdf_link)
print(result.head())
result.to_markdown('reference.md',index=False)
result.to_latex('reference.latex',index=False,encoding='utf-8')
'''