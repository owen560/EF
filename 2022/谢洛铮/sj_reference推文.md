---
SJ参考文献爬取整理小程序，sj_reference.py
---

&emsp;

> **作者：** 谢洛铮 (中山大学)        
> **E-mail:**  <779046193@qq.com>   

&emsp;

---

**目录**

[TOC]

---

&emsp;

## 1. 简介

本片推文将介绍小程序 sj_reference.py。该小程序可以对 **Stata Journal** 网站进行论文信息爬取，将指定文章中所有的参考文献进行整理与输出，可输出为**md**，**latex**格式，方便笔记或书稿引用。
## 2. sj_reference.py 语法介绍  
  
### 2.1 必备软件与资源库  
**必备库、软件**  
>- ChromeDriver(安装对应的版本) [[-官方-]](https://sites.google.com/a/chromium.org/chromedriver/downloads) [[-第三方-]](https://registry.npmmirror.com/binary.html?path=chromedriver/)  
>- py库：tabulate、bs4、selenium、pandas  
  
库的安装
```python
pip install tabulate
pip install bs4
pip install selenium
pip install pandas
```

### 2.2 使用介绍

`sj_reference.py` 的使用如下：

```python
  python sj_reference.py  
  请输入查询文章的DOI  
  DOI：
  请输入文件名
  文件名称：
```
参考文献的所有资料将会保存软件的根目录下
**结果展示**
![例子](https://gitee.com/arlionn/EF/raw/master/2022/%E8%B0%A2%E6%B4%9B%E9%93%AE/%E6%88%AA%E5%B1%8F2023-01-10%2020.30.12.png)
## 3. 代码实现

### 3.1 思路

sj_reference.py 的主要思路为：
> 1) 利用 selenium 命令将[SJ期刊](https://journals.sagepub.com/toc/stja/22/4) 对应文章中所有参考文献的源HTML代码保存下来
> 2) 利用bs4.BeautifulSoup 对HTML进行分析，解析出
>>- 作者  
>>- 作者  
>>- 论文标题  
>>- 期刊 
>>- 页码  
>>- DOI  
>>- 论文GoogleScholar链接地址 
>>- 论文PDF链接地址    
> 3) 将上述提取到的信息合并为一列，方便区分文章作者
> 4) 将文章标题、作者、序号分离，通过文章序号组装文章 PDF 链接、详细信息链接  
>> 最终格式  
>>- 作者，年份，论文标题，期刊: 页码. [DOI], [[-GoogleScholar-]](https://scholar.google.com/scholar_lookup?title=Empirical+Asset+Pricing%3A+The+Cross+Section+of+Stock+Returns&author=T.+G.+Bali&author=R.+F.+Engle&author=S.+Murray&publication_year=2016), [[-PDF-]](https://sci-hub.wf/10.1177/1536867X1301300310)
> 5) 输出结果

### 3.2 具体实现

```python
#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
@Project ：pythonProject2
@File    ：sj_reference.py
@Author  ：谢洛铮(中山大学)
@Date    ：2023/1/10 17:01
Version  : py3.9
System   : MacOS Ventura 13.0.1
E-mail     : 2416092751@qq.com
'''
from lxml import etree
import lxml
from bs4 import BeautifulSoup
from selenium import webdriver
import pandas as pd
#测试DOI：['10.1177/1536867X221141002',  '10.1177/1536867X221141058']

def get_url(DOI):   #获取需文章的DOI
    url = f'''https://doi.org/{DOI}'''
    return url

scholar_link = []
title = []
JR = []
page = []
symbol = ", "
output = []
output_temp = []
DOI = []
pdf_link = []
def get_citation():         #通过爬取SJ对应文章的页面，获取相关信息
    # browser = webdriver.Chrome(executable_path=r'/Users/xieluozheng/Downloads/chromedriver')
    browser = webdriver.Chrome()
    browser.get(url)
    data = browser.page_source
    soup = BeautifulSoup(data, 'lxml')
    references = soup.select('.citation .citation-content')
    googlescholar = soup.select('.citation .external-links')
    doi_d = soup.select('.citation-content a')
    reference_n = len(references)
    for j in range(reference_n):
        if len(references[j]) != 5:
            # references[j].decompose()
            references[j].extract()
    references = soup.select('.citation .citation-content')
    googlescholar = soup.select('.citation .external-links')
    doi_d = soup.select('.citation-content a')
    reference_n = len(references)
    print(reference_n)
    for i in range(0, reference_n):
        try:
            temp = references[i]
            doi_d1 = doi_d[i]
            temp_g = googlescholar[i]
            # print(temp)
            jr_d = str(temp.contents[1])
            jr_d = jr_d.strip("<em>")
            jr_d = jr_d[:-2]
            title.append(str(temp.contents[0]))
            JR.append(str(jr_d))
            page.append(str(temp.contents[2]))
            output_temp = title[i] + symbol + JR[i] + symbol + page[i]
            output.append(output_temp)

            temp_g = temp_g.select('a')[1]
            temp_g = str(temp_g.attrs['href'])
            temp_g = f'''[-GooglScholar-]({temp_g})'''
            scholar_link.append(temp_g)

            doi_d1 = str(doi_d1.attrs['href'])[16:]
            DOI.append(doi_d1)
            pdf_d = f'''[-PDF-](https://sci-hub.wf/{doi_d1})'''
            pdf_link.append(pdf_d)
        except Exception as e:
            pass
        continue
    return
def get_pd():       #将所有信息整合为表格形式输出，可保存为md与latex形式
    result = pd.DataFrame(columns=['Title', 'DOI', 'scholar_link', 'PDF'])
    result['Title'] = pd.Series(output)
    result['DOI'] = pd.Series(DOI)
    result['scholar_link'] = pd.Series(scholar_link)
    result['PDF'] = pd.Series(pdf_link)
    return result

if __name__=="__main__":
    print('请输入查询文章的DOI')
    url = get_url(input('DOI:'))
    aa = get_citation()
    print('请输入文件名称')
    savename = str(input('文件名称：'))
    result = get_pd()
    result.to_markdown(f'''{savename}.md''', index=False)
    result.to_latex(f'''{savename}.latex''', index=False, encoding='utf-8')
    print('查询已完成')
```

### 3.3 可能的自定义修改方式（未来的改进方向）

> 1) 不想一次性出现太多文章？ ———— 对指定作者(领域大佬)的文件，进行着重标记；或对参考文献引入'影响因子'，进行文章排序，节约文献阅读时间 。
> 2) SJ期刊搜索方式太单 ————将程序开头 `url=url = f'''https://doi.org/{DOI}'''` 中的`https:xxx`进行改写，可以更换搜索源。


## 4. 可能出现的bug

> 1) 部分文章所对应的参考文献，可能由于文献时间发布时间太早，缺乏DOI，导致部分参考文献有所缺失。——————不影响使用
> 2) 如发现其他 bug 或错误，请向小程序作者反馈，欢迎批评指正。

## 5.  总结

本推文主要介绍了爬取[SJ期刊](https://journals.sagepub.com/toc/stja/22/4) 网页论文信息的 sj_reference.py 小程序的使用、思路、代码实现等。欢迎使用者配合stata sj小程序配合使用，sj 在stata内查询历年[SJ期刊](https://journals.sagepub.com/toc/stja/22/4) 期刊目录。  

[**SJ推文: 使用指南**](https://gitee.com/Stata002/StataSX2018/blob/master/%E8%AE%B8%E6%A2%A6%E6%B4%81/P2%EF%BC%9A%E7%88%AC%E5%8F%96Stata%20Journal%E6%9C%9F%E5%88%8A%E7%9B%AE%E5%BD%95/sj%E6%8E%A8%E6%96%87_final.md#https://gitee.com/link?target=https%3A%2F%2Fzhuanlan.zhihu.com%2Farlion)
## 参考资料和相关推文
- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [Stata编程：暂元，local！暂元，local！](https://www.lianxh.cn/news/4d57e771feba7.html)
  - [普林斯顿Stata教程(三) - Stata编程](https://www.lianxh.cn/news/0c64a3d1b235d.html)  
  - [Stata-Python交互-1：二者配合的基本设定](https://www.lianxh.cn/news/285493e301c8a.html)
  - [Stata-Python交互-2：在Stata中调用Python的三种方式](https://www.lianxh.cn/news/290a48d428074.html)
  - [Stata-Python交互-3：如何安装Python宏包](https://www.lianxh.cn/news/5c93706797ad1.html)
  - [Stata-Python交互-4：如何调用Python宏包](https://www.lianxh.cn/news/b7fd8023587cf.html)


- 专题：[Stata程序](https://www.lianxh.cn/blogs/26.html)
  - [Stata程序：10 分钟快乐编写 ado 文件](https://www.lianxh.cn/news/a9d7de7ff1d80.html)
  - [Stata：编写ado文件自动化执行常见任务](https://www.lianxh.cn/news/776e42fc6ecd4.html)

- 专题：[文本分析-爬虫](https://www.lianxh.cn/blogs/36.html)
  - [正则表达式语言 - 快速参考](https://www.lianxh.cn/news/cec14affce188.html)