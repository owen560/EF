#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
@Project ：pythonProject2
@File    ：sj_reference.py
@Author  ：谢洛铮
@Date    ：2023/1/10 17:01
Version  : py3.9
System   : MacOS Ventura 13.0.1
mail     : 2416092751@qq.com
'''
from lxml import etree
import lxml
from bs4 import BeautifulSoup
from selenium import webdriver
import pandas as pd
#测试DOI：10.1177/1536867X221141002

def get_url(DOI):
    url = f'''https://doi.org/{DOI}'''
    return url

scholar_link = []
title = []
JR = []
page = []
symbol = ", "
output = []
output_temp = []
DOI = []
pdf_link = []
def get_citation():
    # browser = webdriver.Chrome(executable_path=r'/Users/xieluozheng/Downloads/chromedriver')
    browser = webdriver.Chrome()
    browser.get(url)
    data = browser.page_source
    soup = BeautifulSoup(data, 'lxml')
    references = soup.select('.citation .citation-content')
    googlescholar = soup.select('.citation .external-links')
    doi_d = soup.select('.citation-content a')
    reference_n = len(references)
    for j in range(reference_n):
        if len(references[j]) != 5:
            # references[j].decompose()
            references[j].extract()
    references = soup.select('.citation .citation-content')
    googlescholar = soup.select('.citation .external-links')
    doi_d = soup.select('.citation-content a')
    reference_n = len(references)
    print(reference_n)
    for i in range(0, reference_n):
        try:
            temp = references[i]
            doi_d1 = doi_d[i]
            temp_g = googlescholar[i]
            # print(temp)
            jr_d = str(temp.contents[1])
            jr_d = jr_d.strip("<em>")
            jr_d = jr_d[:-2]
            title.append(str(temp.contents[0]))
            JR.append(str(jr_d))
            page.append(str(temp.contents[2]))
            output_temp = title[i] + symbol + JR[i] + symbol + page[i]
            output.append(output_temp)

            temp_g = temp_g.select('a')[1]
            temp_g = str(temp_g.attrs['href'])
            temp_g = f'''[-GooglScholar-]({temp_g})'''
            scholar_link.append(temp_g)

            doi_d1 = str(doi_d1.attrs['href'])[16:]
            DOI.append(doi_d1)
            pdf_d = f'''[-PDF-](https://sci-hub.wf/{doi_d1})'''
            pdf_link.append(pdf_d)
        except Exception as e:
            pass
        continue
    return
def get_pd():
    result = pd.DataFrame(columns=['Title', 'DOI', 'scholar_link', 'PDF'])
    result['Title'] = pd.Series(output)
    result['DOI'] = pd.Series(DOI)
    result['scholar_link'] = pd.Series(scholar_link)
    result['PDF'] = pd.Series(pdf_link)
    return result

if __name__=="__main__":
    print('请输入查询文章的DOI')
    url = get_url(input('DOI:'))
    aa = get_citation()
    result = get_pd()
    result.to_markdown('example.md', index=False)
    result.to_latex('example.latex', index=False, encoding='utf-8')
    print('查询已完成')