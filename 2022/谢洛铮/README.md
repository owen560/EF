&emsp;

**作者**       : 谢洛铮(中山大学)  
**E-mail**    : <2416092751@qq.com>

sj_reference.py为最终程序文件  
**必备库、软件**  
>- ChromeDriver(安装对应的版本) [[-官方-]](https://sites.google.com/a/chromium.org/chromedriver/downloads) [[-第三方-]](https://registry.npmmirror.com/binary.html?path=chromedriver/)  
>- py库：tabulate、bs4、selenium、pandas  
  
<font color=yellow>**程序说明**</font>
>软件使用爬虫方法，爬取 [SJ期刊](https://journals.sagepub.com/toc/stja/22/4) 里对应文章的所有参考文献的相关资料，并生成对应的md和latex文件。  
>- 作者  
>- 论文标题  
>- 期刊 
>- 页码  
>- DOI  
>- 论文GoogleScholar链接地址 
>- 论文PDF链接地址  
> 
最终，每条参考文献最终格式为：
>作者，年份，论文标题，期刊: 页码. [DOI], [[-GoogleScholar-]](https://scholar.google.com/scholar_lookup?title=Empirical+Asset+Pricing%3A+The+Cross+Section+of+Stock+Returns&author=T.+G.+Bali&author=R.+F.+Engle&author=S.+Murray&publication_year=2016), [[-PDF-]](https://sci-hub.wf/10.1177/1536867X1301300310)  
>   
例子
>![例子](https://gitee.com/arlionn/EF/raw/master/2022/%E8%B0%A2%E6%B4%9B%E9%93%AE/%E6%88%AA%E5%B1%8F2023-01-10%2020.30.12.png)  
>  
