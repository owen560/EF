&emsp;
> **作者：** 周嘉怡 (中山大学)        
> **E-mail:**  <sysu_zhoujy@126.com>   
> 
&emsp;

连老师已经提供了绘制正方形的基本思路，因此对于长方形可以基于此进行修改升级。

对于一个程序设计，在我看来首先应该了解程序的语法，详情可参见 `help language` 和 `help syntax`。连享会中有很多有关于编程的推文值得学习，其中我认为余影和邱枞两位师兄师姐写过的两篇推文对我理解stata编程帮助较大：
- [Stata程序：10 分钟快乐编写 ado 文件](https://www.lianxh.cn/news/a9d7de7ff1d80.html)
- [Stata小白编程：步步为营-以阶乘计算为例](https://www.lianxh.cn/news/f7d4b8f21ccc5.html)

当然，不同于算法类编程设计，绘制长方形设计主要难点是如何与 stata 中的底层绘图工具 `twoway` 实现融合，我曾经给创立 `addplot` 的 Ben Jann 教授发过邮件说明问题，在他的回信中，提到应创立 **twoway_myrect_parse.class** 的文件帮助实现与 `twoway` 的融合，对于 class 的理解我通过 `help classman` 简单认识了一下，但由于水平有限没能做到理解很深刻。

以下为 Ben Jann 教授邮件原文内容：
>At 2022-12-23 16:19:25, 
ben.jann@unibe.ch wrote: 
Hi, addplot only has access to plot types known to -graph twoway-, see -help twoway-. To make your program available you would have to provide file twoway_myrect_parse.class containing code that implements your function in a way that is compatible with twoway. The language to do so is slightly odd (ado class programming) and I don't know whether there is good documentation showing how to write a twoway plottype (apart from -help classman-). Maybe it is easiest to start from an existing implementation; look for twoway_???_parse.class files in your Stata installation. Maybe https://www.stata.com/meeting/dcconf09/dc09_radyakin.pdf is helpful. 
>ben

其中对于 `twoway` 的解析方法我认为可能会给到一些启发：

![绘制一个长方形_Fig1_twoway_周嘉怡](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/绘制一个长方形_Fig1_twoway_周嘉怡.jpg)

最后，Ben教授设计的 addplot.ado 文件我认为也值得分析理解，希望能给到一点帮助：https://github.com/benjann/addplot