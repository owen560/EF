cap program drop arxiv
program define arxiv

version 11

syntax anything(name = keyword) [, ///
		Word            ///   // 
		Mlink           ///   // - [推文标题](URL)
		MText           ///   //   [推文标题](URL)
		Weixin          ///   // 推文标题  URL
		Markdown        ///   // 
		Abs             ///   // 详情页面
	   ]
	
	preserve
	
	clear
	
	qui tempfile arxivinf outcome
	
	//识别是否输入空格
	if ustrregexm(`"`keyword'"', "\s") != 0 {
		local keyworda = ustrregexra("`keyword'", "\s", "\+")
		copy "https://arxiv.org/search/?query=`keyworda'&searchtype=all&abstracts=show&order=-announced_date_first&size=25" "`arxivinf'.html", replace
	}
	else {
		copy "https://arxiv.org/search/?query=`keyword'&searchtype=all&abstracts=show&order=-announced_date_first&size=25" "`arxivinf'.html", replace
	}
	
	qui infix strL inf 1-20000 using "`arxivinf'.html", clear
	
	qui replace inf = ustrregexra(inf, "<span class=\Wsearch-hit mathjax\W>.*>", proper("`keyword'"))
	
	 //抽取序号
	qui gen num = ustrregexs(0) if ustrregexm(inf, "\d{4}\.\d{5}")
	qui gen num2 =  ustrregexs(0) if ustrregexm(inf, "\d{4}\.\d{4}")
	
	//抽取标题 
	qui gen topic = inf if ustrregexm(inf[_n-2], `"<p class="title is-5 mathjax">"'') & ustrregexm(inf[_n+2], "</p>")
	qui drop if ustrregexm(topic, "(pp\.|I\.|H\.|Get status notifications via|pages|Vol\.|T\d+|Working Paper)")
	
	//抽取作者
	qui gen author = ustrregexs(0) if ustrregexm(inf, `"<a href=\"/search/\?searchtype=author&.*</a>"')
	qui replace author = ustrregexra(author, `"<a href=\"/search/\?searchtype=author&amp.*[A-Z]\">"', "")
	qui replace author = ustrregexra(author, "</a>", "")
	qui drop if ustrregexm(author, "(Accessibility Assistance<|Privacy Policy<|Advanced Search<|:|/|>)")

	//将序号、标题合并方便区分文章作者
	qui replace author = ustrregexs(0) if ustrregexm(num, "\d{4}\.\d{5}") 
	qui replace author = ustrregexs(0) if ustrregexm(num2, "\d{4}\.\d{4}") & num ==  ""
	qui replace author = ustrregexs(0) if ustrregexm(topic, "\w+.*")
	qui drop if ustrregexm(author, "\d*\(\d*\)\s\(\d*\)")
	qui keep if author != ""
	qui duplicates drop author, force
	qui drop if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author[_n-1], "\d{4}\.\d{5}")
	
	//分出文章作者
	qui gen title = author[_n+1] if  ustrregexm(author, "\d{4}\.\d{5}") 
	qui gen author1 = author[_n+2] if ustrregexm(author, "\d{4}\.\d{5}") 
	qui replace author1 = "" if ustrregexm(author1, "\d")
	qui gen author2 = author[_n+3] if ustrregexm(author, "\d{4}\.\d{5}") 
	qui replace author2 = "" if ustrregexm(author2, "\d")
	qui gen author3 = author[_n+4] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author2, "\w")	
	qui replace author3 = "" if ustrregexm(author3, "\d")
	qui gen author4 = author[_n+5] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author3, "\w")	
	qui replace author4 = "" if ustrregexm(author4, "\d")
	qui gen author5 = author[_n+6] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author4, "\w")	
	qui replace author5 = "" if ustrregexm(author5, "\d")
	qui gen author6 = author[_n+7] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author5, "\w")	
	qui replace author6 = "" if ustrregexm(author6, "\d")
	qui gen author7 = author[_n+8] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author6, "\w")	
	qui replace author7 = "" if ustrregexm(author7, "\d")
	
	//合并作者
	qui gen author_a = author1
	qui replace author_a = author1 + "," + author2 if author2 != ""
	qui replace author_a = author_a + "," + author3 if author3 != ""
	qui replace author_a = author_a + "," + author4 if author4 != ""
	qui replace author_a = author_a + "," + author5 if author5 != ""
	qui replace author_a = author_a + "," + author6 if author6 != ""
	qui replace author_a = author_a + "," + author7 if author7 != ""
	
	qui drop inf topic author author1 author2 author3 author4 author5 author6 author7
	qui rename author_a author
	qui keep if num != ""
	
	//文章详情网页和pdf网页
	qui gen abs_url =  "https://arxiv.org/abs/" + num
	qui gen abs =`"{browse ""' + abs_url +`"": "' + title +`"}"'   //详情
	
	qui gen pdf_url =  "https://arxiv.org/pdf/" + num + ".pdf"
	qui gen pdf =`"{browse ""' + pdf_url +`"": "' + title +`"}"'   //pdf
	
	//输出格式
	qui gen paper_word = pdf
	
	qui gen paper_markdown = author + ". " + title + ". " + "arxiv working paper. " + pdf
	
	qui gen paper_mlink = "- [" + title + "](" + pdf_url + ")"  // list
	
	qui gen paper_mtext =  " [" + title + "](" + pdf_url + ") " // list2
	
	qui gen paper_weixin= title + ": " + pdf_url
	
	qui gen paper_abs =  author + ". " + title + ". " + abs
	
	qui save "`outcome'", replace
	
	qui use "`outcome'", clear
	local n = _N

	//option识别
	if "`word'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline ""
		}
	}
	
	else if "`mlink'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline paper_mlink[`i'] _newline ""
		}
	}
	
	else if "`mtext'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline paper_mtext[`i'] _newline ""
		}
	}
	
	else if "`weixin'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline paper_weixin[`i'] _newline ""
		}
	}
	
	else if "`markdown'" != "" {
		forvalues i = 1/`n' {
			dis paper_markdown[`i'] _newline ""
		}
	}
	
	else if "`abs'" != "" {
		forvalues i = 1/`n' {
			dis paper_abs[`i'] _newline ""
		}
	}
	
	else {
		forvalues i = 1/`n' {
			dis paper_markdown[`i'] _newline ""
		}
	}

restore

end


