---
arxiv 论文爬取小程序 arxiv.ado
---

&emsp;

> **作者：** 朱镕致 (中山大学)        
> **E-mail:**  <779046193@qq.com>   

&emsp;

---

**目录**

[TOC]

---

&emsp;

## 1. 简介

本推文将介绍小程序 arxiv.ado 。该小程序可以对 arxiv 网站进行论文信息爬取，并将搜索出的发布日期由新到旧的前25篇论文的 **作者**， **论文标题**， **PDF链接**， **详细信息网页** 等信息以指定格式 **（包括 Word，Markdown，weixin，Mlink，Mtext等格式，默认为 Markdown 格式）** 进行输出。

## 2. arxiv.ado 语法介绍

### 2.1 基础语法介绍

`arxiv` 的基础语法结构如下：

```stata
. arxiv keyword [, options]
```

其中， ``keyword`` 是要查找的关键词。例如：
```stata
. arxiv panel
```

**注意：**

（1）输入的关键词可以不唯一，多个关键词之间需要用空格分开。例如：
```stata
. arxiv panel data
. arxiv machine learning
```
（2）由于arxiv网站不支持中文搜索，因此在小程序中输入中文会报错。例如：
```stata
. arxiv 中文
.server reported server error
.r(675);
```

### 2.2 options 介绍

arxiv.ado 的 options 主要用于输出不同格式的文献信息。其选项、格式、输出结果示例如下表所示。

| options | 格式 | 输出结果示例 |
| :--------- | :--: | :------: |
| ``Word`` | 文章标题(URL) | [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19](https://arxiv.org/pdf/2301.03382.pdf) |
| ``Mlink`` | - [文章标题] (URL) | - [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19](https://arxiv.org/pdf/2301.03382.pdf) - [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19] (https://arxiv.org/pdf/2301.03382.pdf)   |
| ``Mltext`` |  [文章标题] (URL) | [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19](https://arxiv.org/pdf/2301.03382.pdf) [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19] (https://arxiv.org/pdf/2301.03382.pdf) |
| ``Weixin`` | 文章标题 : URL | [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19](https://arxiv.org/pdf/2301.03382.pdf) Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19 : https://arxiv.org/pdf/2301.03382.pdf |
| ``Markdown`` | 作者. 文章标题. arxiv working paper. [-PDF-] (URL) | Mansoor Davoodi,Ana Batista,Abhishek Senapati,Justin M. Calabrese. Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19. arxiv working paper.  [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19](https://arxiv.org/pdf/2301.03382.pdf) |
| ``Abs`` | 作者. 文章标题. 文章详情(URL) | Mansoor Davoodi,Ana Batista,Abhishek Senapati,Justin M. Calabrese. Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19.  [Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19](https://arxiv.org/abs/2301.03382)|
| ``" "`` | 默认为 Markdown 格式 | 同上 Markdown 格式 |

## 3. 实操应用

下面将展示 arxiv.ado 的应用与输出结果

```stata
. arxiv covid
```

结果为（篇幅原因仅展示前三条，且无法展示超链接）：

```stata

Mansoor Davoodi,Ana Batista,Abhishek Senapati,Justin M. Calabrese. Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19. arxiv working paper.  Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19 

Juliane Wiese,Nattavudh Powdthavee. How effective are Covid-19 vaccine health messages in reducing vaccine skepticism? Heterogeneity in messageseffectiveness by just world beliefs. arxiv working paper.  How effective are Covid-19 vaccine health messages in reducing vaccine skepticism?Heterogeneity in messages effectiveness by just world beliefs

Eiji Yamamura,Yoshiro Tsutsui,Fumio Ohtake. The Covid-19 vaccination, preventive behaviors and pro-social motivation: panel data analysis from Japan. arxiv working paper. The Covid-19 vaccination, preventive behaviors and pro-social motivation: panel data analysis from Japan 
```

加入 options ：

```stata
. arxiv covid, word

Personnel Scheduling and Testing Strategy during Pandemics: The case of Covid-19

How effective are Covid-19 vaccine health messages in reducing vaccine skepticism?Heterogeneity in messages effectiveness by just world beliefs

The Covid-19 vaccination, preventive behaviors and pro-social motivation: panel data analysis from Japan 
```

## 4. 代码实现

### 4.1 思路

arxiv.ado 的主要思路为：
> 1) 利用 copy 命令将搜索结果页面以临时 html 格式保存到本地
> 2) 利用正则表达式抽取文章编号、标题、作者等信息
> 3) 将上述提取到的信息合并为一列，方便区分文章作者
> 4) 将文章标题、作者、序号分离，通过文章序号组装文章 PDF 链接、详细信息链接
> 5) 输出结果

### 4.2 具体实现

```atata
cap program drop arxiv
program define arxiv

version 11

syntax anything(name = keyword) [, ///
		Word            ///   // 文章标题(URL)
		Mlink           ///   // - [文章标题](URL)
		MText           ///   //   [文章标题](URL)
		Weixin          ///   // 文章标题  URL
		Markdown        ///   // 作者. 文章标题. arxiv working paper. [-PDF-](URL)
		Abs             ///   // 详情页面
	   ]
	
	preserve
	
	clear
	
	qui tempfile arxivinf outcome
	
	//识别是否输入空格并爬取网页
	if ustrregexm(`"`keyword'"', "\s") != 0 {
		local keyworda = ustrregexra("`keyword'", "\s", "\+")
		copy "https://arxiv.org/search/?query=`keyworda'&searchtype=all&abstracts=show&order=-announced_date_first&size=20" "`arxivinf'.html", replace
	}
	else {
		copy "https://arxiv.org/search/?query=`keyword'&searchtype=all&abstracts=show&order=-announced_date_first&size=20" "`arxivinf'.html", replace
	}
	
	qui infix strL inf 1-20000 using "`arxivinf'.html", clear
	
	qui replace inf = ustrregexra(inf, "<span class=\Wsearch-hit mathjax\W>.*>", proper("`keyword'")) //去掉网页表示搜索词代码，方便后续提取
	
	 //抽取序号
	qui gen num = ustrregexs(0) if ustrregexm(inf, "\d{4}\.\d{5}")
	qui gen num2 =  ustrregexs(0) if ustrregexm(inf, "\d{4}\.\d{4}")
	
	//抽取标题 
	qui gen topic = inf if ustrregexm(inf[_n-2], `"<p class="title is-5 mathjax">"'') & ustrregexm(inf[_n+2], "</p>")
	qui drop if ustrregexm(topic, "(pp\.|I\.|H\.|Get status notifications via|pages|Vol\.|T\d+|Working Paper)")
	
	//抽取作者
	qui gen author = ustrregexs(0) if ustrregexm(inf, `"<a href=\"/search/\?searchtype=author&.*</a>"')
	qui replace author = ustrregexra(author, `"<a href=\"/search/\?searchtype=author&amp.*[A-Z]\">"', "")
	qui replace author = ustrregexra(author, "</a>", "")
	qui drop if ustrregexm(author, "(Accessibility Assistance<|Privacy Policy<|Advanced Search<|:|/|>)")

	//将序号、标题合并方便区分文章作者
	qui replace author = ustrregexs(0) if ustrregexm(num, "\d{4}\.\d{5}") 
	qui replace author = ustrregexs(0) if ustrregexm(num2, "\d{4}\.\d{4}") & num ==  ""
	qui replace author = ustrregexs(0) if ustrregexm(topic, "\w+.*")
	qui drop if ustrregexm(author, "\d*\(\d*\)\s\(\d*\)")
	qui keep if author != ""
	qui duplicates drop author, force
	qui drop if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author[_n-1], "\d{4}\.\d{5}")
	
	//分出文章作者
	qui gen title = author[_n+1] if  ustrregexm(author, "\d{4}\.\d{5}") 
	qui gen author1 = author[_n+2] if ustrregexm(author, "\d{4}\.\d{5}") 
	qui replace author1 = "" if ustrregexm(author1, "\d")
	qui gen author2 = author[_n+3] if ustrregexm(author, "\d{4}\.\d{5}") 
	qui replace author2 = "" if ustrregexm(author2, "\d")
	qui gen author3 = author[_n+4] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author2, "\w")	
	qui replace author3 = "" if ustrregexm(author3, "\d")
	qui gen author4 = author[_n+5] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author3, "\w")	
	qui replace author4 = "" if ustrregexm(author4, "\d")
	qui gen author5 = author[_n+6] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author4, "\w")	
	qui replace author5 = "" if ustrregexm(author5, "\d")
	qui gen author6 = author[_n+7] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author5, "\w")	
	qui replace author6 = "" if ustrregexm(author6, "\d")
	qui gen author7 = author[_n+8] if ustrregexm(author, "\d{4}\.\d{5}") & ustrregexm(author6, "\w")	
	qui replace author7 = "" if ustrregexm(author7, "\d")
	
	//合并作者
	qui gen author_a = author1
	qui replace author_a = author1 + "," + author2 if author2 != ""
	qui replace author_a = author_a + "," + author3 if author3 != ""
	qui replace author_a = author_a + "," + author4 if author4 != ""
	qui replace author_a = author_a + "," + author5 if author5 != ""
	qui replace author_a = author_a + "," + author6 if author6 != ""
	qui replace author_a = author_a + "," + author7 if author7 != ""
	
	qui drop inf topic author author1 author2 author3 author4 author5 author6 author7
	qui rename author_a author
	qui keep if num != ""
	
	//文章详情网页和pdf网页
	qui gen abs_url =  "https://arxiv.org/abs/" + num
	qui gen abs =`"{browse ""' + abs_url +`"": "' + title +`"}"'   //详情
	
	qui gen pdf_url =  "https://arxiv.org/pdf/" + num + ".pdf"
	qui gen pdf =`"{browse ""' + pdf_url +`"": "' + title +`"}"'   //pdf
	
	//输出格式
	qui gen paper_word = pdf
	qui gen paper_markdown = author + ". " + title + ". " + "arxiv working paper. " + pdf	
	qui gen paper_mlink = "- [" + title + "](" + pdf_url + ")"  // list	
	qui gen paper_mtext =  " [" + title + "](" + pdf_url + ") " // list2	
	qui gen paper_weixin= title + ": " + pdf_url	
	qui gen paper_abs =  author + ". " + title + ". " + abs	
	
    qui save "`outcome'", replace	
	qui use "`outcome'", clear
	local n = _N

	//option识别并输出结果
	if "`word'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline ""
		}
	}
	
	else if "`mlink'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline paper_mlink[`i'] _newline ""
		}
	}
	
	else if "`mtext'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline paper_mtext[`i'] _newline ""
		}
	}
	
	else if "`weixin'" != "" {
		forvalues i = 1/`n' {
			dis paper_word[`i'] _newline paper_weixin[`i'] _newline ""
		}
	}
	
	else if "`markdown'" != "" {
		forvalues i = 1/`n' {
			dis paper_markdown[`i'] _newline ""
		}
	}
	
	else if "`abs'" != "" {
		forvalues i = 1/`n' {
			dis paper_abs[`i'] _newline ""
		}
	}
	
	else {
		forvalues i = 1/`n' {
			dis paper_markdown[`i'] _newline ""
		}
	}

    restore
end
```

### 4.3 可能的自定义修改方式（未来的改进方向）

> 1) 不想一次性出现太多文章？ ————将程序开头两处 `copy "https: ...... first&size=20"` 的 **25** 修改为想要的出现文章数 **(修改数字只能为25的倍数，否则网页无法输出结果)**。
> 2) 想修改文章检索顺序？ ————将程序开头两处 `copy "https: ...... order=-announced_date_first ......"` 的 **-announced_date_firs** 修改为 **announced_date_firs(发布日期从旧到新)** 。
> 3) 想修改检索方式？ ————将程序开头两处 `copy "https: ...... searchtype=all ......"` 的 **all** 修改为 **title(仅在标题搜索关键词)** 、 **author(仅在作者搜索关键词)** 、 **abstract(仅在摘要搜索关键词)** 等。

## 5. 可能出现的bug

> 1) 非链接结果显示为绿色或白色 ————这是正常的，不会影响使用
> 2) 文章 pdf 链接无法打开 ————可能文章储存在 arxiv 网站的某个下属页面中，该页面与通常文章 pdf 的 URL 不同，因此无法通过通常链接打开，可以通过搜索**文章详细信息页面**找到文章 pdf 。
> 3) 文章作者、标题错位 ———— arxiv 的搜索结果页面自身代码出错（如输错文章序号），如遇到可以和小程序作者反馈或自行在网页搜索。
> 4) 如发现其他 bug 或错误，请向小程序作者反馈，欢迎批评指正。

## 6.  总结

本推文主要介绍了爬取 arxiv 网页论文信息的 arxiv.ado 小程序的使用、思路、代码实现等。

## 参考资料和相关推文

- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [Stata编程：暂元，local！暂元，local！](https://www.lianxh.cn/news/4d57e771feba7.html)
  - [普林斯顿Stata教程(三) - Stata编程](https://www.lianxh.cn/news/0c64a3d1b235d.html)



- 专题：[Stata程序](https://www.lianxh.cn/blogs/26.html)
  - [Stata程序：10 分钟快乐编写 ado 文件](https://www.lianxh.cn/news/a9d7de7ff1d80.html)
  - [Stata：编写ado文件自动化执行常见任务](https://www.lianxh.cn/news/776e42fc6ecd4.html)

- 专题：[文本分析-爬虫](https://www.lianxh.cn/blogs/36.html)
  - [Stata：正则表达式教程](https://www.lianxh.cn/news/1f2376a53e880.html)
  - [用正则表达式整理文献：正文与文末一一对应](https://www.lianxh.cn/news/9dd5a865acd9c.html)
  - [Stata爬虫-正则表达式：爬取必胜客](https://www.lianxh.cn/news/8c6be3c47d2eb.html)
  - [Stata: 正则表达式和文本分析](https://www.lianxh.cn/news/2f765cfd4bffe.html)
  - [在 Visual Studio (vsCode) 中使用正则表达式](https://www.lianxh.cn/news/39021047ce624.html)
  - [正则表达式语言 - 快速参考](https://www.lianxh.cn/news/cec14affce188.html)
