
&emsp;

> **作者**：伍慧铭 (中山大学)    
> **E-Mail:** <wuhm28@mail2.sysu.edu.cn>  

&emsp;

**备注:** 本推文部分摘译自以下论文，特此致谢！  
> **Source**: Karakaplan, Mustafa U. "Panel stochastic frontier models with endogeneity." The Stata Journal 22, no. 3 (2022): 643-663. [-Link-](https://journals.sagepub.com/doi/full/10.1177/1536867X221124539?journalCode=stja)  


# <center> xtsfkk:内生性随机边界模型

[toc]
## 1.背景介绍
&emsp;&emsp;自Aigner, Lovell, and Schmidt(1977)和Meeusen and van den Broeck(1977)引入随机前沿模型以来，已有40多年的历史。模型由确定前沿目标的确定性部分、确定双边误差项的随机部分和确定与随机前沿距离的单边无效误差项组成。该模型可以用来研究不同行业的生产、成本、收入、利润与其他目标，在教育、金融市场、房地产、交通领域等领域得到广泛运用。  
&emsp;&emsp;Stata提供了`frontier`命令估计随机前沿模型的参数，但各种命令都没有提供控制模型内生性的方法。如果边界或无效率项的决定因素与模型的双边误差项相关，则估计量的结果将受到内生性的污染。Karakaplan和Kutlu (2017b)设计了一个随机前沿估计器，可以解决面板中的内生性问题。Stata的`xtfrontier`命令和Belotti等人(2013)的`sfpanel`命令适合面板随机前沿模型，但忽略了内质性问题。在本文中，引入一个新的命令`xtsfkk`，用于拟合Stata中具有内生性的面板随机前沿模型。
## 2.模型简介
&emsp;&emsp;Karakaplan和Kutlu (2017b)提出了以下面板随机前沿模型，其中前沿和无效率项中含有内生解释变量。  
<div align=center>
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/伍慧铭_xtsfkk内生性随机边界模型_Fig01.png.png" width="600" height="350" alt="伍慧铭_xtsfkk内生性随机边界模型_Fig01"/>
</div>
&emsp;&emsp;对应于面板i的观测向量由下标i表示，T<sub>i</sub>为面板i的时间持续长短，S = 1为成本函数，S = -1为生产函数；y<sub>it</sub>是第i个生产单位在t时刻的产量或成本的对数，X<sub>yit</sub>是外生和内生变量的向量；x<sub>it</sub>是所有内生解释变量的向量；z<sub>it</sub> = I<sub>p</sub> * z<sup>'</sup><sub>it</sub> , z<sub>it</sub>是外生变量的向量；ϵ<sub>it</sub>与v<sub>it</sub>是双面误差项，u<sub>it</sub>≥0是捕获低效率的片面误差项；x<sub>uit</sub>是不包括常数项的内生与外生变量的向量；u<sub>i</sub><sup>*</sup>是独立于ϵ<sub>it</sub>与v<sub>it</sub>的生产者特定随机分量；&Omega;是ϵ<sub>it</sub>的方差协方差矩阵；&sigma;<sub>v</sub><sup>2</sup>是v<sub>it</sub>的平方；&rho;是ϵ ̃<sub>it</sub>与v<sub>it</sub>的相关性向量；e<sub>it</sub>条件独立于给定的回归向量x<sub>it</sub>与z<sub>it</sub>；&Phi;表示标准正态分布函数；u<sub>i</sub><sup>*</sup> ∼ N<sup>+</sup>(&micro;; σ<sub>u</sub><sup>2</sup>)，N<sup>+</sup>半正态分布的标准符号；h<sub>it</sub><sup>2</sup>= exp(x<sub>uit</sub><sup>'</sup> &phi;<sub>u</sub>)。       

&emsp;&emsp;此外，为了预测效率Eff<sub>it</sub>=exp⁡(-u<sub>it</sub>), Karakaplan和Kutlu (2017b)给出了以下公式:
<div align=center>
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/伍慧铭_xtsfkk内生性随机边界模型_Fig02.png.png" width="6000" height="70" alt="伍慧铭_xtsfkk内生性随机边界模型_Fig02"/>
</div>
&emsp;&emsp;其中，&Phi;表示标准正态分布函数。     

&emsp;&emsp;最后Karakaplan和Kutlu (2017b)基于标准Durbin-Wu-Hausman检验的推理提供了内质性检验。这个检验是通过观察	&eta;项组成部分的联合显著性来实现的。如果&eta;项的组成部分是共同显著的，那么这将告诉我们模型中存在内生性，则需要通过(1)进行修正。相反，如果不考虑各成分的联合显著性，则不需要对内生性进行修正，可以用传统前沿模型进行拟合。   
## 3.命令介绍
命令安装
```stata
ssc install xtsfkk, replace
```
命令语法
```stata
xtsfkk depvar [indepvars] [if] [in] [weight] [, options]
```
&emsp;&emsp;`depvar`为被解释变量，`indepvars`为解释变量，`weight`用于设定进行不等概率抽样时的权重，此处使用`pweights`、 `aweights`、 `fweights`、与`iweights`都可以。  
`Options`包括：
- `noconstant`：抑制常数项。
- `production`：指定模型为生产前沿模型。     
-  `cost ` :指定模型为成本前沿模型。     
-  `endogenous(endovarlist)` :一般情况下，`xtsfkk`假定模型是外生的。 
- `instruments(ivarlist)`:`ivarlist` 中的变量为处理内生性的工具变量。      
- `exogenous(exovarlist)`: `exogenous()` 与 `leaveout() ` 不能同时使用。   
- `leaveout(lovarlist)`:指定将`lovarlist`中的变量从包含外生变量的默认列表中取出。
- `uhet(uvarlist[, noconstant]) `:指定无效成分是异方差的，方差函数依赖于`uvarlist`的线性组合。 
- `whet(wvarlist)`: 指定异质误差分量是异方差的 。 
- ` initial(matname)`:指定初始值的矩阵。
- `delve`:寻找更好的初始值。
- `difficult`:在非凹区域使用不同的步进算法。
- ` technique(algorithm_spec) `:指定最大化的方法。
- ` iterate(#)`:执行#次迭代。
- `save(filename) `:将当前迭代的结果储存在硬盘。
- ` load(filename) `:从先前保存的文件中加载估计值，并从估算值所在的位置继续。
- `  level(#) `:设定置信度。
- `  header `:在回归的开头显示模型的约束，该命令提供了一种在估计时快速检查模型规格的方法，指导区分不同的回归结果保存在单一日志文件。
- `  timer `:显示命令完成所需时间。
- `  beep[(#)] `:该命令对于多任务处理很有用，在报告结果时产生哔哔声。当#为正数时，报告结果时，它会生成#beeps。若#为负数，则哔哔声就像警报一样，产生连续的哔哔声，直到用户停止。使用`beep`选项，用户将不需要不断监视`Stata Results`窗口的结果。如果模型很复杂、面板数据集很大，`beep`将是不错的选择。
- ` compare `:在显示内生性模型回归结果后显示回归结果。
- `  efficiency(effvar[, replace])`:当估计完成后，生成生产或成本效率变量`effvar_EN`，并显示其统计信息。该选项自动扩展任何变量名`effuar_En`，如果使用了`compare`选项，还会生成`effuar_Ex`（外生模型的生产或成本效率变量），并显示其汇总统计信息。`replace`将现有`effvar_EN`和`effvar_Ex`的内容替换为来自当前模型的新效率值。
- ` test `:检验模型内生性，检验eta项的联合显著性，并在显示回归结果后报告结果。
- `  nicely `:在单个表格中显示回归结果。如果同时指定了`compare`选项，那么表格将并排显示外生和内生模型及其对应的统计信息，以便进行比较。即使没有指定`efficiency()`或`test`选项，也能准确地估计生产或成本效率并测试内生性，并在表格中报告。
- ` mldisplay(display_options) `:控制`ml display`选项。
## 4.Stata实操
### 4.1 面板随机成本前沿模型
```stata
. use https://www.mukarakaplan.com/files/xtsfkkcost  //调用数据
. xtsfkk, version
. xtset id t

. xtsfkk y x1 z1, cost uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) delve header timer beep  //内生面板随机成本前沿模型

//使用iterate、initial、efficiency、beep 与 test选项//
. xtsfkk y x1 z1, cost uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) iterate(6) timer beep(3)   //迭代6次
. matrix EST = e(b) //生成EST矩阵
. xtsfkk y x1 z1, cost uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) init(EST) efficiency(costef) test beep(-1) //指定初始矩阵为EST

//使用compare 与 nicely选项//
. xtsfkk y x1 z1, cost uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) delve iterate(15) compare beep
. xtsfkk y x1 z1, cost uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) compare nicely beep
```  
*Example*  
&emsp;&emsp;在本例中，纵向数据包括2011年至2015年的85个个体，300个观察结果。
```stata
. use https://www.mukarakaplan.com/files/xtsfkkcost 
``` 
&emsp;&emsp;我们先分别通过 id 和 t 作为个体和时间的指标设置面板数据结构：
```stata
. xtset id t
Panel variable: id (unbalanced)
 Time variable: t, 2011 to 2015, but with gaps
         Delta: 1 unit
```
&emsp;&emsp;成本(y)为两个前沿变量(x和z)的函数，而成本效率低下为变量(z<sub>2</sub>)的函数。使用两个工具变量(iv<sub>1</sub>和iv<sub>2</sub>)来处理模型中两个变量(z<sub>1</sub>和z<sub>2</sub>)的潜在内生性。

```stata
. xtsfkk y x1 z1, cost uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) compare nicely beep
```
<div align=center>
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/伍慧铭_xtsfkk内生性随机边界模型_Fig03.png" width="600" height="600" alt="伍慧铭_xtsfkk内生性随机边界模型_Fig03"/>
</div>

&emsp;&emsp;使用了`compare`和`nicely`选项，表格列示有两列结果:模型*Ex*是忽略内生性的模型，而模型*En*是处理内生性的模型。z<sub>1</sub>和z<sub>2</sub>的在0.1%水平上均具有统计学显著性，eta内生性检验结果在0.1%水平上拒绝原假设，这表明对模型内生性的修正是必要的。  
&emsp;&emsp;模型*Ex*和模型*En*的系数大小与统计显著性不同。模型*Ex*中z<sub>1</sub>和z<sub>2</sub>的系数为正，具有统计学意义。在模型*En*中，这些系数显著且为正，但较小。此外，模型*Ex*的平均成本效率为**0.3625**，而模型*En*的平均成本效率为**0.4838**。这告诉我们，在具有内生性的模型中的个体比在忽略内生性的模型中的个体更具成本效益。


### 4.2 面板随机生产前沿模型
```stata
. use https://www.mukarakaplan.com/files/xtsfkkprod, clear //调用数据
. summarize
. xtset firm year

. xtsfkk y x1 x2 x3 z1, production uhet(z2) //外生面板随机生产模型
. xtsfkk y x1 x2 x3 z1, production uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) nicely header beep //内生面板随机生产模型

//使用difficult 与 technique选项 //
. xtsfkk y x1 x2 x3 z1, uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) difficult compare nicely timer
. xtsfkk y x1 x2 x3 z1, uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) technique(nr 5 bfgs 10) compare nicely timer
```  
*Example*  
&emsp;&emsp;本例分析了生产环境中随机生成的数据集。数据包含了1991年至2015年间140家公司的2000个观察结果。
```stata
. use https://www.mukarakaplan.com/files/xtsfkkprod, clear
```
&emsp;&emsp;我们先分别通过 id 和 t 作为个体和时间的指标设置面板数据结构：
```stata
. xtset firm year
Panel variable: firm (unbalanced)
 Time variable: year, 1991 to 2015, but with gaps
         Delta: 1 unit
```  
&emsp;&emsp;生产(y)是前沿变量(x<sub>1</sub>, x<sub>2</sub>, x<sub>3</sub>和z<sub>1</sub>)的函数，效率低下是变量(z<sub>2</sub>)的函数。z<sub>1</sub>和z<sub>2</sub>是内生的，并使用两个工具变量(iv<sub>1</sub>和iv<sub>2</sub>)来处理内生性。为了完全显示模型，将header选项添加到命令行中。
```stata
. xtsfkk y x1 x2 x3 z1, production uhet(z2) endogenous(z1 z2) instruments(iv1 iv2) header efficiency(efv) test timer  
```  

<div align=center>
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/伍慧铭_xtsfkk内生性随机边界模型_Fig0.png" width="700" height="900" alt="伍慧铭_xtsfkk内生性随机边界模型_Fig04"/>
</div>
<div align=center>
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/伍慧铭_xtsfkk内生性随机边界模型_Fig05.png" width="700" height="250" alt="伍慧铭_xtsfkk内生性随机边界模型_Fig05"/>
</div>

&emsp;&emsp;表中z<sub>1</sub>和z<sub>2</sub>的Eta项都有统计学意义。此外，由于指定了`test`选项，因此给出了eta内生性检验结果，表明原假设在0.1%水平上被拒绝，需要对内生性进行修正。  

&emsp;&emsp;从内生变量的系数来看，z<sub>1</sub>和z<sub>2</sub>的系数都是正的，具有统计学意义。如果指定了`compare`选项，则外生模型的结果将显示z<sub>1</sub>和z<sub>2</sub>的系数远远小于所显示的内生性校正模型中的系数。  

&emsp;&emsp;`efficiency`选项是在命令行中指定的，所以技术效率分数被保存为一个变量，并且显示该变量的汇总统计信息。在该模型中，平均技术效率为**0.9719**，中位数技术效率为**0.9722**。如果已经指定了`compare`选项，那么`efficiency`选项还将保存忽略内生性的模型的效率分数。比较这两种模型的技术效率，可以发现一些生产者的生产效率并不像忽略内生性的标准前沿模型那样高。

## 5.结语
&emsp;&emsp;本推文介绍了`xtsfkk`命令在Stata中的应用，`xtsfkk`在边界项和无效率项上都能控制内生变量，并提供了面板研究中非常有用的各种选项，具有很高的实用价值，值得广大用户学习。

## 6.参考推文
Note：产生如下推文列表的 Stata 命令为：   
`lianxh 面板数据`  
`lianxh 内生性-因果推断`  
安装最新版 `lianxh` 命令：    
`ssc install lianxh, replace` 
- 专题：[Stata程序](https://www.lianxh.cn/blogs/26.html)
 - [Stata: 面板数据模型一文读懂](https://www.lianxh.cn/news/bf27906144b4e.html)
 - [内生性！内生性！解决方法大集合](https://www.lianxh.cn/news/224e2b4e170e4.html)
 - [内生性：来源及处理方法-幻灯片下载](https://www.lianxh.cn/blogs/19.html)
 - [工具变量-IV：排他性约束及经典文献解读](https://www.lianxh.cn/news/d4e7734f4d10d.html)