# 各种抽样：gsample命令介绍
> &#x1F353; 点击右上方的【编辑】按钮即可查看本文原始 Markdown 文档，以便了解格式。  

> **作者：** 滕泽鹏 (中山大学)        
> **E-mail:**  <tengzp@mail2.sysu.edu.cn>   

&emsp;

---

**目录**

[TOC]

---
&emsp;   

## 1. 简介

本篇推文介绍 Stata 中用于抽样的外部命令：`gsample` 。该命令增强了 Stata 进行抽样分析的能力，一条命令即可实现多种抽样方法。`gsample` 为用户从内存中的数据中抽取一个随机样本，不仅支持简单随机抽样(SRS)，也支持不等概率抽样(UPS)。并且 SRS 和 UPS 两种方法均提供有放回抽样和无放回抽样。此外，`gsample` 还支持分层抽样和整群抽样。


## 2. 下载及安装
由于 `gsample` 命令脱胎于 `moremata` 命令的 `mm_sample()` 函数 ，因此二者需同步安装。
```Stata
.ssc install gsample, replace
.ssc install moremata, replace
```
若以上无法安装，可尝试如下代码：
```Stata
.net install gsample, replace from(https://raw.githubusercontent.com/benjann/gsample/main/)
.net install moremata, replace from(https://raw.githubusercontent.com/benjann/moremata/master/)

```

## 3. gsample 语法介绍

### 3.1 基础语法介绍

 `gsample` 的语法结构如下：
```Stata
· gsample [#|varname] [if] [in] [weight] [, options]
```
其中：
- `#` 表示样本的大小。在不指定 `#` 时， gsample 的默认设置是将内存中的数据替换为随机顺序的采样观测值。而指定 `#` 以后，`gsample` 会存储一个反映观测值抽样频率的新变量。在不放回抽样的情况下，样本容量必须小于等于数据中的采样单元数,采样单元要么是单个观测值，要么是由 `cluster () ` 选项指定的簇。若没有指定 `#` 或者所指定的 `#` 是空值，那么样本大小将自动设定为数据中观测到的单元数。对于分层抽样，`#` 表示将从 `strata ()` 选项确定的每个类层中选择 `#` 个单元。此处也可指定 **`varname`** 来替代 `#` ，所指定的 **`varname`** 是包含每个类层特定样本容量的变量且需要为常数。
  
- `weight` 用于设定进行不等概率抽样时的权重，在这种情况下，采样概率将与指定的权重成正比（此处使用 `iweight` 或 `aweight` 亦可，并不影响结果）。另，如果权重分布过于不均，可能会导致无放回的不等概率抽样（UPS）失败。

### 3.2 options 介绍

`[, options]`
| options      | 介绍 |
| :--------- | :--: |
| `percent`    |  以总体的百分比表示样本量|
| `wor`    |  不放回抽样|
| `alt`  | 使用更高效的 SRSWOR 算法进行不放回简单随机抽样，`alt` 仅在指定不放回抽样 `wor` 且未提供权重 `weight` 时才可用|
|`nowarn` |在不等概率不放回抽样 ( UPSWOR ) 的基础上允许重复，仅在指定不放回抽样 `wor` 且提供了权重 `weight` 时可用|
| `strata (`**varlist**` ) ` |用于指定类层的变量，即在每个类层内选取样本，类层变量可以是数值型的，也可以是字符串型的。|
|`rround`|  对所有类层的样本总量进行随机取整，该 option 仅在类层`strata(`**varlist**`)`已指定的情况下可用。一般情况下，gsample将对样本容量进行四舍五入，但此时使用选项 `rround` 后，将在分层抽样的情况下进行随机取整。即，若则总样本量由所有类层的样本量之和经四舍五入确定，然后在各类层内进行随机取整，以保持总样本量。举个例子，如果有3个类层，并将样本容量设定为33.3，默认的操作是从每个阶层中抽取33个单位，总样本容量为99，而若使用 `rround` ，则总样本量将为100，3个类层中有2个会抽取33个样本，还有1个抽取34个样本。|
|` cluster(`**varlist**`) `|指定识别重抽样簇的变量(比如初始采样单元)，簇变量可以是数值型的，也可以是字符串型的。如果` cluster(`**varlist**`) `与 `weight` 一起使用，则假设每个簇内部权重相同。|
|`idcluster(`**newvar**`) ` |创建新的簇ID变量，在有放回抽样时格外有用|
|` keep  `|保留不符合if和in的观察值以及出现空值或者0权重的观察值。而在不使用这个 option 时系统默认的处理是去除这些观察值。|
|`generate(`**newvar**`)` |将抽样频率存储在新变量 **newvar** 中，如果使用了`generate(`**newvar**`)`，数据的原始排序顺序将被保留，对于不满足 `if` 或 `in` 条件的观测值，**newvar** 将为 0 或 1，如果同时使用了选项 `keep`，则为 1 ，否则为 0 。|
|`replace`|覆盖替换现有变量|
|` noprreserve `|在运行出现 *break*  或 *error* 时不要恢复原始数据，但在使用了选项 `generate()` 时不可用|


>更多信息和原始代码可见[ `gsample` 命令的 GitHub 主页](https://github.com/benjann/gsample/)

## 4. Stata 实操应用

下面使用 Stata 系统数据展示 `gsample` 命令的实际用法和效果。
```Stata
sysuse "auto.dta", clear //调用系统数据
drop in 11/74            //为便于展示仅使用前 10 个观测值
sort price
gen n=_n                //按照价格对观察值进行排序并编号
drop rep78 headroom trunk weight foreign displacement turn gear_ratio//为便于展示去除其他变量
```
以下为系统数据 "auto.dta" 处理后前10个观察值：
|make	|price	|mpg	|length|	n|
|---------|-------|--------|------- |-----|
|AMC Spirit|	3799|	22|	168|	1|
|Buick Skylark|	4082	|19	|200	|2|
|AMC Concord|	4099|	22|	186	|3|
|Buick Opel	|4453|	26|	170	|4|
|AMC Pacer|	4749	|17|	173|	5|
|Buick Century	|4816	|20	|196|	6|
|Buick Regal	|5189	|20	|200	|7|
|Buick LeSabre	|5788	|18	|218	|8|
|Buick Electra|	7827	|15|	222|	9|
|Buick Riviera	|10372	|16	|207	|10|



### 4.1 gsample 命令运行结果展示
此处以前述数据作为例子进行展示，以下代码为产生大小为_ N (有放回的简单随机样本, SRSWR ) 的 bootstrap 样本，且内存中的数据将被采样的观测值代替。
```Stata
sysuse "auto.dta", clear //调用系统数据
drop in 11/74 //为便于展示仅使用前10个观测值
sort price
gen n=_n //按照价格对观察值进行排序并编号
drop rep78 headroom trunk weight foreign displacement turn gear_ratio//为便于展示去除其他变量
gsample //进行抽样
```
如下为执行 `gsample` 命令后的结果，相比起原始数据，处理后样本中出现了重复观察值， Stata 自动抽取了3个观察值代替了原有的3个观察值。
|make	|price	|mpg	|length|	n|
|---------|-------|--------|------- |-----|
|Buick Opel	|4453|	26	|170|	4|
|Buick Opel|	4453|	26|	170|	4|
|AMC Spirit|	3799	|22|	168	|1|
|Buick Regal	|5189	|20	|200	|7|
|AMC Pacer|	4749	|17	|173|	5|
|Buick Riviera	|10372|	16|	207	|10|
|AMC Spirit	|3799	|22|	168	|1|
|Buick Electra	|7827|	15	|222|	9|
|Buick Century	|4816	|20|	196|	6|
|AMC Pacer	|4749	|17	|173|	5|


### 4.2 部分 options 效果展示

此处仍以系统数据 "auto.dta" 的前10个观察值为例，展示部分 `options` 的运行结果。

#### 4.2.1 简单无放回抽样

```Stata
sysuse "auto.dta", clear //调用系统数据
drop in 11/74 //为便于展示仅使用前10个观测值
sort price
gen n=_n //按照价格对观察值进行排序并编号
drop rep78 headroom trunk weight foreign displacement turn gear_ratio//为便于展示去除其他变量
  gsample 65, percent wor 
```
此处是产生样本容量为总体65% (无放回的简单随机样本)的 bootstrap 样本，且内存中的数据将被采样的观测值代替，以下为抽样结果：

|make	|price	|mpg	|length|	n|
|---------|-------|--------|------- |-----|
|Buick Opel	|4453	|26|	170|	4|
|Buick Regal	|5189	|20	|200	|7|
|AMC Pacer	|4749|	17	|173|	5|
|Buick Riviera	|10372	|16|207	|10|
|AMC Spirit	|3799	|22	|168	|1|
|Buick Skylark	|4082|	19	|200	|2|
|Buick Century	|4816|	20|	196|	6|

#### 4.2.2 有放回不等概率抽样

`weight[]`所设定的变量为抽样的辅助变量，总体中各观察值被抽中的概率与这些观察值的辅助变量大小成正比。此处以变量 **mpg** 作为辅助变量来进行展示。

```Stata
sysuse "auto.dta", clear //调用系统数据
drop in 11/74 //为便于展示仅使用前10个观测值
sort price
gen n=_n //按照价格对观察值进行排序并编号
drop rep78 headroom trunk weight foreign displacement turn gear_ratio//为便于展示去除其他变量
 gsample 1000 [weight=mpg], generate(fre)
```

上述代码以变量 **mpg** 作为辅助变量，进行1000次有放回随机抽样并形成新变量 **fre** 以记录每个观察值被抽取的次数，运行后的数据如下所示：

|make	|price	|mpg	|length|	n|fre|
|---------|-------|--------|------- |-----|----|
|AMC Spirit|	3799	|22	|168|	1	|108|
|Buick Skylark|	4082	|19|	200|	2	|82|
|AMC Concord|	4099	|22|	186	|3|	108|
|Buick Opel|	4453	|26	|170	|4|	130|
|AMC Pacer	|4749	|17	|173|	5	|94|
|Buick Century|	4816|	20|	196|	6	|102|
|Buick Regal	|5189	|20	|200|	7	|103|
|Buick LeSabre	|5788	|18|	218	|8	|106|
|Buick Electra	|7827	|15|	222	|9|	81|
|Buick Riviera	|10372	|16	|207|	10|	86|

在本次抽样中，10个初始观察值中 **mpg** 最大的编号为 4 的观察值被抽取的次数最多，达到了 130 次，而 **mpg** 最小的编号为 9 的观察值仅被抽取了81次。这样的结果能够很直观地展现出辅助变量对于抽样的影响，即某一观察值的辅助变量值越大，其被抽取的概率越高，这就是不等概率抽样。

#### 4.2.3 有放回分层抽样
 此处以变量 **length** 和 **price** 作为分层依据，每个类层抽取2个样本，且内存中的数据不会被抽取的观测值代替，而是生成新变量 **fre** 以记录每个观察值被抽到的次数

 ```Stata
 sysuse "auto.dta", clear //调用系统数据
drop in 11/74 //为便于展示仅使用前10个观测值
sort price
gen n=_n //按照价格对观察值进行排序并编号
drop rep78 headroom trunk weight foreign displacement turn gear_ratio//为便于展示去除其他变量
 gsample 2, strata(length price) generate(fre)
```
以下为运行结果：
|make	|price	|mpg	|length|	n|fre|
|---------|-------|--------|------- |-----|----|
|AMC Spirit	|3799	|22	|168	|1	|2|
|Buick Skylark	|4082	|19	|200	|2	|2|
|AMC Concord|	4099	|22	|186	|3	|2|
|Buick Opel	|4453	|26|170|	4|	2|
|AMC Pacer	|4749|	17	|173	|5|	2|
|Buick Century	|4816|	20|	196|	6|	2|
|Buick Regal	|5189|	20	|200|	7|	2|
|Buick LeSabre	|5788	|18	|218|	8	|2|
|Buick Electra	|7827	|15|	222|	9	|2|
|Buick Riviera	|10372	|16	|207|	10|	2|

可以看出，原始数据并未发生改变，并且由于变量的特征，分层实现了每个观察值作为一个单独的类层，因而每个观察值都被抽样了 2 次。

#### 4.2.4 rround 的作用
在前面分层抽样的基础上，为更清晰地展示 `rround` 的作用，采用如下代码进行抽样：
```Stata
sysuse "auto.dta", clear //调用系统数据
drop in 11/74 //为便于展示仅使用前10个观测值
sort price
gen n=_n //按照价格对观察值进行排序并编号
drop rep78 headroom trunk weight foreign displacement turn gear_ratio//为便于展示去除其他变量
gsample 233 ,percent strata(length price) rround generate(fre)
gsample 233 ,percent strata(length price)  generate(fre2)
```
上列代码对原始数据进行了有放回的分层抽样，以 **length** 和 **price** 作为划分类层的依据，每个类层抽取的样本数为类层中观察值总数的 233% ，并分别形成了 **fre** 和 **fre2** 来记录每个原始观察值被抽取的频率,抽样后数据如下所示：

|make	|price	|mpg	|length|	n|fre|fre2|
|---------|-------|--------|------- |-----|----|------|
|AMC Spirit|	3799|	22	|168	|1|	2	|2|
|Buick Skylark|	4082|	19	|200|	2	|3	|2|
|AMC Concord|	4099	|22	|186	|3|	2	|2|
|Buick Opel	|4453	|26	|170	|4	|3	|2|
|AMC Pacer|	4749	|17	|173	|5	|3|	2|
|Buick Century	|4816	|20	|196|	6	|2	|2|
|Buick Regal	|5189	|20	|200	|7|	2|	2|
|Buick LeSabre|	5788|	18	|218|	8	|2	|2|
|Buick Electra	|7827|	15	|222	|9|	2|	2|
|Buick Riviera|	10372	|16	|207	|10	|2	|2|

可以看出，使用了选项 `rround` 的抽样总共抽取了23个样本，而没有使用的抽样总共抽取了20个样本，这是因为添加了 `rround` 后，`gsample` 会对原始观察值整体进行取整，然后在对各个类层的抽样中，会有一部分类群抽取的样本数量向上取整，从而保证样本数量的充足。此处整体要抽取 23 个样本，由于有 10 个类层，因而在对 7 个类层抽取 2 个样本的同时，会随机挑选 3 个类层抽取 3 个样本。这就是 `rround` 的作用。

## 5. 结语
本篇推文主要使用 Stata 实操举例介绍了 Stata 用于抽样的命令 —— `gsample` ，它能够根据用户需求，对数据进行有放回抽样、无放回抽样、分层抽样、不等概率抽样等，在抽样方法上较为丰富。其语法规则简单易懂，非常容易上手。同时可以提供几种不同的抽样方法以及实用选项，因此该命令有很高的实用价值，值得广大 Stata 用户学习。

## 参考资料和相关推文
- [Jann, B. (2006). gsample: Stata module to draw a random sample.](https://ideas.repec.org/c/boc/bocode/s456716.html)


- 专题：[Stata命令](https://www.lianxh.cn/blogs/43.html)
  - [Stata：手动实现置换检验(permutation)和自抽样(bootstrap)](https://www.lianxh.cn/news/889a49516bcc8.html)
  -  [moremata程序包手动安装方法](https://www.lianxh.cn/news/d203f76175f0a.html)
- 专题：[Stata程序](https://www.lianxh.cn/blogs/26.html)
  -  [Stata: Bootstrap-自抽样-自举法](https://www.lianxh.cn/news/47de6c90ac6b7.html)
