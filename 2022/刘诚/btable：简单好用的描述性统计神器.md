# btable：简单好用的描述性统计神器



> **作者：** 刘诚 (中山大学)        
> **E-mail:**  <liuch553@mail2.sysu.edu.cn>   

&emsp;

---

**目录**

[TOC]

---
&emsp;

## 1. 简介

描述性统计常被用于获得对研究数据的最初步认识，常用指标包括均值、中位数、标准差等。看似简单基础，却能很好地反映出数据的集中趋势、分散程度及频数分布等。一张清晰全面的统计表，不仅能直观展示数据质量，更是彰显作者功底与研究价值，是各大核心期刊高水平论文不可或缺的重要组成部分。

本文将重点介绍 **Lukas Bütikofer** 于 2022 年上传 Github 并开源分享的外部命令`btable`。Lukas 是伯尔尼大学医学院分支机构 CTU Bern 的一名资深数据分析师，研究领域为医疗观察、临床试验等的数据分析与统计汇报。描述性统计同样常见于生物医学类期刊，且通常以 Table 1 即第一张表格的形式出现，作用是向读者展示研究人群的基本特征，而`btable`设计的初衷便是利用 Stata 轻松解决对 Table 1 的绘制。

`btable`能够通过两条简洁的命令完成对各类变量的描述性统计及表格绘制，包括连续变量、类别变量、计数变量、TTE（Time-to-event）变量等，并支持分组统计与组间效应的测算。表格的绘制分为两步，首先，通过`btable`命令生成一张原始表格，报告全样本统计指标，若存在分组，还将分别报告各组统计指标、组间效应及其置信区间与检验 p 值，涵盖数十种不同效应及检验方法，可谓是描述性统计的**集大成者**。进一步地，通过`btable_format`命令一键实现论文级表格排版，**省心、高效又美观**。


## 2. 安装
> **法一**：利用`github`命令从 GitHub 网站下载

GitHub 是一个面向开源及私有软件项目的托管平台，现如今许多学者将程序托管到该平台，用户能够轻松从 GitHub 中找到海量开源代码。为了更方便地安装托管在 GitHub 上的命令，**E. F. Haghish** 开发了 `github` 命令，如今已更新至 v2.3.0，能够实现搜索、安装、移除、更新等功能，详细可参考命令的[ GitHub 主页](https://github.com/haghish/github)或者[作者个人主页](https://haghish.github.io/github/)。

首先，我们需要运行以下代码安装`github`命令。

```Stata
net install github, replace from("https://haghish.github.io/github/")
```
再利用`github`命令从作者的 GitHub 主页下载最新版的`btable`。
```Stata
github install CTU-Bern/btable
```



> **法二**：下载源文件，手动安装

从`btable`的 [GitHub 主页](https://github.com/CTU-Bern/btable)中下载源文件（即**后缀为 .ado 和 .sthlp 的文件**），找到 Stata 安装路径下的【ado】文件夹，将源文件复制到存放外部命令的【plus】文件夹中，并运行以下代码：

```stata
adopath + "D:\Stata17\ado\plus"  //注意修改安装路径
```

`btable`源文件如下图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/btable：简单好用的描述性统计神器_Fig1_btable源文件_刘诚.png.png)

外部命令安装的更多方法可参考连享会 [Stata: 外部命令的搜索、安装与使用](https://www.lianxh.cn/news/c2ab130d9873d.html)

## 3. btable 语法介绍

```stata
help btable
```

### 3.1 基础语法介绍

 `btable` 的基础语法结构如下：
```stata
btable varlist [if] [in] , saving(filename) [by(groupvar) options]
```
- <i>varlist</i> ：支持连续变量、类别变量、计数变量、TTE 变量等，可由 **<u>conti</u>nuous(**<i>varlist</i>**)**、**<u>cat</u>egorical(**<i>varlist</i>**)**、**count(**<i>varlist</i>**)** 以及 **tte(**<i>varlist</i>**)** 对变量进行精确定义。
- **<u>sav</u>ing(**<i>filename</i>**)** ：**必选项**，指定产出表格的文件名，文件格式为数据集（**filename.dta**）。
- **by(**<i>varname</i>**)** ：分组统计并进行组间效应测算，分组变量标签将另外存储于附属文件 **filname_group.dta** 中。

### 3.2 Options 介绍

<table>
    <tbody>
    <tr>
        <th width="60px" style="text-align: center"></th>
        <th width="80px" style="text-align: center">Options</th>
        <th width="500px" style="text-align: center">说明</th>
    </tr>
    <tr>
        <td rowspan="2" style="text-align: center"><b>类别变量</b></td>
        <td style="text-align: center"> <b><u>denom</u>inator(</b><i>nonmiss</i><b>)</b> </td>
        <td>指定非缺失值的数量作为分母，默认使用样本总数。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b><u>all</u>levels</b></td>
        <td>显示类别变量下的全部层级，默认只显示非空层级。</td>
    </tr>
    <tr>
        <td rowspan="3" style="text-align: center"><b>计数变量</b></td>
        <td style="text-align: center"> <b>exp_time(</b><i>varlist</i><b>)</b> </td>
        <td>定义计数变量的暴露时间。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>exp_time_units(</b><i>string</i><b>)</b> </td>
        <td>定义暴露时间的单位，为可自定义的字符串。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>nbreg</b> </td>
        <td>计算负二项回归的 <b>IRR</b>(incidence rate ratio) 及其置信区间，以及 <b>Wald</b> 检验 p 值。</td>
    </tr>
    <tr>
        <td rowspan="5" style="text-align: center"><b>TTE 变量</b></td>
        <td style="text-align: center"> <b>fail(</b><i>varlist</i><b>)</b> </td>
        <td>定义失败指示变量，为二元 0-1 变量，其中 0 表示样本被删减（censored)，1 表示失败或未能存活（failure）。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>surv_time_units(</b><i>string</i><b>)</b> </td>
        <td>定义 TTE 变量的单位，为可自定义的字符串。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>trunc_time(</b><i>numlist</i><b>)</b> </td>
        <td>指定用于计算受限平均生存时间（RMS time）的截断时间。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>rmst_model(</b><i>rmst_modelspec</i><b>)</b> </td>
        <td>指定用于计算受限平均生存时间（RMS time）的模型，可以选择 <b>npar</b>（non-parametric）或 <b>fpm</b>（flexible parametric survival model），默认使用 npar 模型。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>fpm_df(</b><i>numlist</i><b>)</b> </td>
        <td>指定 fpm 模型的自由度，默认为 3。</td>
    </tr>
    <tr>
        <td rowspan="3" style="text-align: center"><b>组间效应</b></td>
        <td style="text-align: center"> <b>order(</b><i>numlist</i><b>)</b> </td>
        <td>指定不同组别在表格中的排序。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>effect(</b><i>effectspec</i><b>)</b> </td>
        <td>指定需要报告的组间效应，默认为全部。其中，连续变量效应包括均值差异 <b>meand</b>，中位数差异 <b>medd</b>，Hodges-Lehmann 中位数差异 <b>hlmd</b> 和 Mann-Whitney 统计量 <b>mws</b>；类别变量效应包括风险差 <b>rd</b>，风险比 <b>rr</b> 和比值比 <b>or</b>；计数变量效应包括发生率 <b>irr</b> 和发生率差异 <b>ird</b>；TTE 变量效应包括危险比 <b>hr</b> 和受限平均生存时间差 <b>rmstd</b>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>test(</b><i>testspec</i><b>)</b> </td>
        <td>指定需要报告的检验，默认为全部。其中，连续变量检验包括 t 检验 <b>ttest</b>，Wilcoxon 秩和检验 <b>ranksum</b>，Wald 检验 <b>qreg</b>，方差分析 <b>anova</b> 和 Kruskal-Wallis 检验 <b>kwallis</b>；类别变量检验包括费雪精确检验 <b>fisher</b> 和卡方检验 <b>chi2</b>；计数变量检验包括泊松精确检验 <b>poisson</b>；TTE 变量检验包括 Log-rank 检验 <b>logrank</b>、Cox 模型检验 <b>cox</b> 和受限平均生存时间差检验 <b>rmstd</b>。</td>
    </tr>
    <tr>
        <td rowspan="8" style="text-align: center"><b>置信区间</b></td>
        <td style="text-align: center"> <b><u>l</u>evel(</b><i>#</i><b>)</b> </td>
        <td>指定置信水平，默认为 95%。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>prci(</b><i>prcispec</i><b>)</b> </td>
        <td>指定比例的置信区间类型，可选项包括 <i>wilson</i>、<i>wald</i>、<i>exact</i>、<i>agresti</i> 和 <i>jeffreys</i>，默认为 <i>wilson</i>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>rdci(</b><i>rdcispec</i><b>)</b> </td>
        <td>指定风险差（risk difference）的置信区间类型，可选项包括 <i>wald</i>、<i>ac</i>、<i>nhs</i>、<i>wall</i> 和 <i>mn</i>，默认为 <i>wald</i>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>rrci(</b><i>rrcispec</i><b>)</b> </td>
        <td>指定风险比（risk ratio）的置信区间类型，可选项包括 <i>katz</i> 和 <i>koopman</i>，默认为 <i>katz</i>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>orci(</b><i>orcispec</i><b>)</b> </td>
        <td>指定比值比（odds ratio）的置信区间类型，可选项包括 <i>woolf</i>、<i>exact</i>、<i>cornfield</i>、<i>agresti</i> 和 <i>gart</i>，默认为 <i>woolf</i>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>hlmdci(</b><i>hlmdcispec</i><b>)</b> </td>
        <td>指定 Hodges-Lehmann 中位数差异的置信区间类型，可选项包括 <i>newson</i> 和 <i>lehmann</i>，默认为 <i>newson</i>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>mwsci(</b><i>mwscispec</i><b>)</b> </td>
        <td>指定 Mann-Whitney 统计量的置信区间类型，可选项包括 <i>fay</i>、<i>delong</i>、<i>jackknife</i>、<i>bamber</i> 和 <i>hanley</i>，默认为 <i>fay</i>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>irrci(</b><i>irrcispec</i><b>)</b> </td>
        <td>指定发生率（incidence rate ratio）的置信区间类型，可选项包括 <i>exact</i>、<i>oim</i> 和 <i>robust</i>，默认为 <i>exact</i>。</td>
    </tr>
<tr>
        <td rowspan="8" style="text-align: center"><b>检验</b></td>
        <td style="text-align: center"> <b>ttest(</b><i>ttest_options</i><b>)</b> </td>
        <td>支持 <b>ttest</b> 的全部可选项，详见 help ttest。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>qreg(</b><i>qreg_options</i><b>)</b> </td>
        <td>支持 <b>qreg</b> 的全部可选项，详见 help qreg。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>lr(</b><i>lrspec</i><b>)</b> </td>
        <td>计算似然比卡方统计量，可选项包括 <i>chi2</i>、<i>poisson</i>、<i>nbreg</i> 和 <i>cox</i>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>ranksum_exact</b> </td>
        <td>计算大样本量（>200）情况下 Wilcoxon 秩和检验的精确 p 值。默认情况下，只有当样本量 <200 时才会计算。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>poisson_test(</b><i>poisson_testspec</i><b>)</b> </td>
        <td>指定泊松检验的类型，可选项包括 <i>midp</i>、<i>exact</i>、<i>oim</i> 和 <i>robust</i>，默认为 <i>midp</i>。</td>
    </tr>
    </tbody>
</table>
>更多信息和原始代码可见 `btable` 的 [GitHub 主页](https://github.com/CTU-Bern/btable)
>
>有关TTE数据分析及其模型的更多信息可参考 [Columbia Public Health](https://www.publichealth.columbia.edu/research/population-health-methods/time-event-data-analysis)

##4.btable_format 语法介绍

```stata
help btable_format
```

### 4.1 基础语法介绍

 `btable_format` 的基础语法结构如下：

```stata
btable_format using filename [, options]
```

- **using** <i>filename</i> ：输入`btable`命令产出的结果表格（**filename.dta**）。

### 4.2 Options 介绍

<table>
    <tbody>
    <tr>
        <th width="60px" style="text-align: center"></th>
        <th width="80px" style="text-align: center">Options</th>
        <th width="500px" style="text-align: center">说明</th>
    </tr>
    <tr>
        <td rowspan="6" style="text-align: center"><b>Main</b></td>
        <td style="text-align: center"> <b>clear</b> </td>
        <td>清除内存数据。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>ncol(</b><i>header</i><b>)</b> </td>
        <td>新增一列，<i>header</i> 为列名。例如：<b>ncol(</b><i>non-missing</i><b>)</b> 为报告非缺失值信息。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>drop(</b><i>colspec</i><b>)</b> </td>
        <td>指定要删除的部分，其中 <b>total</b> 代表总体样本特征，<b>effect</b> 代表组间效应，<b>test</b> 代表检验 p 值，<b>info</b> 代表注释。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>nrow</b> </td>
        <td>新建一行，默认插在第一行。</td>
    </tr>
     <tr>
        <td style="text-align: center"> <b>design(</b><i>designspec</i><b>)</b> </td>
        <td>可选项有 <b>column</b>，描述类型标签展示于列头；<b>row</b>，描述类型标签展示于行头；<b>missing</b>，新增一行报告变量缺失值信息。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>parse(</b><i>string</i><b>)</b> </td>
        <td>指定分隔符，默认为 “|”。</td>
    </tr>
    <tr>
        <td rowspan="2" style="text-align: center"><b>描述</b></td>
        <td style="text-align: center"> <b><u>des</u>criptive(</b><i>descspec</i><b>)</b> </td>
        <td>指定描述的指标类型，基础格式为 <i>varspec statistic</i> 。其中 <i>varspec</i> 包括 <b>conti</b>，<b>cat</b>，<b>count</b>，<b>tte</b>，<i>varname</i> 和 <b>miss</b>，分别表示连续变量，类别变量，计数变量，TTE 变量，某一具体变量和缺失值。<i>statistic</i> 包括 <b>mean/ sd/median/min /max</b>（<b>conti</b>），<b>nlev/ntot/perc</b>（<b>cat</b>），<b>nevents/etime</b>（<b>count </b>），<b>nfails/stime</b>（<b>tte</b>）等，详见 help btable_format。例如：<b>des (</b><b>conti </b> median [lq,uq]<b>)</b> 表示对于连续变量报告中位数与四分位区间。默认格式为 <b>descriptive(</b><b>conti</b> mean (sd) <b>cat</b> nlev (perc%) <b>count</b> nevents (etime) <b>tte</b> nfails (stime) <b>miss</b> nlev (perc%)<b>)</b>。 </td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>format_desc(</b><i>formatsspec</i><b>)</b> </td>
        <td>指定描述的格式，基础格式为 <i>varspec %fmt</i>。默认保留两位小数（即 %8.2g）。</td>
    </tr>
    <tr>
        <td rowspan="4" style="text-align: center"><b>效应</b></td>
        <td style="text-align: center"> <b><u>eff</u>ect(</b><i>effectspec</i><b>)</b> </td>
        <td>指定效应的类型，基础格式为 <i>varspec effectm</i>。其中 <i>effectm</i> 包括 <b>meand /medd/mws</b>（<b>conti</b>），<b>rd/rr/or</b>（<b>cat</b>），<b>irr/ird</b>（<b>count </b>），<b>hr/rmstd</b>（<b>tte</b>）等，详见 help btable_format。例如：<b>eff(</b><b>conti</b> mws (mws_lci to mws_uci)<b>)</b> 表示对于连续变量报告 Mann-Whitney 统计量和 95% 置信区间。默认格式为 <b>effect(</b><b>conti</b> meand (meand_lci to meand_uci) <b>cat</b> rd (rd_lci to rd_uci) <b>count</b> irr (irr_lci to irr_uci) <b>tte</b> hr (hr_lci to hr_uci)<b>)</b>。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>format_effect(</b><i>formatsspec</i><b>)</b> </td>
        <td>指定效应的格式，基础格式为 <i>varspec %fmt</i>。默认保留两位小数（即 %8.2g）。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b><u>perc</u>entage(</b><i>rd mws</i><b>)</b> </td>
        <td>以百分比的形式报告风险差（<b>rd</b>）与 Mann-Whitney 统计量（<b>mws</b>），默认为小数形式。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b><u>abbr</u>eviation</b> </td>
        <td>列名中使用效应的缩写，并通过脚注进行解释。</td>
    </tr>
    <tr>
        <td rowspan="1" style="text-align: center"><b>检验</b></td>
        <td style="text-align: center"> <b>test(</b><i>testspec</i><b>)</b> </td>
        <td>指定检验的类型，基础格式为 <i>varspec test</i>。其中 <i>test</i> 包括 <b>ttest/ranksum /qreg</b>（<b>conti</b>），<b>fisher/chi2</b>（<b>cat</b>），<b>poisson</b>（<b>count</b>），<b>logrank/cox /rmstd</b>（<b>tte</b>）等，详见 help btable_ format。例如：<b>test(</b><b>conti </b> ranksum<b>)</b> 表示对于连续变量报告 t 检验的 p 值。默认格式为 <b>test(</b><b>conti</b> ttest /ranksum <b>cat</b> fisher <b>count</b> poisson <b>tte</b> logrank<b>)</b>。若连续变量的 <b>descriptive</b> 包括均值 <b>mean</b>，则默认使用 <b>ttest</b>；若包括中位数 <b>median</b>，则默认使用 <b>ranksum</b>。</td>
    </tr>
    <tr>
        <td rowspan="3" style="text-align: center"><b>分块</b></td>
        <td style="text-align: center"> <b>block(</b><i>blockspec</i><b>)</b> </td>
        <td>将变量分类汇报，通过分隔符划分。常见分类例如：连续变量、类别变量、计数变量；被解释变量、解释变量、控制变量等。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>block_head(</b><i>block_headspec</i><b>)</b> </td>
        <td>指定分类的标题，通过分隔符划分。</td>
    </tr>
    <tr>
        <td style="text-align: center"> <b>block_parse(</b><i>string</i><b>)</b> </td>
        <td>指定 <b>block</b> 和 <b>block_head</b> 命令的分隔符，默认为“|”。</td>
    </tr>
    </tbody>
</table>
>更多信息和原始代码可见 `btable` 的 [GitHub 主页](https://github.com/CTU-Bern/btable)

## 5. Stata 实操演示

下面将通过几个简单的例子向大家展示`btable`和`btable_format`的用法及实际效果。

###5.1 btable 命令实操演示 

```Stata
sysuse auto2, clear  //调用系统数据

btable price mpg rep78 headroom, saving("excars1")  //未添加Options，文件存储为"excars1.dta"

btable price mpg rep78 headroom, by(foreign) saving("excars2") denom(nonmiss)  
//by(foreign)分组，以非缺失值数量作为分母，文件存储为"excars2.dta"
  
btable price mpg rep78 headroom, by(foreign) saving("excars3") conti(rep78) cat(headroom) 
//指定rep78为连续变量，headroom为类别变量，文件存储为"excars3.dta"
	
btable price mpg rep78 headroom, by(foreign) saving("excars4") rdci(ac)
//指定风险差的置信区间类型为 ac（Agresti and Caffo，2000），文件存储为"excars4.dta"
	
btable price mpg rep78 headroom, by(foreign) saving("excars5") effect(rd meand) test(fisher ttest)
//指定组间效应类型为 rd（风险差）和 meand（均值差异），检验的类型为 fisher（费雪精确检验）和 ttest（t检验），其他效应与检验将不会报告，文件存储为"excars5.dta"
```
- 部分结果展示：

```stata
use "excars1.dta", clear

varname	 varlabel	        vtype level	levlabel nlev ntot_t nnonmiss_t	nlev_t  pr_t 
price	 Price	            conti	.			   .    74	     74		  .
mpg	     Mileage (mpg)      conti	.			   .    74	     74		  .
rep78	 Repair record 1978	 cat1 	.		       5	74	     69	      74	 1	
rep78	 Repair record 1978	 cat	1	   Poor    5	74	     69	      2	  .027027	
rep78	 Repair record 1978	 cat	2	   Fair	   5	74	     69	      8	  .1081081	
rep78	 Repair record 1978	 cat    3	  Average  5	74	     69	      30  .4054054	
rep78	 Repair record 1978	 cat	4	   Good	   5	74	     69	      18  .2432432	
rep78	 Repair record 1978	 cat    5	 Excellent 5	74	     69	      11  .1486486	
headroom Headroom (in.)	    conti	.		 	   .    74	     74		  .            
                                                                                       --接下表
mean_t    sd_t   meanlci_t meanuci_t p50_t p25_t p75_t iqr_t min_t max_t range_t sum_t
6165.257 2949.496 5481.914  6848.6	5006.5 4195	 6342  2147  3291  15906  12615	 456229
21.2973	 5.785503 19.95691 22.63769	  20	18	  25	7	  12    41	   29	  1576
  .			.		  .		   .	  .	    .     .     .     .     .      .       .
  .			.		  .		   .	  .	    .     .     .     .     .      .       .
  .			.		  .		   .	  .     .     .     .     .     .      .       .
  .			.		  .		   .	  .     .     .     .     .     .      .       .
  .			.		  .		   .	  .     .     .     .     .     .      .       .
  .			.		  .		   .	  .     .     .     .     .     .      .       .
2.993243 .8459948 2.797242 3.189244	  3    2.5	 3.5	1	 1.5	5	  3.5	 221.5
```

其中，<i>price</i>，<i>mpg</i>，<i>headroom</i> 为连续变量，<i>rep78</i> 为类别变量。

对于类别变量，`btable`报告了**类别标签**（levlabel）、**样本总数**（ntot_t）、**非缺失值总数**（nnonmiss_t）、**类别样本数**（nlev_t）、**类别占比**（pr_t）及其 95% 置信区间（[prlci_t,pruci_t]）。

对于连续变量，`btable`报告了**均值**（mean_t）及其95%置信区间（[meanlci_t,meanuci_t]）、**标准差**（sd_t）、**中位数**（p50_t）、**四分位数**（p25_t/p75_t）、**四分位距**（iqr_t）、**最值**（min_t/max_t）、**取值范围**（range_t）及**样本和**（sum_t）。

```stata
use "excars2.dta", clear
//增加by(foreign)选项后，除汇报总体与分组样本特征（同上）外，还支持组间效应及其置信区间与检验p值的测算，未指定则默认报告全部效应与检验（见下表）。

varname	 vtype level p_chi2  p_fisher    rd	    rd_lci    rd_uci   rr   rr_lci  rr_uci    or
price	 conti	 .	    .	    .         .        .         .      .     .       .       .
mpg		 conti	 .	    .       .         .        .         .      .     .       .       .
rep78	 cat1    .  .0000176 6.27e-06	  .		   .         .      .     .       .       .
rep78	 cat     1  .3424817	1	 .0416667 -.0148635 .0981968    .     .       .       .
rep78	 cat     2  .0466209 .0952346 .1666667 .0612374	.2720959    .     .       .       .
rep78	 cat     3  .0012143 .0014073 .4196429 .2144742	.6248115 3.9375 1.3413 11.5593 7.714286
rep78	 cat     4  .0358737 .0711467 -.241071 -.47980 -.0023446 .4375 .202858 .943548 .3076923
rep78	 cat     5  .0000535 .0001913 -.386905 -.60598 -.1678294 .0972 .022947 .411914 .4119138
headroom conti   .	    .	    .         .        .         .      .     .       .       .
                                                                                       --接下表
p_ttest	  p_ranksum  p_qreg	    meand	 meand_lci	meand_uci  medd	  hlmd	   mws
.6801851  .2980103	.1043637  -312.2587	 -1816.225	1191.708   -983	  -497	.4230769
.0005254  .0019314	.0002115  -4.945804	 -7.661225	-2.230384	-6	   -5	.2714161
	.		  .		    .	      . 	     .	        .       .      .        . 
	.		  .		    .	      . 	     .	        .       .      .        .
	.		  .		    .	      . 	     .	        .       .      .        .
	.		  .		    .	      . 	     .	        .       .      .        .
	.		  .		    .	      . 	     .	        .       .      .        .
	.		  .		    .	      . 	     .	        .       .      .        .		
.0110486  .0107803	.0034339  .5402098	 .1273867	.9530329	1	   .5	.6857517
```

对于类别变量，`btable`报告了**卡方检验** p 值（p_chi2）、**费雪精确检验** p 值（p_fisher）、**风险差**（rd）及其 95% 置信区间（[rd_lci,rd_uci]）、**风险比**（rr）及其 95% 置信区间（[rr_lci,rr_uci]）、**比值比**（or）及其 95% 置信区间（[or_lci,or_uci]）。

对于连续变量，`btable`报告了 **t 检验** p 值（p_ttest）、**Wilcoxon 秩和检验** p 值（p_ranksum）、**Wald 检验** p 值（p_qreg）、**均值差异**（meand）及其 95% 置信区间（[meand_lci,meand_uci]）、**中位数差异**（medd）及其 95% 置信区间（[medd_lci,medd_uci]）、**Hodges-Lehmann 中位数差异**（hlmd）及其 95% 置信区间（[hlmd_lci,hlmd_ uci]）、**Mann-Whitney 统计量**（mws）及其 95% 置信区间（[mws_lci,mws_uci]）。

###5.2 btable_format 命令实操演示 

```stata
sysuse auto2, clear  //调用系统数据

btable price mpg rep78 headroom, by(foreign) saving("excars") denom(nonmiss) 
//生成原始表格，文件存储为"excars.dta"

btable_format using "excars", clear  //调入原始表格，按论文格式排版

variable	 out_t	           out_1	          out_2             out_d              pv
	    Total (N = 74)	  Domestic (N = 52)	 Foreign (N = 22)  MD or RD* (95% CI)    P-value
	   mean (sd) or n (%) mean (sd) or n (%) mean (sd) or n (%)
Price	   6165 (2949)	    6072 (3097)	       6385 (2622)   -312 (-1816 to 1192)      0.68
Mpg         21 (5.8)	     20 (4.7)	        25 (6.6)     -4.9 (-7.7 to -2.2)      <0.001
Rep78			                                                                      <0.001
  Poor	    2 (2.9%)	     2 (4.2%)	        0 (0.00%)    0.04 (-0.01 to 0.10)     
  Fair	     8 (12%)	     8 (17%)	        0 (0.00%)    0.17 (0.06 to 0.27)
  Average	30 (43%)	     27 (56%)	         3 (14%)     0.42 (0.21 to 0.62)
  Good	    18 (26%)	     9 (19%)	         9 (43%)     -0.24 (-0.48 to -0.00)
  Excellent	11 (16%)	     2 (4.2%)	         9 (43%)     -0.39 (-0.61 to -0.17)
Headroom   3.0 (0.85)	    3.2 (0.92)	        2.6 (0.49)   0.54 (0.13 to 0.95)       0.011
*MD: mean difference, RD: risk difference
```

上表out_t 列报告了全样本特征，out_1 列报告了国内样本特征，out_2 列报告了国外样本特征。其中，连续变量默认报告**平均值（标准差）**，类别变量默认报告**样本数（占比）**。out_d 列报告了组件效应及其置信区间，其中连续变量默认报告**均值差异**，类别变量默认报告**风险差**。pv 列报告了检验 p 值，其中连续变量默认报告 **t 检验** p 值，类别变量默认报告**费雪精确检验** p 值。

- 部分 *Options* 演示

```stata
btable_format using "excars", clear ncol(non-missing) drop(total effect info)
//新增两列报告非缺失值信息，不报告总体样本特征、组间效应与注释

variable	 ns_1	            out_1	           ns_2              out_2            pv
	    Domestic (N = 52)  Domestic (N = 52)  Foreign (N = 22)  Foreign (N = 22)    P-value
	       non-missing    mean (sd) or n (%)    non-missing    mean (sd) or n (%)
Price	      52	        6072 (3097)	            22            6385 (2622)        0.68
mpg           52	          20 (4.7)	            22             25 (6.6)         <0.001
rep78		  48	                                21                              <0.001
  Poor	    	              2 (4.2%)	                           0 (0.00%)         
  Fair	     	              8 (17%)	                           0 (0.00%)    
  Average		              27 (56%)	                            3 (14%)     
  Good	    	              9 (19%)	                            9 (43%)     
  Excellent		              2 (4.2%)	                            9 (43%)    
Headroom      52	         3.2 (0.92)	            22             2.6 (0.49)        0.011
---------------------------------------------------------------------------------------------------
btable_format using "excars", clear design(missing) drop(total effect info)
//新增一行报告变量缺失值特征，不报告总体样本特征、组间效应与注释

variable	 	           out_1	             out_2              pv
	    	          Domestic (N = 52)	    Foreign (N = 22)      P-value
Price - mean(sd)	   	  6072 (3097)	       6385 (2622)         0.68
mpg - mean(sd)        	    20 (4.7)	        25 (6.6)          <0.001
rep78 - n(%)			                                          <0.001
  Poor	   	                2 (4.2%)	        0 (0.00%)         
  Fair	    	            8 (17%)	            0 (0.00%)   
  Average		            27 (56%)	        3 (14%)     
  Good	    	            9 (19%)	            9 (43%)     
  Excellent		            2 (4.2%)	        9 (43%)     
* missing                   4 (7.7%)            1 (4.5%)
Headroom - mean(sd) 	   3.2 (0.92)	       2.6 (0.49)          0.011
---------------------------------------------------------------------------------------------------
btable_format using "excars", clear format_desc(price "%10.0f" conti "%10.2f" cat "%10.1f") drop(total effect info)  //设置各变量描述格式，不报告总体样本特征和组间效应

variable	 	       out_1	             out_2              pv      desc_info      test_info
	    	     Domestic (N = 52)	    Foreign (N = 22)      P-value  Descriptives      Tests
                 mean (sd) or n (%)    mean (sd) or n (%)
Price 	   	        6072 (3097)	          6385 (2622)          0.68      mean(sd)  Student's t-test
mpg         	    19.83 (4.74)	      24.77 (6.61)        <0.001     mean(sd)  Student's t-test
rep78 			                                              <0.001            Fisher's exact test
  Poor	   	          2 (4.2%)	            0 (0.0%)                       n(%)
  Fair	    	      8 (16.7%)	            0 (0.0%)                       n(%)
  Average		      27 (56.3%)	        3 (14.3%)                      n(%)
  Good	    	      9 (18.8%)	            9 (42.9%)                      n(%)
  Excellent		      2 (4.2%)	            9 (42.9%)                      n(%)
Headroom 	         3.15 (0.92)	       2.61 (0.49)         0.011     mean(sd)  Student's t-test
---------------------------------------------------------------------------------------------------
btable_format using "excars", clear drop(total info) desc(conti median [lq, uq]) effect(conti mws (mws_lci to mws_uci)) abbr  //连续变量报告中位数和四分位区间，连续变量组间效应报告Mann-Whitney统计量及其95%置信区间，不报告总体样本特征

variable	 	    out_1	                 out_2                  out_d              pv
	    	  Domestic (N = 52)	       Foreign (N = 22)      MWS or RD* (95% CI)     P-value
	       median [lq, uq] or n (%) median [lq, uq] or n (%)
Price	   	  4783 [4184, 6234]	       5759 [4499, 7140]     0.42 (0.29 to 0.57)      0.30
mpg         	 19 [17, 22]	          25 [21, 28]        0.27 (0.17 to 0.41)      0.002
rep78			                                                                     <0.001
  Poor	    	   2 (4.2%)	               0 (0.00%)         0.04 (-0.01 to 0.10)
  Fair	     	   8 (17%)	               0 (0.00%)         0.17 (0.06 to 0.27)
  Average		   27 (56%)	                3 (14%)          0.42 (0.21 to 0.62)	
  Good	    	   9 (19%)	                9 (43%)          -0.24 (-0.48 to -0.00)
  Excellent		   2 (4.2%)	                9 (43%)          -0.39 (-0.61 to -0.17)
Headroom   	    3.5 [2.3, 4.0]	         2.5 [2.5, 3.0]      0.69 (0.54 to 0.80)      0.011
*MWS: Mann-Whitney statistic, RD: risk difference
---------------------------------------------------------------------------------------------------
btable_format using "excars", clear desc(conti mean (sd) | conti median [lq, uq]) parse(|) design(row) drop(total effect)  //连续变量分别报告均值（标准差）与中位数（四分位区间），类型标签展示于行头，不报告总体样本特征与组间效应

variable	           out_1	         out_2	          pv              test_info
	             Domestic (N = 52)	Foreign (N = 22)	P-value             Tests
Price			
  mean (sd)	         6072 (3097)	  6385 (2622)	     0.68           Student's t-test
  median [lq, uq] 4783 [4184, 6234]	5759 [4499, 7140]	 0.30      Wilcoxon-Mann-Whitney test
mpg		
  mean (sd)	           20 (4.7)	       25 (6.6)	        <0.001          Student's t-test
  median [lq, uq]	 19 [17, 22]	 25 [21, 28]	     0.002     Wilcoxon-Mann-Whitney test
rep78 - n (%)			                                <0.001         Fisher's exact test
  Poor	               2 (4.2%)	       0 (0.00%)	
  Fair	               8 (17%)	       0 (0.00%)	
  Average	           27 (56%)	       3 (14%)	
  Good	               9 (19%)	       9 (43%)	
  Excellent	           2 (4.2%)	       9 (43%)	
Headroom			
  mean (sd)	          3.2 (0.92)	  2.6 (0.49)	     0.011          Student's t-test
  median [lq, uq]	3.5 [2.3, 4.0]	2.5 [2.5, 3.0]	     0.011     Wilcoxon-Mann-Whitney test
---------------------------------------------------------------------------------------------------
btable_format using "excars", clear block(mpg headroom price | rep78) block_head(Continuous variables | Categorical variables) drop(total info) abbr //将变量分为连续变量（Continuous variables）和类别变量（Categorical variables）两类，不报告总体样本特征和注释

variable	                 out_1	              out_2	              out_d	              pv
	                   Domestic (N = 52)	 Foreign (N = 22)	MD or RD* (95% CI)	    P-value
	                   mean (sd) or n (%)	 mean (sd) or n (%)		
Continuous variables				
  mpg	                    20 (4.7)	         25 (6.6)	   -4.9 (-7.7 to -2.2)	    <0.001
  Price	                  6072 (3097)	       6385 (2622)	  -312 (-1816 to 1192)	     0.68
  Headroom      	       3.2 (0.92)	        2.6 (0.49)	   0.54 (0.13 to 0.95)	     0.011
Categorical variables				
  rep78				                                                                    <0.001
    Poor	                2 (4.2%)	        0 (0.00%)	   0.04 (-0.01 to 0.10)	
    Fair	                8 (17%)	            0 (0.00%)	   0.17 (0.06 to 0.27)	
    Average	                27 (56%)	        3 (14%)	       0.42 (0.21 to 0.62)	
    Good	                9 (19%)	            9 (43%)	      -0.24 (-0.48 to -0.00)	
    Excellent	            2 (4.2%)	        9 (43%)	      -0.39 (-0.61 to -0.17)	
*MD: mean difference, RD: risk difference
```

## 6. 结语
本篇推文介绍了 Stata 外部命令`btable`的安装与使用，对命令的语法结构与 *Options* 进行了详细说明，并利用系统数据演示其强大功能。`btable`支持常用变量类型的几乎所有统计指标、组间效应与检验，并能通过`btable_format`命令一键实现论文级排版，省心高效又美观。无论单样本还是分组样本，`btable`都能出色完成描述性统计或是组间对比任务，在充当统计命令整合包的同时，还具有极强的表格绘制能力，实乃**描述性统计神器**。

最后，让我们再次感谢作者 **Lukas Bütikofer** 的创造性贡献！

## 参考资料和相关推文
- `btable`[Github 主页](https://github.com/CTU-Bern/btable)
- Columbia Public Health，[Time-To-Event Data Analysis](https://www.publichealth.columbia.edu/research/population-health-methods/time-event-data-analysis)
- 专题：[Stata入门](https://www.lianxh.cn/blogs/16.html)
  - [25常见种误区：P值、置信区间和统计功效](https://www.lianxh.cn/news/48279b1cf67e2.html)

- 专题：[Stata命令](https://www.lianxh.cn/blogs/43.html)
  - [Stata：描述性统计分析新命令-dstat](https://www.lianxh.cn/news/56f1a1d28823b.html)
  - [Stata：定制论文中表1-table1](https://www.lianxh.cn/news/5ae0fc4c56324.html)

- 专题：[结果输出](https://www.lianxh.cn/blogs/22.html)
  - [Stata结果输出：addest自己定制输出的统计量](https://www.lianxh.cn/news/79bdf97db717e.html)
  - [Stata结果输出-addest：自己添加统计量](https://www.lianxh.cn/news/dd76041a2d96f.html)
  - [baselinetable命令：论文基本统计量表格输出到Excel和Word](https://www.lianxh.cn/news/8635f14e51018.html)
  - [sumup：快速呈现分组统计量](https://www.lianxh.cn/news/9dcab1d774585.html)
  - [Stata：一文搞定论文表1——基本统计量列表](https://www.lianxh.cn/news/7ca2b65c68835.html)
