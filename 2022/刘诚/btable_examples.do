
  adopath + "D:\Stata17\ado\plus"
  sysdir set PLUS "D:\Stata17\ado\plus"          // 外部命令的存放位置
  sysdir set PERSONAL "D:\Stata17\ado\personal"  // 个人文件夹位置
  cd `c(sysdir_personal)'
  
* 安装btable命令
  
  * 法一：使用github命令安装
  net install github, from("https://haghish.github.io/github/") //安装github命令，与ssc类似
	
  github install CTU-Bern/btable  //从作者的github主页下载最新版的btable
  
  * 法二：下载源文件，手动安装
  
* help for btable
  h btable

* help for btable_format
  h btable_format

* 实操演示 
  * btable 命令实操演示
    sysuse auto2, clear  //调用系统数据
  
    btable price mpg rep78 headroom, saving("excars1")  //未添加Options，文件存储为"excars1.dta"

    btable price mpg rep78 headroom, by(foreign) saving("excars2") denom(nonmiss)  
	//by(foreign)分组，以非缺失值数量作为分母，文件存储为"excars2.dta"
  
    btable price mpg rep78 headroom, by(foreign) saving("excars3") conti(rep78) cat(headroom) 
	//指定rep78为连续变量，headroom为类别变量，文件存储为"excars3.dta"
	
	btable price mpg rep78 headroom, by(foreign) saving("excars4") rdci(ac)
	//指定风险差的置信区间类型为 ac（Agresti and Caffo，2000），文件存储为"excars4.dta"
	
	btable price mpg rep78 headroom, by(foreign) saving("excars5") effect(rd meand) test(fisher ttest)
	//指定组间效应类型为 rd（风险差）和 meand（均值差异），检验的类型为 fisher（费雪精确检验）和 ttest（t检验），文件存储为"excars5.dta"
	
	use "excars1", clear
	use "excars2", clear
	
	* btable_format 命令实操演示
	sysuse auto2, clear  //调用系统数据
	
	btable price mpg rep78 headroom, by(foreign) saving("excars") denom(nonmiss)  //生成原始表格，文件存储为"excars.dta"

	btable_format using "excars", clear  //调入原始表格，按论文格式排版
	
	  *部分Options演示
      btable_format using "excars", clear ncol(non-missing) drop(total effect info)
	  //新增两列报告非缺失值信息，不报告总体样本特征、组间效应与注释
	  
	  btable_format using "excars", clear design(missing) drop(total effect info)
	  //新增一行报告变量缺失值特征，不报告总体样本特征、组间效应与注释
	   
      btable_format using "excars", clear format_desc(price "%10.0f" conti "%10.2f" cat "%10.1f") drop(total effect)
	  //设置各变量描述格式，不报告总体样本特征和组间效应
	  
	  btable_format using "excars", clear drop(total) desc(conti median [lq, uq]) effect(conti mws (mws_lci to mws_uci)) abbr
	  //连续变量报告中位数和四分位区间，连续变量组间效应报告Mann-Whitney统计量及其95%置信区间，不报告总体样本特征
	  
	  btable_format using "excars", clear desc(conti mean (sd) | conti median [lq, uq]) parse(|) design(row) drop(total effect)
	  //连续变量分别报告均值（标准差）与中位数（四分位区间），类型标签展示于行头，不报告总体样本特征与组间效应
	  
	  btable_format using "excars", clear block(mpg headroom price | rep78) block_head(Continuous variables | Categorical variables) drop(total info) abbr
      //将变量分为连续变量（Continuous variables）和类别变量（Categorical variables）两类，不报告总体样本特征和注释


