
## 课程报告格式和提交说明

&emsp;


- 点击主页上方的 [**Wiki**](https://gitee.com/arlionn/EF/wikis) 按钮查看详情。

- 或直达：[EF00-选题和课程报告格式要求](https://gitee.com/arlionn/EF/wikis/%E8%AF%BE%E7%A8%8B%E6%8A%A5%E5%91%8A/EF00-%E9%80%89%E9%A2%98%E5%92%8C%E8%AF%BE%E7%A8%8B%E6%8A%A5%E5%91%8A%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md)

<br>


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)