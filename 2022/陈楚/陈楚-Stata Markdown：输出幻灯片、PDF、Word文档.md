> **作者：** 陈楚（中山大学岭南学院）
>
> **E-mail:**  <chench528@mail2.sysu.edu.cn> 

&emsp;

---

**目录**

[TOC]

---

&emsp;

## 1. 引言

`markstat` 是一个将 Stata 的代码和输出与用 Markdown 编写的注释完美结合成一个动态文档的命令，同时可以帮助我们将文档输出成幻灯片、 PDF 或者 Word 的格式，为我们写 Stata 讲义、写 Stata 作业以及制作包含 Stata 代码的幻灯片提供了极大的便利。

在数据分析领域，仔细记录所有遵循的步骤，包括数据处理、数据分析以及表格和图表的生成，对于实现结果的可重复性是至关重要的。实现这一目标最有效的方法是将数据分析代码、各步骤的详细注释和获得的结果的集成在一个单一的文档中，`markstat`可以帮助我们仅仅使用 Stata 就实现这一目标。

总的来看，利用 `markstat` 命令生成一份幻灯片、PDF 或 Word 文档需要以下几个步骤：

- **Step1：下载安装好 Stata 两命令，外部两程序**

  > 两命令：`markstat` 和 `whereis` ，两程序：Pandoc 和 LaTeX

- **Step2：编写和生成 markstat 文件(.stmd)**

  > 以 Do-file 为载体，按照既定的编写规则来编写想要生成的动态文档内容，并保存为 .stmd 文件

- **Step3：借助 `markstat` 语法完成目标格式的输出**

  > markstat using filename [, options ] 是 `markstat` 命令的语法，选择合适的 option 即可完成格式输出

Germán Rodríguez 发表在 *Stata Journal* 上的文章 [*Literate data analysis with Stata and Markdown*](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700304) 详细介绍了这一命令。

&emsp;

## 2. 命令与相关程序的下载与安装

在借助 `markstat` 实现在 Stata 中完成动态文档的编写目标之前，我们需要先安装一些命令和程序。

- `markstat` : 用于将动态文档中 Markdown 和 Stata 代码分别提取到单独的 .md 和 .do 文件中，并将各自的运行结果集成至单一文档中
- `whereis`： 用于指定外部程序和辅助文件的路径目录
- **Pandoc** ：  用于运行 Markdown 代码将其转换成 HTML，PDF 或 DOCX 的程序
- **LaTeX** ：只有需要通过 LaTeX 生成 PDF 文档时，才需要下载该程序。更简单的替代方法是先生成 HTML，然后用 Chrome 等浏览器将其另存为 PDF ；或将文件读取到 Word 中，然后将其另存为PDF

> `markstat` 和 `whereis` 是 Stata 中的两个外部命令，需要用如下命令安装：

```Stata
*-下载/更新 markstat 和 whereis 命令
. ssc install markstat, replace 
. ssc install whereis, replace 
```



> **Pandoc** 和 **LaTeX** 是两个外部程序，可以分别点击 [Pandoc](https://pandoc.org/installing) 和 [LaTeX](https://www.latex-project.org/) 跳转到其对应官网进行下载。
>
> 下载安装好后，在 Stata 中借助 `whereis` 命令指定对应程序的位置

```Stata
*-借助 whereis 命令指定 Pandoc 程序的位置（引号内为你电脑里程序的位置路径）
. whereis pandoc "c:\program files (x86)\pandoc\pandoc.exe"

*-借助 whereis 命令指定 pdflatex 程序的位置（引号内为你电脑里程序的位置路径）
. whereis pdflatex "c:\program files (x86)\pandoc\pdflatex.exe"
* Note: pdflatex 是 LaTeX 的 PDF 转换器，下载安装好 LaTeX 后找到其中的 pdflatex 程序路径即可
```

&emsp;

## 3. markstat 文件(.stmd) 的生成和编写

在 Stata 中，`markstat` 命令仅适用于 markstat 文件(.stmd)，因此我们需要先将包含 Stata 代码和 Markdown 注释的动态文档转成 .stmd 格式，再借助 `markatat` 命令将该文档输出成我们想要的形式。通常的做法是我们现在 Do-file 中编写动态文档，然后将其另存为 markstat 文件(.stmd)。

###  3.1 新建 Do-file 并编写动态文档

在 Stata 中新建 Do-file 后即可编写文档，其中 Stata 代码需要缩进1个 tab 或4个空格以作区分。如下例所示，正常编写的是 Markdown 代码，进行了缩进的是 Stata 代码。 

```STATA
Let us read the fuel efficiency data that ships with Stata
                                                                
    sysuse auto, clear                                          
                                                                
To study how fuel efficiency depends on weight it is useful to  
transform the dependent variable from "miles per gallon" to
"gallons per 100 miles"     

    gen gphm = 100/mpg
	
We then obtain a fairly linear relationship
                                                                
    twoway scatter gphm weight || lfit gphm weight,  /// 
        ytitle(Gallons per 100 Miles) legend(off)
    graph export auto.png, width(500) replace

![Fuel Efficiency by Weight](auto.png)

The regression equation estimated by OLS is

    regress gphm weight

Thus, a car that weighs 1,000 pounds more than another requires 
on average an extra 1.4 gallons to travel 100 miles. 

That's all for now!
```



### 3.2 动态文档的编写方式及技巧

#### 3.2.1 Markdown注释

markstat 文件支持绝大多数 Markdown 语法，我们可以直接在用 Do-file 编写动态文档时使用 Markdown 语法以实现想要的注释效果。

> **标题**
>
> - 只要一段文字前加 # 号即可定义标题
>
> - \# 号的个数代表标题的级数，共有六级标题
>
> - 书写时，建议在 # 号后加一个空格，这是最标准的 Markdown 语法  
>
> ```stata
> # 一级标题
> ## 二级标题
> ### 三级标题
> ```



> **文字效果和分隔线**
>
> ```stata
> *斜体*
> **粗体**
> ~~删除线~~
> **** (分隔线1)
> ---- (分隔线2)
> ```



> **列表**
>
> - **无序列表**使用星号 (*)、加号(+)或是减号(-) 作为列表标记，其后需要加空格
>
> ```stata
> * 红色     
> + 绿色
> - 蓝色
> ```
>
> - **有序列表**则使用数字接着一个英文句点
>
> ```stata
> 1. 打开冰箱
> 2. 塞入大象
> 3. 关上冰箱
> ```



> **文本链接**
>
> - **行内式**： \[文本](链接地址)
>   也可以采用这种格式：\[文本](链接地址 "可选说明文字")。 好处是当鼠标停留在文本上时，可以显示可选文字说明的内容
>
> ```stata
> 学习 [Stata](http:\\http://www.stata.com) 过程中遇到问题，
> 可以查看 [老连](http://www.lingnan.sysu.edu.cn/lnshizi/faculty_vch.asp?name=lianyj
> "连玉君主页")
> 的 [知乎专栏](https://zhuanlan.zhihu.com/arlion "连玉君 Stata 知乎专栏")。
> ```
>
> - **参考式**：\[文本][标签] + 空行 + [标签]：链接地址"可选文字说明"
>   - 第一定义与第二种定义之间要有一个空行
>   - 参考式比较适合定义那些将在文中多次出现的文本链接
>   - 使用参考式的最大好处是代码的可读性比较高
>
> ```stata
> 学习 [Stata][1] 时遇到问题，
> 可以查看 [老连][2]
> 的 [知乎专栏][Lzh]。
> [老连][2] 用 [Stata][1] 很多年了，人很好。
> 
> [1]: http:\\http://www.stata.com
> [2]: http://www.lingnan.sysu.edu.cn/lnshizi/faculty_vch.asp?name=lianyj "连玉君主页"
> [Lzh]: https://zhuanlan.zhihu.com/arlion "连玉君 Stata 知乎专栏"
> ```



> **图片链接**
>
> - 插入图片的方法与文字链接非常相似，也分为行内式和参考式两种类型，这里仅介绍行内式
>
> ```stata
> *- 语法格式：
> ![image](图片链接地址) 或
> ![image](图片链接地址 "说明文字")
> ```



> **表格**
>
> - ----: 为右对齐
> - :---- 为左对齐
> - :---: 为居中对齐
> - ----- 为使用默认居中对齐
> - 输入代码时，无需对齐，下述呈现方式只是为了美观
>
> ```stata
> | Tables        | Are           | Cool  |
> | ------------- |:-------------:| -----:|
> | col 3 is      | right-aligned | $1600 |
> | col 2 is      | centered      |   $12 |
> | zebra stripes | are neat      |    $1 |
> ```



**Note：**关于 Markdown 更多的使用方式可以点击 [Stata连享会：Markdown笔记](https://zhuanlan.zhihu.com/p/29707051) 参考学习~



#### 3.2.2 Mathematical Equations  数学方程

正如 Markdown 语言一样，markstat 文件可以借助 Pandoc 程序将美元符号 $ 之间的任何文本解释为 LaTeX 公式，借助这种方式我们在 Do-file 中也可以轻易写出复杂的数学方程。

> - **行内公式**：\$行内公式$
> - **行间公式**：\$\$行间公式$$
> - 在公式行尾添加 `\quad (#)` 可以为公式手动添加编号，其中`#` 为公式编号
>
> ```stata
> $$
> y_i = \beta_0 + x_i\beta_1 + \epsilon_i    \quad(1)
> $$
> 其中，$\epsilon_i$ 表示干扰项，$i=1, 2, \cdots, N$。
> ```
>
> **输出效果：**
>
> > $$
> > y_i = \beta_0 + x_i\beta_1 + \epsilon_i    \quad(1)
> > $$
> > 其中，$\epsilon_i$ 表示干扰项，$ i=1, 2, \cdots, N $。



#### 3.2.3 Metadata 元数据

Pandoc 允许将文档的标题、作者和日期作为元数据，只需在文档的前三行写明对应的信息并以 % 开头。

> - % 文档标题
> - % 文档作者信息
> - % 文档完成日期
>
> ```stata
> % Dynamic Documents with Stata and Markdown
> % Germán Rodríguez, Princeton University
> % 4 November 2017
> ```
>
> - 我们也可以使用YAML格式，以三个短划线 - 来开始和结束整个区块，可以达到同样的效果
>
> ```stata
> ---
> title: Dynamic Documents with Stata and Markdown
> author: Germán Rodríguez, Princeton University
> date: 4 November 2017
> ---
> ```
>
> **输出效果：**
>
> > ![陈楚_markstat_Fig01](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈楚_markstat_Fig01.png)

**Note：** 如果想使用文档运行时的日期，可以键入\`s c(current_date)\` 而不是确切的日期。



#### 3.2.4 Fenced Code Blocks 代码块

通过 “一个制表符或四个空格” 的简单规则来区分Stata和Markdown代码非常便捷且效果不错，但这使得一些高级的 Markdown 用法无法政策使用，如嵌套列表。因此 `markstat` 的 strict 选项使用代码块来区分 Stata 代码。

> - 以 \```s 开头，\``` 结尾，在这中间编写 Stata 代码
> - 以 \```{s} 开头，\``` 结尾，与上一种方式等价
>
> ````stata
> ```s
> 	sysuse auto, clear 
> 	reg price weight length
> ```
> ````
>
> **输出效果：**
>
> > ![陈楚_markstat_Fig02](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈楚_markstat_Fig02.png)
>
> 
>
> - 以 \```s/ 开头，\``` 结尾，只显示输出结果，不现实 Stata 的代码命令
>
> ````stata
> ```s/
> 	sysuse auto, clear 
> 	reg price weight length
> ```
> ````
>
> **输出效果：**
>
> ![陈楚_markstat_Fig03](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈楚_markstat_Fig03.png)



#### 3.2.4 Inline Code 内联代码

我们可以通过使用内联代码作为叙述的一部分来引用结果，`markstat`  命令将借助 Stata 的 `display` 命令将内联代码显示化，并将输出与文本内联拼接。

> - 内联代码语法：\`s  [fmt] expression\`   ，其中 [fmt] 是一个可选格式，后面跟着一个表达式
>
> ```stata
> 	qui sysuse auto, clear
> 	qui reg price weight length
> 回归得到的R方 `s %5.2f e(r2)` 是模型的可决系数
> ```
>
> **输出效果：**
>
> ![陈楚_markstat_Fig04](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈楚_markstat_Fig04.png)



#### 3.2.5 Figures 图像

默认情况下，图像在 HTML 中使用其实际大小呈现，在 PDF 中使用行宽的 75% 呈现，但我们可以为其添加宽度属性以改变其大小。

> - 使用 in 设定英寸宽度
> - 使用 cm 设定厘米宽度
> - 使用 % 设定相对尺寸
>
> ```stata
> *- 语法格式
> ![caption](filename.png){width=100%}
> ```



#### 3.2.6 Citations 引用

`markstat` 命令还可以帮助我们处理参考书目的引用问题，我们首先要为引用的文献创建一个 BibTeX 数据库，并在文档 YAML 元数据中包含对该文件的引用。

> **Step 1: ** 创建 BibTeX 数据库
>
> 找到引文的 BibTeX 引用格式，将其复制到一个空白的文本文档中，而后将文档的 .txt 扩展名更改为 .bib，使其变成 BibTeX 源文件。*若无法更改文件扩展名可以参考下文 2.3 的解决方式。*
>
> ![陈楚_markstat_Fig05](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈楚_markstat_Fig5.png)
>
> **Note：** Google 学术中的文献直接可以选择 BibTeX 的引用格式，而知网等网站里的中文文献没有该引用格式。中文文献的 BibTeX 引用可以参考 [知网 BibTeX 自动生成]([(13条消息) 知网 BibTeX自动生成（使用BibTeX引用中文参考文献）_奕小星的博客-CSDN博客_知网bib参考文献](https://blog.csdn.net/fegxig1230/article/details/118458288)) 来完成。
>
> 
>
> **Step 2: ** 在 YAML 的 metadata 中包含引用的文献信息
>
> 在本文 2.2.3 讲 Metadata 元数据的时候我们提到了 YAML 格式，此时只需要在之前设置的基础上加上 bibliography 的源文件名称即可，本文中 BibTeX 源文件名称为 citation.bib 。
>
> ```stata
> ---
> title: Dynamic Documents with Stata and Markdown
> author: Germán Rodríguez, Princeton University
> date: 4 November 2017
> bibliography: citation.bib
> ---
> ```
>
> 在后文的写作中我们可以用 @符号 + 引文的键名进行引用
>
> ```stata
> Germán Rodríguez [-@rodriguez2017literate] 发表在 Stata Journal 上的文章详细介绍了这一命令。
> 
> ## References    // 在文末给引用部分加上标题
> ```
>
> **Note：** 不同的引用方式可以参考 [Bibliographic Citations](https://grodri.github.io/markstat/citations) 
>
> 
>
> **Step 3: ** 在后面执行 `markstat` 命令时加上 bibliography 选项。



#### 3.2.7 幻灯片编写方式

`markstat` 命令可以借助S5引擎将幻灯片输出成 HTML 文件，也可以借助 Beamer 将幻灯片输出成 PDF 格式。

> - **幻灯片分页**
>
>   通常，我们借助 `---` 来手动分割幻灯片页面，并将一级标题默认为幻灯片标题。而在结构简单的幻灯片中，`markstat` 可以自动识别各一级标题来分割幻灯片。如果使用 Beamer ，要在包含 Stata 命令和输出的幻灯片页的标题中添加`{.fragile}` 
>
> ```stata 
> * 借助 --- 进行页面分割
> # 1st page  // 这是第1页
> The content of 1st page
> ---
> 
> ## 2nd page### The content of 2nd page // 这是第2页
> Hello, world!
> ---
> 
> # 3rd page // 这是第3页
> Thanks
> 
> * 根据幻灯片一级标题进行页面分割
> # 1st page  // 这是第1页 
> The content of 1st page
> 
> ## lst page### The content of 2nd page
> Hello, world!
> 
> # 2rd page  // 这是第2页
> Thanks
> ```
>
> - **分列式幻灯片**
>
>   为了使幻灯片页面结构更加清晰，我们可以采用分列布局，例如简洁明了的左右结构，左边展示 Stata 代码，右边展示 Stata 代码的输出结果。
>
>   > - 以 `:::::: {.columns}` 表明分列的开始
>   > - 以 `::: {.column}` 开始， `:::` 结束，中间为各列想要展示的内容
>   > - 以 `::::::` 为结尾表明分列结束
>
> ````stata
> % Two-Column Slides
> % Germán Rodríguez
> % November 10, 2017
> 
> # A side-by-side structure {.fragile}
> 
> Here's the code and graph side-by-side:
> 
> :::::: {.columns}
> ::: {.column}  // 第一次分列内容开始
> 
> ```s
>     sysuse auto, clear
> 	gen gphm = 100/mpg
> 	qui twoway scatter gphm weight ///
> 	|| lfit gphm weight,  /// 
> 	ytitle(Gallons per 100 Miles) ///
> 	legend(off)
> 	graph export A.png,replace
> ```
> :::  // 第一次分列内容结束
> 
> ::: {.column}  // 第二次分列内容开始
> ![](A.png){width="80%"}
> :::  // 第二次分列内容开始
> 
> ::::::
> 
> Above is a clear example. This is a slide structure worth learning.
> ````
>
> **输出效果：**
>
> ![陈楚_markstat_Fig06](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/202211250043562.png)





### 3.3 markstat 文件 (.stmd) 的保存

我们将写好的 Do-file 保存为 auto.do 文件，其中 auto 是自定义的文件名称，.do 是 Do-file 文件的扩展名。然后通过重命名文件来更改文件扩展名为 .stmd，使得最终文件名变为 auto.stmd 即可，此时该文件成功变成 markstat 文件。

> **Note:** 有些电脑为文件隐藏了扩展名，在这种情况下即使将文件重命名为 auto.stmd ，在后续运行`markstat` 命令时 Stata 仍会报错：“<font color=#FF0000> *file auto.stmd not found* </font> ”。解决这个问题的方法是在电脑中打开 <u>文件资源管理器选项</u> ，点击  <u>查看</u> 窗口，取消勾选 <u>隐藏已知文件类型的扩展名</u> 。此时我们会发现原本命名为 auto.stmd 的文件的完整名称为 auto.stmd.do，我们通过重命名删掉 .do 后缀即可。



## 4. `markstat` 命令使用

`markstat` 命令唯一需要的参数是 markstat 文件 (.stmd) 的文件名，前面我们已经完成了 markstat 文件的编写，接下来我们将详细介绍 `markstat` 命令的使用方法。

- `markstat` **命令基本语法**

```stata
markstat using filename [, pdf docx slides beamer mathjax bundle bibliography strict nodo nor keep ]
```

> **Note:** 在使用 `markstat` 命令前需要确保 Stata 找得到 markstat 文件，可以借助 `cd` 命令调整 Stata 的当前路径与文件路径一致，也可以用包含文件路径的形式来输入文件名。



- `markstat` **命令选项介绍**

> **Note:** 不添加任何选项时，`markstat` 命令默认生成 HTML 文件

**pdf：** 用于生成 PDF 文件，使用该选项需要下载 pdflatex 程序，并用 `whereis` 命令指定程序位置，然后需要输入命令 `copy http://www.stata-journal.com/production/sjlatex/stata.sty stata.sty` 命令来下载stata.sty。替代方法是先将文档输出为 HTML 文件，然后用 Chrome 等浏览器将其另存为 PDF 文件 ；或先输出为 Word 文档，然后将其另存为 PDF 文件

**docx：** 用于生成 Word 文档

**slides：** 用于利用 S5 生成 HTML 形式的幻灯片，可以在括号中添加幻灯片主题，例如 `slides(santiago)` 使用 santiago 主题

**beamer：** 通过 LaTeX 使用 Beamer 生成 PDF 形式的幻灯片，可以选择任意 Beamer 主题，例如 `beamer(marid)`。可以点击 [here]([Beamer theme gallery (uab.es)](https://deic.uab.es/~iblanes/beamer_gallery/)) 查看不同的主题样式

**mathjax：** 用于激活 MathJax JavaScript 库，它能很好地在网页上呈现数学方程式

**bundle：**  使用 base64 对所有图像和辅助文件进行编码，来生成含带的 HTML 文档，

**bibliography：**  用于使用 BibTeX 数据库解析引文，并在文档末尾生成参考文献列表

**strict：** 决定如何分离文档中的 Markdown 和 Stata 代码。 “一个 tab 或四个空格”的区分规则很简单，但它限制了一些 Markdown 的高级用法。 strict 使得文档严格根据代码块来区分 Markdown 和 Stata 代码。

**nodo：**  用于在调整文档时跳过运行 Stata 代码。可以使我们将输出的幻灯片从 S5 更改为 Beamer，或尝试不同的主题时，无需重新运行 Stata 代码，而是直接输出最近的 Stata 运行结果

**nor：** 对于与 `nodo` 命令类似，将使文档在输出时跳过 R 代码，而直接使用最新的输出结果

**keep：** 将保存默认情况下删除的中间文件。 默认情况下，只保留 smcl 和 rout 文件以确保 `nodo` 和 `nor` 命令的正常运行，当然还有输出文件。 使用 `keep` 命令则保留所有内容，使用 `keep(list)` 可保留选定的文件，例如：`keep(do md)` 保留生成的 Stata do 文件和 Markdown 文件。



## 5. 实例演示

将本文3.1小节中的 Stata 代码另存为 example.stmd 文件，然后借助 `markstat` 命令的简单语法即可完成输出。

```stata
whereis pandoc "c:\program files (x86)\pandoc\pandoc.exe"   // 指定 Pandoc 程序的位置
whereis pdflatex "c:\program files (x86)\pandoc\pdflatex.exe"   // 指定 pdflatex 程序的位置

cd "D:\study\sysu\EF\markstat"   // 调整 Stata 的当前路径与.stmd文件路径一致

markstat using example   // 生成HTML
markstat using example, docx   // 生成Word
markstat using example, pdf   // 生成PDF
markstat using example, beamer  // 生成PDF形式的幻灯片（记得加上“---”来分割页面）
```

- **输出效果（以PDF为例）**

![输出效果](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/202212120945716.png)



## 6. 参考资料

- 命令主页：[markstat ](https://grodri.github.io/markstat/)

- Germán Rodríguez. Literate Data Analysis with Stata and Markdown[J]. The Stata Journal,2017,17(3).  [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700304)



## 7. 相关推文

- 专题：[Stata命令](https://www.lianxh.cn/blogs/43.html)

- 专题：[Markdown-LaTeX](https://www.lianxh.cn/blogs/30.html)
  - [用Markdown制作幻灯片-五分钟学会Marp（上篇）-M110a](https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fnews%2F97fccdca2d7a5.html)
  - [用Markdown制作幻灯片-五分钟学会Marp（下篇）-M110b](https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fnews%2F521900220dd33.html)
  - [连享会工具：Beamer幻灯片制作](https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fnews%2F4700422eee871.html)
  - [Latex系列2：Beamer入门-学术范儿的幻灯片](https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fnews%2F53b16c1690792.html)
  - [Latex系列2：Beamer入门-学术范儿的幻灯片](https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fnews%2F7f123b27e3e01.html)
  - [学术范儿的幻灯片制作：Beamer in Rmarkdown](https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fnews%2F12bddc02e2703.html)
