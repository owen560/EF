# Double/Debiased Machine Learning  implementation for Stata
&emsp;
> **作者：** 李金桐(中山大学)        
> **E-mail:**  <lijt65@mail2.sysu.edu.cn>
> &emsp;

---

**目录**
[[TOC]]

---

&emsp;
## 1. 理论基础
### 1.1 Why：为什么我们需要DDML
实证研究往往会面临一个质疑：**模型设定是正确的吗**？例如，如果希望研究在班级中加入助教对教学质量的影响，常见的方法是构造回归方程：
$$
Score=\beta_0 + \beta_1 Assistant + \sum_j \beta_j \times Control + \varepsilon
$$
其中，$Score$代表成绩；$Assistant$是标志是否加入助教的二元变量；$Control$为控制变量，可能包括：每天学习时间、作业完成率、出勤率等。那么这些特征的关系真的是线性的吗？显然不是。例如随着学习时间增加，成绩自然会提高，然而学习时间过长很可能导致疲惫、睡眠不足等，进而造成学习效率下降，反而使得成绩下降。需要注意的是，我们实际上并不关心学习时间对成绩的影响，我们只希望研究$\beta_1$，我们只是需要处理控制变量对$\beta_1$造成的影响。

接下来用更严谨的方法描述上述问题。考虑因果模型：
$$
\begin{array}{cl}
Y=D \theta_0+g_0(X)+U, & \mathrm{E}[U \mid X, D]=0 \\
D=m_0(X)+V, & \mathrm{E}[V \mid X]=0
\nonumber
\end{array}
$$
其中$Y$是模型的Outcome，$D$是因果模型的treatment。这里，我们关注$\theta_0$，即treatment的因果效应。
一种常见的思路是，通过假设（例如常见的线性假设），或者利用一定方法（通常是机器学习）估计，得到$\hat{g}_0$，随后就可以利用线性回归得到$\hat{\theta}_0$：
$$
\hat{\theta}_0=\frac{\operatorname{cov}\left(D, Y-\hat{g}_0(X)\right)}{\operatorname{var}(D)}=\frac{\frac{1}{n} \sum_{i \in I} D_i\left(Y_i-\hat{g}_0\left(X_i\right)\right)}{\frac{1}{n} \sum_{i \in I} D_i^2}
\tag{1}
$$

接下来，很自然的想要研究这个估计量是否无偏。遗憾的是$\hat{\theta}_0$往往是有偏的：
$$
\begin{aligned}
\sqrt{n}\left(\hat{\theta}_0-\theta_0\right) &=\sqrt{n} \frac{\frac{1}{n} \sum_{i \in I} D_i\left(Y_i-\hat{g}_0\left(X_i\right)\right)}{\frac{1}{n} \sum_{i \in I} D_i^2} \\
&-\left(\sqrt{n} \frac{\frac{1}{n} \sum_{i \in I} D_i\left(Y_i-g_0\left(X_i\right)\right)}{\frac{1}{n} \sum_{i \in I} D_i^2}-\sqrt{n} \frac{\frac{1}{n} \sum_{i \in I} D_i U_i}{\frac{1}{n} \sum_{i \in I} D_i^2}\right) \\
&=\underbrace{\left(\frac{1}{n} \sum_{i \in I} D_i^2\right)^{-1} \frac{1}{\sqrt{n}} \sum_{i \in I} D_i U_i}_{:=a}+\underbrace{\left(\frac{1}{n} \sum_{i \in I} D_i^2\right)^{-1} \frac{1}{\sqrt{n}} \sum_{i \in I} D_i\left(g_0\left(X_i\right)-\hat{g}_0\left(X_i\right)\right)}_{:=b}
\end{aligned}
$$

可以看出误差分为两项。$a$项来自于$U$和$D$的独立性，即$\frac{\operatorname{cov}(D, U)}{\operatorname{var}(D)}$，若二者不独立则会造成偏误。

然而问题来源于$b$项，我们将其展开为以下形式：
$$
b=\left(E\left[D_i^2\right]\right)^{-1} \frac{1}{\sqrt{n}} \sum_{i \in I} m_0\left(X_i\right)\left(g_0\left(X_i\right)-\hat{g}_0\left(X_i\right)\right)+o_P(1)
$$
注意到$m_0\left(X_i\right)\left(g_0\left(X_i\right)-\hat{g}_0\left(X_i\right)\right)$项。
首先，$g_0$的估计往往存在误差，例如对于高维数据，往往会采用正则项处理，造成正则化误差，此时$b$项发散；
此外，$m_0\left(X_i\right)$是数据本身的性质，因此数据会决定偏误的大小而无法改变，导致估计非常不稳健。

综合以上推论，可以说因果模型treatment effect的传统估计方法并不完美。因此，我们引入**Double/Debiased Machine Learning** (DDML)的概念，**为因果估计提供更为稳健的方法**。

&emsp;

### 1.2 What：DDML的本质
根据上述推论，传统因果估计方法不稳健的核心在于$m_0\left(X_i\right)\left(g_0\left(X_i\right)-\hat{g}_0\left(X_i\right)\right)$项，其中$g_0\left(X_i\right)-\hat{g}_0\left(X_i\right)$部分是$g_0$的估计误差，显然是难以避免的。因此更为实际的考虑是消除$m_0\left(X_i\right)$。其出现原因在于用于回归的$D$实际上包含了$X$的信息
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2022_李金桐final_fig01.png)

如图，注意到$V$实际上可以看作工具变量，此时可以构造估计：
$$
\theta_0=\frac{\operatorname{cov}\left(V, Y-g_0(X)\right)}{\operatorname{cov}(V, D)}=\frac{\operatorname{cov}\left(D-m_0(X), Y-g_0(X)\right)}{\operatorname{cov}\left(D-m_0(X), D\right)}=\frac{\operatorname{cov}\left(D-m_0(X), D \theta_0+U\right)}{\operatorname{cov}\left(D-m_0(X), D\right)}
$$

为了求$V$，可以采用：
$$
\hat{V}=D-\hat{m}_0(X)
$$
其中$\hat{m}_0(X)$可以通过$X$对$D$回归得到，因此我们得到一种新的估计：
$$
\breve{\theta}_0=\frac{\frac{1}{n} \sum_{i \in I} \hat{V}_i\left(Y_i-\hat{g}_0\left(X_i\right)\right)}{\frac{1}{n} \sum_{i \in I} \widehat{V_i} D_i}
\tag{2}
$$

在这个估计下，新的$b$项变为：
$$
b^*=\left(E\left[D_i^2\right]\right)^{-1} \frac{1}{\sqrt{n}} \sum_{i \in I}\left(\hat{m}_0\left(X_i\right)-m_0\left(X_i\right)\right)\left(g_0\left(X_i\right)-\hat{g}_0\left(X_i\right)\right)
$$

此时偏误仅仅取决于回归误差，因此这个估计**更为稳健**。直观角度上，线性回归是拟合$Y$在特征空间$X$的最佳投影(既误差最小化)，所以残差垂直于样本空间$X$，最大限度消除了$X$的相关性。

此时我们已经在因果模型中构造了稳健的估计量，
接下来，我们希望从中总结出一套普适性更强的方法论，以用于更多场景。

我们对上述过程进行总结：

* 存在目标函数$\theta_0$和其他不太关心的回归函数$g_0$和$m_0$；
* $\theta_0$的传统估计方法中，如果拟合的函数$\hat{g}_0$存在误差，则误差项不收敛；
* 需要构造估计量使得$g_0$和$m_0$出现误差时，偏误依然足够小。


这里我们用***Gateaux Dervative***描述$g_0$估计误差对偏误的影响：
$$
\partial_f E\left[\varphi\left(W ; \theta_0, f_0\right)\right]=\lim _{r \rightarrow 0^{+}} \frac{E\left[\varphi\left(W ; \theta_0, f_0+r\left(f-f_0\right)\right)\right]-E\left[\varphi\left(W ; \theta_0, f_0\right)\right]}{r}
$$

我们将(1)式的传统估计方法重新写成：
$$
\frac{1}{n} \sum_{i \in I} \varphi\left(W ; \hat{\theta}_0, \hat{g}_0\right)=0
$$
其问题在于Gateaux Dervative不为零：
$$
\partial_g E\left[\varphi\left(W ; \theta_0, g_0\right)\right]\left[g-g_0\right] \neq 0
$$
换言之，$g_0$的微小扰动会导致$\phi$发生较大的变化，因此估计不准时存在较大误差。

我们将(2)式的估计方法重新写成：
$$
\frac{1}{n} \sum_{i \in I} \psi\left(W ; \breve{\theta}_0, \hat{\eta}_0\right)=0
$$
其中$\hat{\eta}_0=\left(\hat{m}_0, \hat{g}_0\right)$称为*Nuisance Parameter*。
可以证明这个估计量的Gateaux Dervative为零：
$$
\partial_\eta E\left[\varphi\left(W ; \theta_0, \eta_0\right)\right]\left[\eta-\eta_0\right]=0
$$


直观来说，Gateaux Dervative事实上刻画了$\phi$对$g_0$变化的敏感程度。
如果Gateaux Dervative等于零，意味着这个估计量更为稳健。
我们称这个性质为***Neyman Orthogonality***。

这种构造就是Double/Debiased Machine Learning的思想基础。

此外，估计无偏重要的一步是Cross-fitting，用来降低overfitting带来的估计偏差。将总样本分成样本1，样本2两份。先用样本1估计残差，样本2估计$\theta_1$，再用样本2估计残差，样本1估计$\theta_2$，取平均得到最终的估计$\theta=\left(\theta_1+\theta_2\right) / 2$。当然也可以进一步使用K-Fold来增加估计的稳健性。

&emsp;

### 1.3 How：如何构造DDML
接下来，我们希望找到通用的方法构造$\psi$满足Neyman Orthogonality
不妨设$\max _{\theta, \beta} E_p[l(W ; \theta, \beta)]$，我们的目标可以通过最大化某个函数得到，通常为对数似然，故在极值处导数为0：
$$
E_p\left[\partial_\theta l\left(W ; \theta_0, \beta_0\right)\right]=0, E_p\left[\partial_\beta l\left(W ; \theta_0, \beta_0\right)\right]=0
$$
因此，一个自然的想法是令$\phi(W ; \theta, \beta)=\partial_\theta l(W ; \theta, \beta)$。然而，为了满足Neyman Orthogonality，我们还需要自己构造估计量：
$$
\psi(W ; \theta, \beta)=\partial_\theta l(W ; \theta, \beta)-\mu \partial_\beta l(W ; \theta, \beta)
$$
此时只需要找到合适的$\mu$使$\psi$满足Neyman Orthogonality：
$$
\partial_\beta \psi(W ; \theta, \beta)=\partial_{\theta \beta} l(W ; \theta, \beta)-\mu \partial_{\beta \beta} l(W ; \theta, \beta)=0
$$
可以得到解析解：$\mu=J_{\theta \beta} J_{\beta \beta}^{-1}$

&emsp;

## 2. Stata实现
### 2.1 Stata包基本介绍
**ddml**是用于Double Debiased Machine Learning的Stata包，支持五种不同的模型，允许二元或连续treatment变量，支持内生性、高维控制，并支持工具变量，且支持多种不同的机器学习程序。可通过下面的语句在Stata中进行安装。
```stata
net install ddml, from(https://raw.githubusercontent.com/aahrens1/ddml/master)
```
利用**ddml**包进行Double Debiased Machine Learning分为四步进行
1. 初始化并选择模型：
```stata
ddml init model [if] [in] [ , mname(name) kfolds(integer) fcluster(varname) foldvar(varlist) reps(integer) norandom tabfold vars(varlist) ]
```
2. 添加监督机器学习以估计条件期望：
```stata
ddml eq [ , mname(name) vname(varname) learner(varname) vtype(string) predopt(string) ] :  command depvar vars [ , cmdopt ]
```
3. Cross-fitting
```stata
ddml crossfit [ , mname(name) shortstack ]
```
4. 估计因果效应：
```stata
ddml estimate [ , mname(name) robust cluster(varname) vce(type) att trim(real) ]
```

&emsp;

### 2.2 模型设定
>Partial linear model [*partial*]
$$
\begin{array}{cc}
Y=D \theta_{0}+g_{0}(X)+\zeta, & \mathbb{E}(\zeta \mid D, X)=0 \\
D=m_{0}(X)+V, & \mathbb{E}(V \mid X)=0
\end{array}
$$
其中目标是在控制$X$的同时估计$\theta_0$。为此，我们使用有监督的机器学习估计条件期望$E[Y|X]$和E$[D|X]$。

>Partial linear IV model [*iv*]
$$
\begin{array}{cc}
Y-D \theta_{0}=g_{0}(X)+\zeta, & \mathbb{E}(\zeta \mid Z, X)=0 \\
Z=m_{0}(X)+V, & \mathbb{E}(V \mid X)=0
\end{array}
$$

在这个模型中，Y和D之间存在结构或因果关系，其关系由$\theta$来表示，$g_{0}(X)+\zeta$可以看作是随机误差，其中$g_{0}(X)$可以被协变量$X$所解释，由于$Y$和$D$是共同确定的，所以我们引入一个外部因素，称为工具$Z$，以解释$D$的外生变化。注意，Z应该影响$D$。这里的X再次作为confounding factors，因此我们可以认为$Z$中的变化是外生的，仅以$X$为条件。

一个来自生物统计学的例子：$Y$为健康指标，$D$为吸烟的指标，则$\theta_{0}$代表着抽烟对健康的影响，健康指标$Y$和吸烟行为$D$同时被确定。$X$代表患者特征，$Z$是医生建议不要吸烟(或其他行为治疗)，这一行为由$X$决定，$Z$只能通过改变行为$D$去影响结果$Y$。

>Interactive model [*interactive*]
$$
\begin{array}{c}
Y=g_{0}(D, X)+U, \quad \mathbb{E}(U \mid X, D)=0 \\
D=m_{0}(X)+V, \quad \mathbb{E}(V \mid X)=0
\end{array}
$$

在该模型设定中，treatment effect完全异质且$D$为二元变量。由于$D$与$X$是不可线性分离的，所以在$D$为二元变量的情况下，该模型比部分线性模型的应用广泛性更好。
我们在模型中感兴趣的目标参数是平均treatment effect(ATE)：
$$
\theta_{0}=\mathbb{E}\left[g_{0}(1, X)-g_{0}(0, X)\right] 
$$
另一个常见的目标参数是接受treat者的平均treatment effect(ATTE)
$$
\theta_{0}=\mathbb{E}\left[g_{0}(1, X)-g_{0}(0, X) \mid D=1\right]
$$

>Interactive IV model [*interactiveiv*]
$$
\begin{array}{cl}
Y=\ell_{0}(D, X)+\zeta, & \mathbb{E}(\zeta \mid Z, X)=0 \\
Z=m_{0}(X)+V, & \mathbb{E}(V \mid X)=0
\end{array}
$$
在该模型设定中，我们考虑$D$是二元变量，$Z \in\{0,1\}$, 利用$D$和$Z$对局部平均treatment effect(LATE)进行估计。
其中$g_{0}$将 $(Z, X)$ 映射到 $\mathbb{R}$ 上，$r_{0}$和$m_{0}$分别将$(Z, X)$ 和$X$映射到$(\epsilon, 1-\epsilon)$,$\epsilon \in(0,1 / 2)$。
$$
\begin{array}{cl}
Y=g_{0}(Z, X)+\nu, & \mathbb{E}(\nu \mid Z, X)=0 \\
D=r_{0}(Z, X)+U, & \mathbb{E}(U \mid Z, X)=0 \\
Z=m_{0}(X)+V, & \mathbb{E}(V \mid X)=0
\end{array}
$$
其中我们感兴趣的估计为:
$$
\theta_{0}=\frac{\mathbb{E}\left[g_{0}(1, X)\right]-\mathbb{E}\left[g_{0}(0, X)\right]}{\mathbb{E}\left[r_{0}(1, X)\right]-\mathbb{E}\left[r_{0}(0, X)\right]}
$$
$\theta_{0}$是平均处理效果

>High-dimensional IV model [*ivhd*]
$$
\begin{aligned}
& Y=D\theta_0+g_0(X)+U, \quad \mathbb{E}(U \mid X, D)=0  \\
& D=m_0(Z)+g_0(X)+V, \quad \mathbb{E}(V \mid X)=0
\end{aligned}
$$

&emsp;

## 3. 应用实例
### 3.1 catteneo2实例
我们以Stata的示例数据集**cattaneo2**为例。该数据是Almond, D., Chay, K. Y., & Lee, D. S. (2005)和Cattaneo, M. D. (2010)在期刊文章中分析的数据子集。数据集包括1989年至1991年间宾夕法尼亚州单胎婴儿及其父母的信息，如婴儿出生体重、父母年龄、父母教育程度、怀孕期间吸烟频率、怀孕期间是否有饮酒等。原始数据集包括近50万名新生儿。Stata示例数据集包括4642例新生儿数据。可通过 `webuse cattaneo2`调用该数据集。

> 研究问题：孕妇吸烟对婴儿出生体重有什么影响？

我们设定模型的outcome（即模型中的$Y$）为变量*bweight*，是以克为单位的婴儿出生体重，我们关注的treatment（即模型中的$D$）为变量*mbsmoke*，是表示孕妇是否吸烟的二元变量，其余变量（即模型中的$X$）包括母亲年龄、是否在妊娠一个月内进行首次产检、母亲是否已婚、婴儿是否是首胎、母亲的教育程度。首先采用Partial linear model进行估计：
```stata
global Y bweight
global D mbsmoke
global X mage prenatal1 mmarried fbaby mage medu

gen fid=_n>2321

ddml init partial, foldvar(fid)
ddml E[Y|X], gen(regy): reg $Y $X
ddml E[D|X], gen(regd): reg $D $X
ddml crossfit

ddml estimate, vce(hc2)
```
结果如下表所示
```
--------------------------------------------------------------------------
         |             Robust HC2
 bweight | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
 mbsmoke |   -222.324     22.560    -9.85   0.000     -266.540    -178.108
--------------------------------------------------------------------------
```

接下来采用Interactive model进行估计，这里我们对ATE和ATTE的结果均进行输出：
```stata
global Y bweight
global D mbsmoke
global X mage prenatal1 mmarried fbaby mage medu

gen fid=_n>2321

ddml init interactive, foldvar(fid)
ddml E[Y|X,D], gen(regy): reg $Y $X
ddml E[D|X], gen(regd): logit $D $X
ddml crossfit

ddml estimate, trim(0)  
ddml estimate, atet trim(0)
```
结果如下表所示
```
--------------------------------------------------------------------------
         |               Robust
 bweight | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
 mbsmoke |   -231.810     23.482    -9.87   0.000     -277.834    -185.786
--------------------------------------------------------------------------


--------------------------------------------------------------------------
         |               Robust
 bweight | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
 mbsmoke |   -222.652     23.537    -9.46   0.000     -268.783    -176.521
--------------------------------------------------------------------------
```

&emsp;

### 3.2 401(k)实例
Chernozhukov, V., & Hansen, C. (2004)曾用工具分位数回归方法，使用Survey of Income and Program Participation的数据来检验401(k)计划对财富的影响。使用401(k)资格作为参与的工具变量，估计了参与401(k))计划对若干财富指标的分位数treatment effect。我们使用相同的数据，可以通过`use https://github.com/aahrens1/ddml/raw/master/data/sipp1991.dta`调用。

> 研究问题：参与401(k)计划对家庭财富有什么影响?

首先，在不采用工具变量的情况下，我们用Partial linear model和Interactive model两种模型分别进行估计。我们设定模型的outcome（即模型中的$Y$）为变量*net_tfa*，表示金融资产净额，我们关注的treatment（即模型中的$D$）为变量*e401*，是表示是否有401(k)资格的二元变量，其余变量（即模型中的$X$）包括总资产、户主年龄、家庭收入、家庭规模、户主的教育年限、固定福利养老金状况指标、是否已婚、是否是双职工家庭、是否参与IRA、是否拥有房子。

```stata
global Y net_tfa
global D e401
global X tw age inc fsize educ db marr twoearn pira hown

gen fid = mod(_n,3)+1

*partial
ddml init partial, foldvar(fid)
ddml E[Y|X]: reg $Y $X 
ddml E[D|X]: reg $D $X
ddml crossfit 
ddml estimate , vce(hc3) 

*interactive
ddml init interactive, foldvar(fid)
ddml E[Y|X,D]: reg $Y $X 
ddml E[D|X]: logit $D $X
ddml crossfit 
ddml estimate  , trim(0)
ddml estimate , atet trim(0)
```
结果如下表所示
```
*partial
--------------------------------------------------------------------------
         |             Robust HC3
 net_tfa | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
    e401 |   5210.205   1087.900     4.79   0.000     3077.959    7342.451
--------------------------------------------------------------------------
*interactive ATE
--------------------------------------------------------------------------
         |               Robust
 net_tfa | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
    e401 |   2633.680   2771.162     0.95   0.342    -2797.697    8065.058
--------------------------------------------------------------------------
*interactive ATTE
--------------------------------------------------------------------------
         |               Robust
 net_tfa | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
    e401 |    563.385   6963.070     0.08   0.936    -1.31e+04   14210.752
--------------------------------------------------------------------------
```

接下来，我们在模型中引入工具变量，并用Partial和Interactive两种模型分别进行估计。这里我们将treatment（即模型中的$D$）设定为变量*p401*，表示是否参加401(k)，将变量*e401*作为工具变量（即模型中的$Z$），其余设定与无工具变量的模型相同。
```stata
global Y net_tfa
global D p401
global Z e401
global X tw age inc fsize educ db marr twoearn pira hown

gen fid = mod(_n,3)+1

*partial IV
ddml init iv, foldvar(fid)
ddml E[Y|X]: reg $Y $X 
ddml E[Z|X]: reg $Z $X
ddml E[D|X]: reg $D $X
ddml crossfit 
ddml estimate , robust

*interactive IV
ddml init late, foldvar(fid)  
ddml E[Y|X,Z]: reg $Y $X 
ddml E[D|X,Z]: logit $D $X
ddml E[Z|X]: logit $Z $X
ddml crossfit 
ddml estimate ,  trim(0)
```
结果如下表所示
```
*partial IV
--------------------------------------------------------------------------
         |               Robust
 net_tfa | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
    p401 |   7515.858   1567.327     4.80   0.000     4443.954   10587.763
--------------------------------------------------------------------------

*interactive IV
--------------------------------------------------------------------------
         |               Robust
 net_tfa | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
---------+----------------------------------------------------------------
    p401 |   3829.545   4028.866     0.95   0.342    -4066.886   11725.977
--------------------------------------------------------------------------
```


&emsp;
## 4. 总结讨论
利用机器学习方法进行因果推断已被广泛地研究和应用。一方面，机器学习因果推断可以减少对函数形式的假设，并且可以对高维数据进行建模。此外，机器学习自带正则化，可以达到变量选择的效果。然而，传统的机器学习因果推断并不完美，这主要体现在其并不理想的稳健性上。传统方法中，除非函数估计绝对精确或者函数形式设定完全正确（这在实际应用中几乎不可能达到），否则对于treatment effect的估计误差并不收敛，与数据本身的性质有关，需要权衡偏差和方差，造成了不稳健。Double/Debiased Machine Learning在传统方法的基础上，引入残差建模的思想，消除偏差，提供了更为稳健的因果推断方法。此外，DDML方法还能构建置信区间，且其收敛速度$\sqrt{n}$一般大于传统机器学习方法。

Stata的程序包**ddml**提供了便捷的DDML实现。高效正确地进行DDML的关键是合理的模型设定。**ddml**包支持五种不同的模型，包括Partial linear model、Partial linear IV model、Interactive model、Interactive IV model、High-dimensional IV model。模型设定的关键在于treatment与其他控制变量是否能够线性分离，以及是否加入工具变量进入模型。

机器学习擅长给出精准的预测，而经济学注重特征对目标影响的无偏估计。Double/Debiased Machine Learning把经济学的方法和思想与机器学习相结合，在经济学框架下用机器学习方法给出特征对目标影响的无偏估计，提供了更为稳健的机器学习因果推断手段。


&emsp;
## 5. 参考文献
- Chernozhukov V, Chetverikov D, Demirer M, et al. Double/debiased machine learning for treatment and structural parameters[J]. 2018. [-PDF-](https://sci-hub.se/10.3386/w23564)
- Fisher A, Kennedy E H. Visually communicating and teaching intuition for influence functions[J]. The American Statistician, 2021, 75(2): 162-172. [-PDF-](https://sci-hub.se/10.1080/00031305.2020.1717620)
- Díaz I. Machine learning in the estimation of causal effects: targeted minimum loss-based estimation and double/debiased machine learning[J]. Biostatistics, 2020, 21(2): 353-358. [-PDF-](https://watermark.silverchair.com/kxz042.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAwMwggL_BgkqhkiG9w0BBwagggLwMIIC7AIBADCCAuUGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMPCE-gOvrZq0GNvCyAgEQgIICtteRJQ32Ucw776LzoE7tar75ys8Km9y9aeIxsMoyxNifzKRN8XvXxTYPvh2LZfrIEp1XTrHx4Z_NcbH8JmTVIjY1EYh06GomK4M0cqyO3dsbNEVXSBG5-TBCJeC8wNF8ILgZ0P_a5QdDmJP6gejt_nt4WjB2mCsUD_6VgLxazDksjcPwR2weavLb3E_JPMAVrEH1tmQOdOcoe9NBgL32nhnL4B4Ihhi3gZGa7TamyJTAKlYFz75WUm3lu9YLMd83YnTijfN-f8G_-bLF8Tzzw4WUd2KDQYD-Vu_2N5BGui8tSLIPAl7_uuFA7SRMRmBD4ySBjtdIt8iTr5klGdT40-SDIYXTXbW_0n8S0ah5XuyWNto3-MslqMVFezaxABMtrDodFzn2K__X9vSMAWBSa57kkjZGFiYTuOC7RqsWJOuyhpaIf7sUz1LKqci3ucTSrlL5Ynz4OgR7G1o8jZr0Hpevg42W-sEA-khTL4919vMRifQ4LJp3ROLvd6xeYl2ltqQ73RiZKeIqCI5vqJUL1y0h2i-eQ8buCq3G19D_inadW8bicjrfqs2L4Qjfh-EHL4byNTzijgczEovjXVzex6d91LEmEY1pUv-9Gq5pTaSuGNgvZGXGxyTontGbFKrYMp0gugjRqTMZsxf6jW41K7fuwMpPHH107ZBsdRzpvCCXbDMwDQt9W52hpj42H8Pryeta36788-YR7QrzWVKBCRxprGTJas_bOag1pbb4bQhLCYTdF5aYhEQ1wEBT_TQ8qctyI3yGvHPwBMSmr6oQiI7_yXuKTpkzsxWX6o4ufyEditrMtpyCp5_Auj40enCrwQCmP4l2f2t9rIAiSSoJGul0QsOTAyClFzETNt1JOETHbCbJKMkisbQyRTWO6srd72W2bkEChw24HN-GLnPKQ2Xn9KMyEzg)
- Almond D, Chay K Y, Lee D S. The costs of low birth weight[J]. The Quarterly Journal of Economics, 2005, 120(3): 1031-1083. [-PDF-](https://www.nber.org/system/files/working_papers/w10552/w10552.pdf)
- Cattaneo M D. Efficient semiparametric estimation of multi-valued treatment effects under ignorability[J]. Journal of Econometrics, 2010, 155(2): 138-154. [-PDF-](https://sci-hub.se/10.1016/j.jeconom.2009.09.023)
- Chernozhukov V, Hansen C. The effects of 401 (k) participation on the wealth distribution: an instrumental quantile regression analysis[J]. Review of Economics and statistics, 2004, 86(3): 735-751. [-PDF-](https://sci-hub.se/10.1111/j.1468-0262.2005.00570.x)
- Hull I, Grodecka-Messi A. Measuring the Impact of Taxes and Public Services on Property Values: A Double Machine Learning Approach[J]. arXiv preprint arXiv:2203.14751, 2022. [-PDF-](https://arxiv.org/pdf/2203.14751.pdf)
- MIT OpenCourseWare: Victor Chernozhukov and Iván Fernández-Val. 14.382 *Econometrics*. Spring 2017. L12. *TREATMENT EFFECTS* [-PDF-](https://ocw.mit.edu/courses/14-382-econometrics-spring-2017/797fa3bf36a71ad7b733034b3f49161c_MIT14_382S17_lec12.pdf)

&emsp;
## 6. 相关推文

- 专题：[内生性-因果推断](https://www.lianxh.cn/blogs/19.html)
  - [一组动图读懂因果推断](https://www.lianxh.cn/news/aa3e050bc1398.html)
  - [因果推断：混杂因素敏感性分析实操(下)-tesensitivity](https://www.lianxh.cn/news/5f38356e6485c.html)
  - [因果推断：混杂因素敏感性分析理论(上)](https://www.lianxh.cn/news/a5930b14f07bb.html)
  - [Stata因果推断：hettreatreg-用OLS估计异质性处理效应](https://www.lianxh.cn/news/eb362aedd59e7.html)
  - [Stata：因果推断方法综述和Stata操作](https://www.lianxh.cn/news/df1cff50dc28a.html)
  - [fect：基于面板数据的因果推断（上）-T218a](https://www.lianxh.cn/news/a68c15f714711.html)
  - [fect：基于面板数据的因果推断（下）-T218b](https://www.lianxh.cn/news/e0c3a12b20ef3.html)
  - [因果推断：未测量混杂因素的敏感性分析-T249](https://www.lianxh.cn/news/0b63b5245d127.html)
  - [用FE-固定效应模型能做因果推断吗？](https://www.lianxh.cn/news/e1a6f3d1a9596.html)
  - [经典文献回顾：政策评价-因果推断的计量方法](https://www.lianxh.cn/news/b787111782621.html)
  - [因果推断好书：Causal-Inference-Measuring-the-Effect-of-X-on-y](https://www.lianxh.cn/news/64d690b8d566b.html)
  - [Stata因果推断新书：The-SAGE-Handbook-of-Regression-Analysis-and-Causal-Inference](https://www.lianxh.cn/news/f29e67e8937a6.html)
  - [Stata新命令：konfound - 因果推断的稳健性检验](https://www.lianxh.cn/news/4832e3735dc81.html)
- 专题：[机器学习](https://www.lianxh.cn/blogs/47.html)
  - [知乎热议：如何学习机器学习](https://www.lianxh.cn/news/6e5cb7260859b.html)
  - [机器学习在经济学领域的应用前景](https://www.lianxh.cn/news/5ec10a29aaf88.html)
  - [机器学习如何用？金融+能源经济学文献综述](https://www.lianxh.cn/news/be7339df45904.html)
  - [知乎热议：纠结-计量经济、时间序列和机器学习](https://www.lianxh.cn/news/fa2ecb07451b7.html)
  - [机器学习：随机森林算法的Stata实现](https://www.lianxh.cn/news/b057fea5adb6a.html)
  - [Stata：机器学习分类器大全](https://www.lianxh.cn/news/a50bbdeb589cc.html)
