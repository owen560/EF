&emsp;

>**作者：** 陈蕴晴（中山大学)  
>**E-mail：**<949567067@qq.com>  

&emsp;

---

**目录**  
[TOC]  

---

&emsp;

## 1. 为何、何时使用 gtools 命令组  

相信对于大部分 Stata 用户来说，相对小量、简单的数据统计或数据处理乃日常上机必走的工序，用户对 Stata 的运行效率并没有什么感知。  

然而，当因数据集异常庞大，你不得不对着久久缓冲不过的界面发愣，而旁边的人却由于用上 gtools 而早早看到输出结果时，这种“知觉”将立马苏醒！  

话不多说，一图胜千言(\ˇ∀ˇ/)  

![速度对比](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/gtools：一组大幅提升运算速度的命令_Fig01_速度对比_陈蕴晴.png.png)

- Note: Stata 17 由于对 `sort` 和 `collapse` 进行大幅的速度优化，在 MP 版本下， `collapse` 甚至比 gtools 中的 `gcollapse` 还快2倍。

Mauricio Caceres（2018）应用 C 语言插件及 Hashes 原理 (即通过将关键码值映射到 Hash Table 中的某个位置以直接访问记录) 编写了 gtools 这一利器。据测算，在命令执行上，gtools 的速度最高可达 Stata 传统命令的近 20 倍，可以说是当之无愧的效率达人！  


## 2. 如何安装  

gtools 是一个外部命令，作者已将其上传至 ssc，用户可直接安装：
```Stata
· ssc install gtools  //安装  
· gtools, upgrade  //升级到最新版  
```


## 3. 如何使用

### 3.1 命令速览

安装完毕后，用户通过 `help gtools` 可以查看命令组中所包含的主要命令，很多都是基于传统的 Stata 指令编制，小编在此整理了指令对照表，如下：

| gtools 指令 | 等价 Stata 指令 |
|    :--:    |   :--:  |
| `gcollaspe` & `gcontract` | `collaspe` & `contract` |
| `gquantiles` | `pctile`, `xtile`, `_pctile` |
| `greshape` | `reshape` |
| `gegen` | `egen` |
| `gisid` | `isid` |
| `gduplicates` | `duplicates` |
| `glevelsof` | `levelsof` |
| `gstat tab` | `tabstat` |
| `gstat sum` | `sum,detail` |


此外，gtools 命令组还包含了一些为所其特有的指令，其中有一部分的功能类似于 Stata 外部命令，对照表（部分）如下：

| gtools 指令 | 类似外部指令（SSC/SJ）|
|    :--:    |   :--:  |
| `gunique` | `unique` |
| `gdistinct` | `distinct` |
| `gtop` ( `gtoplevelsof` ) | `groups`, `select()` |
| `gstats winsor` | `winsor2` |
| `gstats range` | `rangestat` |
| `fasterxtile` | `fastxtile` |
| `gatats hdfe` | / |
| `gstats transform` | /|

就连回归模型， gtools 命令组也有相应的指令帮你实现：
| gtools 函数|回归模型 | 类似Stata 函数|
|    :--:    |   :--:  |    :--:  |
|  `gregress` | OLS |`regress`, `reghdfe` |
| `givregress`  | 2SLS|`ivregress 2sls`, `ivreghdfe` |
|`gglm` |IRLS|`logit`, `poisson`, `ppmlhdfe` |

### 3.2 用法详引：部分例子

在本节中，小编将对 gtools 中**除了提高运算效率外，还能提供额外功能**的九个命令（除回归命令外） `gcollapse`, `greshape`, `gquantiles`, `gdistinct`, `gegen`, `glevelsof`, `gtoplevelsof`， `gstats sum/tab` 和 `gstats transform/range/moving` 依次进行详细的用法指引，配有例子帮助读者理解。

#### 3.2.1 gcollapse 命令

`gcollapse` 用于批量生成描述性统计信息，命令语法如下： 
```Stata
· gcollapse clist [if] [in] [weight] [, options]
```

其中，
`clist` 必须是数字变量，如：
```Stata
· mean       //均值（默认）
· geomean    //几何平均数
· count      //非遗漏观测数
· nmissing   //遗漏观测数
· nunique    //去重后元素个数
· median     //中位数
· p#         //分位数( # 必须严格介于1~100之间)
· iqr        //四分位距
· range      //范围(最大-最小)
· first      //第一个观测值
· lastnm     //最后一个非缺失观测值
· semean     //均值标准误
· skewness   //偏度
· kurtosis   //峰度
……
```

`weight` 允许所有权重类型，分别是:
- `aweight`：分析权重；
- `fweight`：频数权重；
- `pweight`：概率权重/抽样权重；
- `iweight`：重要性权重。
> Note：详情可参照连享会往期推文 [-Stata 权重设定-](https://www.lianxh.cn/news/4dbc40eb41c3d.html) 

`options` 主要包括：
- `by(varlist)`：按 `varlist` 分类计算 `clist` 的值；
- `cw`：删除缺失值所在行的全部观测值；
- `fast`:若用户按Break键，则不恢复原始数据集；
- `merge`：将统计数据合并到原始数据集；
- `labelformat(str)`：指定输出结果的标签。
  
小示例：
```Stata
· sysuse auto, clear

· gcollapse (mean) price, by(foreign)  
//统计出国内外汽车价格均值
· gcollapse (mean) mean_price=price, by(foreign)  
//统计出国内外汽车价格均值，并命名为 mean_price
· gcollapse (mean) price (mean) rep78, by(foreign) cw  
//由于 rep78 存在较多缺失值，使用 cw 指令会将缺失值所在行的观测值全部删去，故此时统计出来的国内外汽车价格均值与前文不同
· list  
//显示结果

· gcollapse (mean) price, labelformat(#stat#: #sourcelabel#)  
· disp _n(1) "`:var label price'"   
//将统计结果的标签指定为 mean:Price
```

#### 3.2.2 greshape 命令

`greshape` 命令用于对数据进行横纵变换处理，不仅较 `reshape` 有处理速度上的提升，还增加了与 R 语言 `tidyr` 包中的 `spread()` 函数、`gather()` 函数等价的功能。该命令语法如下：
```Stata
**宽变长**
· greshape long stubnames, by(varlist) [options]
· greshape gather varlist, keys(varlist) values(varname) [options] 

**长变宽**
· greshape wide stubnames, by(varlist) keys(varname) [options]
· greshape spread varlist, keys(varname) [options] 
```
当然，`greshape` 也支持传统的语法：
```Stata
· greshape long stubnames, i(varlist) [options]                 //宽变长
· greshape wide stubnames, i(varlist) j(varname) [options]      //长变宽
```
> Note：数据横纵变换的逻辑本文暂不展开，读者可参考连享会往期推文 [-Stata数据处理：纵横长宽转换-reshape命令-](https://www.lianxh.cn/news/5cc1220a0ea6f.html)

其中，`options` (宽变长)包括：
- `by(varlist)`：ID 变量（等价于 i() ）；
- `keys(varname)`：`stubnames` 的子集变量名（等价于 j() ）（不存在）；
- `string`：是否允许将观测值为字符型的 key 与 stub 进行匹配（见示例）；
- `match(str)`：和 @ 的用法一致（默认通过占位符 @ 的使用来限定数值在变量名中的位置）；
- `dropmiss`：删除通过横纵变换后变量的缺失值；
- `nodupcheck`：允许重复值存在。

`options` (长变宽)包括：
- `by(varlist)`：ID 变量（等价于 i() ）；
- `keys(varlist)`：`stubnames` 的子集变量名（等价于 j() ）（已经存在）；
- `colsepparate(str)`：当存在多个 key 的时候使用特定的分隔符；
- `match(str)`：和 @ 的用法一致（默认通过占位符 @ 的使用来限定数值在变量名中的位置）；
- `labelformat(str)`：使用占位符自定义标签格式（默认为 `#keyvalue# #stublabel#` ）；
- `prefix(str)`：对横纵变换后的变量重命名；
- `nomisscheck`：允许缺失值存在。

共用的 `options`：
- `fast`：不将 `reshape` 的结果 `preserve` 或者 `restore`;
- `unsorted`：不对 `reshape` 后的数据进行排序；
- `nochecks`：`fast` + `unsorted` + `nodupcheck` + `nomisscheck`；
- `xi(drop)`：不在 `reshape` 、`by()` 或者 `keys()` 中删除变量。
  
小示例：
```Stata
**感受 `match()` 的使用方法及效果**
· webuse reshape3, clear
· list    
//先粗略看一下数据结构，判断为宽型数据
     +----------------------------------------------------------+
     | id   sex   inc80r   inc81r   inc82r   ue80   ue81   ue82 |
     |----------------------------------------------------------|
  1. |  1     0     5000     5500     6000      0      1      0 |
  2. |  2     1     2000     2200     3300      1      0      0 |
  3. |  3     0     3000     2000     1000      0      0      1 |
     +----------------------------------------------------------+

· greshape long inc@r ue, by(id) keys(year)
· greshape long inc[year]r ue, by(id) keys(year) match([year])
· list, sepby(id)
//两种语法得到的结果一致如下
     +-----------------------------+
     | id   year   incr   ue   sex |
     |-----------------------------|
  1. |  1     80   5000    0     0 |
  2. |  1     81   5500    1     0 |
  3. |  1     82   6000    0     0 |
     |-----------------------------|
  4. |  2     80   2000    1     1 |
  5. |  2     81   2200    0     1 |
  6. |  2     82   3300    0     1 |
     |-----------------------------|
  7. |  3     80   3000    0     0 |
  8. |  3     81   2000    0     0 |
  9. |  3     82   1000    1     0 |
     +-----------------------------+
```
```Stata
**对比感受 `string` 的使用效果**
· webuse reshape4, clear
· list    
//同样是一份宽型数据
    +-------------------------+
     | id   kids   incm   incf |
     |-------------------------|
  1. |  1      0   5000   5500 |
  2. |  2      1   2000   2200 |
  3. |  3      2   3000   2000 |
     +-------------------------+

· greshape long inc, by(id) keys(sex) string
· list    
//注意这里的 key，即变量 sex 的观测值为 f m（字符型）
     +------------------------+
     | id   sex    inc   kids |
     |------------------------|
  1. |  1     f   5500      0 |
  2. |  1     m   5000      0 |
  3. |  2     f   2200      1 |
  4. |  2     m   2000      1 |
  5. |  3     f   2000      2 |
     |------------------------|
  6. |  3     m   3000      2 |
     +------------------------+

· greshape long inc, by(id) keys(sex)
//会报错：variable j contains all missing values
```
```Stata
** `labelformat(str)` 的用法**
· sysuse auto, clear

· local labelformat labelformat(#stubname#, #keyname# == #keyvaluelabel#)
· greshape wide mpg, by(make) key(foreign) `labelformat'
//这里的 key 是 0 和 1
· desc mpg*
-----------------------------------------------------------------------
Variable      Storage   Display    Value
    name         type    format    label      Variable label
-----------------------------------------------------------------------
mpg0            int     %8.0g                 mpg, foreign == Domestic
mpg1            int     %8.0g                 mpg, foreign == Foreign
-----------------------------------------------------------------------
```

#### 3.2.3 gquantiles 命令

`gquantiles` 可用于高效生成变量百分位数、分位数、类别和频数信息，功能强于 Stata 传统指令 `xtile`, `pctile` 和 `_pctile`，并支持带有 `xtile` 和 `pctile` 的 `by()`。该命令语法如下：
```Stata
· gquantiles [newvar =] exp [if] [in] [weight], {pctile | xtile | _ pctile} quantiles_method [ gquantiles_options ]
```

语法中，
`weight` 允许 `fweight`, `aweight`, `pweight` 三种权重类型，但不可与 `altdef` 同时使用。

`quantiles method` 主要有：
- `nqantiles(#)`：生成变量分位数信息，括号可以指定将变量分为 # 等份，如 `nq(10)` 表示生成第 10，20，...，90 分位对应的数。默认 `nq(2)`, 即生成变量中位数；
- `cutpoints(varname)`：指定 `varname` 的取值作为变量的分割点；
- `cutoffs（numlist）`：类似于 `cutpoints`，只不过分割点的属性由 varible 变更为 numlist ；
- `quantiles(numlist)` / `percentiles(numlist)`：生成 `numlist` 所指定的分位数信息。如 `quantiles(10(20)90)` 表示生成变量第 10，30，50，70，90 百分位对应的值。

`gquantiles options` 主要包括：
- `genp(newvar)`：生成一个包含百分位信息的新变量；
- `by(varlist)`：分组计算分位数（仅限于 `xtile` 和 `pctile`），对 `groupid(varname)` 非常有用；
- `groupid(varname)`：生成表示 group ID 的变量，括号指定变量名称；
- `_pctile`：不可与 `by()` 同时使用，等价于 Stata 传统指令 `_pctile`，生成变量分位数信息并将返回值存储在 `r(1)`，`r(2)`，…… 以及 `r(quantiles_used)` 或 `r(cutoffs_used)` 中，提供返回功能；
- `pctile`：等价于 Stata 传统指令 `pctile`，用于生成变量分位数信息；
- `xtile`：等价于 Stata 传统指令 `xtile`，用于生成分位点（即观测值处于第几分位数的位置），方便后续划分类别变量；
- `binfreq` / `binfrep(newvar)`：生成变量频数信息，用括号指定 `newvar` 时，返回值将存储在新变量中，否则存储在矩阵 `r(quantiles_bincount)` 或 `r(cutoffs_bincount)` 中。

小示例：
```Stata
· sysuse auto, clear

· gquantiles price, _pctile nq(10)  
//返回变量 price 第 10，20，……，90 分位数对应的值，不直接列示，但可以通过 return list 查看存储情况
· matrix list r(quantiles_used)  
//以矩阵形式呈现结果
        c1
-----------        
r1    3895
r2    4099
r3    4425
r4    4647
r5  5006.5
r6    5705
r7    6165
r8    7827
r9   11385
-----------

· gquantiles p10 = price, pctile nq(10)  
//生成新变量 p10，存放源变量price 第 10，20，……，90 分位数对应的值

· gquantiles m = price [w = weight], xtile cutpoints(p10) by(make)  
//以 weight 作为权重变量，按照变量 make 分组，组内以变量 p10 的取值作为分割点，生成包含变量 price 分位点信息的新变量 m
```

#### 3.2.4 gdistinct 命令

`gdistinct` 可以代替 `distinct` 报告变量不同观测值的数量（默认剔除缺失值），且可将统计结果以表格形式直观呈现，还额外增加了部分选项。该命令语法如下：
```Stata
· gdistinct [varlist] [if] [in] [, missing abbrev(#) joint minimum(#) maximum(#) ] 
```

其中，`options` 主要包括：
- `missing`：计数时包括缺失值（默认不包括）。
- `abbrev(#)`：指定要报告的变量名称最多显示 # 个字符。注意该选项不能与 `joint` 同时使用，否则无效；
- `joint`：联合考虑多个变量的取值情况；
- `minimum(#)`：指定仅显示计数 ≥# 的变量；
- `maximum(#)`：指定仅显示计数 ≤# 的变量；
- `sort(order)`：指定输出结果按照计数情况进行排序。

小示例：
```Stata
· sysuse auto, clear

· gdistinct
//例如对比 total 栏和 distinct 栏，我们可以发现变量 mpg 虽然共有74个观测值，但仅存在21个不同的观测值；变量 rep78 应该有 5 个缺失值
--------------------------------------
              |        Observations
              |      total   distinct
--------------+-----------------------
         make |         74         74
        price |         74         74
          mpg |         74         21
        rep78 |         69          5
     headroom |         74          8
        trunk |         74         18
       weight |         74         64
       length |         74         47
         turn |         74         18
 displacement |         74         31
   gear_ratio |         74         36
      foreign |         74          2
--------------------------------------

· gdistinct, sort(-distinct)
//按照不同的观测值的个数降序输出结果
-------------------------------------
              |        Observations
              |      total   distinct
--------------+----------------------
        price |         74         74
         make |         74         74
       weight |         74         64
       length |         74         47
   gear_ratio |         74         36
 displacement |         74         31
          mpg |         74         21
         turn |         74         18
        trunk |         74         18
     headroom |         74          8
        rep78 |         69          5
      foreign |         74          2
-------------------------------------
```
```Stata
· gdistinct make-headroom, missing abbrev(6)
//仅报告数据集中由变量 make 到 headroom 的计数情况，计数时允许包括缺失值，且变量名最多显示6个字符
-------------------------------
        |        Observations
        |      total   distinct
--------+----------------------
   make |         74         74
  price |         74         74
    mpg |         74         21
  rep78 |         74          6
 head~m |         74          8
-------------------------------

· gdistinct foreign rep78, joint
//联合报告变量 foreign 和 rep78 的计数情况
------------------------------
        Observations
      total   distinct
         69          8
------------------------------

· gdistinct, max(10)
//只报告计数不超过 10 的变量
-------------------------------------
              |        Observations
              |      total   distinct
--------------+----------------------
        rep78 |         69          5
     headroom |         74          8
      foreign |         74          2
-------------------------------------
```

#### 3.2.5 gegen 命令

`gegen` 基于 `egen` 编制，绝大多数情况下都能发挥提效的性能。不过，如果用户需要针对单个变量生成多种描述性统计信息，或许 `gcollapse` 配合使用 `merge` 会更快些。`gegen` 语法如下：
```Stata
· gegen [type] newvar = fcn(arguments) [if] [in] [weight] [, replace fcn_options gtools_options ] 
```

其中，
`weight` 允许所有权重类型，不过仅配合部分描述性统计函数有效，如 total, sum, mean, sd, count, median, iqr, percent, semean, sebinomial, sepoisson, percentiles, skewness, kurtosis；特别地，
- `pweight` 不支持统计 sd, semean, sebinomial 或 sepoisson；
- `iwight` 不支持统计 semean, sebinomial 或 sepoisson；
- `aweight` 不支持统计 sebinomial 或 sepoisson。

`fcn(arguments)` 可以支持以下三种函数：
- ID 生成：`group(varlist) [, missing counts(newvarname) fill(real)]`，不能与 `by()` 同时使用；
- 标签分组：`tag(varlist) [, missing]`，将变量不同观测值的第一个值标记为1，其余标记为0，如某变量的观测值为（2，3，2，1，2，5，5），那么经标记就会变为（1，1，0，1，0，1，0）。需注意的是，`tag()` 不能与 `by()` 同时使用；
- 统计性函数：`first|last|firstnm|lastnm(exp)`, `count(exp)`, `nunique(exp)`, `iqr(exp)`, `max|min(exp)`, `select(exp)`, `cv(exp)`, `sum(exp)` 等等。

小示例：
```Stata
· sysuse auto, clear

· gegen id = group(turn)
//为变量 turn 的分组信息编码
· tabdisp turn, cell(id)
//利用表格展现数据(此处使用了 tabdisp 命令)

· gegen tag = tag(rep78)
//将变量 rep78 第一个出现的观测值标记为零
· egen ndistinct = total(tag), by(foreign)
//计算不同 foreign 类别下，变量 rep78 各有多少个非重复的观测值
· tabdisp foreign, cell(ndistinct)
----------------------
Car       |
origin    |  ndistinct
----------+-----------
 Domestic |          5
  Foreign |          0
----------------------

· gegen mean = mean(price), by(foreign)
//按照变量 foreign 分组，生成每组中变量 price 的均值，存放在新变量 mean 中
```

#### 3.2.6 glevelsof 命令

`glevelsof` 用于对指定变量不同的观测值进行排序，与 `levelsof` 不同的是，它还能够支持多个变量同时排序。该命令语法如下：
```Stata
· glevelsof varlist [if] [in] [, options ] 
//这里的 varlist 也可以具体化为 [+|-] varname [[+|-] varname ...]，仅改变观测值的排列顺序
```

`options` 有以下常用选项：
- `clean`：清除字符串变量观测值显示时的复合双引号；
- `local(macname)`：定义局部暂元并将观测值存储在其中；
- `missing`：将缺失值也考虑在观测值内；
- `separate(separator)`：将各观测组以指定符号分隔开；
- `colseparate(separator)`：将观测组内不同变量的观测值以指定符号分隔开；
- `gen([prefix], [replace])`：将 varlist 的存储在一个以 prefix 为前缀的新 varlist 中，或直接替代原来的 varlist。`prefix` 和 `，replace` 两个选项互斥;
- `matasave[(str)]`：将报告结果保存在 mata 对象中（默认对象名为 GtoolsByLevels ）；
- `silent`：不显示结果，常与 `gen()` 或 `matasave()` 连用。

小示例：
```Stata
· sysuse auto, clear

· glevelsof rep78
1 2 3 4 5
· glevelsof -rep78
5 4 3 2 1
· glevelsof rep78, missing
1 2 3 4 5 .

· local varlist foreign rep78
//将变量 foreign 和 rep78 的观测值存放在局部暂元 varlist 中
· glevelsof `varlist', sep("|") colsep("，")
//用 `|` 将 varlist 不同取值分隔开；用 `,` 将 varlist 内部不同变量的观测值分隔开

`"0，1"'|`"0, 2"'|`"0, 3"'|`"0, 4"'|`"0, 5"'|`"1, 3"'|`"1, 4"'|`"1, 5"'

· glevelsof turn trunk in 1/10, gen(ulev_) nolocal
//生成前缀为 ulev 的新变量，存储级别由 1 到 10 的观测组（组内有两个源变量， turn 和 trunk ）
· list turn trunk ulev_*
     +------------------------------------+
     | turn   trunk   ulev_t~n   ulev_t~k |
     |------------------------------------|
  1. |   40      11         34         10 |
  2. |   40      11         35         12 |
  3. |   35      12         40         11 |
  4. |   40      16         40         16 |
  5. |   43      20         42         13 |
     |------------------------------------|
  6. |   43      21         42         16 |
  7. |   34      10         43         17 |
  8. |   42      16         43         20 |
  9. |   43      17         43         21 |
 10. |   42      13          .          . |
     |------------------------------------|
```

#### 3.2.7 gtoplevelsof 命令

`gtoplevelsof` / `gtop` 将观测值的出现频数、累积频数、百分比和累积百分比信息以表格方式呈现（默认按照观测值出现频数由大到小排序）。该命令语法如下：
```Stata
· gtoplevelsof varlist [if] [in] [weight] [, options ] 
//和 glevelsof 一样，这里的 varlist 也可以替代为 [+|-] varname [[+|-] varname ...]，仅改变观测值的排列顺序
```

在 `glevelsof` 命令的基础上，`gtop` 还增加了以下选项：
`weight`：支持 `aweight`, `fweight` 和 `pweight` 三种权重类型；

常用的`options`：
- `ntop(int)`：报告出现次数最多(或者权重最大)的前 int 个观测值；
- `freqabove(int)`：报告出现次数超过 int 的观测值；
- `pctabove(real)`：报告占比超过 int 的观测值；
- `missrow`：增加一行报告缺失观测值的信息（当有多个变量时，只有观测组内所有变量的值都是缺失的才会被单独报告）；
- `groupmissing`：当有多个变量时，观测组内只要出现一个缺失值就会变成 missing row，从而与 `missrow` 连用时这一行的统计信息就会被单独列出来；
- `colmax(numlist)`：最多显示字符型观测值的 n 个字符。

小示例：
```Stata
**对比感受 `ntop` 和 `missrow` 的使用效果**

· sysuse auto, clear
· gtoplevelsof rep78
   rep78 |    N  Cum   Pct (%)   Cum Pct (%) 
 --------------------------------------------
       3 |   30   30      40.5          40.5 
       4 |   18   48      24.3          64.9 
       5 |   11   59      14.9          79.7 
       2 |    8   67      10.8          90.5 
       . |    5   72       6.8          97.3 
       1 |    2   74       2.7         100.0 

· gtop rep78, missrow
     rep78 |    N  Cum   Pct (%)   Cum Pct (%) 
 ---------------------------------------------
        3 |   30   30      40.5          40.5 
        4 |   18   48      24.3          64.9 
        5 |   11   59      14.9          79.7 
        2 |    8   67      10.8          90.5 
        1 |    2   69       2.7          93.2 
 ---------------------------------------------
  Missing |    5   74       6.8         100.0 

· gtop rep78, ntop(3)
             rep78 |    N  Cum   Pct (%)   Cum Pct (%) 
 ------------------------------------------------------
                 3 |   30   30      40.5          40.5 
                 4 |   18   48      24.3          64.9 
                 5 |   11   59      14.9          79.7 
 ------------------------------------------------------
  Other (3 groups) |   15   74      20.3         100.0 

· gtop rep78, ntop(3) missrow
             rep78 |    N  Cum   Pct (%)   Cum Pct (%) 
 ------------------------------------------------------
                 3 |   30   30      40.5          40.5 
                 4 |   18   48      24.3          64.9 
                 5 |   11   59      14.9          79.7 
 ------------------------------------------------------
           Missing |    5   64       6.8          86.5 
  Other (2 groups) |   10   74      13.5         100.0 
```
```Stata
**感受 `novaluelab` 和 `colmax()` 的使用效果**

· gtop foreign
   foreign |    N  Cum   Pct (%)   Cum Pct (%) 
 ----------------------------------------------
  Domestic |   52   52      70.3          70.3 
   Foreign |   22   74      29.7         100.0 

· gtop foreign, novaluelab
  foreign |    N  Cum   Pct (%)   Cum Pct (%) 
 ---------------------------------------------
        0 |   52   52      70.3          70.3 
        1 |   22   74      29.7         100.0 

· gtop foreign, colmax(3)
  foreign |    N  Cum   Pct (%)   Cum Pct (%) 
 ---------------------------------------------
   Dom... |   52   52      70.3          70.3 
   For... |   22   74      29.7         100.0 
```
```Stata
**对比感受 missrow 和 groupmiss 的区别**

· gtop foreign rep78, ntop(4) missrow
   foreign   rep78 |    N  Cum   Pct (%)   Cum Pct (%) 
 ------------------------------------------------------
  Domestic       3 |   27   27      36.5          36.5 
  Domestic       4 |    9   36      12.2          48.6 
   Foreign       4 |    9   45      12.2          60.8 
   Foreign       5 |    9   54      12.2          73.0 
 ------------------------------------------------------
  Other (6 groups) |   20   74      27.0         100.0 

· gtop foreign rep78, ntop(4) missrow groupmiss
           foreign   rep78 |    N  Cum   Pct (%)   Cum Pct (%) 
 --------------------------------------------------------------
          Domestic       3 |   27   27      36.5          36.5 
          Domestic       4 |    9   36      12.2          48.6 
           Foreign       4 |    9   45      12.2          60.8 
           Foreign       5 |    9   54      12.2          73.0 
 --------------------------------------------------------------
  Missing (any) (2 groups) |    5   59       6.8          79.7 
          Other (4 groups) |   15   74      20.3         100.0 
```

#### 3.2.8 gstats sum/tab 命令

`gstats sum/tab` 以 `tabstat` 和 `sum, detail` 的样式高效进行描述性统计。当需要分组统计时，该命令配上选项 `by()` 将会比使用 `gcollapse` 还快。其语法如下：
```Stata
· gstats summarize varlist [if] [in] [weight] [, by(varlist) options] 
· gstats tabstat varlist [if] [in] [weight] [, by(varlist) options] 
```

`tabstat options` ：
- `by(varlist)`：分组统计；
- `statistics(stat [...])` / `s(stat [...])`：报告指定的统计量（默认是观测值数量、和、均值、标准差、最小值和最大值）；
- `columns(stat|var)`：表格的呈现形式，若括号内为 stat ，则将每一种统计量作为一列；若为 var，则将每一个变量作为一列；
- `prettystats`：呈现标准的表头；
- `labelwidth(int)`：设定变量标签/观测值最大宽度；
- `format[(%fmt)]`：格式化输出。

`summarize options`：
- `by(varlist)`：分组统计；
- `nodetail` / `nod`：不显示完整的统计信息；
- `meanonly`：仅统计变量观测值的数量、和、均值、最小值和最大值；
- `separator(#)`：每隔 # 个变量后绘制一条分割线；
- `tabstat`：以 `tabstat` 的样式输出统计信息。

共有 `options`：
- `matasave[(str)]`
- `pooled`：将所有变量汇总视为一个整体进行描述性统计；
- `noprint`：不显示统计结果；
- `format`：使用源变量的呈现格式；
- `nomissing`：与 `by()` 连用，统计时会忽略存在缺失值的组别。

小示例：
```Stata
**对比感受 `columns(stat)` 和 `columns(var)` 的差异**

· sysuse auto, clear
· gstats tab price mpg,col(stat)
   variable |    n        sum       mean        min        max         sd
-------------------------------------------------------------------------
      price |   74     456229   6165.257       3291      15906   2949.496
        mpg |   74       1576    21.2973         12         41   5.785503
-------------------------------------------------------------------------

· gstats tab price mpg,col(var)
   statistic |       price        mpg
-------------------------------------
           n |          74         74
         sum |      456229       1576
        mean |    6165.257    21.2973
         min |        3291         12
         max |       15906         41
          sd |    2949.496   5.785503
-------------------------------------
```

```Stata
**命令 `gstats tab` 和 `gstats sum` 的对比**

· gstats tab price mpg, pretty
//可以发现默认的呈现方式跟使用 `col(stat)` 一样
   variable |   Count        Sum       Mean        Min        Max    St Dev.
----------------------------------------------------------------------------
      price |      74     456229   6165.257       3291      15906   2949.496
        mpg |      74       1576    21.2973         12         41   5.785503
----------------------------------------------------------------------------

· gstats sum price mpg, nod
//只有在使用 `nod` 选项时，两个命令的呈现样式才会相同
    Variable |    Obs        Mean    Std. Dev.       Min        Max
-------------+----------------------------------------------------------
       price |     74    6165.257     2949.496      3291      15906
         mpg |     74     21.2973     5.785503        12         41
```

```Stata
**选项 `pooled` 的用法**

· gstats sum price *, nod pool
    Variable |          Obs        Mean  Std. Dev.        Min        Max
-------------+----------------------------------------------------------
[Pooled Var] |          883    1325.747    2623.11          0      15906
```

#### 3.2.9 gstats transform 命令

`gstats transform` 是一个分组统计函数，其语法如下：
```Stata
· gstats transform clist [if] [in] [weight] [, by(varlist) options] 
```
其中，`clist` 可以是：
- `[(stat)] varlist [ [(stat)] ... ]`
- `[(stat)] target_var=varname [target_var=varname ...] [ [(stat)] ...]`
- 任意 `varlist` 或 `target_var` 的组合体

`stat` 如下表所示：
| 统计量 | 描述 |
| :--: | :--: |
|demean|减去均值（默认）|
|demedian|减去中位数|
|normalize / standardize|(x-均值) / 标准差|
|moving stat [#]|移动数据表，[#] 指定边距；与 `window()` 连用|
|range stat [...]|为指定区间内的观测值进行描述性统计，与 `interval()` 连用|
|cumsum [+/- [varname]]|累计和（可以通过正负号升序或降序）；与 `cumby()` 连用|
|shift [[+/-]#]|滞后（-），提前（+）；与 `shiftby()` 连用|
|rank|对变量观测值进行排序；与 `ties()` 连用 |

`options`包括：
- `replace`：替代现有变量；
- `wildparse`
- `labelformat(str)`：指定输出结果的标签格式，默认为 `(#stat#) #sourcelabel#` ;
- `labelprogram(str)`
- `autorename[(str)]`：根据目标统计数据自动命名，默认为 `#source#_#stat#` ;
- `nogreedy`：使用运算更慢但是节省内存的算法；
- `types(str)`：（慎用！）更改目标统计数据的数据类型。

小示例：
```Stata
**基本用法**

· sysuse auto, clear
· gegen norm_price  = normalize(price), by(foreign)
//生成新变量 norm_price，存储源变量 price 经标准化的数据（按变量 foreign 分组） 
· gegen lag1_price  = shift(price), by(foreign) shiftby(-1)
//源变量 price 滞后一期的数据
· gegen lead2_price = shift(price), by(foreign) shiftby(2)
//源变量 price 提前两期的数据
```

```Stata
** 选项 `moving/range stat` 的用法**

· sysuse auto, clear
· gstats transform (range mean -0.5sd 0.5sd) x = invest
//生成新变量 x，计算半个标准差内投资的平均值

· clear
· set obs 20
·gen g = _n > 10
· gen x = _n
· gen w = mod(_n, 7)
· gegen x1 = moving_mean(x), window(-2 2) by(g)
· gstats transform (moving mean -1 3) x2 = x, by(g)
```

```Stata
** 选项 `cumsum` 的用法**
· clear
· set obs 20
· gen g = _n > 10
· gen x = mod(_n, 17)
· gen w = mod(_n, 7)
· gen r = mod(_n, 5)
· local c1 (cumsum -) x2 = x
· local c2 (cumsum +) x3 = x
· local c3 (cumsum - w) x4 = x
· local c4 (cumsum + w) x5 = x
· local c5 (cumsum) x6 = x
· gegen x1 = cumsum(x), by(g)
· gstats transform `c1' `c2' `c3' `c4' `c5', by(g) cumby(- r) l, sepby(g)
     +----------------------------------------------+
     |  x   w   r   g   x1   x2   x3   x4   x5   x6 |
     |----------------------------------------------|
  1. |  1   1   1   0    1   55    1   48    8   40 |
  2. |  2   2   2   0    3   54    3   39   18   33 |
  3. |  3   3   3   0    6   52    6   28   30   24 |
  4. |  4   4   4   0   10   49   10   15   44   13 |
  5. |  5   5   0   0   15   45   15   11   49   55 |
  6. |  6   6   1   0   21   40   21    6   55   39 |
  7. |  7   0   2   0   28   34   28   55    7   31 |
  8. |  8   1   3   0   36   27   36   47   16   21 |
  9. |  9   2   4   0   45   19   45   37   27    9 |
 10. | 10   3   0   0   55   10   55   25   40   50 |
     |----------------------------------------------|
 11. | 11   4   1   1   11   81   17   41   57   69 |
 12. | 12   5   2   1   23   70   29   28   71   42 |
 13. | 13   6   3   1   36   58   42   13   87   29 |
 14. | 14   0   4   1   50   45   56   87   14   14 |
 15. | 15   1   0   1   65   31   71   73   29   84 |
 16. | 16   2   1   1   81   16   87   58   45   58 |
 17. |  0   3   2   1   81   87    0   42   45   42 |
 18. |  1   4   3   1   82   87    1   42   46   30 |
 19. |  2   5   4   1   84   86    3   30   59   16 |
 20. |  3   6   0   1   87   84    6   16   74   87 |
     +----------------------------------------------+
```

## 4. 参考资料

- [gtools 主页](https://gtools.readthedocs.io/en/latest/index.html)
- [gtools 作者主页](https://mcaceresb.github.io/)

## 5. 相关推文
> Note：产生以下推文列表，可以通过 Stata 命令 `lianxh 关键词` 实现
> 安装最新版命令：`ssc install lianxh, replace`

- 专题：[Stata 命令](https://www.lianxh.cn/blogs/43)
  - [Stata 新命令：超级强大的-gtools-命令组](https://www.lianxh.cn/news/5e43d7d1f05f3.html)
- 专题：[回归分析](https://www.lianxh.cn/blogs/32)
  - [Stata 权重设定-fweight-pweight](https://www.lianxh.cn/news/4dbc40eb41c3d.html)
- 专题：[数据处理](https://www.lianxh.cn/blogs/25)
  - [纵横长宽转换-reshape命令一文读懂！（上）](https://www.lianxh.cn/news/5cc1220a0ea6f.html)
  - [纵横长宽转换-reshape命令一文读懂！（下）](https://www.lianxh.cn/news/4496d980350ea.html)
  - [纵横长宽转换-reshape的兄弟-gather和spread](https://www.lianxh.cn/news/264d23c9fda53.html)
  - [快速转换Wind数据-reshapewind](https://www.lianxh.cn/news/abac8de37fd18.html)
  - [变量非重复值统计-distinct](https://www.lianxh.cn/news/e81e33529e242.html)
  - [gen 和 egen 中的 sum() 函数异同](https://www.lianxh.cn/news/75645a7971acc.html)
