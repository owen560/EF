# lmfreg：OLS Linear vs Log-Linear Functional Form Tests

> **作者：** 陈宇灿 (中山大学)        
> **E-mail:**  <chenyc56@mail2.sysu.edu.cn>   

> **Source:** *[Emad Shehata](https://econpapers.repec.org/RAS/psh494.htm)*  and *[Sahra Mickaiel](https://econpapers.repec.org/RAS/pmi764.htm)* , LMFREG: Stata module to Compute OLS Linear vs Log-Linear Functional Form Tests.  [-Link-](https://econpapers.repec.org/software/bocbocode/s457507.htm)


&emsp;


---

**目录**

[TOC]

---



## 1. 简介

​		在利用Stata进行OLS回归分析时，对于模型设定上常常有水平-水平，水平-对数，对数-水平与对数-对数四种形式，其中采用对数形式的函数设定其回归系数有弹性与半弹性的经济学含义解释，而在实证研究中令研究者苦恼的往往是如何选择合适的函数形式。对于非嵌套模型，我们常常采用RESET检验以及比较调整R-Squared的方法来选择模型；而对于嵌套模型，我们常常利用F检验来进行模型选择。本文将结合 `lmfreg` 命令，全面地介绍 `Linear Model`  与  `Log-Linear Model`  的模型形式设定的检验原理与 Stata 实操。

​		`lmfreg` 主要实现了对通过6种不同方法分别计算出水平-水平以及对数-水平模型的$R^2$、LLF、Antilog $R^2$ 、Box-Cox统计量、BM统计量以及DM统计量，并以此来提供模型选择的建议。

---

## 2. lmfreg 对 OLS Linear 与 Log-Linear 进行模型选择的6种指标介绍

为便于后续介绍各方法进行模型选择的各种方法的原理，我们先对模型及有关记号的定义如下：

本文中研究的Linear Model（**Model I**）为常规的线性模型，也即回归方程如下所示：
$$
y=\pmb{\beta}'x+\varepsilon_1
$$
相比而言本文研究的Log-Linear Model（**Model II**）由下述方程所示：
$$
lny=\pmb{\beta}'x+\varepsilon_2
$$
假定以上模型均满足Gauss-Markov 假定，特别的其中的扰动项$\varepsilon_1$与$\varepsilon_2$服从正态性假定，也即有：
$$
\varepsilon_1 \sim  N(0,\sigma_1^2)  \ \ \ \ \ \ \ \  \varepsilon_2 \sim  N(0,\sigma_2^2)
$$

---

### 2.1 R-squared

该方法的核心思想为比较两个模型的模型拟合优度，选取模型拟合优度大的作为更优的选择，拟合优度可由统计指标$R^2$（R-Squared）表示：
$$
R^2=\frac{SSE}{SST}=1-\frac{SSR}{SST}
$$
如果考虑模型中解释变量**X**的数目对拟合优度R-Squared的影响，我们还可以对其变量个数K施加“惩罚”，利用$\bar{R}^2$（adj-R-Squared）来比较：
$$
\bar{R}^2=1-\frac{SSR/N-K-1}{SST/N-1}=1-\frac{(1-R^2)(N-1)}{N-K-1}
$$
​		在解释变量个数不同的非浅套模型中，常常使用$\bar{R}^2$进行模型选择，而由于本文中比较的两个模型的解释变量个数相同，因此可以只使用$R^2$，进行选择的基本思路是分别对(1)、(2)进行OLS估计，选择两个模型中$R^2$更大的一个作为更接近真实的模型数据生成过程。

---

### 2.2 Log Likelihood Function (LLF)

对数似然函数(LLF)源于极大似然估计(Maximum Likelihood Estimation , MLE)。对于线性模型(Model I)，根据有关计量理论可以知道：当扰动项服从正态分布时，根据正态分布理论，在给定X时y的条件密度函数为：
$$
f(y|X)=\left(2 \pi \tilde{\sigma}^2\right)^{-n / 2} \exp \left\{-\frac{1}{2 \tilde{\sigma}^2}\sum_{i=1}^n(y_i-\hat{\beta}'x)^2\right\}
$$
进而根据似然的定义我们可以得到对数似然函数(LLF)为：
$$
LLF=-\frac{n}{2} \log (2 \pi)-\frac{n}{2} \log \left(\tilde{\sigma}^2\right)-\frac{1}{2 \tilde{\sigma}^2}\sum_{i=1}^n(y_i-\hat{\beta}'x)^2
$$
通过极大似然估计可以得到模型的ML估计量$\hat{\beta}$与$\tilde{\sigma}^2$，将估计量代入(7)中可以得到模型的极大对数似然。

其中Model I的对数似然为：
$$
LLF_1=-\frac{n}{2} \log \left(\frac{2 \pi}{n}\right)-\frac{n}{2}-\frac{n}{2} \log (SSR_1)
$$


而对于具有对数形式被解释变量的Model II的对数似然为：
$$
LLF_2=-\frac{n}{2} \log \left(\frac{2 \pi}{n}\right)-\frac{n}{2}-\frac{n}{2} \log (SSR_2)-\sum_{i=1}^nlny_i
$$
​		基于MLE最大化似然函数的思想，我们可以从比较LLF的角度进行模型选择。基于更优的模型应具有更大的LLF这一推断，我们通过选择LLF较大的模型作为我们的回归模型。

---

### 2.3 Antilog R-squared

​		Antilog，顾名思义为反对数，换言之也就是指数(exp)。从直观上看该方法的思想为利用exp与log的互逆关系构造相同水平上的回归方程进而计算拟合优度$R^2$，模型(1)、(2)的Antilog-$R^2$可以通过下面流程得到：

#### 2.3.1 线性模型（Model I）

基于 Model II 的拟合值，用$e^{\widehat{lny}}$对$y$进行无常数项的回归，将得到的$R^2$定义为 Model I 的Antilog R-Squared

#### 2.3.2 对数模型（Model II）

基于 Model I 的拟合值，用 $ln(\hat{y})$ 对 $lny$ 进行无常数项的回归，将得到$R^2$ 定义为Model II 的 Antilog R-Squared

再根据2.1中的思想，从备选模型中选择 Antilog-$R^2$ 更大的模型作为我们的回归模型。

---

### 2.4 Box-Cox Test

​		Box-Cox Test源于统计分析中著名的Box-Cox变换：$y\mapsto y(\lambda)$，其中：
$$
y(\lambda)=\left\{\begin{matrix}
 lny \ ,\lambda=0 \\ \frac{y^\lambda-1}{\lambda} \ ,\lambda\ne0

\end{matrix}\right.
$$
参数$\lambda$代表了不同的变量代换，而本文研究的Model I与Model II的区别正是Box-Cox变换中$\lambda$=0的特例，我们进而可以通过假设检验构造统计量来进行模型选择：Box与Cox在1964年基于Box-Cox变换提出了Box-Cox检验，该检验的原假设为：$ H_0:\lambda=0$，该条件对应的正为选择Model II中的对数形式模型，Box-Cox检验统计量由下式给出：
$$
BC \ Statistic=\frac{n}{2} \left | log(\frac{SSR_1}{SSR_2} \sqrt[n]{y_1y_2...y_n} )  \right |
$$
​		在大样本的情况下，Box 与 Cox提出当$H_0$成立时，该统计量服从自由度为1的卡方分布。基于上述构造，当我们拒绝原假设时，我们应该选择对数模型Model II，而当我们无法拒绝原假设时，我们应选用线性模型Model I，更进一步对于具有偏态的被解释变量y时，我们也可以采用Box-Cox提出的确定最优$\lambda$的方法，利用Box-Cox变换使$y(\lambda)$更接近正态分布，此时对于线性模型而言会具有更好的准确度和更小的方差。

---

​		也许你已经从2.3中隐隐发现了对于两个备选模型，当我们在构造Antilog R-squared时，具体的做法是将Model II的拟合值的指数对Model I的真实值进行回归，同时将Model I的拟合值的对数于Model II的真实值进行回归。上述进行回归的方法，在对 Model I 进行验证时利用了Model II的水平拟合值，并通过Antilog R-Squared反映出来的 Model II 能否捕捉 Model I 原始样本数据 y 中的大部分信息来进行选择。反之对Model II进行验证的方法类似，使用了Model I的对数值。

​		一言以蔽之，Antilog R-Squared方法具有“你中有我，我中有你”交互融合来进行比较的特点，下面即将介绍的 Bera-McAleer(BM) Test 与 Davidson-Mackinnon(DM) Test 也具有类似的特点。

### 2.5 Bera-McAleer (BM) Test

​		针对Model I与Model II进行BM检验的流程如下：

#### 2.5.1 线性模型（Model I）

Step1：将$e^{\widehat{lny}}$对x进行无常数项回归，并记录所得的残差为$\hat{r}_1$；

Step2：将y对x与Step1中所得的残差$\hat{r}_1$进行无常数项回归；

Step3：对Step2中$\hat{r}_1$的系数$\gamma_1$的统计显著性进行假设检验。

#### 2.5.2 对数模型（Model II）

Step1’：将$ln(\hat{y})$对x进行无常数项回归，并记录所得的残差为$\hat{r}_2$；

Step2’：将$lny$对x与Step1中所得的残差$\hat{r}_2$进行无常数项回归；

Step3’：对Step2中$\hat{r}_2$的系数$\gamma_2$的统计显著性进行假设检验。

---

​		Bera与McAleer提出在进行上述流程的原始模型更优的原假设下，其系数$\gamma$的F统计量服从第一自由度为1，第二自由度为N-K-2的F分布。于是当Step3中拒绝原假设时，意味着我们应该选择Model II；而当Step3’中拒绝原假设时，我们应该选择Model I。

---

### 2.6 Davidson-Mackinnon(DM) Test

​		类似的，Davidson 与 Mackinnon 提出的DM检验的流程也可表示如下：

#### 2.6.1 线性模型（Model I）

Step1：基于Model II定义两个模型拟合值的差异$f_2=\widehat{lny}-ln(\hat{y})$；

Step2：基于Model I的水平值，将y对x与$f_2$进行无常数项回归；

Step3：对Step2中$f_2$的系数$\gamma_1$的统计显著性进行假设检验。

#### 2.6.2 对数模型（Model II）

Step1’：基于Model I定义两个模型拟合值的差异$f_1=\hat{y}-e^\widehat{lny}$；

Step2’：基于Model II的对数值，将y对x与$f_1$进行无常数项回归；

Step3’：对Step2’中$f_1$的系数$\gamma_2$的统计显著性进行假设检验。

---

​		与 BM 检验的思路类似， Davidson 与 Mackinnon 指出在上述流程的原始模型更优的原假设下，其系数$\gamma$的F统计量同样服从第一自由度为1，第二自由度为N-K-2的F分布。因此当Step3中拒绝原假设时，意味着我们应该选择 Model II ；而当Step3’中拒绝原假设时，我们应该选择 Model I。

---

## 3. lmfreg命令介绍

​		下面我们将结合上述理论的Stata应用，利用Elmessih Shehata与Sahra Khaleel编写的`lmfreg`命令来选择合适的模型形式。

### 3.1 lmfreg命令安装

```stata
ssc install lmfreg
```

### 3.2 lmfreg命令语法

```stata
lmfreg depvar indepvars [if] [in] , [ noconstant coll]
```

其中：

- `depvar`：模型的被解释变量y；
- `indepvars`：为模型的解释变量**x**；
- `nonconstant`：指定回归为不含有常数项的回归；
- `coll`：指定模型在回归时保留共线的变量；

### 3.3 lmfreg命令示例

​		首先我们利用lmfreg提供的数据集lmfreg.dta进行验证。

```stata
. use http://fmwww.bc.edu/repec/bocode/l/lmfreg.dta,clear

. lmfreg y x1 x2
==============================================================================
* Ordinary Least Squares (OLS)
==============================================================================
  y = x1 + x2
------------------------------------------------------------------------------
  Sample Size       =          17
  Wald Test         =    273.3662   |   P-Value > Chi2(2)       =      0.0000
  F-Test            =    136.6831   |   P-Value > F(2 , 14)     =      0.0000
 (Buse 1973) R2     =      0.9513   |   Raw Moments R2          =      0.9986
 (Buse 1973) R2 Adj =      0.9443   |   Raw Moments R2 Adj      =      0.9984
  Root MSE (Sigma)  =      5.5634   |   Log Likelihood Function =    -51.6471
------------------------------------------------------------------------------
- R2h= 0.9513   R2h Adj= 0.9443  F-Test =  136.68 P-Value > F(2 , 14)  0.0000
- R2v= 0.9513   R2v Adj= 0.9443  F-Test =  136.68 P-Value > F(2 , 14)  0.0000
------------------------------------------------------------------------------
           y | Coefficient  Std. err.      t    P>|t|     [95% conf. interval]
-------------+----------------------------------------------------------------
          x1 |   1.061709   .2666739     3.98   0.001     .4897506    1.633668
          x2 |  -1.382986   .0838143   -16.50   0.000    -1.562749   -1.203222
       _cons |   130.7066   27.09429     4.82   0.000     72.59515    188.8181
------------------------------------------------------------------------------
==============================================================================
*** OLS Linear vs Log-Linear Functional Form Tests
==============================================================================
 (1) R-squared
      Linear  R2                   =    0.9513
      Log-Log R2                   =    0.9711
---------------------------------------------------------------------------
 (2) Log Likelihood Function (LLF)
      LLF - Linear                 =  -51.6471
      LLF - Log-Log                =  -47.5914
---------------------------------------------------------------------------
 (3) Antilog R2
      Linear  vs Log-Log: R2Lin    =    0.9649
      Log-Log vs Linear : R2log    =    0.9576
---------------------------------------------------------------------------
 (4) Box-Cox Test                  =    4.0556   P-Value > Chi2(1)   0.0440
      Ho: Choose Log-Log Model - Ha: Choose Linear  Model
---------------------------------------------------------------------------
 (5) Bera-McAleer BM Test
      Ho: Choose Linear  Model     =   11.9464   P-Value > F(1, 13)  0.0043
      Ho: Choose Log-Log Model     =    6.1092   P-Value > F(1, 13)  0.0280
---------------------------------------------------------------------------
 (6) Davidson-Mackinnon PE Test
      Ho: Choose Linear  Model     =   11.9462   P-Value > F(1, 13)  0.0043
      Ho: Choose Log-Log Model     =    6.1092   P-Value > F(1, 13)  0.0280
------------------------------------------------------------------------------
```

​		上述结果表明方法(1)~(6)均倾向于拒绝选择线性模型，故此时应选择对数模型也即$ lny = \beta_1 x_1 +\beta_2 x_2+ \varepsilon$进行回归。

---

​		接下来我们尝试使用Stata自带的nlsw88.dta工资数据集进行回归分析，回归模型的被解释变量为劳动者的工资水平`wage`，解释变量包括工作时间`hours`、工作经验`ttl_exp` 、工作年薪`tenure` 、年龄`age`。

```stata
. sysuse nlsw88,clear
(NLSW, 1988 extract)

. lmfreg wage hours ttl_exp tenure age

==============================================================================
* Ordinary Least Squares (OLS)
==============================================================================
  wage = hours + ttl_exp + tenure + age
------------------------------------------------------------------------------
  Sample Size       =        2227
  Wald Test         =    203.7791   |   P-Value > Chi2(4)       =      0.0000
  F-Test            =     50.9448   |   P-Value > F(4 , 2222)   =      0.0000
 (Buse 1973) R2     =      0.0840   |   Raw Moments R2          =      0.6764
 (Buse 1973) R2 Adj =      0.0824   |   Raw Moments R2 Adj      =      0.6758
  Root MSE (Sigma)  =      5.5240   |   Log Likelihood Function =  -6963.6318
------------------------------------------------------------------------------
- R2h= 0.0840   R2h Adj= 0.0824  F-Test =   50.94 P-Value > F(4 , 2222)0.0000
- R2v= 0.0840   R2v Adj= 0.0824  F-Test =   50.94 P-Value > F(4 , 2222)0.0000
------------------------------------------------------------------------------
        wage | Coefficient  Std. err.      t    P>|t|     [95% conf. interval]
-------------+----------------------------------------------------------------
       hours |   .0545902   .0115028     4.75   0.000     .0320329    .0771474
     ttl_exp |   .2848398   .0317393     8.97   0.000      .222598    .3470817
      tenure |   .0366771   .0260101     1.41   0.159    -.0143296    .0876837
         age |  -.1211649   .0385944    -3.14   0.002    -.1968498     -.04548
       _cons |   6.713919   1.571873     4.27   0.000     3.631426    9.796413
------------------------------------------------------------------------------
==============================================================================
*** OLS Linear vs Log-Linear Functional Form Tests
==============================================================================
 (1) R-squared
      Linear  R2                   =    0.0840
      Log-Log R2                   =    0.1725
---------------------------------------------------------------------------
 (2) Log Likelihood Function (LLF)
      LLF - Linear                 =-6963.6318
      LLF - Log-Log                =-5879.4299
---------------------------------------------------------------------------
 (3) Antilog R2
      Linear  vs Log-Log: R2Lin    =    0.0772
      Log-Log vs Linear : R2log    =    0.1629
---------------------------------------------------------------------------
 (4) Box-Cox Test                  = 1084.2019   P-Value > Chi2(1)   0.0000
      Ho: Choose Log-Log Model - Ha: Choose Linear  Model
---------------------------------------------------------------------------
 (5) Bera-McAleer BM Test
      Ho: Choose Linear  Model     =    0.2863   P-Value > F(1, 2221)0.5926
      Ho: Choose Log-Log Model     =    7.0295   P-Value > F(1, 2221)0.0081
---------------------------------------------------------------------------
 (6) Davidson-Mackinnon PE Test
      Ho: Choose Linear  Model     =    0.2863   P-Value > F(1, 2221)0.5927
      Ho: Choose Log-Log Model     =    7.0295   P-Value > F(1, 2221)0.0081
------------------------------------------------------------------------------
```

​		 从上述检验结果比较可以发现方法(1)\~(3)倾向于选择选择对数模型，(4)~(6)倾向于选择线性模型；和lmfreg自带的数据集结果不同，此时不同方法给出了截然相反的模型选择策略，那么如果遇到这种情况，我们又该作何选择呢？一般情况下，对工资、成绩、房价等被解释变量取对数能使得该变量的分布更趋近于正态分布，因此方法(1)(3)中往往会出现Log-Linear Model更优的情况，而当方法(4)~(6)显著拒绝选择选择对数模型时，我们应对采用R-Squared与LLF得到的结论提出一定的质疑，到底是由于对数变换使得数据更加平滑，还是由于真实的模型更倾向于对数形式的解释变量，这一点结合从研究主题的有关理论和文献参考，综合考虑后再做出判断。

​		例如在研究影响房产价格的特征因素时，由经济学家Sherwin Rosen提出的Hedonic特征价格模型中，Rosen 对房价给消费者带来的效用建模，推导出消费者对特征房产的出价函数，再结合消费者效用最大化的一阶条件求解出均衡条件位于消费者出价曲线与特征价格函数相切处 ，此时模型均衡如下图所示：

![Hedonic模型的均衡曲线](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20221223171115049.png)





从上述图像的性质可以发现，房产价格P与其特征向量集Z的函数关系应符合对数价格的变动，因此在研究Hedonic模型时应该采用对数ln(price)作为模型的被解释变量。

​		总而言之，利用lmfreg得到的检验结果仅仅是为模型选择提供了参考，在进行实证研究设定模型形式时不能忘记的是模型背后支撑的理论基础，同时我们在操作时也应适当参考有关论文的做法，以选择最合适的函数形式。

---

## 4. 参考资料与相关推文

#### 参考文献：

- James G. MacKinnon; Halbert White; Russell Davidson (1983). *Tests for model specification in the presence of alternative hypotheses: Some further results. , 21(1), 53–70.  [-PDF-](https://doi.org/10.1016/0304-4076(83)90119-7)

- Box, G. E. P., & Cox, D. R. (1964). *An Analysis of Transformations. Journal of the Royal Statistical Society: Series B (Methodological), 26(2), 211–243.*  [-PDF-](https://doi.org/10.1111/j.2517-6161.1964.tb00553.x)

- Badi H. Baltagi (1997). *Testing linear and loglinear error components regressions against Box-Cox alternatives. , 33(1), 63–68.* [-PDF-](https://doi.org/10.1016/s0167-7152(96)00110-1)

- L. G. Godfrey and M. R. Wickens (1981). *Testing Linear and Log-Linear Regressions for Functional Form. The Review of Economic Studies, 48(3), 487–496.*[-PDF-](https://doi.org/10.2307/2297160)


#### 相关推文：

- 专题：[回归分析](https://www.lianxh.cn/blogs/32.html)
  - [取对数！取对数？](https://www.lianxh.cn/news/feb8ffdcb6a87.html)
  - [Stata：RDD与RKD的最优模型选择-pzms](https://www.lianxh.cn/news/c4a4360380d3b.html)
  - [Stata：自己动手做组间系数差异检验-bootstrap-bdiff](https://www.lianxh.cn/news/c961273d03ae3.html)
  - [Stata检验：AIC-BIC-MSE-MAE-等信息准则的计算](https://www.lianxh.cn/news/fc6331df15b45.html)
  - [Stata：手动实现置换检验(permutation)和自抽样(bootstrap)](https://www.lianxh.cn/news/889a49516bcc8.html)
  - [Stata：回归后假设检验一览](https://www.lianxh.cn/news/d4eb90f8f8fb6.html)